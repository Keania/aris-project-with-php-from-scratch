<?php

use Lib\Request;
use Lib\Router;
use Model\DegreeAward;
use Model\ProgramType;
use Model\DegreeType;



$router->get('/programtype', function (Request $request) {
    require BASE_URL . '/setup/program_type.php';
});

$router->get('/api/programtype', function (Request $request) {
    $programtype = ProgramType::find(['ProgramTypeID' => $request->programtype_id]);
    return json_encode($programtype);
});

$router->get('/api/degreetype', function (Request $request) {
    $degreetype = DegreeType::find(['DegreeTypeID' => $request->degreetype_id]);
    return json_encode($degreetype);
});



// route and action to save
$router->post('/save/degreetype', function (Request $request) {
    // check for CSRF TOKEN 
    if (Router::verifyCsrfToken($request->_csrf_token)) {

        try {

            //check for empty values
            $request->validateEmpty(['degreetype'=>'Please enter a Degree Type']);

            //Duplicate check
            $duplicateCheck = DegreeType::find(['DegreeType' => $request->degreetype])->count();
            if ($duplicateCheck > 0) {
                throw new \Exception('Degree type alreday exists in database');
            }


            // try carrying out action
            $degreetype = new DegreeType;
            $degreetype->DegreeType = $request->degreetype;
            $degreetype->save();
        } catch (\Exception $e) {
            // temporal satus storage our request will read from
            $_SESSION['tmp_status']['error'] = $e->getMessage();
            // redirect back to referer
            return redirect($request->httpReferer);
        }
        $_SESSION['tmp_status']['success'] = 'degree type was successfully saved';
        return redirect($request->httpReferer);
    }
    //No CSRF Token found redirect back with error
    $_SESSION['tmp_status']['error'] = 'CSRF Token not found for this post request';
    return redirect($request->httpReferer);
});

// route and action to update
$router->post('/update/degreetype', function (Request $request) {
    // check for CSRF TOKEN 
    if (Router::verifyCsrfToken($request->_csrf_token)) {
        try {

            //check for empty values
            $request->validateEmpty(
                ['degreetype_id'=>' Please select a Degree Type', 
                'degreetype'=>' Please enter  Degree Type you want to edit'
                ]);


            //Duplicate check
            $duplicateCheck = DegreeType::find(['DegreeType' => $request->degreetype])->count();
            if ($duplicateCheck > 0) {
                throw new \Exception('Degree type alreday exists in database');
            }
            $degreetype = DegreeType::find(['DegreeTypeID' => $request->degreetype_id])->first();

            // integrity checks
            $awardIntegrityChecks = $degreetype->awards()->count();
            $degreecourseIntegrityChecks = $degreetype->degreecourses()->count();
            $feeIntegrityCheck = $degreetype->fees()->count();
            $academicSessionMgtIntegrityChecks = $degreetype->academic_session_mgts()->count();

            if (array_sum([$awardIntegrityChecks, $degreecourseIntegrityChecks, $feeIntegrityCheck, $academicSessionMgtIntegrityChecks]) > 0) {
                throw new \Exception('The Degree Type cannot be modified because it is involved in sensitive transactions');
            }


            // save operation
            $degreetype->DegreeType = $request->degreetype;
            $degreetype->save();
        } catch (\Exception $e) {

            // temporal satus storage our request will read from
            $_SESSION['tmp_status']['error'] = $e->getMessage();
            // redirect back to referer
            return redirect($request->httpReferer);
        }
        $_SESSION['tmp_status']['success'] = 'Degree Award was successfully saved';
        return redirect($request->httpReferer);
    }
    //No CSRF Token found redirect back with error
    $_SESSION['tmp_status']['error'] = 'CSRF Token not found for this post request';
    return redirect($request->httpReferer);
});

// route and action to delete
$router->post('/delete/degreetype', function (Request $request) {
    // check for CSRF TOKEN 
    if (Router::verifyCsrfToken($request->_csrf_token)) {
        try {
            // check for empty request
            $request->validateEmpty(['degreetype_id'=>' Please select a Degree type']);

            // find the predata to update
            $degreetype = DegreeType::find(['DegreeTypeID' => $request->degreetype_id])->first();
            if (is_null($degreetype)) {
                throw new \Exception('Degree Type specified not found');
            }

            // integrity checks
            $awardIntegrityChecks = $degreetype->awards()->count();
            $degreecourseIntegrityChecks = $degreetype->degreecourses()->count();
            $feeIntegrityCheck = $degreetype->fees()->count();
            $academicSessionMgtIntegrityChecks = $degreetype->academic_session_mgts()->count();
            $academicCourseIntegrityChecks = $degreetype->academic_courses()->count();
            $hostelIntegrityChecks = $degreetype->hostels()->count();
            $medicalRegimeIntegrityChecks = $degreetype->medical_regimes()->count();
            $serviceTypeIntegrityChecks = $degreetype->service_types()->count();
            $otherFeesIntegrityChecks = $degreetype->other_fees()->count();

            if (array_sum([
                $awardIntegrityChecks, $degreecourseIntegrityChecks, $feeIntegrityCheck, $academicSessionMgtIntegrityChecks,
                $academicCourseIntegrityChecks, $hostelIntegrityChecks, $medicalRegimeIntegrityChecks, $serviceTypeIntegrityChecks,
                $otherFeesIntegrityChecks
            ]) > 0) {
                throw new \Exception('The Degree Type cannot be deleted because it is involved in sensitive transactions');
            }

            $degreetype->delete();
        } catch (\Exception $e) {
            // temporal satus storage our request will read from
            $_SESSION['tmp_status']['error'] = $e->getMessage();
            // redirect back to referer
            return redirect($request->httpReferer);
        }
        $_SESSION['tmp_status']['success'] = 'Degree Type was successfully deleted';
        return redirect($request->httpReferer);
    }
    //No CSRF Token found redirect back with error
    $_SESSION['tmp_status']['error'] = 'CSRF Token not found for this post request';
    return redirect($request->httpReferer);
});





// route and action to save
$router->post('/save/programtype', function (Request $request) {
    // check for CSRF TOKEN 
    if (Router::verifyCsrfToken($request->_csrf_token)) {

        try {

            //check for empty values
            $request->validateEmpty([
                'programtype'=>' Please enter a Program Type', 
                'programtypecode'=>' Please enter a Program Type Code']);

            //Duplicate check
            $typeduplicateCheck = ProgramType::find(['ProgramType' => $request->programtype])->count();
            if ($typeduplicateCheck > 0) {
                throw new \Exception('Program type defined alreday exists in database');
            }
            //Duplicate check
            $codeduplicateCheck = ProgramType::find(['ProgramTypeCode' => $request->programtypecode])->count();
            if ($codeduplicateCheck > 0) {
                throw new \Exception('Program Type Code defined alreday exists in database');
            }


            // try carrying out action
            $programtype = new ProgramType;
            $programtype->ProgramType = $request->programtype;
            $programtype->ProgramTypeCode = $request->programtypecode;
            $programtype->save();
        } catch (\Exception $e) {
            // temporal satus storage our request will read from
            $_SESSION['tmp_status']['error'] = $e->getMessage();
            // redirect back to referer
            return redirect($request->httpReferer);
        }
        $_SESSION['tmp_status']['success'] = 'Program type was successfully saved';
        return redirect($request->httpReferer);
    }
    //No CSRF Token found redirect back with error
    $_SESSION['tmp_status']['error'] = 'CSRF Token not found for this post request';
    return redirect($request->httpReferer);
});

// route and action to update
$router->post('/update/programtype', function (Request $request) {
    // check for CSRF TOKEN 
    if (Router::verifyCsrfToken($request->_csrf_token)) {
        try {

            //check for empty values
            $request->validateEmpty([
                'programtype_id'=>' Please select a Program Type ', 
                'programtype'=>' Please enter a Program Type',
                'programtypecode'=>' Please enter a Program Type Code']);


          //duplicate checks
            $typecheck = ProgramType::where(['ProgramTypeID','<>',$request->programtype_id],['ProgramType','=',$request->programtype])->count();
            $codecheck = ProgramType::where(['ProgramTypeID','<>',$request->programtype_id],['ProgramTypeCode','=',$request->programtypecode])->count();

            if($typecheck > 0) throw new Exception('Program Type alreday exists in database');
            if($codecheck > 0) throw new Exception('Program Code already exists in database');

            $programtype = ProgramType::find(['ProgramTypeID' => $request->programtype_id])->first();

           

            // integrity checks
            $assignedCourseIntegrityChecks = $programtype->assigned_courses()->count();
            $degreecourseIntegrityChecks = $programtype->degreecourses()->count();
            $feeIntegrityCheck = $programtype->fees()->count();
            $academicSessionMgtIntegrityChecks = $programtype->academic_session_mgts()->count();
            $feeOthersIntegrityCheck = $programtype->other_fees()->count();
            $medicalRegimeIntegrityChecks = $programtype->medical_regimes()->count();
            $serviceTypeIntegrityChecks = $programtype->service_types()->count();

            if (array_sum([
                $assignedCourseIntegrityChecks, $degreecourseIntegrityChecks, $feeIntegrityCheck, $academicSessionMgtIntegrityChecks,
                $feeOthersIntegrityCheck, $medicalRegimeIntegrityChecks, $serviceTypeIntegrityChecks
            ]) > 0) {
                throw new \Exception('The Degree Type cannot be modified because it is involved in sensitive transactions');
            }

            // save operation
            $programtype->ProgramType = $request->programtype;
            $programtype->ProgramTypeCode = $request->programtypecode;
            $programtype->save();
        } catch (\Exception $e) {

            // temporal satus storage our request will read from
            $_SESSION['tmp_status']['error'] = $e->getMessage();
            // redirect back to referer
            return redirect($request->httpReferer);
        }
        $_SESSION['tmp_status']['success'] = 'Program Type was successfully saved';
        return redirect($request->httpReferer);
    }
    //No CSRF Token found redirect back with error
    $_SESSION['tmp_status']['error'] = 'CSRF Token not found for this post request';
    return redirect($request->httpReferer);
});

// route and action to delete
$router->post('/delete/programtype', function (Request $request) {
    // check for CSRF TOKEN 
    if (Router::verifyCsrfToken($request->_csrf_token)) {
        try {
            // check for empty request
            $request->validateEmpty(['programtype_id'=>' Please select Program Type']);

            // find the predata to update
            $programtype = ProgramType::find(['ProgramTypeID' => $request->programtype_id])->first();
            if (is_null($programtype)) {
                throw new \Exception('Program Type specified not found');
            }

            // integrity checks
            $assignedCourseIntegrityChecks = $programtype->assigned_courses()->count();
            $degreecourseIntegrityChecks = $programtype->degreecourses()->count();
            $feeIntegrityCheck = $programtype->fees()->count();
            $academicSessionMgtIntegrityChecks = $programtype->academic_session_mgts()->count();
            $feeOthersIntegrityCheck = $programtype->other_fees()->count();
            $medicalRegimeIntegrityChecks = $programtype->medical_regimes()->count();
            $serviceTypeIntegrityChecks = $programtype->service_types()->count();

            if (array_sum([
                $assignedCourseIntegrityChecks, $degreecourseIntegrityChecks, $feeIntegrityCheck, $academicSessionMgtIntegrityChecks,
                $feeOthersIntegrityCheck, $medicalRegimeIntegrityChecks, $serviceTypeIntegrityChecks
            ]) > 0) {
                throw new \Exception('The Degree Type cannot be modified because it is involved in sensitive transactions');
            }



            $programtype->delete();
        } catch (\Exception $e) {
            // temporal satus storage our request will read from
            $_SESSION['tmp_status']['error'] = $e->getMessage();
            // redirect back to referer
            return redirect($request->httpReferer);
        }
        $_SESSION['tmp_status']['success'] = 'Program Type was successfully deleted';
        return redirect($request->httpReferer);
    }
    //No CSRF Token found redirect back with error
    $_SESSION['tmp_status']['error'] = 'CSRF Token not found for this post request';
    return redirect($request->httpReferer);
});
