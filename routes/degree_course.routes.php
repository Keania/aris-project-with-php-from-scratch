<?php
use Lib\Request;
use Lib\Router;
use Model\Department;
use Model\Faculty;
use Model\User;
use Model\User_VW95;
use Model\VW8_DegreeCourse;
use Model\DegreeCourse;

$router->get('/degreecourse',function(Request $request){
    
    require BASE_URL.'/department/degreecourse.php';
});

$router->get('/api/get/faculty/departments',function(Request $request){
    try {
        $faculty = Faculty::find(['FacultyID'=>$request->faculty_id])->first();
    }catch(Exception | Throwable $e){
        return json_encode($e->getMessage());
    }
  
   return json_encode($faculty->vw7_departments());
});

$router->get('/api/get/department',function(Request $request){
    try{
        $dept = Department::find(['DeptID'=>$request->dept_id]);
    }catch(Exception | Throwable $e){
        return json_encode($e->getMessage());
    }
    
    return json_encode($dept);
 });




 $router->get('/api/degreecourses',function(Request $request){
    try{
        $degree_courses = VW8_DegreeCourse::find([
            'DegreeTypeID'=>$request->degreetype_id,
            'ProgramTypeID'=>$request->programtype_id,
            'DeptID'=>$request->department_id
            ]);
    }catch(Exception | Throwable $e){

        return json_encode($e->getMessage());
    }
    return json_encode($degree_courses);
 });



$router->get('/api/get/faculty/department/users',function(Request $request){
    try{
        $dept_id = $request->dept_id;
        $faculty_id = $request->faculty_id;
        $users = User_VW95::find(['UserDeptID'=>$dept_id,'FacultyID'=>$faculty_id]);
    }catch(Exception | Throwable $e){
        return json_encode($e->getMessage());
    }
  
    return json_encode($users);
});

$router->get('/api/degreecourse',function(Request $request){
    try{
        $degree_course = DegreeCourse::find(['DegreeCourseID'=>$request->degreecourse_id])->first();
    }catch(Exception | Throwable $e){
        return json_encode($e->getMessage());
    }
   
    return json_encode($degree_course);
});


// route and action to save
$router->post('/save/degreecourse',function(Request $request){
    // check for CSRF TOKEN 
   if(Router::verifyCsrfToken($request->_csrf_token)){
       try {

          $request->validateEmpty([
              'degreetype_id'=>' Please select a Degree Type',
              'programtype_id'=>' Please select a Program Type',
              'faculty_id'=>' Please select a Faculty',
              'department_id'=>' Please select a Department',
              'degreeaward_id'=>' Please select a Degree Award',
              'degreecourse'=>' Please enter a Degree Course',
              'degreecourse_duration'=>' Please enter a Duration '
          ]);

          // perform the duplicate check
          $duplicateCheck = DegreeCourse::find([
              'DegreeTypeID'=>$request->degreetype_id,
              'ProgramTypeID'=>$request->programtype_id,
             
              'DeptID'=>$request->department_id,
              'DegreeAwardID'=>$request->degreeaward_id,
              'DegreeCourse'=>strtoupper($request->degreecourse),
              'DegreeCourseDuration'=>$request->degreecourse_duration
              ])->count();
            
        
        if($duplicateCheck >  0){
            throw new \Exception('Degree Course defined already exists in database');
        }

        // persists into the database
        $degree_course = new DegreeCourse;
        $degree_course->DegreeTypeID = $request->degreetype_id;
        $degree_course->ProgramTypeID = $request->programtype_id;
        $degree_course->FacultyID = $request->faculty_id;
        $degree_course->DeptID = $request->department_id;
        $degree_course->DegreeAwardID = $request->degreeaward_id;
        $degree_course->DegreeCourse = strtoupper($request->degreecourse);
        $degree_course->DegreeCourseDuration = $request->degreecourse_duration;
        $degree_course->save();

       }catch(\Exception $e){
            // temporal satus storage our request will read from
            $_SESSION['tmp_status']['error'] = $e->getMessage();
            // redirect back to referer
           return redirect($request->httpReferer);
       }
       $_SESSION['tmp_status']['success'] = 'Degree Course was successfully saved';
       return redirect($request->httpReferer);
   } 
   //No CSRF Token found redirect back with error
   $_SESSION['tmp_status']['error'] = 'CSRF Token not found for this post request';
   return redirect($request->httpReferer);
});

// route and action to update
$router->post('/update/degreecourse',function(Request $request){
    // check for CSRF TOKEN 
   if(Router::verifyCsrfToken($request->_csrf_token)){
       try {
           
        $request->validateEmpty([
            'degreetype_id'=>' Please select a Degree Type',
            'programtype_id'=>' Please select a Program Type',
            'faculty_id'=>' Please select a Faculty',
            'department_id'=>' Please select a Department',
            'degreeaward_id'=>' Please select a Degree Award',
            'degreecourse'=>' Please enter a Degree Course',
            'degreecourse_duration'=>' Please enter a Duration ',
            'degreecourse_id'=>' Please select a Degree Course to edit'
        ]);

   

        // check
        $check = DegreeCourse::where(
            ['DegreeTypeID','=',$request->degreetype_id],
            ['ProgramTypeID','=',$request->programtype_id],
           
            ['DeptID','=',$request->department_id],
            ['DegreeAwardID','=',$request->degreeaward_id],
            ['DegreeCourse','=',strtoupper($request->degreecourse)],
            ['DegreeCourseDuration','=',$request->degreecourse_duration],
            ['DegreeCourseID','<>',$request->degreecourse_id]
            )->count();
        
            if($check > 0){
                throw new \Exception(' Degree Course Defined already exists');
            }

            $degree_course = DegreeCourse::find([
                'DegreeTypeID'=>$request->degreetype_id,
                'ProgramTypeID'=>$request->programtype_id,
                'DeptID'=>$request->department_id,
                ])->first();

            
            
            if(is_null($degree_course)){
                throw new \Exception('Degree Course Specified not found');
            }


            //integrity checks
            $hasStudents = $degree_course->students()->count();
            $hasAdmission = $degree_course->admissions()->count();
            $hasFeeDetails = $degree_course->fee_details()->count();
            $hasEntryRequirementAlt = $degree_course->entry_requirement_alt()->count();
            $hasEntryRequirement = $degree_course->entry_requirement()->count();
            $hasEligibilityOlevel = $degree_course->eligibility_olevel()->count();
            $hasEligibilityJamb = $degree_course->eligibility_jamb()->count();
            $hasCourseLevel = $degree_course->course_level()->count();

            if(array_sum(
                [$hasStudents,$hasAdmission,$hasFeeDetails,$hasEntryRequirementAlt,$hasEntryRequirement,$hasEligibilityOlevel,$hasEligibilityJamb,$hasCourseLevel]
                ) > 0){
                    throw new \Exception('The Degree Course cannot be modified because it is involved in sensitive transactions');
                }
            
                //persist degree course data
                $degree_course->DegreeTypeID = $request->degreetype_id;
                $degree_course->ProgramTypeID = $request->programtype_id;
                $degree_course->FacultyID = $request->faculty_id;
                $degree_course->DeptID = $request->department_id;
                $degree_course->DegreeAwardID = $request->degreeaward_id;
                $degree_course->DegreeCourse = strtoupper($request->degreecourse);
                $degree_course->DegreeCourseDuration = $request->degreecourse_duration;
                $degree_course->save();


       }catch(\Exception $e){
            // temporal satus storage our request will read from
            $_SESSION['tmp_status']['error'] = $e->getMessage();
            // redirect back to referer
           return redirect($request->httpReferer);
       }
       $_SESSION['tmp_status']['success'] = 'Degree Course was successfully saved';
       return redirect($request->httpReferer);
   } 
   //No CSRF Token found redirect back with error
   $_SESSION['tmp_status']['error'] = 'CSRF Token not found for this post request';
   return redirect($request->httpReferer);
});

// route and action to delete
$router->post('/delete/degreecourse',function(Request $request){
    // check for CSRF TOKEN 
   if(Router::verifyCsrfToken($request->_csrf_token)){
       try {
            
        $request->validateEmpty([
            'degreetype_id'=>' Please select a Degree Type',
            'programtype_id'=>' Please select a Program Type',
            'faculty_id'=>' Please select a Faculty',
            'department_id'=>' Please select a Department',
            'degreeaward_id'=>' Please select a Degree Award',
            'degreecourse'=>' Please enter a Degree Course',
            'degreecourse_duration'=>' Please enter a Duration ',
            'degreecourse_id'=>' Please select a Degree Course to edit'
        ]);

       

            $degree_course = DegreeCourse::find([
                'DegreeTypeID'=>$request->degreetype_id,
                'ProgramTypeID'=>$request->programtype_id,
                'DeptID'=>$request->department_id,
                'DegreeAwardID'=>$request->degreeaward_id,
                'DegreeCourse'=>strtoupper($request->degreecourse),
                'DegreeCourseDuration'=>$request->degreecourse_duration
                ])->first();
            
            if(is_null($degree_course)){
                throw new \Exception('Degree Course Specified not found');
            }


            //integrity checks
            $hasStudents = $degree_course->students()->count();
            $hasAdmission = $degree_course->admissions()->count();
            $hasFeeDetails = $degree_course->fee_details()->count();
            $hasEntryRequirementAlt = $degree_course->entry_requirement_alt()->count();
            $hasEntryRequirement = $degree_course->entry_requirement()->count();
            $hasEligibilityOlevel = $degree_course->eligibility_olevel()->count();
            $hasEligibilityJamb = $degree_course->eligibility_jamb()->count();
            $hasCourseLevel = $degree_course->course_level()->count();

            if(array_sum(
                [$hasStudents,$hasAdmission,$hasFeeDetails,$hasEntryRequirementAlt,$hasEntryRequirement,$hasEligibilityOlevel,$hasEligibilityJamb,$hasCourseLevel]
                ) > 0){
                    throw new \Exception('The Degree Course cannot be deleted because it is involved in sensitive transactions');
                }

        // delete degree course
        $degree_course->delete();
       }catch(\Exception $e){
            // temporal satus storage our request will read from
            $_SESSION['tmp_status']['error'] = $e->getMessage();
            // redirect back to referer
           return redirect($request->httpReferer);
       }
       $_SESSION['tmp_status']['success'] = 'Degree Course was successfully deleted';
       return redirect($request->httpReferer);
   } 
   //No CSRF Token found redirect back with error
   $_SESSION['tmp_status']['error'] = 'CSRF Token not found for this post request';
   return redirect($request->httpReferer);
});
