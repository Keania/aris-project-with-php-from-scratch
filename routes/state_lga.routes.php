<?php
use Lib\Request;
use Lib\Router;
use Model\State;
use Model\LGA;


$router->get('/state_lga',function(Request $request){
    require BASE_URL.'/setup/states.php';
});


$router->get('/api/get/state',function(Request $request){
    $state = State::find(['StateID'=>$request->state_id]);
    return json_encode($state);
});

$router->get('/api/get/lga',function(Request $request){
    $lga = LGA::find(['LGAID'=>$request->lga_id]);
    return json_encode($lga);
});

$router->get('/api/get/state/lgas',function(Request $request){
    $state = State::find(['StateID'=>$request->state_id])->first();
    return json_encode($state->lgas());
});

// route and action to save
$router->post('/save/lga',function(Request $request){
    // check for CSRF TOKEN 
   if(Router::verifyCsrfToken($request->_csrf_token)){
       try {

            //check for empty values
            $request->validateEmpty([
                'state_id'=>' Please select a state',
                'lga'=>' Please enter a Local Government Area'
                ]);

           //Duplicate check
            $duplicateCheck = LGA::find(['StateID'=>$request->state_id,'LGA'=>strtoupper($request->lga)])->count();
            if($duplicateCheck > 0){
                throw new \Exception('Local Government Area  defined alreday exists');
            }
            // try carrying out action
            $lga = new LGA;
            $lga->StateID = $request->state_id;
            $lga->LGA = strtoupper($request->lga);
            $lga->save();
       }catch(\Exception $e){
            // temporal satus storage our request will read from
            $_SESSION['tmp_status']['error'] = $e->getMessage();
            // redirect back to referer
           return redirect($request->httpReferer);
       }
       $_SESSION['tmp_status']['success'] = 'Local Government Area was successfully saved';
       return redirect($request->httpReferer);
   } 
   //No CSRF Token found redirect back with error
   $_SESSION['tmp_status']['error'] = 'CSRF Token not found for this post request';
   return redirect($request->httpReferer);
});

// route and action to update
$router->post('/update/lga',function(Request $request){
    // check for CSRF TOKEN 
   if(Router::verifyCsrfToken($request->_csrf_token)){
       try {
            // check for empty request
            $request->validateEmpty([
                'state_id'=>' Please select a state',
                'lga'=>' Please enter an edit value for Local Government Area',
                'lga_id'=> ' Please select a Local Government Area']);
           
           //Duplicate check
            $duplicateCheck = LGA::find(['StateID'=>$request->state_id,'LGA'=>strtoupper($request->lga),])->count();
            if($duplicateCheck > 0){
                throw new \Exception('Local Government Area  defined alreday exists');
            }
            // find the lga to update
            $lga = LGA::find(['StateID'=>$request->state_id,'LGAID'=>$request->lga_id])->first();
            if(is_null($lga)){
                throw new \Exception('Local Government Area specified not found');
            }

            // integrity check
            $student_check = $lga->students()->count();

            if($student_check > 0){
                throw new \Exception('This data cannot be modified because it is involved in sensitive data');
            }
            $lga->StateID = $request->state_id;
            $lga->LGA = strtoupper($request->lga);
            $lga->save();

       }catch(\Exception $e){
            // temporal satus storage our request will read from
            $_SESSION['tmp_status']['error'] = $e->getMessage();
            // redirect back to referer
           return redirect($request->httpReferer);
       }
       $_SESSION['tmp_status']['success'] = 'Local Government Area was successfully saved';
       return redirect($request->httpReferer);
   } 
   //No CSRF Token found redirect back with error
   $_SESSION['tmp_status']['error'] = 'CSRF Token not found for this post request';
   return redirect($request->httpReferer);
});

// route and action to delete
$router->post('/delete/lga',function(Request $request){
    // check for CSRF TOKEN 
   if(Router::verifyCsrfToken($request->_csrf_token)){
       try {
            // check for empty request
            $request->validateEmpty([
                'state_id'=>' Please select a state',
                'lga_id'=>' Please select a Local Government Area'
                ]);

            // find the predata to update
            $lga = LGA::find(['StateID'=>$request->state_id,'LGAID'=>$request->lga_id])->first();
            if(is_null($lga)){
                throw new \Exception('Local Government Area specified not found');
            }
             // integrity check
             $student_check = $lga->students()->count();

             if($student_check > 0){
                 throw new \Exception('This data cannot be modified because it is involved in sensitive data');
             }
           
            $lga->delete();
       }catch(\Exception $e){
            // temporal satus storage our request will read from
            $_SESSION['tmp_status']['error'] = $e->getMessage();
            // redirect back to referer
           return redirect($request->httpReferer);
       }
       $_SESSION['tmp_status']['success'] = 'Local Government Area was successfully deleted';
       return redirect($request->httpReferer);
   } 
   //No CSRF Token found redirect back with error
   $_SESSION['tmp_status']['error'] = 'CSRF Token not found for this post request';
   return redirect($request->httpReferer);
});


// route and action to save
$router->post('/save/state',function(Request $request){
    // check for CSRF TOKEN 
   if(Router::verifyCsrfToken($request->_csrf_token)){
       try {

            //check for empty values
            $request->validateEmpty(['state'=>' Please enter a state']);

           //Duplicate check
            $duplicateCheck = State::find(['State'=>strtoupper($request->state)])->count();
            if($duplicateCheck > 0){
                throw new \Exception('State  defined alreday exists');
            }
            // try carrying out action
            $state = new State;
            $state->State = strtoupper($request->state);
            $state->save();
       }catch(\Exception $e){
            // temporal satus storage our request will read from
            $_SESSION['tmp_status']['error'] = $e->getMessage();
            // redirect back to referer
           return redirect($request->httpReferer);
       }
       $_SESSION['tmp_status']['success'] = 'State was successfully saved';
    
       return redirect($request->httpReferer);
   } 
   //No CSRF Token found redirect back with error
   $_SESSION['tmp_status']['error'] = 'CSRF Token not found for this post request';
   return redirect($request->httpReferer);
});

// route and action to update
$router->post('/update/state',function(Request $request){
    // check for CSRF TOKEN 
   if(Router::verifyCsrfToken($request->_csrf_token)){
       try {
            // check for empty request
            $request->validateEmpty(
                ['state_id'=> ' Please select a state',
                'state'=>' Please enter a value for state'
            ]);

         
           //Duplicate check
            $duplicateCheck = State::where(['StateID','<>',$request->state_id],['State','=',$request->state])->count();
          
            if($duplicateCheck > 0){
                throw new \Exception('State  defined alreday exists');
            }
            // find the predata to update
            $state = State::find(['StateID'=>$request->state_id])->first();

            if(is_null($state)){
                throw new \Exception('State specified not found');
            }

            // Integrity Check
            $integrityCheck = $state->lgas()->count();
            
            if($integrityCheck > 0){
                throw new \Exception('The State cannot be modified because it is involved in sensitive operations');
            }
           
            $state->State = $request->state;
            $state->save();

       }catch(\Exception $e){
            // temporal satus storage our request will read from
            $_SESSION['tmp_status']['error'] = $e->getMessage();
            // redirect back to referer
           return redirect($request->httpReferer);
       }
       $_SESSION['tmp_status']['success'] = 'State was successfully saved';
       return redirect($request->httpReferer);
   } 
   //No CSRF Token found redirect back with error
   $_SESSION['tmp_status']['error'] = 'CSRF Token not found for this post request';
   return redirect($request->httpReferer);
});

// route and action to delete
$router->post('/delete/state',function(Request $request){
    // check for CSRF TOKEN 
   if(Router::verifyCsrfToken($request->_csrf_token)){
       try {
            // check for empty request
            $request->validateEmpty(['state_id'=>' Please select a state']);

            // find the predata to update
            $state= State::find(['StateID'=>$request->state_id])->first();
            if(is_null($state)){
                throw new \Exception('State specified not found');
            }
             // Integrity Check
             $integrityCheck = $state->lgas()->count();
            
             if($integrityCheck > 0){
                 throw new \Exception('The State cannot be deleted because it is involved in sensitive operations');
             }
            $state->delete();

       }catch(\Exception $e){
            // temporal satus storage our request will read from
            $_SESSION['tmp_status']['error'] = $e->getMessage();
            // redirect back to referer
           return redirect($request->httpReferer);
       }
       $_SESSION['tmp_status']['success'] = ' State was successfully deleted';
       return redirect($request->httpReferer);
   } 
   //No CSRF Token found redirect back with error
   $_SESSION['tmp_status']['error'] = 'CSRF Token not found for this post request';
   return redirect($request->httpReferer);
});


?>