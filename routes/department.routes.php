<?php
use Lib\Request;
use Lib\Router;
use Model\Department;
use Model\Faculty;
use Model\User;
use Model\User_VW95;

$router->get('/department_management',function(Request $request){
    
    require BASE_URL.'/department/department.php';
});

$router->get('/api/get/faculty/departments',function(Request $request){
   $faculty = Faculty::find(['FacultyID'=>$request->faculty_id])->first();
   return json_encode($faculty->vw7_departments());
});

$router->get('/api/get/department',function(Request $request){
    $dept = Department::find(['DeptID'=>$request->dept_id]);
    return json_encode($dept);
 });

$router->get('/api/get/faculty/department/users',function(Request $request){
    $dept_id = $request->dept_id;
    $faculty_id = $request->faculty_id;
    $users = User_VW95::find(['UserDeptID'=>$dept_id,'FacultyID'=>$faculty_id]);
    return json_encode($users);
});


// route and action to save
$router->post('/update/department/hod',function(Request $request){
    // check for CSRF TOKEN 
   if(Router::verifyCsrfToken($request->_csrf_token)){
       try {

            //check for empty values
            $request->validateEmpty([
            'dept_id'=>' Please select a department',
            'faculty_id'=>' Please select a faculty',
            'hod_id'=>' Please select a HOD'
            ]);

          
           $department = Department::find(['DeptID'=>$request->dept_id,'FacultyID'=>$request->faculty_id])->first();

           if(is_null($department)){
               throw new \Exception('Department specified was not found');
           }

           // persist the hod details
           $department->HODID = $request->hod_id;
           $department->save();

       }catch(\Exception $e){
            // temporal satus storage our request will read from
            $_SESSION['tmp_status']['error'] = $e->getMessage();
            // redirect back to referer
           return redirect($request->httpReferer);
       }
       $_SESSION['tmp_status']['success'] = 'Head Of Department successfully saved';
       return redirect($request->httpReferer);
   } 
   //No CSRF Token found redirect back with error
   $_SESSION['tmp_status']['error'] = 'CSRF Token not found for this post request';
   return redirect($request->httpReferer);
});


// route and action to save
$router->post('/save/department',function(Request $request){
    // check for CSRF TOKEN 
   if(Router::verifyCsrfToken($request->_csrf_token)){
       try {

            //check for empty values
            $request->validateEmpty([
                'dept_name'=>' Please enter a department name',
                'dept_code'=>' Please enter a department code',
                'dept_faculty_id'=>' Please select a faculty',
                'dept_digit_code'=>' Please enter a digit code']
            );

           //Duplicate check
            $duplicateNameCheck = Department::find(['DeptName'=>strtoupper($request->dept_name)])->count();
            if($duplicateNameCheck > 0){
                throw new \Exception('Department Name  defined already exists');
            }

             //Duplicate check
             $duplicateCodeCheck = Department::find(['DeptCode'=>strtoupper($request->dept_code)])->count();
             if($duplicateCodeCheck > 0){
                 throw new \Exception('Department Code  defined already exists');
             }

              //Duplicate check
              $duplicateDigitCodeCheck = Department::find(['DigitCode'=>strtoupper($request->dept_digit_code)])->count();
              if($duplicateDigitCodeCheck > 0){
                  throw new \Exception('Department Digit Code  defined already exists');
              }
            // try carrying out action
            $Department = new Department;
            $Department->DeptName = strtoupper($request->dept_name);
            $Department->DeptCode = strtoupper($request->dept_code);
            $Department->FacultyID = $request->dept_faculty_id;
            $Department->DigitCode = strtoupper($request->dept_digit_code);
            $Department->HODID = (int) null;
            $Department->save();

       }catch(\Exception $e){
            // temporal satus storage our request will read from
            $_SESSION['tmp_status']['error'] = $e->getMessage();
            // redirect back to referer
           return redirect($request->httpReferer);
       }
       $_SESSION['tmp_status']['success'] = 'Department was successfully saved';
       return redirect($request->httpReferer);
   } 
   //No CSRF Token found redirect back with error
   $_SESSION['tmp_status']['error'] = 'CSRF Token not found for this post request';
   return redirect($request->httpReferer);
});

// route and action to update
$router->post('/update/department',function(Request $request){
    // check for CSRF TOKEN 
   if(Router::verifyCsrfToken($request->_csrf_token)){
       try {
            //check for empty values
            $request->validateEmpty([
                'dept_name'=>' Please enter Department Name',
                'dept_code'=>' Please  enter Department Code',
                'dept_faculty_id'=>' Please select a faculty',
                'dept_digit_code'=>' Please enter a digit code',
                'dept_id'=>' Please select a department']);

           //Duplicate check
            $duplicateNameCheck = Department::where(['DeptName','=',strtoupper($request->dept_name)],['DeptID','<>',$request->dept_id])->count();
            $duplicateCodeCheck = Department::where(['DeptCode','=',strtoupper($request->dept_code)],['DeptID','<>',$request->dept_id])->count();
            $duplicateDigitCodeCheck = Department::where(['DigitCode','=',strtoupper($request->dept_digit_code)],['DeptID','<>',$request->dept_id])->count();
          
            if($duplicateNameCheck > 0){
                throw new \Exception('Department Name  defined alreday exists');
            }

            if($duplicateCodeCheck > 0 ){
                throw new \Exception('Department Code defined already exists');
            }

            if($duplicateDigitCodeCheck > 0 ){
                throw new \Exception('Department Digit Code defined already exists');
            }
            // find the predata to update
            $department = Department::find(['DeptID'=>$request->dept_id,'FacultyID'=>$request->dept_faculty_id])->first();
            if(is_null($department)){
                throw new \Exception('Department Specified  not found');
            }

          //integrity checks
          $degree_courses = $department->degree_courses()->count();
          $fee_details = $department->fee_details()->count();
          $service_types = $department->service_types()->count();
          $academic_courses = $department->academic_courses()->count();

          if(array_sum([$degree_courses,$fee_details,$service_types,$academic_courses]) > 0 ){
              throw new \Exception('The Department cannot be modified because it is involved in sensitive transactions');
          }

          // persist the Department

          $department->DeptName = strtoupper($request->dept_name);
          $department->DeptCode = strtoupper($request->dept_code);
          $department->DigitCode = strtoupper($request->dept_digit_code);
          $department->save();
            

       }catch(\Exception $e){
            // temporal satus storage our request will read from
            $_SESSION['tmp_status']['error'] = $e->getMessage();
            // redirect back to referer
           return redirect($request->httpReferer);
       }
       $_SESSION['tmp_status']['success'] = 'Department was successfully saved';
       return redirect($request->httpReferer);
   } 
   //No CSRF Token found redirect back with error
   $_SESSION['tmp_status']['error'] = 'CSRF Token not found for this post request';
   return redirect($request->httpReferer);
});

// route and action to delete
$router->post('/delete/department',function(Request $request){
    // check for CSRF TOKEN 
   if(Router::verifyCsrfToken($request->_csrf_token)){
       try {
            
              //check for empty values
            $request->validateEmpty([
                'dept_name'=>' Please enter a department name',
                'dept_code'=>' Please enter a department code',
                'dept_faculty_id'=>' Please select a faculty',
                'dept_digit_code'=>' Please enter a digit code',
                'dept_id'=>' Please select a department']
            );


            // find the predata to update
            $department = Department::find(['DeptID'=>$request->dept_id,'FacultyID'=>$request->dept_faculty_id])->first();
            if(is_null($department)){
                throw new \Exception('Department Specified  not found');
            }

          //integrity checks
          $degree_courses = $department->degree_courses()->count();
          $fee_details = $department->fee_details()->count();
          $service_types = $department->service_types()->count();
          $academic_courses = $department->academic_courses()->count();

          if(array_sum([$degree_courses,$fee_details,$service_types,$academic_courses]) > 0 ){
              throw new \Exception('The Department cannot be modified because it is involved in sensitive transactions');
          }

          // delete department
          $department->delete();
           
  
       }catch(\Exception $e){
            // temporal satus storage our request will read from
            $_SESSION['tmp_status']['error'] = $e->getMessage();
            // redirect back to referer
           return redirect($request->httpReferer);
       }
       $_SESSION['tmp_status']['success'] = 'Department was successfully deleted';
       return redirect($request->httpReferer);
   } 
   //No CSRF Token found redirect back with error
   $_SESSION['tmp_status']['error'] = 'CSRF Token not found for this post request';
   return redirect($request->httpReferer);
});


?>