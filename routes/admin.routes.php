<?php
use Lib\Request;
use Lib\Router;
use Model\Department;
use Model\Faculty;
use Model\User;
use Model\User_VW95;
use Model\CourseLevel;
use Model\AssignedCourse;
use Model\UserCategory;

$router->get('/user_management',function(Request $request){
    
    require BASE_URL.'/admin/user_mgt.php';
});

$router->get('/api/get/user_category',function(Request $request){
    try{
        //$user_category = UserCategory::find(['UserGroupID'=>$request->group_id])->first();
        $user_category = UserCategory::all();
    }catch(Exception | Throwable $e){
        return json_encode($e->getMessage());
    }
    return json_encode($user_category);
   
});

$router->get('/api/search/user',function(Request $request){
    try{
       
        $users = User_VW95::orWhere(
            ['UserPIN',' like ',"%$request->search_data%"],
            ['Surname',' like ',"%$request->search_data%"],
            ['Others',' like ',"%$request->search_data%"],
            ['Title',' like ',"%$request->search_data%"],
            ['UserGroup',' like ',"%$request->search_data%"],
            ['UserCategory',' like ',"%$request->search_data%"],
            ['FacultyName',' like ',"%$request->search_data%"],
            ['DeptName',' like ',"%$request->search_data%"],
            ['Phone',' like ',"%$request->search_data%"],
            ['Email',' like ',"%$request->search_data%"]
        );
    }catch(Exception | Throwable $e){
        return json_encode($e->getMessage());
    }
    return json_encode($users);
   
});

$router->get('/api/get/user',function(Request $request){
    try{
       $user = User_VW95::find(['UserID'=>$request->user_id])->first();
       
    }catch(Exception | Throwable $e){
        return json_encode($e->getMessage());
    }
    return json_encode($user);
   
});


$router->get('/api/get/faculty/departments',function(Request $request){
   $faculty = Faculty::find(['FacultyID'=>$request->faculty_id])->first();
   return json_encode($faculty->departments());
});

$router->get('/api/get/users',function(Request $request){
    $dept_id = $request->dept_id;
    $faculty_id = $request->faculty_id;
    $users = User::find(['UserDeptID'=>$dept_id,'FacultyID'=>$faculty_id]);
    return json_encode($users);
});


// route and action to save
$router->post('/save/user',function(Request $request){
    // $user_vw95 = User_VW95::where(['UserPIN',' is ',null])->first();
    // $user_vw95->UserPIN = $user_vw95->PIN;
    // $user_vw95->save();
 
    // check for CSRF TOKEN 
   if(Router::verifyCsrfToken($request->_csrf_token)){
       try {

            //check for empty values
            $request->validateEmpty(
                ['surname'=>' Please enter surname',
                'othernames'=> ' Please enter other name',
                'sex'=>' Please enter user sex',
                'title'=> ' Please select user title',
                'user_group_id'=> ' Please select the user group',
                'user_category_id'=> ' Please select user category',
                'faculty_id' =>' Please select faculty',
                'dept_id' =>' Please select department'
                ]
            );

            if(empty($request->faculty_id) && ($request->user_category_id == 3 || $request->user_category_id == 4)){
                throw new \Exception("Please select the faculty");  
            }
            if(empty($request->dept_id) && ($request->user_category_id == 3 || $request->user_category_id == 4)){
                throw new Exception("Please select the department");
                
            }

            $user = new User;
            $user->Surname = strtoupper($request->surname);
            $user->Others = strtoupper($request->othernames);
            $user->Title = strtoupper($request->title);
            $user->Sex = strtoupper($request->sex);
            $user->UserCategoryID = $request->user_category_id;
            $user->UserGroupID  = $request->user_group_id;
            $user->UserDeptID = $request->dept_id;
            $user->UserStatusID = 0;
            
            if($user->save()){
                $user_vw95 = User_VW95::where(['UserPIN',' is ',null]);
              
                foreach($user_vw95 as $each_user_vw95){
                    $each_user_vw95->UserPIN = $each_user_vw95->PIN;
                    $each_user_vw95->UserPwd = sha1($each_user_vw95->PIN);
                    $each_user_vw95->UserStatusID = 1;
                    $each_user_vw95->save();
                }
            }
          
       }catch(\Exception $e){
          
            // temporal satus storage our request will read from
            $_SESSION['tmp_status']['error'] = $e->getMessage();
            // redirect back to referer
           return redirect($request->httpReferer);
       }
       $_SESSION['tmp_status']['success'] = 'User successfully saved';
       return redirect($request->httpReferer);
   } 
   //No CSRF Token found redirect back with error
   $_SESSION['tmp_status']['error'] = 'CSRF Token not found for this post request';
   return redirect($request->httpReferer);
});

// route and action to delete
$router->post('/delete/user',function(Request $request){
    // check for CSRF TOKEN 
   if(Router::verifyCsrfToken($request->_csrf_token)){
       try {

            //check for empty values
            $request->validateEmpty(
               ['user_id'=>' Please select a user']
            );

            // integrity checks
            $userIsHod  = Department::find(['HODID'=>$request->user_id])->count();
            $userIsFacultyDean = Faculty::find(['FacultyDeanID'=>$request->user_id])->count();
            $userIsAdviser = CourseLevel::find(['AcademicAdviserID'=>$request->user_id])->count();
            $userIsLecturer = AssignedCourse::find(['LecturerID'=>$request->user_id])->count();
            $userIsSenateRep = AssignedCourse::find(['SenateRepID'=>$request->user_id])->count();
            $userIsCourseHOD = AssignedCourse::find(['HODID'=>$request->user_id])->count();
            $userIsDean = AssignedCourse::find(['DeanID'=>$request->user_id])->count();



            if(array_sum([$userIsHod,$userIsFacultyDean,$userIsAdviser,$userIsLecturer,$userIsSenateRep,$userIsCourseHOD,$userIsDean]) > 0){
                throw new \Exception("User cannot be deleted beacuse he/she is involved in sensitive data");
                
            }
            
          $user = User::find(['UserID'=>$request->user_id])->first();
          if(is_null($user)){
              throw new \Exception(' User specified not found');
          }

          $user->delete();
          
       }catch(\Exception $e){
            // temporal satus storage our request will read from
            $_SESSION['tmp_status']['error'] = $e->getMessage();
            // redirect back to referer
           return redirect($request->httpReferer);
       }
       $_SESSION['tmp_status']['success'] = 'User successfully saved';
       return redirect($request->httpReferer);
   } 
   //No CSRF Token found redirect back with error
   $_SESSION['tmp_status']['error'] = 'CSRF Token not found for this post request';
   return redirect($request->httpReferer);
});



// route and action to save
$router->post('/update/user/btn1',function(Request $request){
    // check for CSRF TOKEN 

   if(Router::verifyCsrfToken($request->_csrf_token)){
       try {
          
            //check for empty values
            $request->validateEmpty([
                'surname'=>' Please enter surname',
                'othernames'=> ' Please enter other names',
                'sex'=>' Please enter user sex',
                'title'=> ' Please select user title',
                'user_group_id'=> ' Please select the user group',
                'user_category_id'=> ' Please select user category',
                'faculty_id' =>' Please select faculty',
                'dept_id' =>' Please select department',
                'user_id'=>' A user must be selected'
                ]
            );

            if(empty($request->faculty_id) && ($request->user_category_id == 3 || $request->user_category_id == 4)){
                throw new \Exception("Please select the faculty");  
            }
            if(empty($request->dept_id) && ($request->user_category_id == 3 || $request->user_category_id == 4)){
                throw new Exception("Please select the department");
                
            }

            $user = User::find(['UserID'=>$request->user_id])->first();
            if(is_null($user)){
                throw new  \Exception(" No user found for details given");
            }
            $user->Surname = strtoupper($request->surname);
            $user->Others = strtoupper($request->othernames);
            $user->Title = strtoupper($request->title);
            $user->Sex = strtoupper($request->sex);
            $user->UserCategoryID = $request->user_category_id;
            $user->UserGroupID  = $request->user_group_id;
            $user->UserDeptID = $request->dept_id;
            
           $user->save();
          
       }catch(\Exception $e){
            // temporal satus storage our request will read from
            $_SESSION['tmp_status']['error'] = $e->getMessage();
            // redirect back to referer
           return redirect($request->httpReferer);
       }
       $_SESSION['tmp_status']['success'] = 'User successfully saved';
       return redirect($request->httpReferer);
   } 
   //No CSRF Token found redirect back with error
   $_SESSION['tmp_status']['error'] = 'CSRF Token not found for this post request';
   return redirect($request->httpReferer);
});

// route and action to save
$router->post('/update/user/btn2',function(Request $request){
    // check for CSRF TOKEN 
   if(Router::verifyCsrfToken($request->_csrf_token)){
       try {

           $request->validateEmpty([
               'user_id'=>' Please select a user',
               'email'=> ' Please enter an email address',
               'phone'=> ' Please enter a phone number',
               'contact_address'=>' Please enter a contact address'
           ]);

           $mailControl = substr(strstr($request->email, "@"), 0, 1);
            $mailValidity = ($mailControl == '@' ? 1:0);
            if($mailValidity == 0){
                throw new \Exception('Please Enter a valid email address');
            }

           
            $user = User::find(['UserID'=>$request->user_id])->first();
            if(is_null($user)){
                throw new  \Exception(" No user found for details given");
            }
           
            $user->Email = $request->email;
            $user->Phone = $request->phone;
            $user->ContactAddress = $request->contact_address;
           $user->save();
          
       }catch(\Exception $e){
            // temporal satus storage our request will read from
            $_SESSION['tmp_status']['error'] = $e->getMessage();
            // redirect back to referer
           return redirect($request->httpReferer);
       }
       $_SESSION['tmp_status']['success'] = 'User successfully saved';
       return redirect($request->httpReferer);
   } 
   //No CSRF Token found redirect back with error
   $_SESSION['tmp_status']['error'] = 'CSRF Token not found for this post request';
   return redirect($request->httpReferer);
});

// route and action to save
$router->post('/update/user/btn3',function(Request $request){
    // check for CSRF TOKEN 

   if(Router::verifyCsrfToken($request->_csrf_token)){

   
       try {
           $request->validateEmpty([
               'user_id'=>' Please select a user',
               'staff_number'=> ' Please the staff number',
               'grade_level'=>' Please enter the grade level',
               'dyofb'=> ' Please enter a day of birth',
               'mofb'=>' Please enter a month of birth',
               'appt_date'=>' Please select an appointment date',
               'emp_date'=>' Please select an employment date'
           ]);

           if(  ($request->mofb == 2 && $request->dyofb > 29) || (($request->mofb == 9 || $request->mofb == 4 || $request->mofb == 6 || $request->mofb == 11) && $request->dyofb == 31)){
               throw new \Exception(' Please select a valid day of birth');
           }

          

            $user = User::find(['UserID'=>$request->user_id])->first();
            if(is_null($user)){
                throw new  \Exception(" No user found for details given");
            }
           
            $user->StaffNumber = $request->staff_number;
            $user->DyofB = $request->dyofb;
            $user->MofB = $request->mofb;
            $user->GradeLevel = $request->grade_level;
            $user->AppointmentDate = $request->appt_date;
            $user->EmploymentDate = $request->emp_date;
           $user->save();
          
       }catch(\Exception $e){
            // temporal satus storage our request will read from
            $_SESSION['tmp_status']['error'] = $e->getMessage();
            // redirect back to referer
           return redirect($request->httpReferer);
       }
       $_SESSION['tmp_status']['success'] = 'User successfully saved';
       return redirect($request->httpReferer);
   } 
   //No CSRF Token found redirect back with error
   $_SESSION['tmp_status']['error'] = 'CSRF Token not found for this post request';
   return redirect($request->httpReferer);
});

// route and action to save
$router->post('/reset/user/password',function(Request $request){
    // check for CSRF TOKEN 
   if(Router::verifyCsrfToken($request->_csrf_token)){
       try {

           $request->validateEmpty([
               'user_id'=>' Please select a user',
           ]);          
           $user_vw95 = User_VW95::where(['UserID','=',$request->user_id])->first();
            $user_vw95->UserPwd = sha1($user_vw95->PIN);
            $user_vw95->save();
        
       }catch(\Exception $e){
            // temporal satus storage our request will read from
            $_SESSION['tmp_status']['error'] = $e->getMessage();
            // redirect back to referer
           return redirect($request->httpReferer);
       }
       $_SESSION['tmp_status']['success'] = 'User Password successfully reset';
       return redirect($request->httpReferer);
   } 
   //No CSRF Token found redirect back with error
   $_SESSION['tmp_status']['error'] = 'CSRF Token not found for this post request';
   return redirect($request->httpReferer);
});

// route and action to save
$router->post('/deactivate/user',function(Request $request){
   
    // check for CSRF TOKEN 
   if(Router::verifyCsrfToken($request->_csrf_token)){
       try {
        
           
           $request->validateEmpty([
               'user_id'=>' Please select a user',
           ]);  
           $user = User::find(['UserID'=>$request->user_id])->first();
           if(is_null($user)){
               throw  new \Exception(' User specified does not exists');
           }  

          $new_status = $user->UserStatusID == 1  ? 0 : 1;
          $remark = $user->UserStatusID == 1  ? 'Deactivated' : 'Activated';

         
          $user->UserStatusID = $new_status;
          $user->save();

        
       }catch(\Exception $e){
            // temporal satus storage our request will read from
            $_SESSION['tmp_status']['error'] = $e->getMessage();
            // redirect back to referer
           return redirect($request->httpReferer);
       }
       $_SESSION['tmp_status']['success'] = "User has been successfully $remark";
       return redirect($request->httpReferer);
   } 
   //No CSRF Token found redirect back with error
   $_SESSION['tmp_status']['error'] = 'CSRF Token not found for this post request';
   return redirect($request->httpReferer);
});







?>