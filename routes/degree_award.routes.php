<?php
use Lib\Request;
use Lib\Router;
use Model\DegreeAward;
use Model\DegreeType;
use Model\ProgramType;



$router->get('/degreeaward',function(Request $request){
    require BASE_URL.'/setup/degree_award.php';
});

$router->get('/api/all/degreeawards',function(Request $request){
    return json_encode(DegreeAward::all());
});


$router->get('/api/get/degreeaward',function(Request $request){
    $award = DegreeAward::find(['DegreeAwardID'=>$request->degreeaward_id]);
    return json_encode($award);
});

$router->get('/api/degreetype/degreeawards',function(Request $request){
    $degreetype = DegreeType::find(['DegreeTypeID'=>$request->degreetype_id])->first();
    return json_encode($degreetype->awards());
});

$router->get('/api/programtype/degreeawards',function(Request $request){
    $degreeawards = DegreeAward::find(['ProgramTypeID'=>$request->programtype_id]);
    return json_encode($degreeawards);
});



// route and action to save
$router->post('/save/degreeaward',function(Request $request){
    // check for CSRF TOKEN 
   if(Router::verifyCsrfToken($request->_csrf_token)){
      
       try {

            //check for empty values
            $request->validateEmpty(['degreeaward'=>' Please enter Degree Award',
                                    'degreetype_id'=>' Please select Degree Type',
                                    'degreeawardcode'=>' Please enter Degree Award Code']);

           //Duplicate check
            $awardDuplicateCheck = DegreeAward::find(['DegreeTypeID'=>$request->degreetype_id,'DegreeAward'=>$request->degreeaward])->count();
            if($awardDuplicateCheck > 0){
                throw new \Exception('Degree degree award alreday exists for Degree Type');
            }

            //Duplicate check
            $codeDuplicateCheck = DegreeAward::find(['DegreeTypeID'=>$request->degreetype_id,'DegreeAwardCode'=>$request->degreeawardcode])->count();
            if($codeDuplicateCheck > 0){
                throw new \Exception('Degree Award   defined alreday exists for Degree Type');
            }
            // try carrying out action
            $degreeaward = new DegreeAward;
            $degreeaward->DegreeAward = $request->degreeaward;
            $degreeaward->DegreeTypeID = $request->degreetype_id;
            $degreeaward->DegreeAwardCode = $request->degreeawardcode;
            $degreeaward->save();

       }catch(\Exception $e){
            // temporal satus storage our request will read from
            $_SESSION['tmp_status']['error'] = $e->getMessage();
            // redirect back to referer
           return redirect($request->httpReferer);
       }
       $_SESSION['tmp_status']['success'] = 'degree award was successfully saved';
       return redirect($request->httpReferer);
   } 
   //No CSRF Token found redirect back with error
   $_SESSION['tmp_status']['error'] = 'CSRF Token not found for this post request';
   return redirect($request->httpReferer);
});

// route and action to update
$router->post('/update/degreeaward',function(Request $request){
    // check for CSRF TOKEN 
   if(Router::verifyCsrfToken($request->_csrf_token)){
       try {
          
            //check for empty values
            $request->validateEmpty(['degreeaward'=>' Please enter Degree Award',
                                        'degreetype_id'=>' Please select Degree Type',
                                        'degreeawardcode'=>' Please enter a Degree Award Code',
                                        'degreeaward_id'=>' Please select a Degree Award']);

          
            //Duplicate check
            $awardDuplicateCheck = DegreeAward::where(['DegreeTypeID','<>',$request->degreetype_id],['DegreeAward','=',$request->degreeaward])->count();
            if($awardDuplicateCheck > 0){
                throw new \Exception('Degree Award   defined alreday exists for Degree Type');
            }

            //Duplicate check
            $codeDuplicateCheck = DegreeAward::where(['DegreeTypeID','<>',$request->degreetype_id],['DegreeAwardCode','=',$request->degreeawardcode])->count();
            if($codeDuplicateCheck > 0){
                throw new \Exception('Degree Award   defined alreday exists for Degree Type');
            }
             // find the predata to update
             $degreeaward = DegreeAward::find(['DegreeAwardID'=>$request->degreeaward_id,'DegreeTypeID'=>$request->degreetype_id])->first();
             if(is_null($degreeaward)){
                 throw new \Exception('Degree Award specified not found');
             }

            //integrity check
             if($degreeaward->degreecourses()->count() > 0){
                 throw new \Exception('The Degree Award cannot be modified because it is involved in sensitive operations');
             }

             // save operation
             $degreeaward->DegreeAward = $request->degreeaward;
             $degreeaward->DegreeAwardCode = $request->degreeawardcode;
             $degreeaward->DegreeTypeID = $request->degreetype_id;
             $degreeaward->save();

       }catch(\Exception $e){
         
            // temporal satus storage our request will read from
            $_SESSION['tmp_status']['error'] = $e->getMessage();
            // redirect back to referer
           return redirect($request->httpReferer);
       }
       $_SESSION['tmp_status']['success'] = 'Degree Award was successfully saved';
       return redirect($request->httpReferer);
   } 
   //No CSRF Token found redirect back with error
   $_SESSION['tmp_status']['error'] = 'CSRF Token not found for this post request';
   return redirect($request->httpReferer);
});

// route and action to delete
$router->post('/delete/degreeaward',function(Request $request){
    // check for CSRF TOKEN 
   if(Router::verifyCsrfToken($request->_csrf_token)){
       try {
            // check for empty request
            $request->validateEmpty(['degreetype_id'=>' Please select a Degree Type ','degreeaward_id'=>' Please select Degree Award']);

            // find the predata to update
            $degreeaward = DegreeAward::find(['DegreeAwardID'=>$request->degreeaward_id,'DegreeTypeID'=>$request->degreetype_id])->first();
            if(is_null($degreeaward)){
                throw new \Exception('Degree Award specified not found');
            }

           //integrity check
            if($degreeaward->degreecourses()->count() > 0){
                throw new \Exception('The Degree Award cannot be modified because it is involved in sensitive operations');
            }
            $degreeaward->delete();
       }catch(\Exception $e){
            // temporal satus storage our request will read from
            $_SESSION['tmp_status']['error'] = $e->getMessage();
            // redirect back to referer
           return redirect($request->httpReferer);
       }
       $_SESSION['tmp_status']['success'] = 'Degree Award was successfully deleted';
       return redirect($request->httpReferer);
   } 
   //No CSRF Token found redirect back with error
   $_SESSION['tmp_status']['error'] = 'CSRF Token not found for this post request';
   return redirect($request->httpReferer);
});



?>