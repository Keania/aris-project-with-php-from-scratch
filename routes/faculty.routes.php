<?php
use Lib\Request;
use Lib\Router;
use Model\Faculty;


$router->get('/faculty_management',function(Request $request){
    require BASE_URL.'/faculty/faculty.php';
});

$router->get('/api/get/faculty',function(Request $request){
    $faculty = Faculty::find(['FacultyID'=>$request->faculty_id]);
    return json_encode($faculty);
});

$router->get('/api/get/faculty/users',function(Request $request){
    $faculty = Faculty::find(['FacultyID'=>$request->faculty_id])->first();
    return json_encode($faculty->vw95_users());
});


// route and action to save
$router->post('/save/faculty',function(Request $request){
    // check for CSRF TOKEN 
   if(Router::verifyCsrfToken($request->_csrf_token)){
       try {

            //check for empty values
            $request->validateEmpty(
                ['faculty_name'=>' Please enter faculty name',
                'faculty_code'=>' Please enter faculty code',
                'faculty_colour_id'=>' Please select a colour']
            );

           //Duplicate check
            $duplicateNameCheck = Faculty::find(['FacultyName'=>strtoupper($request->faculty_name)])->count();
            if($duplicateNameCheck > 0){
                throw new \Exception('Faculty Name  defined already exists');
            }

             //Duplicate check
             $duplicateCodeCheck = Faculty::find(['FacultyCode'=>strtoupper($request->faculty_code)])->count();
             if($duplicateCodeCheck > 0){
                 throw new \Exception('Faculty Code  defined already exists');
             }
            // try carrying out action
            $faculty = new Faculty;
            $faculty->FacultyName = strtoupper($request->faculty_name);
            $faculty->FacultyCode = strtoupper($request->faculty_code);
            $faculty->FacultyColourID = $request->faculty_colour_id;
            $faculty->FacultyDeanID = (int) null;
            
            $faculty->save();

       }catch(\Exception $e){
           
            // temporal satus storage our request will read from
            $_SESSION['tmp_status']['error'] = $e->getMessage();
            // redirect back to referer
           return redirect($request->httpReferer);
       }
       $_SESSION['tmp_status']['success'] = 'Faculty was successfully saved';
       return redirect($request->httpReferer);
   } 
   //No CSRF Token found redirect back with error
   $_SESSION['tmp_status']['error'] = 'CSRF Token not found for this post request';
   return redirect($request->httpReferer);
});

// route and action to update
$router->post('/update/faculty',function(Request $request){
    // check for CSRF TOKEN 
   if(Router::verifyCsrfToken($request->_csrf_token)){
       try {
            // check for empty request
            $request->validateEmpty([
                'faculty_id'=>' Please select a faculty',
                'faculty_name'=>' Please enter a Faculty Name to edit',
                'faculty_colour_id'=>' Please select a colour',
                'faculty_code'=>' Please enter a Faculty Code']);

           //Duplicate check
            $duplicateNameCheck = Faculty::where(['FacultyName','=',strtoupper($request->faculty_name)],['FacultyID','<>',$request->faculty_id])->count();
            $duplicateCodeCheck = Faculty::where(['FacultyCode','=',strtoupper($request->faculty_code)],['FacultyID','<>',$request->faculty_id])->count();
          
            if($duplicateNameCheck > 0){
                throw new \Exception('Faculty Name  defined alreday exists');
            }

            if($duplicateCodeCheck > 0 ){
                throw new \Exception('Faculty Code defined already exists');
            }
            // find the predata to update
            $faculty = Faculty::find(['FacultyID'=>$request->faculty_id])->first();
            if(is_null($faculty)){
                throw new \Exception('Faculty Specified specified not found');
            }
          //integrity checks
          $depts = $faculty->departments()->count();
          $fee_details = $faculty->feedetails()->count();
          $service_types = $faculty->service_types()->count();

          if(array_sum([$depts,$fee_details,$service_types]) > 0 ){
              throw new \Exception('The Faculty cannot be modified because it is involved in sensitive transactions');
          }

          // persist the faculty

          $faculty->FacultyName = strtoupper($request->faculty_name);
          $faculty->FacultyCode = strtoupper($request->faculty_code);
          $faculty->FacultyColourID = $request->faculty_colour_id;
        
          $faculty->save();
            

       }catch(\Exception $e){
            // temporal satus storage our request will read from
            $_SESSION['tmp_status']['error'] = $e->getMessage();
            // redirect back to referer
           return redirect($request->httpReferer);
       }
       $_SESSION['tmp_status']['success'] = 'Faculty was successfully saved';
       return redirect($request->httpReferer);
   } 
   //No CSRF Token found redirect back with error
   $_SESSION['tmp_status']['error'] = 'CSRF Token not found for this post request';
   return redirect($request->httpReferer);
});

// route and action to delete
$router->post('/delete/faculty',function(Request $request){
    // check for CSRF TOKEN 
   if(Router::verifyCsrfToken($request->_csrf_token)){
       try {
             // check for empty request
             $request->validateEmpty(['faculty_id'=>' Please select a faculty to delete']);

           
              // find the predata to update
              $faculty = Faculty::find(['FacultyID'=>$request->faculty_id])->first();
              if(is_null($faculty)){
                  throw new \Exception('Faculty Specified specified not found');
              }
            //integrity checks
            $depts = $faculty->departments()->count();
            $fee_details = $faculty->feedetails()->count();
            $service_types = $faculty->service_types()->count();
  
            if(array_sum([$depts,$fee_details,$service_types]) > 0 ){
                throw new \Exception('The Faculty cannot be modified because it is involved in sensitive transactions');
            }

            // delete faculty
            $faculty->delete();
  
       }catch(\Exception $e){
            // temporal satus storage our request will read from
            $_SESSION['tmp_status']['error'] = $e->getMessage();
            // redirect back to referer
           return redirect($request->httpReferer);
       }
       $_SESSION['tmp_status']['success'] = 'Faculty was successfully deleted';
       return redirect($request->httpReferer);
   } 
   //No CSRF Token found redirect back with error
   $_SESSION['tmp_status']['error'] = 'CSRF Token not found for this post request';
   return redirect($request->httpReferer);
});

// route and action to save
$router->post('/save/faculty/dean',function(Request $request){
    // check for CSRF TOKEN 
   if(Router::verifyCsrfToken($request->_csrf_token)){
       try {

            //check for empty values
            $request->validateEmpty(
                ['faculty_id'=>' Please select a faculty',
                'faculty_dean_id'=>' Please select a user to be made Dean',
               ]
            );

           //Duplicate check
           

            
            // try carrying out action
            $faculty = Faculty::find(['FacultyID'=>$request->faculty_id])->first();
            $faculty->FacultyDeanID = $request->faculty_dean_id;
            $faculty->save();

       }catch(\Exception $e){
           
            // temporal satus storage our request will read from
            $_SESSION['tmp_status']['error'] = $e->getMessage();
            // redirect back to referer
           return redirect($request->httpReferer);
       }
       $_SESSION['tmp_status']['success'] = 'Faculty dean successfully saved';
       return redirect($request->httpReferer);
   } 
   //No CSRF Token found redirect back with error
   $_SESSION['tmp_status']['error'] = 'CSRF Token not found for this post request';
   return redirect($request->httpReferer);
});




?>