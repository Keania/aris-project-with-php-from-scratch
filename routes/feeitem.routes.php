<?php
use Lib\Request;
use Lib\Router;
use Model\FeeItem;
use Model\Predata;


$router->get('/feeitem',function(Request $request){
    require BASE_URL.'/setup/feeitem.php';
});

$router->get('/api/get/feeitem',function(Request $request){
    $feeitem = FeeItem::find(['FeeItemID'=>$request->feeitem_id])->first();
    return json_encode($feeitem);
});

$router->get('/api/get/feeitems/feetype',function(Request $request){
    $feeitem = FeeItem::find(['FeeTypeID'=>$request->feetype_id]);
    return json_encode($feeitem);
});


// route and action to save
$router->post('/save/feeitem',function(Request $request){
    // check for CSRF TOKEN 
   if(Router::verifyCsrfToken($request->_csrf_token)){
       try {

           
            //check for empty values
            $request->validateEmpty(
                ['feetype_id'=>' Please select the Fee Type',
                'feeitem'=> ' Please Enter the fee item'
                ]
            );

           $check = FeeItem::find(['FeeItem'=>$request->feeitem,'FeeTypeID'=>$request->feetype_id])->count();
           if($check > 0){
                throw new \Exception('Fee Item Defined already exists in database');
           }

           $feeitem = new FeeItem;
           $feeitem->FeeItem = strtoupper($request->feeitem);
           $feeitem->FeeTypeID = $request->feetype_id;
           $feeitem->save();

       }catch(\Exception $e){
            // temporal satus storage our request will read from
            $_SESSION['tmp_status']['error'] = $e->getMessage();
            // redirect back to referer
           return redirect($request->httpReferer);
       }
       $_SESSION['tmp_status']['success'] = 'Fee item was successfully saved';
       return redirect($request->httpReferer);
   } 
   //No CSRF Token found redirect back with error
   $_SESSION['tmp_status']['error'] = 'CSRF Token not found for this post request';
   return redirect($request->httpReferer);
});

$router->post('/update/feeitem',function(Request $request){
    // check for CSRF TOKEN 
   if(Router::verifyCsrfToken($request->_csrf_token)){
       try {

           
            //check for empty values
            $request->validateEmpty(
                [
                'feetype_id'=>' Please select the Fee Type',
                'feeitem'=> ' Please Enter the fee item',
                'feeitem_id'=>' Please select a fee item'
                ]
            );

           $check = FeeItem::find(['FeeItem'=>strtoupper($request->feeitem),'FeeItemID'=>$request->feeitem_id])->count();
           if($check > 0){
                throw new \Exception('Fee Item Defined already exists in database');
           }

           $feeitem = FeeItem::find(['FeeTypeID'=>$request->feetype_id,'FeeItemID'=>$request->feeitem_id])->first();
           if(is_null($feeitem)){
               throw new \Exception('Fee Item specified cannot be found');
           }

           // integrity checks
           $fee_details_check = $feeitem->fee_details()->count();
           $other_fee_check = $feeitem->other_fees()->count();
           $trnxs_check = $feeitem->trnxs()->count();

           if(array_sum([$fee_details_check,$other_fee_check,$trnxs_check]) > 0 ){

               throw new \Exception('The Fee Item cannot be modified because it is involved in sensitive operations');
           }
           // persists the fee item
           $feeitem->FeeItem = strtoupper($request->feeitem);
           $feeitem->FeeTypeID = $request->feetype_id;
           $feeitem->save();
           
       }catch(\Exception $e){
            // temporal satus storage our request will read from
            $_SESSION['tmp_status']['error'] = $e->getMessage();
            // redirect back to referer
           return redirect($request->httpReferer);
       }
       $_SESSION['tmp_status']['success'] = 'Fee Item was successfully saved';
       return redirect($request->httpReferer);
   } 
   //No CSRF Token found redirect back with error
   $_SESSION['tmp_status']['error'] = 'CSRF Token not found for this post request';
   return redirect($request->httpReferer);
});

$router->post('/delete/feeitem',function(Request $request){
    // check for CSRF TOKEN 
   if(Router::verifyCsrfToken($request->_csrf_token)){
       try {

           
            //check for empty values
            $request->validateEmpty(
                ['feetype_id'=>' Please select the Fee Item',
                'feeitem_id'=>' Please select a fee item'
                ]
            );

          
           $feeitem = FeeItem::find(['FeeTypeID'=>$request->feetype_id,'FeeItemID'=>$request->feeitem_id])->first();
           if(is_null($feeitem)){
               throw new \Exception('Fee Item specified cannot be found');
           }

           // integrity checks
           $fee_details_check = $feeitem->fee_details()->count();
           $other_fee_check = $feeitem->other_fees()->count();
           $trnxs_check = $feeitem->trnxs()->count();

           if(array_sum([$fee_details_check,$other_fee_check,$trnxs_check]) > 0 ){

               throw new \Exception('The Fee Item cannot be modified because it is involved in sensitive operations');
           }
           // delete the fee item
           $feeitem->delete();
           
       }catch(\Exception $e){
            // temporal satus storage our request will read from
            $_SESSION['tmp_status']['error'] = $e->getMessage();
            // redirect back to referer
           return redirect($request->httpReferer);
       }
       $_SESSION['tmp_status']['success'] = 'Fee Item was successfully deleted';
       return redirect($request->httpReferer);
   } 
   //No CSRF Token found redirect back with error
   $_SESSION['tmp_status']['error'] = 'CSRF Token not found for this post request';
   return redirect($request->httpReferer);
});




?>