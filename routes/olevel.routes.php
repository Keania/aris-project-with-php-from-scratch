<?php
use Lib\Request;
use Lib\Router;
use Model\OlevelSubject;

$router->get('/olevel',function(Request $request){
    require BASE_URL.'/setup/olevel.php';
});

$router->get('/api/olevelsubject',function(Request $request){
    $subject = OlevelSubject::find(['SubjectID'=>$request->subject_id]);
    return json_encode($subject);
});


$router->post('/save/olevelsubject',function(Request $request){
    // check for CSRF TOKEN 
   if(Router::verifyCsrfToken($request->_csrf_token)){
       try {

            //check for empty values
            $request->validateEmpty(['subject'=>' Please enter a subject']);

           //Duplicate check
            $duplicateCheck = OlevelSubject::find(['Subject'=>$request->subject])->count();
            if($duplicateCheck > 0){
                throw new \Exception('Subject  defined alreday exists');
            }
            // try carrying out action
            $subject = new OlevelSubject;
            $subject->Subject = $request->subject;
            $subject->save();
       }catch(\Exception $e){
            // temporal satus storage our request will read from
            $_SESSION['tmp_status']['error'] = $e->getMessage();
            // redirect back to referer
           return redirect($request->httpReferer);
       }
       $_SESSION['tmp_status']['success'] = 'Subject was successfully saved';
       return redirect($request->httpReferer);
   } 
   //No CSRF Token found redirect back with error
   $_SESSION['tmp_status']['error'] = 'CSRF Token not found for this post request';
   return redirect($request->httpReferer);
});

// route and action to update
$router->post('/update/olevelsubject',function(Request $request){
    // check for CSRF TOKEN 
   if(Router::verifyCsrfToken($request->_csrf_token)){
       try {
            // check for empty request
            $request->validateEmpty(
                ['subject_id'=>' Please select a subject',
                'subject'=>' Please enter a subject to edit']
            );
           
           
           //Duplicate check
            $duplicateCheck = OlevelSubject::find(['SubjectID'=>$request->subject_id,'Subject'=>$request->subject,])->count();

           
            if($duplicateCheck > 0){
                throw new \Exception('Subject  defined alreday exists');
            }
            // find the predata to update
            $subject = OlevelSubject::find(['SubjectID'=>$request->subject_id])->first();
            if(is_null($subject)){
                throw new \Exception('Subject specified not found');
            }

            
            // integrity checks goes here
            $olevel_check = $subject->olevel_eligibilities()->count();
            $jamb_check = $subject->jamb_eligibilities()->count();
            $entry_re_check = $subject->entry_requirements()->count();
            $entry_re_alt_check = $subject->entry_requirement_alts()->count();
            
            if(array_sum($olevel_check,$jamb_check,$entry_re_check,$entry_re_alt_check) > 0){
                throw new \Exception("The Subject cannot be modified because it is involved in sensitive operations ");
                
            }
            
            $subject->Subject = $request->subject;
            $subject->save();

       }catch(\Exception $e){
          
            // temporal satus storage our request will read from
            $_SESSION['tmp_status']['error'] = $e->getMessage();
            // redirect back to referer
           return redirect($request->httpReferer);
       }
       $_SESSION['tmp_status']['success'] = 'Subject was successfully saved';
       return redirect($request->httpReferer);
   } 
   //No CSRF Token found redirect back with error
   $_SESSION['tmp_status']['error'] = 'CSRF Token not found for this post request';
   return redirect($request->httpReferer);
});

// route and action to delete
$router->post('/delete/olevelsubject',function(Request $request){
    // check for CSRF TOKEN 
   if(Router::verifyCsrfToken($request->_csrf_token)){
       try {
            // check for empty request
            $request->validateEmpty(['subject_id'=>' Please select a subject to delete']);

            // find the predata to update
            $subject = OlevelSubject::find(['SubjectID'=>$request->subject_id])->first();
            if(is_null($subject)){
                throw new \Exception('Subject specified not found');
            }

            
              // integrity checks goes here
              $olevel_check = $subject->olevel_eligibilities()->count();
              $jamb_check = $subject->jamb_eligibilities()->count();
              $entry_re_check = $subject->entry_requirements()->count();
              $entry_re_alt_check = $subject->entry_requirement_alts()->count();
  
              if(array_sum($olevel_check,$jamb_check,$entry_re_check,$entry_re_alt_check) > 0){
                  throw new \Exception("The Subject cannot be modified because it is involved in sensitive operations ");
                  
              }
           
            $subject->delete();
       }catch(\Exception $e){
            // temporal satus storage our request will read from
            $_SESSION['tmp_status']['error'] = $e->getMessage();
            // redirect back to referer
           return redirect($request->httpReferer);
       }
       $_SESSION['tmp_status']['success'] = 'Olevel Subject was successfully deleted';
       return redirect($request->httpReferer);
   } 
   //No CSRF Token found redirect back with error
   $_SESSION['tmp_status']['error'] = 'CSRF Token not found for this post request';
   return redirect($request->httpReferer);
});
