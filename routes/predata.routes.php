<?php
use Lib\Request;
use Lib\Router;
use Model\Predata;
use Model\PredataCategory;
use Model\FacultyColour;


$router->get('/predata',function(Request $request){
    
    require BASE_URL.'/setup/predata.php';
});

$router->get('/api/get/predata',function(Request $request){
    $predata = Predata::find(['DataID'=>$request->predata_id]);
    return json_encode($predata);
});

$router->get('/api/category/predatas',function(Request $request){
    $category = PredataCategory::find(['CategoryID'=>$request->category_id])->first();
    return json_encode($category->predatas());
});



// route and action to save
$router->post('/save/predata',function(Request $request){
    // check for CSRF TOKEN 
   if(Router::verifyCsrfToken($request->_csrf_token)){
       try {

           
            //check for empty values
            $request->validateEmpty(
                ['cat_id'=>' Please select the data category',
                'predata'=> ' Please Enter the predata'
                ]
            );

           //Duplicate check
            $duplicateCheck = Predata::find(['CategoryID'=>$request->cat_id,'Data'=>$request->predata])->count();
            if($duplicateCheck > 0){
                throw new \Exception('Predata  defined alreday exists');
            }
            // try carrying out action
            $predata = new Predata;
            $predata->CategoryID = $request->cat_id;
            $predata->Data = $request->predata;
            $predata->save();
       }catch(\Exception $e){
            // temporal satus storage our request will read from
            $_SESSION['tmp_status']['error'] = $e->getMessage();
            // redirect back to referer
           return redirect($request->httpReferer);
       }
       $_SESSION['tmp_status']['success'] = 'Predata was successfully saved';
       return redirect($request->httpReferer);
   } 
   //No CSRF Token found redirect back with error
   $_SESSION['tmp_status']['error'] = 'CSRF Token not found for this post request';
   return redirect($request->httpReferer);
});

// route and action to update
$router->post('/update/predata',function(Request $request){
    // check for CSRF TOKEN 
   if(Router::verifyCsrfToken($request->_csrf_token)){
       try {
          
            // check for empty request
            $request->validateEmpty(
                ['cat_id'=>' Please select data category',
                'predata'=>' Please enter predata',
                'predata_id'=>' Please select predata you want to edit'
                ]
            );

           
           //Duplicate check
            $duplicateCheck = Predata::find(['CategoryID'=>$request->cat_id,'Data'=>$request->predata,])->count();
            if($duplicateCheck > 0){
                throw new \Exception('Predata  defined alreday exists');
            }
            // find the predata to update
            $predata = Predata::find(['CategoryID'=>$request->cat_id,'DataID'=>$request->predata_id])->first();
            if(is_null($predata)){
                throw new \Exception('Predata specified not found');
            }
            $predata->CategoryID = $request->cat_id;
            $predata->Data = $request->predata;
            $predata->save();

       }catch(\Exception $e){
            // temporal satus storage our request will read from
            $_SESSION['tmp_status']['error'] = $e->getMessage();
            // redirect back to referer
           return redirect($request->httpReferer);
       }
       $_SESSION['tmp_status']['success'] = 'Predata was successfully saved';
       return redirect($request->httpReferer);
   } 
   //No CSRF Token found redirect back with error
   $_SESSION['tmp_status']['error'] = 'CSRF Token not found for this post request';
   return redirect($request->httpReferer);
});

// route and action to delete
$router->post('/delete/predata',function(Request $request){
    // check for CSRF TOKEN 
   if(Router::verifyCsrfToken($request->_csrf_token)){
       try {
            // check for empty request
            $request->validateEmpty(['cat_id'=>' Please select a Data Category',
            'predata_id'=>' Please select a predefined data']);

            // find the predata to update
            $predata = Predata::find(['CategoryID'=>$request->cat_id,'DataID'=>$request->predata_id])->first();
            if(is_null($predata)){
                throw new \Exception('Predata specified not found');
            }
           
            $predata->delete();
       }catch(\Exception $e){
            // temporal satus storage our request will read from
            $_SESSION['tmp_status']['error'] = $e->getMessage();
            // redirect back to referer
           return redirect($request->httpReferer);
       }
       $_SESSION['tmp_status']['success'] = 'Predata was successfully deleted';
       return redirect($request->httpReferer);
   } 
   //No CSRF Token found redirect back with error
   $_SESSION['tmp_status']['error'] = 'CSRF Token not found for this post request';
   return redirect($request->httpReferer);
});



//route action for faculty colour fetch
$router->get('/api/faculty/colour',function(Request $request){
    $colour = FacultyColour::find(['ColourID'=>$request->colour_id]);
    return json_encode($colour);
});


// route and action to save
$router->post('/save/faculty/colour',function(Request $request){
    // check for CSRF TOKEN 
   if(Router::verifyCsrfToken($request->_csrf_token)){
       try {

            //check for empty values
            $request->validateEmpty(['colour_name'=>' Please enter a colour']);

           //Duplicate check
            $duplicateCheck = FacultyColour::find(['ColourName'=>$request->colour_name])->count();
            if($duplicateCheck > 0){
                throw new \Exception('Colour  defined alreday exists');
            }
            // try carrying out action
            $colour = new FacultyColour;
            $colour->ColourName = $request->colour_name;
            $colour->save();
       }catch(\Exception $e){
            // temporal satus storage our request will read from
            $_SESSION['tmp_status']['error'] = $e->getMessage();
            // redirect back to referer
           return redirect($request->httpReferer);
       }
       $_SESSION['tmp_status']['success'] = 'Colour was successfully saved';
       return redirect($request->httpReferer);
   } 
   //No CSRF Token found redirect back with error
   $_SESSION['tmp_status']['error'] = 'CSRF Token not found for this post request';
   return redirect($request->httpReferer);
});

// route and action to update
$router->post('/update/faculty/colour',function(Request $request){
    // check for CSRF TOKEN 
   if(Router::verifyCsrfToken($request->_csrf_token)){
       try {
            // check for empty request
            $request->validateEmpty([
                'colour_id'=>' Please select a colour',
                'colour_name'=>' Please enter a colour'
            ]);

         
           //Duplicate check
            $duplicateCheck = FacultyColour::find(['ColourID'=>$request->colour_id,'ColourName'=>$request->colour_name])->count();
          
            if($duplicateCheck > 0){
                throw new \Exception('Colour  defined alreday exists');
            }
            // find the predata to update
            $colour = FacultyColour::find(['ColourID'=>$request->colour_id])->first();
            if(is_null($colour)){
                throw new \Exception('Colour specified not found');
            }

            //integrity check
            $colour_check = $colour->faculties()->count();
            if($colour_check > 0){
                throw new \Exception('This colour cannot be modified because it is involved sensitive transaction');
            }
          
            $colour->ColourName = $request->colour_name;
            $colour->save();

       }catch(\Exception $e){
            // temporal satus storage our request will read from
            $_SESSION['tmp_status']['error'] = $e->getMessage();
            // redirect back to referer
           return redirect($request->httpReferer);
       }
       $_SESSION['tmp_status']['success'] = 'Colour was successfully saved';
       return redirect($request->httpReferer);
   } 
   //No CSRF Token found redirect back with error
   $_SESSION['tmp_status']['error'] = 'CSRF Token not found for this post request';
   return redirect($request->httpReferer);
});

// route and action to delete
$router->post('/delete/faculty/colour',function(Request $request){
    // check for CSRF TOKEN 
   if(Router::verifyCsrfToken($request->_csrf_token)){
       try {
            // check for empty request
            $request->validateEmpty(['colour_id'=>' Please select a colour you want to delete']);

            // find the predata to update
            $colour= FacultyColour::find(['ColourID'=>$request->colour_id])->first();
            if(is_null($colour)){
                throw new \Exception('Colour specified not found');
            }
             //integrity check
             $colour_check = $colour->faculties()->count();
             if($colour_check > 0){
                 throw new \Exception('This colour cannot be deleted because it is involved sensitive transaction');
             }
           
            $colour->delete();
       }catch(\Exception $e){
            // temporal satus storage our request will read from
            $_SESSION['tmp_status']['error'] = $e->getMessage();
            // redirect back to referer
           return redirect($request->httpReferer);
       }
       $_SESSION['tmp_status']['success'] = 'Colour was successfully deleted';
       return redirect($request->httpReferer);
   } 
   //No CSRF Token found redirect back with error
   $_SESSION['tmp_status']['error'] = 'CSRF Token not found for this post request';
   return redirect($request->httpReferer);
});


?>