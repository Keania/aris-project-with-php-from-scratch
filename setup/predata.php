<?php

use Lib\Connection;
use Model\PredataCategory;
use Model\Predata;
use Model\FacultyColour;

$page_title = 'Setup Predefined data';
$token = generateCSRFToken();
include BASE_URL . '/setup/header.php';

// Database Operations of getting data
$predatacategories = PredataCategory::all();
$colours = FacultyColour::all();

?>


<!-- content section of this page goes here -->
<!-- Main content -->
<div class="main-content">
    <!-- Top navbar -->
    <nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
        <div class="container-fluid">
            <!-- Brand -->
            <!-- <a class="h4 mb-0 text-secondary d-none_ d-lg-inline-block" href="#">Setup</a>
        <a class="h4 mb-0 text-white d-none_ d-lg-inline-block" href="#"> | Pre-Defined Data</a>
         -->
            <ul class="font-weight-300 nav">
                <li class="nav-item">
                    <a href="#" class="nav-link text-white-50 d-lg-inline-block">Setup Management</a>
                </li> <span class="text-white-50 h2">|</span>
                <li class="nav-item">
                    <a href="#" class="nav-link text-white-50 d-lg-inline-block" style="font-size: 1.35rem;line-height: 1;">Pre-Defined Data</a>
                </li>
            </ul>

            <!-- Form -->
            <!--  <form class="navbar-search navbar-search-dark form-inline mr-3 d-none d-md-flex ml-lg-auto">
          <div class="form-group mb-0">
            <div class="input-group input-group-alternative">
              <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-search"></i></span>
              </div>
              <input class="form-control" placeholder="Search" type="text">
            </div>
          </div>
        </form> -->
            <!-- User -->
            <ul class="navbar-nav align-items-center d-none d-md-flex">
                <li class="nav-item dropdown">
                    <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <div class="media align-items-center">
                            <span class="avatar avatar-sm rounded-circle">
                                <img alt="Image placeholder" src="<?php echo resource('public/assets/img/theme/team-4-800x800.jpg');?>">
                            </span>
                            <div class="media-body ml-2 d-none d-lg-block">
                                <span class="mb-0 text-sm  font-weight-bold">Jonathan Doe <i class="fa fa-caret-down"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
                        <div class=" dropdown-header noti-title">
                            <h6 class="text-overflow m-0">Welcome!</h6>
                        </div>
                        <a href="../pages/profile.html" class="dropdown-item">
                            <i class="ni ni-single-02"></i>
                            <span>My profile</span>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="#!" class="dropdown-item">
                            <i class="ni ni-user-run"></i>
                            <span>Logout</span>
                        </a>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
    <!-- Header -->
    <div class="header bg-gradient-primary py-5">
        <div class="container-fluid">
            <div class="header-body">
                <!-- Card stats -->

            </div>
        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt-md-4">
    <div class="row">
            <div class="col-md-6  offset-md-1 mb-3">
                <?php
                // check if there is a message in the incoming request
                if (isset($request->session)) {

                    // check if an error exists in the incoming request
                    if (isset($request->session['error'])) {
                        $error = $request->session['error'];
                        echo "<div class='alert alert-danger'>$error</div>";
                    }

                    // check if there is a success msg in the incoming request
                    if (isset($request->session['success'])) {
                        $success = $request->session['success'];
                        echo "<div class='alert alert-success'>$success</div>";
                    }
                }
                ?>
            </div>
        </div>
        <div class="row">

            <div class="col-md-6 offset-md-1 mb-3">
                <div class="">
                    <div class="border-0">
                        <h3 class="mb-0 text-muted">Pre-Data Definition</h3>
                        <hr class="my-3">
                    </div>
                    <div class="form-container" id="predataFormContainer">

                        <form method="POST" :action="url" id="predataMainForm" class="mx-col">


                            <input type="hidden" name="_csrf_token" value="<?php echo $token; ?>">

                            <div class="row">

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-email">Data Category</label>
                                        <div class="input-group">
                                            <select class="form-control" id="dataCategorySelect" name="cat_id" v-model.number="predata_category_select" @change="getPredatasForCategory">
                                                <?php
                                                if (count($predatacategories) > 0) {
                                                    echo "<option disabled value=''>Choose...</option>";
                                                    foreach ($predatacategories as $category) {
                                                        ?>
                                                        <option value="<?php echo $category->CategoryID ?>"><?php echo $category->DataCategory; ?></option>
                                                    <?php
                                                    }
                                                } else {
                                                    echo " <option>---- No data available --</option>";
                                                }
                                                ?>
                                            </select>
                                            <div class="input-group-append">
                                                <button id='edittoggler' class="btn btn-primary" :disabled="disabledButton" @click="">
                                                    <input type="checkbox" id="" v-model:checked="checked" v-if="!disabledButton">
                                                    <span>Edit</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>



                                <div class="col-md-12" id="predata_edit" v-if="show_edit">
                                    <div class="form-group">
                                        <label class="form-control-label">Choose Predata</label>
                                        <select class="form-control" id="predatas" v-model.number="predata_select" @change="getPredata" name="predata_id">
                                            <option value=''>Choose</option>
                                        </select>
                                    </div>
                                </div>



                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-control-label">Predata</label>
                                        <input type="text" class="form-control " name="predata" id="predata" v-model='predata'>
                                    </div>

                                </div>

                                <div class="col-md-12 mb-3">
                                    <div class="form-group ">
                                        <button type="submit" name="submit" class="btn btn-primary">Save <i class="fa fa-save"></i></button>
                                        <button class="btn btn-danger" @click="deleteUrl">Delete <i class="fa fa-trash"></i>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="alert alert-danger" id="predata_ajax_error" v-if="hasError">
                                        {{error}}
                                    </div>

                                </div>




                            </div>


                        </form>

                    </div>
                </div>



            </div>

            <div class="col-md-6 offset-md-1 mb-3">
                <div class="" id="facultyColourFormContainer">
                    <div class="border-0">
                        <h3 class="mb-0 text-muted">Faculty Colour</h3>
                        <hr class="my-3">
                    </div>

                    <form class="mx-col" method="POST" :action="url">

                        <input type="hidden" name="_csrf_token" value="<?php echo $token; ?>">
                        <div class="row">

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="form-control-label" for="input-email">Faculty Selection</label>
                                    <div class="input-group">
                                        <select id="select_colour" v-model.number="select_colour" name="colour_id" class="form-control" @change="getColour">
                                            <?php
                                            if (count($colours) > 0) {
                                                echo "<option  value=''>Choose...</option>";
                                                foreach ($colours as $colour) {
                                                    ?>
                                                    <option value="<?php echo $colour->ColourID ?>"><?php echo $colour->ColourName; ?></option>
                                                <?php
                                                }
                                            } else {
                                                echo " <option value=''>---- No data available --</option>";
                                            }
                                            ?>
                                        </select>
                                        <div class="input-group-append">
                                            <button id='edittoggler' class="btn btn-primary">
                                                <input type="checkbox" id="" v-model:checked="checked">
                                                <span>Edit</span>
                                            </button>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="form-control-label" for="input-email">Colour</label>
                                    <input type="text" class="form-control" name="colour_name" v-model="faculty_colour">
                                </div>
                            </div>

                            <div class="col-md-12 mb-3">
                                <button type="submit" class="btn btn-primary">Save <i class="fa fa-save"></i></button>
                                <button class="btn btn-danger" @click="deleteUrl">Delete <i class="fa fa-trash"></i></button>
                            </div>
                            <div class="col-md-12 mb-3">
                                <div class="alert alert-dismissible fade show d-none" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            </div>
                        </div>

                    </form>

                </div>
            </div>

        </div>
        <!-- Footer -->
        <footer class="footer">
            <div class="row align-items-center justify-content-xl-between">
                <div class="col-xl-6">
                    <div class="copyright text-center text-xl-left text-muted">
                        &copy; 2019 <a href="#" class="font-weight-bold ml-1" target="_blank">Softech Global Associates.</a> All Rights Reserved.
                    </div>
                </div>
                <div class="col-xl-6">
                    <ul class="nav nav-footer justify-content-center justify-content-xl-end">
                        <div class="copyright text-center text-xl-right text-muted">
                            <a href="#">Policy</a>
                        </div>
                    </ul>
                </div>
            </div>
        </footer>
    </div>
</div>
<!-- end of content section of this page -->

<!-- important scrpts goes here -->
<?php
include BASE_URL . '/setup/scripts.php';
?>
<!-- important script ends here -->


<script>
    let predataForm = new Vue({
        el: '#predataFormContainer',
        data: {
            disabledButton: true,
            url: 'save/predata',
            predata_category_select: '',
            show_edit: false,
            checked: false,
            predata_select: '',
            predata: '',
            hasError: false,
            error: '',
        },

        watch: {
            predata_category_select: function(val) {
                isNaN(val) ? this.disabledButton = true : this.disabledButton = false;
            },
            checked: function(val) {
                if (!this.disabledButton) {
                    this.show_edit = !this.show_edit;
                    this.show_edit ? this.url = 'update/predata' : this.url = 'save/predata';
                    this.getPredatasForCategory()
                }
            },
        },
        methods: {

            getPredata() {
                $vm = this;
                axios.get('api/get/predata', {
                    params: {
                        predata_id: this.predata_select
                    }
                }).then(function(response) {
                    $vm.predata = response.data[0].Data;
                }).catch(function(error) {
                    $vm.hasError = true;
                    $vm.error = error.messageText;
                })
            },

            getPredatasForCategory() {

                if (this.show_edit) {
                    $vm = this;
                    // if(this.predata_select == ''){
                    //     this.hasError  = true;
                    //     this.error = 'Cannot get predatas for empty string';
                    //     this.predata = ''
                    //     return;
                    // }
                    axios.get('api/category/predatas', {
                        params: {
                            category_id: this.predata_category_select
                        }
                    }).then(function(response) {

                        //reset predatas to empty
                        $('#predatas').text('');
                        // reset the predata value
                        $vm.predata = ''

                        if (response.data.length > 0) {

                            $('#predatas').append("<option value='' >Choose Predata...</option>");

                            $.each(response.data, function($key, $obj) {

                                $('#predatas').append('<option value=' + $obj.DataID + '>' + $obj.Data + '</option>');
                            });

                        } else {

                            $('#predatas').append("<option value='' > No data found </option>");
                        }


                    }).catch(function(error) {

                        console.log(error)
                    });
                }

            },
            deleteUrl() {
                this.url = 'delete/predata';
                console.log('am deleteing this data')
                return true;
            }
        }

    });

    let colourForm = new Vue({
        el: '#facultyColourFormContainer',
        data: {
            error: '',
            hasError: false,
            isEdit: false,
            url: 'save/faculty/colour',
            select_colour: '',
            faculty_colour: '',
            checked: false,
        },
        watch: {
            checked: function(val) {
                if (val) {
                    this.url = 'update/faculty/colour';

                    if (!isNaN(this.select_colour)) {
                        this.getColour();
                    }
                } else {
                    this.url = 'save/faculty/colour'
                    this.faculty_colour = ''
                }

            },

        },
        methods: {
            deleteUrl() {

                this.url = 'delete/faculty/colour'
            },
            getColour() {
                if (!this.checked) {
                    return;
                }
                $vm = this;
                axios.get('api/faculty/colour', {
                    params: {
                        colour_id: this.select_colour
                    }
                }).then(function(response) {
                    console.log(response);
                    $vm.faculty_colour = response.data[0].ColourName;
                }).catch(function(error) {
                    console.log('error')
                    this.hasError = true;
                    this.error = error.messageText;
                });
            }
        }
    });
</script>
<!-- page script content goes here -->
<script type="text/javacript"></script>
<!-- page script contents ends here -->
<?php
include BASE_URL . '/setup/footer.php';
?>