<?php

use Lib\Connection;
use Model\State;

$page_title = 'Setup State & LGA';
$token = generateCSRFToken();
include BASE_URL . '/setup/header.php';

// Database Operations of getting data
$states = State::all();

?>


<div class="main-content">
    <!-- Top navbar -->
    <nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
        <div class="container-fluid">
            <!-- Brand -->
            <!-- <a class="h4 mb-0 text-secondary d-none_ d-lg-inline-block" href="#">Setup</a>
        <a class="h4 mb-0 text-white d-none_ d-lg-inline-block" href="#"> | Pre-Defined Data</a>
         -->
            <ul class="font-weight-300 nav">
                <li class="nav-item">
                    <a href="#" class="nav-link text-white-50 d-lg-inline-block">Setup Management</a>
                </li> <span class="text-white-50 h2">|</span>
                <li class="nav-item">
                    <a href="#" class="nav-link text-white-50 d-lg-inline-block" style="font-size: 1.35rem;line-height: 1;">States & Local Goverments</a>
                </li>
            </ul>

            <!-- Form -->
            <!--  <form class="navbar-search navbar-search-dark form-inline mr-3 d-none d-md-flex ml-lg-auto">
          <div class="form-group mb-0">
            <div class="input-group input-group-alternative">
              <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-search"></i></span>
              </div>
              <input class="form-control" placeholder="Search" type="text">
            </div>
          </div>
        </form> -->
            <!-- User -->
            <ul class="navbar-nav align-items-center d-none d-md-flex">
                <li class="nav-item dropdown">
                    <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <div class="media align-items-center">
                            <span class="avatar avatar-sm rounded-circle">
                                <img alt="Image placeholder" src="<?php echo resource('public/assets/img/theme/team-4-800x800.jpg');?>">
                            </span>
                            <div class="media-body ml-2 d-none d-lg-block">
                                <span class="mb-0 text-sm  font-weight-bold">Jonathan Doe <i class="fa fa-caret-down"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
                        <div class=" dropdown-header noti-title">
                            <h6 class="text-overflow m-0">Welcome!</h6>
                        </div>
                        <a href="../pages/profile.html" class="dropdown-item">
                            <i class="ni ni-single-02"></i>
                            <span>My profile</span>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="#!" class="dropdown-item">
                            <i class="ni ni-user-run"></i>
                            <span>Logout</span>
                        </a>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
    <!-- Header -->
    <div class="header bg-gradient-primary py-5">
        <div class="container-fluid">
            <div class="header-body">
                <!-- Card stats -->

            </div>
        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt-md-4">
    <div class="row">
            <div class="col-md-6  offset-md-1 mb-3">
                <?php
                // check if there is a message in the incoming request
                if (isset($request->session)) {

                    // check if an error exists in the incoming request
                    if (isset($request->session['error'])) {
                        $error = $request->session['error'];
                        echo "<div class='alert alert-danger'>$error</div>";
                    }

                    // check if there is a success msg in the incoming request
                    if (isset($request->session['success'])) {
                        $success = $request->session['success'];
                        echo "<div class='alert alert-success'>$success</div>";
                    }
                }
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 offset-md-1 mb-3">
                <div class="" id="stateFormContainer">
                    <div class="border-0">
                        <h3 class="mb-0 text-muted">State Definition</h3>
                        <hr class="my-3">
                    </div>

                    <form class="mx-col" method="POST" :action="url">

                        <input type="hidden" name="_csrf_token" value="<?php echo $token; ?>">
                        <div class="row">

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="form-control-label" for="input-email">Select State</label>
                                    <div class="input-group">
                                        <select v-model.number="select_state" name="state_id" class="form-control" @change="getState">
                                            <?php
                                            if (count($states) > 0) {
                                                echo "<option  value=''>Choose...</option>";
                                                foreach ($states as $state) {
                                                    ?>
                                                    <option value="<?php echo $state->StateID ?>"><?php echo $state->State; ?></option>
                                                <?php
                                                }
                                            } else {
                                                echo " <option value='' >---- No data available --</option>";
                                            }
                                            ?>
                                        </select>
                                        <div class="input-group-append">
                                            <button id='edittoggler' class="btn btn-primary">
                                                <input type="checkbox" id="" v-model:checked="checked">
                                                <span>Edit</span>
                                            </button>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="form-control-label" for="input-email">State</label>
                                    <input type="text" class="form-control" name="state" v-model="state">
                                </div>
                            </div>

                            <div class="col-md-12 mb-3">
                                <button type="submit" class="btn btn-primary">Save <i class="fa fa-save"></i></button>
                                <button class="btn btn-danger" @click="deleteUrl">Delete <i class="fa fa-trash"></i></button>
                            </div>
                            <div class="col-md-12 mb-3">
                                <div class="alert alert-dismissible fade show d-none" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            </div>
                        </div>

                    </form>

                </div>
            </div>

            <div class="col-md-6 offset-md-1 mb-3">
                <div class="">
                    <div class="border-0">
                        <h3 class="mb-0 text-muted">LGA Definition</h3>
                        <hr class="my-3">
                    </div>
                    <div class="form-container" id="lgaFormContainer">

                        <form method="POST" :action="url" class="mx-col">


                            <input type="hidden" name="_csrf_token" value="<?php echo $token; ?>">

                            <div class="row">

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-email">Select State</label>
                                        <div class="input-group">
                                            <select class="form-control" name="state_id" v-model.number="state_select" @change="getStateLGA">
                                                <?php
                                                if (count($states) > 0) {
                                                    echo "<option  value=''>Choose...</option>";
                                                    foreach ($states as $state) {
                                                        ?>
                                                        <option value="<?php echo $state->StateID ?>"><?php echo $state->State; ?></option>
                                                    <?php
                                                    }
                                                } else {
                                                    echo " <option value=''>---- No data available --</option>";
                                                }
                                                ?>
                                            </select>
                                            <div class="input-group-append">
                                                <button id='edittoggler' class="btn btn-primary" :disabled="disabledButton" @click="">
                                                    <input type="checkbox" id="" v-model:checked="checked" v-if="!disabledButton">
                                                    <span>Edit</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>



                                <div class="col-md-12" id="predata_edit" v-if="show_edit">
                                    <div class="form-group">
                                        <label class="form-control-label">Choose LGA</label>
                                        <select class="form-control"  v-model.number="lga_select" @change="getLGA" name="lga_id" id="lgas">
                                            <option value='' disabled>Choose</option>
                                        </select>
                                    </div>
                                </div>



                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-control-label">LGA</label>
                                        <input type="text" class="form-control " name="lga" id="lga" v-model='lga'>
                                    </div>

                                </div>

                                <div class="col-md-12 mb-3">
                                    <div class="form-group ">
                                        <button type="submit" name="submit" class="btn btn-primary">Save <i class="fa fa-save"></i></button>
                                        <button class="btn btn-danger" @click="deleteUrl">Delete <i class="fa fa-trash"></i>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="alert alert-danger" id="predata_ajax_error" v-if="hasError">
                                        {{error}}
                                    </div>

                                </div>




                            </div>


                        </form>

                    </div>
                </div>



            </div>



        </div>
        <!-- Footer -->
        <footer class="footer">
            <div class="row align-items-center justify-content-xl-between">
                <div class="col-xl-6">
                    <div class="copyright text-center text-xl-left text-muted">
                        &copy; 2019 <a href="#" class="font-weight-bold ml-1" target="_blank">Softech Global Associates.</a> All Rights Reserved.
                    </div>
                </div>
                <div class="col-xl-6">
                    <ul class="nav nav-footer justify-content-center justify-content-xl-end">
                        <div class="copyright text-center text-xl-right text-muted">
                            <a href="#">Policy</a>
                        </div>
                    </ul>
                </div>
            </div>
        </footer>
    </div>
</div>

<!-- important scrpts goes here -->
<?php
include BASE_URL . '/setup/scripts.php';
?>
<!-- important script ends here -->


<script>
    let stateForm = new Vue({
        el: '#stateFormContainer',
        data: {
            error: '',
            hasError: false,
            isEdit: false,
            url: 'save/state',
            select_state: '',
            state: '',
            checked: false,
        },
        watch: {
            checked: function(val) {
                if (val) {
                    this.url = 'update/state';

                    if (!isNaN(this.select_state)) {
                        this.getState();
                    }
                } else {
                    this.url = 'save/state'
                    this.state = '';
                }
            },

        },
        methods: {
            deleteUrl() {
                this.url = 'delete/state'
            },
            getState() {
                if (!this.checked) {
                    return;
                }
                $vm = this;
                axios.get('api/get/state', {
                    params: {
                        state_id: this.select_state
                    }
                }).then(function(response) {
                    console.log(response);
                    $vm.state = response.data[0].State;
                }).catch(function(error) {
                    console.log('error')
                    this.hasError = true;
                    this.error = error.messageText;
                });
            }
        }
    });
    let lgaForm = new Vue({
        el: '#lgaFormContainer',
        data: {
            disabledButton: true,
            url: 'save/lga',
            state_select: '',
            show_edit: false,
            checked: false,
            lga_select: '',
            lga: '',
            hasError: false,
            error: '',
        },

        watch: {
            state_select: function(val) {
                isNaN(val) ? this.disabledButton = true : this.disabledButton = false;
            },
            checked: function(val) {
                if (!this.disabledButton) {
                    this.show_edit = !this.show_edit;
                    this.show_edit ? this.url = 'update/lga' : this.url = 'save/lga';
                    this.getStateLGA()
                }
                if(!val){
                    this.lga = ''
                }
            },
        },
        methods: {

            getLGA() {
                $vm = this;
                axios.get('api/get/lga', {
                    params: {
                        lga_id: this.lga_select
                    }
                }).then(function(response) {
                    $vm.lga = response.data[0].LGA;
                }).catch(function(error) {
                    $vm.hasError = true;
                    $vm.error = error.messageText;
                })
            },

            getStateLGA() {

                if (this.show_edit) {
                    $vm = this;
                    // if(this.lga_select == ''){
                    //     this.hasError  = true;
                    //     this.error = 'Cannot get lgas for empty string';
                    //     this.lga = ''
                    //     return;
                    // }
                    axios.get('api/get/state/lgas', {
                        params: {
                            state_id: this.state_select
                        }
                    }).then(function(response) {

                        //reset lgas to empty
                        $('#lgas').text('');
                        // reset the lga value
                        $vm.lga = ''

                        if (response.data.length > 0) {

                            $('#lgas').append("<option value='' >Choose lga...</option>");

                            $.each(response.data, function($key, $obj) {

                                $('#lgas').append('<option value=' + $obj.LGAID + '>' + $obj.LGA + '</option>');
                            });

                        } else {

                            $('#lgas').append("<option value=''  > No data found </option>");
                        }


                    }).catch(function(error) {

                        console.log(error)
                    });
                }

            },
            deleteUrl() {
                this.url = 'delete/lga';
                console.log('am deleteing this data')
                return true;
            }
        }

    });
</script>
<!-- page script content goes here -->
<script type="text/javacript"></script>
<!-- page script contents ends here -->
<?php
include BASE_URL . '/setup/footer.php';
?>