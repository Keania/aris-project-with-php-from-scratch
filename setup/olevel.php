<?php

use Lib\Connection;
use Model\OlevelSubject;


$page_title = 'Setup Olevel Subject';
$token = generateCSRFToken();
include BASE_URL . '/setup/header.php';

// Database Operations of getting data
$olevelsubjects = OlevelSubject::all();
?>


<!-- content section of this page goes here -->
<div class="main-content">
    <!-- Top navbar -->
    <nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
        <div class="container-fluid">
            <!-- Brand -->
            <!-- <a class="h4 mb-0 text-secondary d-none_ d-lg-inline-block" href="#">Setup</a>
        <a class="h4 mb-0 text-white d-none_ d-lg-inline-block" href="#"> | Pre-Defined Data</a>
         -->
            <ul class="font-weight-300 nav">
                <li class="nav-item">
                    <a href="#" class="nav-link text-white-50 d-lg-inline-block">Setup Management</a>
                </li> <span class="text-white-50 h2">|</span>
                <li class="nav-item">
                    <a href="#" class="nav-link text-white-50 d-lg-inline-block" style="font-size: 1.35rem;line-height: 1;">Olevel Subject</a>
                </li>
            </ul>

            <!-- Form -->
            <!--  <form class="navbar-search navbar-search-dark form-inline mr-3 d-none d-md-flex ml-lg-auto">
          <div class="form-group mb-0">
            <div class="input-group input-group-alternative">
              <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-search"></i></span>
              </div>
              <input class="form-control" placeholder="Search" type="text">
            </div>
          </div>
        </form> -->
            <!-- User -->
            <ul class="navbar-nav align-items-center d-none d-md-flex">
                <li class="nav-item dropdown">
                    <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <div class="media align-items-center">
                            <span class="avatar avatar-sm rounded-circle">
                                <img alt="Image placeholder" src="<?php echo resource('public/assets/img/theme/team-4-800x800.jpg');?>">
                            </span>
                            <div class="media-body ml-2 d-none d-lg-block">
                                <span class="mb-0 text-sm  font-weight-bold">Jonathan Doe <i class="fa fa-caret-down"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
                        <div class=" dropdown-header noti-title">
                            <h6 class="text-overflow m-0">Welcome!</h6>
                        </div>
                        <a href="../pages/profile.html" class="dropdown-item">
                            <i class="ni ni-single-02"></i>
                            <span>My profile</span>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="#!" class="dropdown-item">
                            <i class="ni ni-user-run"></i>
                            <span>Logout</span>
                        </a>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
    <!-- Header -->
    <div class="header bg-gradient-primary py-5">
        <div class="container-fluid">
            <div class="header-body">
                <!-- Card stats -->

            </div>
        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt-md-4">
    <div class="row">
            <div class="col-md-6  offset-md-1 mb-3">
                <?php
                // check if there is a message in the incoming request
                if (isset($request->session)) {

                    // check if an error exists in the incoming request
                    if (isset($request->session['error'])) {
                        $error = $request->session['error'];
                        echo "<div class='alert alert-danger'>$error</div>";
                    }

                    // check if there is a success msg in the incoming request
                    if (isset($request->session['success'])) {
                        $success = $request->session['success'];
                        echo "<div class='alert alert-success'>$success</div>";
                    }
                }
                ?>
            </div>
        </div>
        <div class="row">

            <div class="col-md-6 offset-md-1 mb-3">
                <div class="" id="olevelSubjectFormContainer">
                    <div class="border-0">
                        <h3 class="mb-0 text-muted">New Subject</h3>
                        <hr class="my-3">
                    </div>

                    <form class="mx-col" method="POST" :action="url">

                        <input type="hidden" name="_csrf_token" value="<?php echo $token; ?>">
                        <div class="row">

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="form-control-label" for="input-email">Subject</label>
                                    <div class="input-group">
                                        <select id="select_colour" class="form-control" name="subject_id" v-model.number="olevelsubject_id" @change="getOlevelSubject">
                                            <?php
                                            if (count($olevelsubjects) > 0) {
                                                echo "<option  value=''>Choose...</option>";
                                                foreach ($olevelsubjects as $olevelsubject) {
                                                    ?>
                                                    <option value="<?php echo $olevelsubject->SubjectID ?>"><?php echo $olevelsubject->Subject; ?></option>
                                                <?php
                                                }
                                            } else {
                                                echo " <option value=''>---- No data available --</option>";
                                            }
                                            ?>
                                        </select>
                                        <div class="input-group-append">
                                            <button id='edittoggler' class="btn btn-primary">
                                                <input type="checkbox" id="" v-model:checked="checked">
                                                <span>Edit</span>
                                            </button>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="form-control-label" for="input-email">Subject</label>
                                    <input type="text" class="form-control"name="subject" id="olevelsubject" v-model='olevelsubject'>
                                </div>
                            </div>

                            <div class="col-md-12 mb-3">
                                <button type="submit" class="btn btn-primary">Save <i class="fa fa-save"></i></button>
                                <button class="btn btn-danger" @click="deleteUrl">Delete <i class="fa fa-trash"></i></button>
                            </div>
                            <div class="col-md-12 mb-3">
                                <div class="alert alert-dismissible fade show d-none" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            </div>
                        </div>

                    </form>

                </div>
            </div>
            <div class="col-md-8" style="height:20px;"></div>
            <div class="col-md-8 offset-md-1">
            <table class=" table table-stripped table-bordered" id="data_table">
                <thead>
                    <tr>
                        <th>Subject</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        foreach($olevelsubjects as $subject){
                            echo "<tr><td>$subject->Subject</td></tr>";
                        }
                    ?>
                </tbody>
            </table>
            </div>

        </div>
        <!-- Footer -->
        <footer class="footer">
            <div class="row align-items-center justify-content-xl-between">
                <div class="col-xl-6">
                    <div class="copyright text-center text-xl-left text-muted">
                        &copy; 2019 <a href="#" class="font-weight-bold ml-1" target="_blank">Softech Global Associates.</a> All Rights Reserved.
                    </div>
                </div>
                <div class="col-xl-6">
                    <ul class="nav nav-footer justify-content-center justify-content-xl-end">
                        <div class="copyright text-center text-xl-right text-muted">
                            <a href="#">Policy</a>
                        </div>
                    </ul>
                </div>
            </div>
        </footer>
    </div>
</div>
<!-- end of content section of this page -->

<!-- important scrpts goes here -->
<?php
include BASE_URL . '/setup/scripts.php';
?>
<!-- important script ends here -->


<script>
    let olevelsubjectForm = new Vue({
        el: '#olevelSubjectFormContainer',
        data: {
            url: 'save/olevelsubject',
            checked: false,
            olevelsubject_id: '',
            olevelsubject: '',
            hasError: false,
            error: '',
        },

        watch: {
            checked: function(val) {
                if (val) {
                    this.url = 'update/olevelsubject';

                    if (!isNaN(this.olevelsubject_id)) {
                        this.getOlevelSubject()
                    }
                } else {
                    this.url = 'save/olevelsubject'
                    this.olevelsubject = ''
                }

            },
        },
        methods: {

            getOlevelSubject() {
                if(!this.checked){
                    return;
                }
                $vm = this;
                axios.get('api/olevelsubject', {
                    params: {
                        subject_id: this.olevelsubject_id
                    }
                }).then(function(response) {

                    //reset olevelsubjects to empty
                    $('#olevelsubjects').text('');
                    // reset the olevelsubject value
                    $vm.olevelsubject = response.data[0].Subject;



                }).catch(function(error) {

                    console.log(error)
                });


            },
            deleteUrl() {
                this.url = 'delete/olevelsubject';
                console.log('am deleteing this data')
                return true;
            }
        }

    });
</script>
<!-- page script content goes here -->
<script type="text/javacript"></script>
<!-- page script contents ends here -->
<?php
include BASE_URL . '/setup/footer.php';
?>