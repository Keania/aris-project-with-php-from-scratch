<?php

use Lib\Connection;
use Model\PredataCategory;
use Model\ProgramType;
use Model\DegreeType;


$page_title = 'Setup Degree and Program Types ';
$token = generateCSRFToken();
include BASE_URL . '/setup/header.php';

// Database Operations of getting data
$programtypes = ProgramType::all();
$degreetypes = DegreeType::all();

?>


<!-- content section of this page goes here -->

<!-- end of content section of this page -->
<div class="main-content">
    <!-- Top navbar -->
    <nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
        <div class="container-fluid">
            <!-- Brand -->
            <!-- <a class="h4 mb-0 text-secondary d-none_ d-lg-inline-block" href="#">Setup</a>
        <a class="h4 mb-0 text-white d-none_ d-lg-inline-block" href="#"> | Pre-Defined Data</a>
         -->
            <ul class="font-weight-300 nav">
                <li class="nav-item">
                    <a href="#" class="nav-link text-white-50 d-lg-inline-block">Setup Management</a>
                </li> <span class="text-white-50 h2">|</span>
                <li class="nav-item">
                    <a href="#" class="nav-link text-white-50 d-lg-inline-block" style="font-size: 1.35rem;line-height: 1;">Program Type & Degree Type</a>
                </li>
            </ul>

            <!-- Form -->
            <!--  <form class="navbar-search navbar-search-dark form-inline mr-3 d-none d-md-flex ml-lg-auto">
          <div class="form-group mb-0">
            <div class="input-group input-group-alternative">
              <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-search"></i></span>
              </div>
              <input class="form-control" placeholder="Search" type="text">
            </div>
          </div>
        </form> -->
            <!-- User -->
            <ul class="navbar-nav align-items-center d-none d-md-flex">
                <li class="nav-item dropdown">
                    <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <div class="media align-items-center">
                            <span class="avatar avatar-sm rounded-circle">
                                <img alt="Image placeholder" src="<?php echo resource('public/assets/img/theme/team-4-800x800.jpg');?>">
                            </span>
                            <div class="media-body ml-2 d-none d-lg-block">
                                <span class="mb-0 text-sm  font-weight-bold">Jonathan Doe <i class="fa fa-caret-down"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
                        <div class=" dropdown-header noti-title">
                            <h6 class="text-overflow m-0">Welcome!</h6>
                        </div>
                        <a href="../pages/profile.html" class="dropdown-item">
                            <i class="ni ni-single-02"></i>
                            <span>My profile</span>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="#!" class="dropdown-item">
                            <i class="ni ni-user-run"></i>
                            <span>Logout</span>
                        </a>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
    <!-- Header -->
    <div class="header bg-gradient-primary py-5">
        <div class="container-fluid">
            <div class="header-body">
                <!-- Card stats -->

            </div>
        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt-md-4">
        <div class="row">
            <div class="col-md-6  offset-md-1 mb-3">
                <?php
                // check if there is a message in the incoming request
                if (isset($request->session)) {

                    // check if an error exists in the incoming request
                    if (isset($request->session['error'])) {
                        $error = $request->session['error'];
                        echo "<div class='alert alert-danger'>$error</div>";
                    }

                    // check if there is a success msg in the incoming request
                    if (isset($request->session['success'])) {
                        $success = $request->session['success'];
                        echo "<div class='alert alert-success'>$success</div>";
                    }
                }
                ?>
            </div>
        </div>
        <div class="row">

            <div class="col-md-6 offset-md-1 mb-3">
                <div class="">
                    <div class="border-0">
                        <h3 class="mb-0 text-muted">Degree Type Definition</h3>
                        <hr class="my-3">
                    </div>
                    <div class="form-container" id="degreeTypeFormContainer">

                        <form method="POST" :action="url" class="mx-col">


                            <input type="hidden" name="_csrf_token" value="<?php echo $token; ?>">

                            <div class="row">

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-email">Select Degree Type</label>
                                        <div class="input-group">
                                            <select class="form-control" v-model.number="select_degreetype" name="degreetype_id" class="form-control" @change="getDegreeType">
                                                <?php
                                                if (count($degreetypes) > 0) {
                                                    echo "<option  value=''>Choose...</option>";
                                                    foreach ($degreetypes as $degreetype) {
                                                        ?>
                                                        <option value="<?php echo $degreetype->DegreeTypeID ?>"><?php echo $degreetype->DegreeType; ?></option>
                                                    <?php
                                                    }
                                                } else {
                                                    echo " <option value=''>---- No data available --</option>";
                                                }
                                                ?>
                                            </select>
                                            <div class="input-group-append">
                                                <button id='edittoggler' class="btn btn-primary" :disabled="disabledButton" @click="">
                                                    <input type="checkbox" id="" v-model:checked="checked" v-if="!disabledButton">
                                                    <span>Edit</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>



                                <!-- <div class="col-md-12" id="predata_edit" v-if="show_edit">
                                    <div class="form-group">
                                        <label class="form-control-label">Choose Predata</label>
                                        <select class="form-control" id="degreeawards" v-model.number="degreeaward_id" @change="getDegreeAward" name="degreeaward_id">
                                            <option value='' disabled>Choose</option>
                                        </select>
                                    </div>
                                </div> -->



                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-control-label col-md-4">Degree Type</label>
                                        <input type="text" class="form-control" name="degreetype" v-model="degreetype">
                                    </div>

                                </div>


                                <div class="col-md-12 mb-3">
                                    <div class="form-group ">
                                        <button type="submit" name="submit" class="btn btn-primary">Save <i class="fa fa-save"></i></button>
                                        <button class="btn btn-danger" @click="deleteUrl">Delete <i class="fa fa-trash"></i>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="alert alert-danger" id="predata_ajax_error" v-if="hasError">
                                        {{error}}
                                    </div>

                                </div>




                            </div>


                        </form>

                    </div>
                </div>



            </div>

            <div class="col-md-6 offset-md-1 mb-3">
                <div class="">
                    <div class="border-0">
                        <h3 class="mb-0 text-muted">Program Type Definition</h3>
                        <hr class="my-3">
                    </div>
                    <div class="form-container" id="programTypeFormContainer">

                        <form method="POST" :action="url" class="mx-col">


                            <input type="hidden" name="_csrf_token" value="<?php echo $token; ?>">

                            <div class="row">

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-email">Select Program Type</label>
                                        <div class="input-group">
                                            <select v-model.number="select_programtype" name="programtype_id" class="form-control" @change="getProgramType">
                                                <?php
                                                if (count($programtypes) > 0) {
                                                    echo "<option  value=''>Choose...</option>";
                                                    foreach ($programtypes as $programtype) {
                                                        ?>
                                                        <option value="<?php echo $programtype->ProgramTypeID ?>"><?php echo $programtype->ProgramType; ?></option>
                                                    <?php
                                                    }
                                                } else {
                                                    echo " <option value=''>---- No data available --</option>";
                                                }
                                                ?>
                                            </select>
                                            <div class="input-group-append">
                                                <button id='edittoggler' class="btn btn-primary" :disabled="disabledButton" @click="">
                                                    <input type="checkbox" id="" v-model:checked="checked" v-if="!disabledButton">
                                                    <span>Edit</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>



                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-control-label">Program Type</label>
                                        <input type="text" class="form-control" name="programtype" v-model="programtype">
                                    </div>

                                </div>

                                <div class="col-md-12">
                                    <div class="form-group ">
                                        <label class="form-control-label">Program Type Code</label>
                                        <input type="text" class="form-control" name="programtypecode" v-model="programtypecode">
                                    </div>
                                </div>


                                <div class="col-md-12 mb-3">
                                    <div class="form-group ">
                                        <button type="submit" name="submit" class="btn btn-primary">Save <i class="fa fa-save"></i></button>
                                        <button class="btn btn-danger" @click="deleteUrl">Delete <i class="fa fa-trash"></i>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="alert alert-danger" id="predata_ajax_error" v-show="hasError">
                                        {{error}}
                                    </div>

                                </div>




                            </div>


                        </form>

                    </div>
                </div>



            </div>


        </div>
        <!-- Footer -->
        <footer class="footer">
            <div class="row align-items-center justify-content-xl-between">
                <div class="col-xl-6">
                    <div class="copyright text-center text-xl-left text-muted">
                        &copy; 2019 <a href="#" class="font-weight-bold ml-1" target="_blank">Softech Global Associates.</a> All Rights Reserved.
                    </div>
                </div>
                <div class="col-xl-6">
                    <ul class="nav nav-footer justify-content-center justify-content-xl-end">
                        <div class="copyright text-center text-xl-right text-muted">
                            <a href="#">Policy</a>
                        </div>
                    </ul>
                </div>
            </div>
        </footer>
    </div>
</div>
<!-- important scrpts goes here -->
<?php
include BASE_URL . '/setup/scripts.php';
?>
<!-- important script ends here -->


<script>
    let degreeTypeForm = new Vue({
        el: '#degreeTypeFormContainer',
        data: {
            error: '',
            hasError: false,
            isEdit: false,
            url: 'save/degreetype',
            select_degreetype: '',
            degreetype: '',
            checked: false,
        },
        watch: {
            checked: function(val) {
                if (val) {
                    this.url = 'update/degreetype';

                    if (!isNaN(this.select_degreetype)) {
                        this.getDegreeType();
                    }
                } else {
                    this.url = 'save/degreetype'
                    this.degreetype = ''
                }

            },

        },
        methods: {
            deleteUrl() {
                this.url = 'delete/degreetype'
            },
            getDegreeType() {
                if (!this.checked) {
                    return;
                }
                $vm = this;
                axios.get('api/degreetype', {
                    params: {
                        degreetype_id: this.select_degreetype
                    }
                }).then(function(response) {
                    console.log(response);
                    $vm.degreetype = response.data[0].DegreeType;
                }).catch(function(error) {
                    console.log('error')
                    this.hasError = true;
                    this.error = error.messageText;
                });
            }
        }
    });

    let programTypeForm = new Vue({
        el: '#programTypeFormContainer',
        data: {
            error: '',
            hasError: false,
            isEdit: false,
            url: 'save/programtype',
            select_programtype: '',
            programtype: '',
            programtypecode: '',
            checked: false,
        },
        watch: {
            checked: function(val) {
                if (val) {
                    this.url = 'update/programtype';

                    if (!isNaN(this.select_programtype)) {
                        this.getProgramType();
                    }
                } else {
                    this.url = 'save/programtype'
                    this.programtype = ''
                    this.programtypecode = ''
                }
            },

        },
        methods: {
            deleteUrl() {
                this.url = 'delete/programtype'
            },
            getProgramType() {
                if (!this.checked) {
                    return;
                }
                $vm = this;
                axios.get('api/programtype', {
                    params: {
                        programtype_id: this.select_programtype
                    }
                }).then(function(response) {
                    console.log(response);
                    $vm.programtype = response.data[0].ProgramType;
                    $vm.programtypecode = response.data[0].ProgramTypeCode
                }).catch(function(error) {
                    console.log('error')
                    this.hasError = true;
                    this.error = error.messageText;
                });
            }
        }
    });
</script>
<!-- page script content goes here -->
<script type="text/javacript"></script>
<!-- page script contents ends here -->
<?php
include BASE_URL . '/setup/footer.php';
?>