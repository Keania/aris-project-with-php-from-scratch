<?php

use Lib\Connection;
use Model\DegreeType;
use Model\DegreeAward;


$page_title = 'Setup Degree Award';
$token = generateCSRFToken();
include BASE_URL . '/setup/header.php';

// Database Operations of getting data
$degreetypes = DegreeType::all();
$degreeawards = DegreeAward::all();

?>


<!-- content section of this page goes here -->
<div class="main-content">
    <!-- Top navbar -->
    <nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
        <div class="container-fluid">
            <!-- Brand -->
            <!-- <a class="h4 mb-0 text-secondary d-none_ d-lg-inline-block" href="#">Setup</a>
        <a class="h4 mb-0 text-white d-none_ d-lg-inline-block" href="#"> | Pre-Defined Data</a>
         -->
            <ul class="font-weight-300 nav">
                <li class="nav-item">
                    <a href="#" class="nav-link text-white-50 d-lg-inline-block">Setup Management</a>
                </li> <span class="text-white-50 h2">|</span>
                <li class="nav-item">
                    <a href="#" class="nav-link text-white-50 d-lg-inline-block" style="font-size: 1.35rem;line-height: 1;">Degree Award</a>
                </li>
            </ul>

            <!-- Form -->
            <!--  <form class="navbar-search navbar-search-dark form-inline mr-3 d-none d-md-flex ml-lg-auto">
          <div class="form-group mb-0">
            <div class="input-group input-group-alternative">
              <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-search"></i></span>
              </div>
              <input class="form-control" placeholder="Search" type="text">
            </div>
          </div>
        </form> -->
            <!-- User -->
            <ul class="navbar-nav align-items-center d-none d-md-flex">
                <li class="nav-item dropdown">
                    <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <div class="media align-items-center">
                            <span class="avatar avatar-sm rounded-circle">
                                <img alt="Image placeholder" src="<?php echo resource('public/assets/img/theme/team-4-800x800.jpg');?>">
                            </span>
                            <div class="media-body ml-2 d-none d-lg-block">
                                <span class="mb-0 text-sm  font-weight-bold">Jonathan Doe <i class="fa fa-caret-down"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
                        <div class=" dropdown-header noti-title">
                            <h6 class="text-overflow m-0">Welcome!</h6>
                        </div>
                        <a href="../pages/profile.html" class="dropdown-item">
                            <i class="ni ni-single-02"></i>
                            <span>My profile</span>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="#!" class="dropdown-item">
                            <i class="ni ni-user-run"></i>
                            <span>Logout</span>
                        </a>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
    <!-- Header -->
    <div class="header bg-gradient-primary py-5">
        <div class="container-fluid">
            <div class="header-body">
                <!-- Card stats -->

            </div>
        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt-md-4">
        <div class="row">
            <div class="col-md-6  offset-md-1 mb-3">
                <?php
                // check if there is a message in the incoming request
                if (isset($request->session)) {

                    // check if an error exists in the incoming request
                    if (isset($request->session['error'])) {
                        $error = $request->session['error'];
                        echo "<div class='alert alert-danger'>$error</div>";
                    }

                    // check if there is a success msg in the incoming request
                    if (isset($request->session['success'])) {
                        $success = $request->session['success'];
                        echo "<div class='alert alert-success'>$success</div>";
                    }
                }
                ?>
            </div>
        </div>
        <div class="row">

            <div class="col-md-6 offset-md-1 mb-3">
                <div class="">
                    <div class="border-0">
                        <h3 class="mb-0 text-muted">Degree Award Definition</h3>
                        <hr class="my-3">
                    </div>
                    <div class="form-container" id="degreeAwardFormContainer">

                        <form method="POST" :action="url" class="mx-col">


                            <input type="hidden" name="_csrf_token" value="<?php echo $token; ?>">

                            <div class="row">

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-email">Select Degree Type</label>
                                        <div class="input-group">
                                            <select class="form-control" name="degreetype_id" v-model.number="degreetype_select" @change="getDegreAwards">
                                                <?php
                                                if (count($degreetypes) > 0) {
                                                    echo "<option disabled value=''>Choose...</option>";
                                                    foreach ($degreetypes as $degreetype) {
                                                        ?>
                                                        <option value="<?php echo $degreetype->DegreeTypeID ?>"><?php echo $degreetype->DegreeType; ?></option>
                                                    <?php
                                                    }
                                                } else {
                                                    echo " <option>---- No data available --</option>";
                                                }
                                                ?>
                                            </select>
                                            <div class="input-group-append">
                                                <button id='edittoggler' class="btn btn-primary" :disabled="disabledButton" @click="">
                                                    <input type="checkbox" id="" v-model:checked="checked" v-if="!disabledButton">
                                                    <span>Edit</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>



                                <div class="col-md-12" id="predata_edit" v-if="show_edit">
                                    <div class="form-group">
                                        <label class="form-control-label">Choose Predata</label>
                                        <select class="form-control" id="degreeawards" v-model.number="degreeaward_id" @change="getDegreeAward" name="degreeaward_id">
                                            <option value='' disabled>Choose</option>
                                        </select>
                                    </div>
                                </div>



                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-control-label">Degree Award</label>
                                        <input type="text" class="form-control " name="degreeaward" id="degreeaward" v-model='degreeaward'>
                                    </div>

                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-control-label">Degree Award Code</label>
                                        <input type="text" class="form-control " name="degreeawardcode" id="degreeawardcode" v-model='degreeawardcode'>
                                    </div>

                                </div>

                                <div class="col-md-12 mb-3">
                                    <div class="form-group ">
                                        <button type="submit" name="submit" class="btn btn-primary">Save <i class="fa fa-save"></i></button>
                                        <button class="btn btn-danger" @click="deleteUrl">Delete <i class="fa fa-trash"></i>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="alert alert-danger" id="predata_ajax_error" v-if="hasError">
                                        {{error}}
                                    </div>

                                </div>




                            </div>


                        </form>

                    </div>
                </div>



            </div>
            <div class="col-md-8" style="height:20px;"></div>

            <div class="col-md-8 offset-md-1">
                <table class="table table-stripped" id="data_table">
                    <thead>
                        <tr>
                            <th>Degree Award</th>
                            <th>Degree Award Code</th>
                        </tr>
                    </thead>
                    <tbody id="degreetable_body">

                    </tbody>
                </table>
            </div>

        </div>
        <!-- Footer -->
        <footer class="footer">
            <div class="row align-items-center justify-content-xl-between">
                <div class="col-xl-6">
                    <div class="copyright text-center text-xl-left text-muted">
                        &copy; 2019 <a href="#" class="font-weight-bold ml-1" target="_blank">Softech Global Associates.</a> All Rights Reserved.
                    </div>
                </div>
                <div class="col-xl-6">
                    <ul class="nav nav-footer justify-content-center justify-content-xl-end">
                        <div class="copyright text-center text-xl-right text-muted">
                            <a href="#">Policy</a>
                        </div>
                    </ul>
                </div>
            </div>
        </footer>
    </div>
</div>
<!-- end of content section of this page -->
<!-- end of content section of this page -->

<!-- important scrpts goes here -->
<?php
include BASE_URL . '/setup/scripts.php';
?>
<!-- important script ends here -->


<script>
    let degreeawardForm = new Vue({
        el: '#degreeAwardFormContainer',
        data: {
            disabledButton: true,
            url: 'save/degreeaward',
            degreetype_select: '',
            show_edit: false,
            checked: false,
            degreeaward_id: '',
            degreeaward: '',
            degreeawardcode: '',
            hasError: false,
            error: '',
        },

        watch: {
            degreetype_select: function(val) {
                isNaN(val) ? this.disabledButton = true : this.disabledButton = false;
            },
            checked: function(val) {
                if (!this.disabledButton) {
                    this.show_edit = !this.show_edit;
                    this.show_edit ? this.url = 'update/degreeaward' : this.url = 'save/degreeaward';
                    this.getDegreAwards()
                }

                if (!val) {
                    this.degreeaward = '';
                    this.degreeawardcode = '';
                }
            },
        },
        created() {
            this.getAllDegreeAwards();
        },
        methods: {

            getDegreeAward() {
                $vm = this;
                axios.get('api/get/degreeaward', {
                    params: {
                        degreeaward_id: this.degreeaward_id
                    }
                }).then(function(response) {
                    $vm.degreeaward = response.data[0].DegreeAward;
                    $vm.degreeawardcode = response.data[0].DegreeAwardCode;
                }).catch(function(error) {
                    $vm.hasError = true;
                    $vm.error = error.messageText;
                })
            },

            getDegreAwards() {

                if (this.show_edit) {
                    $vm = this;
                    // if(this.degreeaward_id == ''){
                    //     this.hasError  = true;
                    //     this.error = 'Cannot get degreeawards for empty string';
                    //     this.degreeaward = ''
                    //     return;
                    // }
                    axios.get('api/degreetype/degreeawards', {
                        params: {
                            degreetype_id: this.degreetype_select
                        }
                    }).then(function(response) {

                        //reset degreeawards to empty
                        $('#degreeawards').text('');
                        // reset the degreeaward value
                        $vm.degreeaward = ''

                        if (response.data.length > 0) {

                            $('#degreeawards').append("<option value='' >Choose degreeaward...</option>");

                            $.each(response.data, function($key, $obj) {

                                $('#degreeawards').append('<option value=' + $obj.DegreeAwardID + '>' + $obj.DegreeAward + '</option>');
                            });

                        } else {

                            $('#degreeawards').append("<option value=''  > No data found </option>");
                        }


                    }).catch(function(error) {

                        console.log(error)
                    });
                }

            },

            getAllDegreeAwards() {
                axios.get('api/all/degreeawards').then(function(response) {

                    //reset degreeawards to empty
                    $('#degreetable_body').text('');
                    // reset the degreeaward value

                    if (response.data.length > 0) {

                        $.each(response.data, function($key, $obj) {

                            $('#degreetable_body').append('<tr><td>' + $obj.DegreeAward + '</td><td>' + $obj.DegreeAwardCode + '</td></tr>');
                        });

                    } else {


                    }


                }).catch(function(error) {

                    console.log(error)
                });
            },
            deleteUrl() {
                this.url = 'delete/degreeaward';
                console.log('am deleteing this data')
                return true;
            }
        }

    });
</script>
<!-- page script content goes here -->
<script type="text/javacript">


</script>
<!-- page script contents ends here -->
<?php
include BASE_URL . '/setup/footer.php';
?>