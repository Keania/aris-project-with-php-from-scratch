<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="ARIS - University of Port Harcourt">
  <meta name="author" content="Keme Kenneth">
  <title>Setup | ARIS - University of Port Harcourt</title>
  <!-- Favicon -->
  <link href="../assets/img/brand/favicon.png" rel="icon" type="image/png">
  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
  <!-- Icons -->
  <link href="<?php echo resource('public/assets/vendor/nucleo/css/nucleo.css');?>" rel="stylesheet">
  <link href="<?php echo resource('public/assets/vendor/@fortawesome/fontawesome-free/css/all.min.css');?>" rel="stylesheet">
  <!-- Argon CSS -->
  <link type="text/css" href="<?php echo resource('public/assets/css/argon.css?v=1.0.0');?>" rel="stylesheet">
  <!-- Custom / Overwriting CSS -->
  <link type="text/css" href="<?php echo resource('public/assets/css/aris.custom.css?v=1.0.0');?>" rel="stylesheet">
  <!-- Bootstrap DataTables -->
  <link href="<?php echo resource('public/assets/css/dataTables.min.css');?>" rel="stylesheet">
  <!-- <link href="../public/assets/css/dataTable.bootstrap4.min.css" rel="stylesheet"> -->
</head>

<body>
  <!-- Sidenav -->
  <nav class="navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-white" id="sidenav-main">
    <div class="container-fluid">
      <!-- Toggler -->
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <!-- Brand -->
      <a class="navbar-brand pt-0" href="#">
        <img src="<?php echo resource('public/assets/img/brand/blue.png');?>" class="navbar-brand-img" alt="...">
      </a>
      <!-- User -->
      <ul class="nav align-items-center d-md-none">
        <li class="nav-item dropdown">
          <a class="nav-link nav-link-icon" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="ni ni-bell-55"></i>
          </a>
          <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right" aria-labelledby="navbar-default_dropdown_1">
            <a class="dropdown-item" href="#">Action</a>
            <a class="dropdown-item" href="#">Another action</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#">Something else here</a>
          </div>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <div class="media align-items-center">
              <span class="avatar avatar-sm rounded-circle">
                <img alt="Image placeholder" src="<?php echo resource('public/assets/img/theme/team-1-800x800.jpg');?>">
              </span>
            </div>
          </a>
          <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
            <div class=" dropdown-header noti-title">
              <h6 class="text-overflow m-0">Welcome!</h6>
            </div>
            <a href="../pages/profile.html" class="dropdown-item">
              <i class="ni ni-single-02"></i>
              <span>My profile</span>
            </a>
            <a href="../pages/profile.html" class="dropdown-item">
              <i class="ni ni-settings-gear-65"></i>
              <span>Settings</span>
            </a>
            <a href="../pages/profile.html" class="dropdown-item">
              <i class="ni ni-calendar-grid-58"></i>
              <span>Activity</span>
            </a>
            <a href="../pages/profile.html" class="dropdown-item">
              <i class="ni ni-support-16"></i>
              <span>Support</span>
            </a>
            <div class="dropdown-divider"></div>
            <a href="#!" class="dropdown-item">
              <i class="ni ni-user-run"></i>
              <span>Logout</span>
            </a>
          </div>
        </li>
      </ul>
      <!-- Collapse -->
      <div class="collapse navbar-collapse" id="sidenav-collapse-main">
        <!-- Collapse header -->
        <div class="navbar-collapse-header d-md-none">
          <div class="row">
            <div class="col-6 collapse-brand">
              <a href="#">
                <img src="<?php echo resource('../public/assets/img/brand/blue.png');?>">
              </a>
            </div>
            <div class="col-6 collapse-close">
              <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle sidenav">
                <span></span>
                <span></span>
              </button>
            </div>
          </div>
        </div>
        <!-- Form -->
        <form class="mt-4 mb-3 d-md-none">
          <div class="input-group input-group-rounded input-group-merge">
            <input type="search" class="form-control form-control-rounded form-control-prepended" placeholder="Search" aria-label="Search">
            <div class="input-group-prepend">
              <div class="input-group-text">
                <span class="fa fa-search"></span>
              </div>
            </div>
          </div>
        </form>
        <!-- Navigation -->
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" href="#">
              <i class="ni ni-tv-2 text-primary"></i> Dashboard
            </a>
          </li>
         
          <li class="nav-item">
            <a class="nav-link" href="<?php echo url('faculty_management');?>">
              <i class="fa fa-university text-primary"></i> Faculty
            </a>
          </li>
          <li class="nav-item">

          <li class="nav-item nav-toggler dropdown">
            <a class="nav-link" data-toggle="collapse" data-target="#setupSubmenu" href="#">
              <i class="ni ni-settings text-primary"></i> Setup
            </a>
            <ul class="nav collapse bg-gradient-secondary" id="setupSubmenu">
              <li class="nav-item">
                <a class="nav-link pl-md-4" href="<?php echo url('predata');?>">
                  <i class="ni ni-bold-right text-primary"></i> Predefined Data
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link pl-md-4" href="<?php echo url('state_lga');?>">
                  <i class="ni ni-bold-right text-primary"></i> State & LGA
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link pl-md-4" href="<?php echo url('degreeaward');?>">
                  <i class="ni ni-bold-right text-primary"></i> Degree Award
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link pl-md-4" href="<?php echo url('programtype');?>">
                  <i class="ni ni-bold-right text-primary"></i> Program Type
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link pl-md-4" href="<?php echo url('feeitem');?>">
                  <i class="ni ni-bold-right text-primary"></i> Fee Item
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link pl-md-4" href="<?php echo url('olevel');?>">
                  <i class="ni ni-bold-right text-primary"></i> O-Level Subjects
                </a>
              </li>

            </ul>
          </li>
          </li>
          <li class="nav-item">

            <li class="nav-item nav-toggler dropdown">
              <a class="nav-link" data-toggle="collapse" data-target="#departmentSubmenu" href="#">
                <i class="ni ni-hat-3 text-primary"></i> Department
              </a>
              <ul class="nav collapse bg-gradient-secondary" id="departmentSubmenu">
                <li class="nav-item">
                  <a class="nav-link pl-md-4" href="<?php echo url('department_management');?>">
                    <i class="ni ni-bold-right text-primary"></i> Department
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link pl-md-4" href="<?php echo url('degreecourse');?>">
                    <i class="ni ni-bold-right text-primary"></i> Degree Course
                  </a>
                </li>


              </ul>
            </li>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">
              <i class="ni ni-badge text-primary"></i> Admission
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">
              <i class="ni ni-archive-2 text-primary"></i> Results
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">
              <i class="ni ni-credit-card text-primary"></i> Fees
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">
              <i class="fa fa-info text-primary"></i> Info
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">
              <i class="ni ni-single-copy-04 text-primary"></i> Documents
            </a>
          </li>
         
          <li class="nav-item">

            <li class="nav-item nav-toggler dropdown">
              <a class="nav-link" data-toggle="collapse" data-target="#adminSubmenu" href="#">
                <i class="ni ni-settings-gear-65 text-primary"></i> Admin
              </a>
              <ul class="nav collapse bg-gradient-secondary" id="adminSubmenu">
                <li class="nav-item">
                  <a class="nav-link pl-md-4" href="<?php echo url('user_management');?>">
                    <i class="ni ni-bold-right text-primary"></i> User Management
                  </a>
                </li>
               


              </ul>
            </li>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">
              <i class="ni ni-single-02 text-primary"></i> Profile
            </a>
          </li>
        </ul>
      </div>
    </div>
  </nav>