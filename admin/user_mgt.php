<?php

use Lib\Connection;
use Model\DegreeType;
use Model\ProgramType;
use Model\Faculty;
use Model\DegreeCourse;
use Model\VW1_Predata;

$page_title = 'Setup Predefined data';
$token = generateCSRFToken();
include BASE_URL . '/setup/header.php';

// Database Operations of getting data

$faculties = Faculty::all();
$degreecourses = DegreeCourse::all();
$vw1_predatas = VW1_Predata::where(['DataCategory', ' like ', '%TITLE%']);
$months = [
    1 => 'Jan',
    2 => 'Feb',
    3 => 'March',
    4 => 'April',
    5 => 'May',
    6 => 'June',
    7 => 'July',
    8 => 'August',
    9 => 'Sept',
    10 => 'Oct',
    11 => 'Nov',
    12 => 'Dec'
];


?>


<!-- content section of this page goes here -->
<!-- Main content -->
<div class="main-content">
    <!-- Top navbar -->
    <nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
        <div class="container-fluid">
            <!-- Brand -->
            <!-- <a class="h4 mb-0 text-secondary d-none_ d-lg-inline-block" href="#">Setup</a>
        <a class="h4 mb-0 text-white d-none_ d-lg-inline-block" href="#"> | Pre-Defined Data</a>
         -->
            <ul class="font-weight-300 nav">
                <li class="nav-item">
                    <a href="#" class="nav-link text-white-50 d-lg-inline-block">Setup Management</a>
                </li> <span class="text-white-50 h2">|</span>
                <li class="nav-item">
                    <a href="#" class="nav-link text-white-50 d-lg-inline-block" style="font-size: 1.35rem;line-height: 1;">User</a>
                </li>
            </ul>

            <!-- User -->
            <ul class="navbar-nav align-items-center d-none d-md-flex">
                <li class="nav-item dropdown">
                    <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <div class="media align-items-center">
                            <span class="avatar avatar-sm rounded-circle">
                                <img alt="Image placeholder" src="<?php echo resource('public/assets/img/theme/team-4-800x800.jpg');?>">
                            </span>
                            <div class="media-body ml-2 d-none d-lg-block">
                                <span class="mb-0 text-sm  font-weight-bold">Jonathan Doe <i class="fa fa-caret-down"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
                        <div class=" dropdown-header noti-title">
                            <h6 class="text-overflow m-0">Welcome!</h6>
                        </div>
                        <a href="../pages/profile.html" class="dropdown-item">
                            <i class="ni ni-single-02"></i>
                            <span>My profile</span>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="#!" class="dropdown-item">
                            <i class="ni ni-user-run"></i>
                            <span>Logout</span>
                        </a>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
    <!-- Header -->
    <div class="header bg-gradient-primary py-5">
        <div class="container-fluid">
            <div class="header-body">
                <!-- Card stats -->

            </div>
        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt-md-4">
    <div class="row">
            <div class="col-md-6  offset-md-1 mb-3">
                <?php
                // check if there is a message in the incoming request
                if (isset($request->session)) {

                    // check if an error exists in the incoming request
                    if (isset($request->session['error'])) {
                        $error = $request->session['error'];
                        echo "<div class='alert alert-danger'>$error</div>";
                    }

                    // check if there is a success msg in the incoming request
                    if (isset($request->session['success'])) {
                        $success = $request->session['success'];
                        echo "<div class='alert alert-success'>$success</div>";
                    }
                }
                ?>
            </div>
        </div>
        <div class="row">

            <div class="col-md-10 offset-md-1 mb-3">
                <div class="">
                    <div class="border-0">
                        <h3 class="mb-0 text-muted">Admin</h3>
                        <hr class="my-3">
                    </div>
                    <div class="form-container" id="newUserFormContainer">

                        <form method="POST" :action="url" id="predataMainForm" class="mx-col">


                            <input type="hidden" name="_csrf_token" value="<?php echo $token; ?>">
                            <input type="hidden" name="status_id" v-model.number="status_id">
                            <div class="row mb-3">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="search" name="searchdata" v-model="search_data" class="form-control">
                                            <div class="input-group-append">
                                                <button class="btn btn-primary" @click.prevent="SearchUser">

                                                    <span><i class="fa fa-search"></i></span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-email"> Select User</label>
                                        <div class="input-group">
                                            <select id="users" v-model.number="user_select" name="user_id" class="form-control" @change="getUser">
                                                <option value=''>Select User</option>
                                            </select>
                                            <div class="input-group-append">
                                                <button id='edittoggler' class="btn btn-primary" :disabled="disabledButton" @click="">
                                                    <input type="checkbox" id="" v-model:checked="checked" v-if="!disabledButton">
                                                    <span>Edit</span>
                                                </button>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <h3 class="mb-0 text-muted">New User</h3>
                            <hr class="my-3">
                            <div class="row mb-3">

                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="form-control-label col-md-4">Surname</label>
                                        <div class="input-group col-md-8">
                                            <input class="form-control" name="surname" v-model="surname">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="form-control-label col-md-4">Others</label>
                                        <div class="input-group col-md-8">
                                            <input class="form-control" name="othernames" v-model="other_name">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row mb-3">

                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="form-control-label col-md-4">Sex</label>
                                        <div class="input-group col-md-8">
                                            <select class="form-control" name="sex" v-model="sex">
                                                <option value="">Choose Sex</option>
                                                <option value="MALE"> Male</option>
                                                <option value="FEMALE">Female</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="form-control-label col-md-4">Title</label>
                                        <div class="input-group col-md-8">
                                            <select class="form-control" name="title" v-model="title">

                                                <?php

                                                if (count($vw1_predatas) > 0) {
                                                    echo "<option value =''>Choose Title</option>";
                                                    foreach ($vw1_predatas as $predata) {
                                                        ?>
                                                        <option value="<?php echo strtoupper($predata->Data); ?>"><?php echo $predata->Data; ?></option>
                                                    <?php
                                                    }
                                                } else {
                                                    echo "<option value='' > No data Available</option>";
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row mb-3">

                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="form-control-label col-md-4">User Group</label>
                                        <div class="input-group col-md-8">
                                            <select class="form-control" name="user_group_id" v-model="user_group" @change="getUserCategory">
                                                <option value="">Choose Group</option>
                                                <option value="1"> ACADEMIC STAFF</option>
                                                <option value="2">NON ACADEMIC STAFF</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="form-control-label col-md-4">User Category</label>
                                        <div class="input-group col-md-8">
                                            <select class="form-control" name="user_category_id" v-model="user_category" id="user_categories">
                                                <option value="">Choose Category</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="row mb-3">

                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="form-control-label col-md-4" for="input-email">Faculty</label>
                                        <div class="input-group col-md-8">
                                            <select class="form-control" id="dataCategorySelect" name="faculty_id" v-model.number="faculty_select" @change="getDepartments">
                                                <?php
                                                if (count($faculties) > 0) {
                                                    echo "<option disabled value=''>Choose...</option>";
                                                    foreach ($faculties as $faculty) {
                                                        ?>
                                                        <option value="<?php echo $faculty->FacultyID ?>"><?php echo $faculty->FacultyName; ?></option>
                                                    <?php
                                                    }
                                                } else {
                                                    echo " <option>---- No data available --</option>";
                                                }
                                                ?>
                                            </select>

                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="form-control-label col-md-4" for="input-email">Department</label>
                                        <div class="input-group col-md-8">
                                            <select class="form-control" id="departments" name="dept_id" v-model="department_select">
                                                <option value='' disabled>Choose Department</option>
                                            </select>

                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="row mb-3">

                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="form-control-label col-md-4" for="input-email">PIN</label>
                                        <div class="input-group col-md-8">
                                            <input type='text' v-model="PIN" disabled class="form-control">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="form-control-label col-md-4" for="input-email">Status</label>
                                        <div class="input-group col-md-8">
                                            <select class="form-control"  v-model="status_id" disabled>
                                                <option value='1'>Activated</option>
                                                <option value="0">Deactivated</option>
                                            </select>

                                        </div>
                                    </div>
                                </div>

                            </div>

                            <!-- First Buttons -->
                            <div class="row">
                                <div class="col-md-8 mb-3">
                                    <div class="col-md-12 mb-3">
                                        <div class="form-group ">
                                            <button type="submit" name="submit" class="btn btn-primary">Save <i class="fa fa-save"></i></button>
                                            <button class="btn btn-danger" @click="deleteUrl">Delete <i class="fa fa-trash"></i>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="alert alert-danger" id="predata_ajax_error" v-if="hasError">
                                            {{error}}
                                        </div>

                                    </div>
                                </div>




                            </div>
                            <!-- End First Buttons -->



                            <div class="row" style="height:20px;"></div>
                            <hr class="my-3">



                            <!-- First Edit Section -->
                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="form-control-label col-md-4">Email</label>
                                        <div class="input-group col-md-8">
                                            <input class="form-control" name="email" v-model="email">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="form-control-label col-md-4">Phone</label>
                                        <div class="input-group col-md-8">
                                            <input class="form-control" name="phone" v-model="phone">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="form-control-label col-md-4">Contact </label>
                                        <div class="inoput-group col-md-8">
                                            <textarea class="form-control" name="contact_address">{{contact_address}}</textarea>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <!-- End First Edit Section -->



                            <!-- Edit Button 2 -->
                            <div class="row">
                                <div class="col-md-8 mb-3">

                                    <div class="col-md-12 mb-3">
                                        <div class="form-group ">
                                            <button type="submit" name="submit" class="btn btn-primary" @click="UrlButton2">Save <i class="fa fa-save"></i></button>

                                        </div>
                                    </div>


                                </div>

                            </div>
                            <!-- Edit Button 2 Ends Here -->



                            <div class="row" style="height:20px;"></div>
                            <hr class="my-3">

                            <!-- Second Editing Section -->
                            <div class="row mb-3">

                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="form-control-label col-md-4">Staff Number</label>
                                        <div class="input-group col-md-8">
                                            <input type="text" class="form-control" name="staff_number" v-model="staff_number">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="form-control-label col-md-4">Grade Level</label>
                                        <div class="input-group col-md-8">
                                            <select name="grade_level" class="form-control" v-model="grade_level">
                                                <option value="">Choose Grade Level</option>
                                                <?php
                                                foreach (range(1, 17) as $index => $value) {
                                                    ?>
                                                    <option value="<?php echo $value; ?>"><?php echo $value; ?></option>
                                                <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row mb-3">

                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="form-control-label col-md-4">Empl. Date</label>
                                        <div class="input-group col-md-8">
                                            <input type="date" class="form-control" name="emp_date" v-model="emp_date">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="form-control-label col-md-4">Appt. Date</label>
                                        <div class="input-group col-md-8">
                                            <input type="date" class="form-control" name="appt_date" v-model="appt_date">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row mb-3">

                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="form-control-label col-md-4">Month of Birth</label>
                                        <div class="input-group col-md-8">
                                            <select class="form-control" name="mofb" v-model="Mofb">
                                                <option value=''>Choose Month</option>
                                                <?php
                                                foreach ($months as $index => $value) {
                                                    ?>
                                                    <option value="<?php echo $index; ?>"><?php echo $value; ?></option>
                                                <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="form-control-label col-md-4">Day of Birth</label>
                                        <div class="input-group col-md-8">
                                            <select class="form-control" name="dyofb" v-model="Dyofb">
                                                <option value=''>Choose Day</option>
                                                <?php
                                                foreach (range(1, 31) as $index => $value) {
                                                    ?>
                                                    <option value="<?php echo $value; ?>"><?php echo $value; ?></option>
                                                <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <!-- Second Edit Section Ends Here -->

                            <!-- Edit Button 3 -->
                            <div class="row">
                                <div class="col-md-8 mb-3">
                                    <div class="col-md-12 mb-3">
                                        <div class="form-group ">
                                            <button type="submit" name="submit" class="btn btn-primary" @click="UrlButton3">Save <i class="fa fa-save"></i></button>

                                        </div>
                                    </div>

                                </div>

                            </div>
                            <!-- Edit Button 3 Ends Here -->

                            <div class="row" style="height:20px;"></div>
                            <hr class="my-3">


                            <!-- Reset and Deactivate Buttons -->
                            <div class="row">
                                <div class="col-md-8 mb-3">
                                    <div class="col-md-12 mb-3">
                                        <div class="form-group ">
                                            <button @click="ResetPassword" class="btn btn-light">Reset Password <i class="fa fa-save"></i></button>
                                            <button class="btn btn-info" @click="Deactivate">Deactivate <i class="fa fa-trash"></i>
                                        </div>
                                    </div>


                                </div>




                            </div>
                            <!-- Reset and Deactivate Buttons Ends Here -->


                        </form>

                    </div>
                </div>



            </div>



        </div>
        <!-- Footer -->
        <footer class="footer">
            <div class="row align-items-center justify-content-xl-between">
                <div class="col-xl-6">
                    <div class="copyright text-center text-xl-left text-muted">
                        &copy; 2019 <a href="#" class="font-weight-bold ml-1" target="_blank">Softech Global Associates.</a> All Rights Reserved.
                    </div>
                </div>
                <div class="col-xl-6">
                    <ul class="nav nav-footer justify-content-center justify-content-xl-end">
                        <div class="copyright text-center text-xl-right text-muted">
                            <a href="#">Policy</a>
                        </div>
                    </ul>
                </div>
            </div>
        </footer>
    </div>
</div>
<!-- end of content section of this page -->

<!-- important scrpts goes here -->
<?php
include BASE_URL . '/setup/scripts.php';
?>
<!-- important script ends here -->


<script>
    let predataForm = new Vue({
        el: '#newUserFormContainer',
        data: {
            disabledButton: true,
            url: 'save/user',
            surname: '',
            other_name: '',
            sex: '',
            title: '',
            user_group: '',
            user_category: '',
            faculty_select: '',
            department_select: '',
            user_select: '',
            search_data: '',
            show_edit: false,
            checked: false,
            hasError: false,
            error: '',
            email: '',
            phone: '',
            grade_level: "",
            contact_address: '',
            staff_number: "",
            emp_date: '',
            appt_date: '',
            Mofb: '',
            Dyofb: '',
            status_id: 0,
            PIN : '',

        },

        watch: {
            user_select: function(val) {
                isNaN(val) ? this.disabledButton = true : this.disabledButton = false;
            },
            checked: function(val) {
                if (val) {
                    this.url = 'update/user/btn1';

                    if (!isNaN(this.user_select)) {
                        this.getUser();
                    }
                } else {
                    this.url = 'save/user'
                    this.surname = '';
                    this.sex = '';
                    this.other_name = '';
                    this.title = '';
                    this.user_group = '';
                    this.user_category = '';
                    this.faculty_select = '';
                    this.department_select = '';
                    this.email = '';
                    this.phone = '';
                    this.contact_address = '';
                    this.staff_number = '';
                    this.grade_level = '';
                    this.emp_date = '';
                    this.appt_date = '';
                    this.Mofb = '';
                    this.Dyofb = '';
                    this.status_id = 0,
                    this.PIN = ''
                }
            },
            faculty_select : function(val){
                this.getDepartments();
            },
            user_group : function(val){
                this.getUserCategory();
            }
        },
        methods: {
            getUserCategory() {

                $vm = this;
                axios.get('api/get/user_category', {
                    group_id: $vm.user_group
                }).then(function(response) {
                    let category_select = $('#user_categories')
                    category_select.text('');
                    if (response.data.length > 0) {

                        category_select.append("<option value='' >Choose Category...</option>");

                        $.each(response.data, function($key, $obj) {

                            category_select.append('<option value=' + $obj.UserCategoryID + '>' + $obj.UserCategory + '</option>');
                        });

                    } else {
                        category_select.append("<option value=''>No data Available</option>")
                    }




                }).catch(function(error) {
                    $vm.error = error.message;
                    $vm.hasError = true
                    setTimeout(function() {
                        $vm.hasError = false;
                        $vm.error = '';
                    }, 3000);
                });
            },

            getDepartments() {

                if(this.faculty_select.length < 1){
                    return;
                }
                $vm = this;
                axios.get('api/get/faculty/departments', {
                    params: {
                        faculty_id: this.faculty_select
                    }
                }).then(function(response) {

                    //reset departments to empty
                    $('#departments').text('');


                    if (response.data.length > 0) {

                        $('#departments').append("<option value='' >Choose Department...</option>");

                        $.each(response.data, function($key, $obj) {

                            $('#departments').append('<option value=' + $obj.DeptID + '>' + $obj.DeptName + '</option>');
                        });

                    } else {

                        $('#departments').append("<option value='' > No data found </option>");
                    }


                }).catch(function(error) {

                    $vm.error = error.message;
                    $vm.hasError = true
                    setTimeout(function() {
                        $vm.hasError = false;
                        $vm.error = '';
                    }, 30000);
                });
            },
            getUser() {
                if (!this.checked) {
                    return;
                }
                $vm = this;
                axios.get('api/get/user', {
                    params: {
                        user_id: this.user_select
                    }
                }).then(function(response) {
                    console.log(response);
                    $vm.surname = response.data.Surname;
                    $vm.sex = response.data.Sex;
                    $vm.other_name = response.data.Others;
                    $vm.title = response.data.Title;
                    $vm.user_group = response.data.UserGroupID;
                    $vm.faculty_select = response.data.FacultyID;
                   
                    $vm.department_select = response.data.UserDeptID;
                   
                    $vm.user_category = response.data.UserCategoryID;

                    // second section datas
                    $vm.email = response.data.Email;
                    $vm.phone = response.data.Phone;
                    $vm.contact_address = response.data.ContactAddress;

                    // third section data
                    $vm.staff_number = response.data.StaffNumber;
                    $vm.grade_level = response.data.GradeLevel;
                    $vm.emp_date = response.data.EmploymentDate;
                    $vm.appt_date = response.data.AppointmentDate;
                    $vm.Mofb = response.data.MofB;
                    $vm.Dyofb = response.data.DyofB;

                    $vm.status_id = parseInt(response.data.UserStatusID);
                    $vm.PIN = response.data.UserPIN;
                })
            },
            SearchUser() {
                if (this.search_data.length == 0) {
                    return;
                }
                $vm = this;
                axios.get('api/search/user', {
                    params: {
                        search_data: this.search_data
                    }
                }).then(function(response) {
                    $('#users').text('');
                    // reset the user value
                    $vm.user_select = ''


                    if (response.data.length > 0) {

                        $('#users').append("<option value='' >Choose user...</option>");

                        $.each(response.data, function($key, $obj) {

                            $('#users').append('<option value=' + $obj.UserID + '>' + $obj.UserName + '</option>');
                        });

                    } else {

                        $('#users').append("<option value=''  > No data found </option>");
                    }

                }).catch(function(error) {

                    $vm.error = error.message;
                    $vm.hasError = true
                    setTimeout(function() {
                        $vm.hasError = false;
                        $vm.error = '';
                    }, 3000);
                });
            },
            deleteUrl() {
                this.url = 'delete/user';
                console.log('am deleteing this data')
                return true;
            },
            UrlButton2() {
                this.url = 'update/user/btn2'
                return true;
            },
            UrlButton3() {
                this.url = 'update/user/btn3'
                return true;
            },
            ResetPassword() {
                this.url = 'reset/user/password'
                return true;
            },
            Deactivate() {
                this.url = 'deactivate/user'
                return true;
            }

        }

    });
</script>
<!-- page script content goes here -->
<script type="text/javacript"></script>
<!-- page script contents ends here -->
<?php
include BASE_URL . '/setup/footer.php';
?>