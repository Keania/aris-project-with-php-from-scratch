<?php

use Model\Faculty;

$page_title = 'Setup Predefined data';
$token = generateCSRFToken();
include BASE_URL . '/setup/header.php';

// Database Operations of getting data
$faculties  = Faculty::all();

?>


<!-- content section of this page goes here -->
<!-- Main content -->
<div class="main-content">
    <!-- Top navbar -->
    <nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
        <div class="container-fluid">
            <!-- Brand -->
            <!-- <a class="h4 mb-0 text-secondary d-none_ d-lg-inline-block" href="#">Setup</a>
        <a class="h4 mb-0 text-white d-none_ d-lg-inline-block" href="#"> | Pre-Defined Data</a>
         -->
            <ul class="font-weight-300 nav">
                <li class="nav-item">
                    <a href="#" class="nav-link text-white-50 d-lg-inline-block">Setup Management</a>
                </li> <span class="text-white-50 h2">|</span>
                <li class="nav-item">
                    <a href="#" class="nav-link text-white-50 d-lg-inline-block" style="font-size: 1.35rem;line-height: 1;">Department Mgt</a>
                </li>
            </ul>

            <!-- User -->
            <ul class="navbar-nav align-items-center d-none d-md-flex">
                <li class="nav-item dropdown">
                    <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <div class="media align-items-center">
                            <span class="avatar avatar-sm rounded-circle">
                                <img alt="Image placeholder" src="<?php echo resource('public/assets/img/theme/team-4-800x800.jpg');?>">
                            </span>
                            <div class="media-body ml-2 d-none d-lg-block">
                                <span class="mb-0 text-sm  font-weight-bold">Jonathan Doe <i class="fa fa-caret-down"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
                        <div class=" dropdown-header noti-title">
                            <h6 class="text-overflow m-0">Welcome!</h6>
                        </div>
                        <a href="../pages/profile.html" class="dropdown-item">
                            <i class="ni ni-single-02"></i>
                            <span>My profile</span>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="#!" class="dropdown-item">
                            <i class="ni ni-user-run"></i>
                            <span>Logout</span>
                        </a>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
    <!-- Header -->
    <div class="header bg-gradient-primary py-5">
        <div class="container-fluid">
            <div class="header-body">
                <!-- Card stats -->

            </div>
        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt-md-4">
    <div class="row">
            <div class="col-md-6  offset-md-1 mb-3">
                <?php
                // check if there is a message in the incoming request
                if (isset($request->session)) {

                    // check if an error exists in the incoming request
                    if (isset($request->session['error'])) {
                        $error = $request->session['error'];
                        echo "<div class='alert alert-danger'>$error</div>";
                    }

                    // check if there is a success msg in the incoming request
                    if (isset($request->session['success'])) {
                        $success = $request->session['success'];
                        echo "<div class='alert alert-success'>$success</div>";
                    }
                }
                ?>
            </div>
        </div>
        <div class="row">

            <div class="col-md-6 offset-md-1 mb-3">
                <div class="">
                    <div class="border-0">
                        <h3 class="mb-0 text-muted">New Department</h3>
                        <hr class="my-3">
                    </div>
                    <div class="form-container" id="departmentMgtContainer">

                        <form method="POST" :action="url" id="DepartmentMainForm" class="mx-col">


                            <input type="hidden" name="_csrf_token" value="<?php echo $token; ?>">

                            <div class="row">

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-email">Select Faculty</label>
                                        <div class="input-group">
                                            <select class="form-control" id="dataCategorySelect" name="dept_faculty_id" v-model.number="dept_faculty_select" @change="getFacultyDepartment">
                                                <?php
                                                if (count($faculties) > 0) {
                                                    echo "<option disabled value=''>Choose...</option>";
                                                    foreach ($faculties as $faculty) {
                                                        ?>
                                                        <option value="<?php echo $faculty->FacultyID ?>"><?php echo $faculty->FacultyName; ?></option>
                                                    <?php
                                                    }
                                                } else {
                                                    echo " <option value=''>---- No data available --</option>";
                                                }
                                                ?>
                                            </select>
                                            <div class="input-group-append">
                                                <button id='edittoggler' class="btn btn-primary" :disabled="disabledButton" @click="">
                                                    <input type="checkbox" id="" v-model:checked="checked" v-if="!disabledButton">
                                                    <span>Edit</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>



                                <div class="col-md-12" id="Department_edit" v-if="show_edit">
                                    <div class="form-group">
                                        <label class="form-control-label">Choose Department</label>
                                        <select class="form-control" id="departments" v-model.number="department_select" @change="getDepartment" name="dept_id">
                                            <option value='' disabled>Choose</option>
                                        </select>
                                    </div>
                                </div>



                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-control-label">Department</label>
                                        <input type="text" class="form-control " name="dept_name" id="Department" v-model='department'>
                                    </div>

                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-control-label">Department Code</label>
                                        <input type="text" class="form-control " name="dept_code" id="Department" v-model='department_code'>
                                    </div>

                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-control-label">Department Digit Code</label>
                                        <input type="text" class="form-control " name="dept_digit_code" id="Department" v-model='department_digit_code'>
                                    </div>

                                </div>

                                <div class="col-md-12 mb-3">
                                    <div class="form-group ">
                                        <button type="submit" name="submit" class="btn btn-primary">Save <i class="fa fa-save"></i></button>
                                        <button class="btn btn-danger" @click="deleteUrl">Delete <i class="fa fa-trash"></i>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="alert alert-danger" id="Department_ajax_error" v-if="hasError">
                                        {{error}}
                                    </div>

                                </div>




                            </div>


                        </form>

                    </div>
                </div>



            </div>

            <div class="col-md-6 offset-md-1 mb-3">
                <div class="" id="hodDepartmentManagementFormContainer">
                    <div class="border-0">
                        <h3 class="mb-0 text-muted">Head Department Management</h3>
                        <hr class="my-3">
                    </div>

                    <form class="mx-col" method="POST" :action="url">

                        <input type="hidden" name="_csrf_token" value="<?php echo $token; ?>">
                        <div class="row">

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="form-control-label" for="input-email">Select Faculty</label>
                                    <div class="input-group">
                                        <select class="form-control" id="dataCategorySelect" name="faculty_id" v-model.number="faculty_select" @change="getFacultyDepartment">
                                            <?php
                                            if (count($faculties) > 0) {
                                                echo "<option disabled value=''>Choose...</option>";
                                                foreach ($faculties as $faculty) {
                                                    ?>
                                                    <option value="<?php echo $faculty->FacultyID ?>"><?php echo $faculty->FacultyName; ?></option>
                                                <?php
                                                }
                                            } else {
                                                echo " <option value=''>---- No data available --</option>";
                                            }
                                            ?>
                                        </select>
                                       
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12" id="Department_edit" >
                                    <div class="form-group">
                                        <label class="form-control-label">Choose Department</label>
                                        <select class="form-control" id="hod-departments" v-model.number="department_select" @change="getFacultyDepartmentUsers" name="dept_id">
                                            <option value='' disabled>Choose</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12" id="Department_edit">
                                    <div class="form-group">
                                        <label class="form-control-label">Choose HOD</label>
                                        <select class="form-control" id="users" v-model.number="user_select"  name="hod_id">
                                            <option value='' disabled>Choose</option>
                                        </select>
                                    </div>
                                </div>

                            <div class="col-md-12 mb-3">
                                <button type="submit" class="btn btn-primary">Save <i class="fa fa-save"></i></button>
                               
                            </div>
                            <div class="col-md-12 mb-3">
                                <div class="alert alert-dismissible fade show d-none" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            </div>
                        </div>

                    </form>

                </div>
            </div>

        </div>
        <!-- Footer -->
        <footer class="footer">
            <div class="row align-items-center justify-content-xl-between">
                <div class="col-xl-6">
                    <div class="copyright text-center text-xl-left text-muted">
                        &copy; 2019 <a href="#" class="font-weight-bold ml-1" target="_blank">Softech Global Associates.</a> All Rights Reserved.
                    </div>
                </div>
                <div class="col-xl-6">
                    <ul class="nav nav-footer justify-content-center justify-content-xl-end">
                        <div class="copyright text-center text-xl-right text-muted">
                            <a href="#">Policy</a>
                        </div>
                    </ul>
                </div>
            </div>
        </footer>
    </div>
</div>
<!-- end of content section of this page -->

<!-- important scrpts goes here -->
<?php
include BASE_URL . '/setup/scripts.php';
?>
<!-- important script ends here -->


<script>
    let departmentMgt = new Vue({
        el: '#departmentMgtContainer',
        data: {
            disabledButton: true,
            url: 'save/department',
            dept_faculty_select: '',
            show_edit: false,
            checked: false,
            department_select: '',
            department_code: '',
            department_digit_code: '',
            department: '',
            hasError: false,
            error: '',
        },

        watch: {
            dept_faculty_select: function(val) {
                isNaN(val) ? this.disabledButton = true : this.disabledButton = false;
            },
            checked: function(val) {
                if (!this.disabledButton) {
                    this.show_edit = !this.show_edit;
                    this.show_edit ? this.url = 'update/department' : this.url = 'save/department';
                    this.getFacultyDepartment()
                }
            },
        },
        methods: {

            getDepartment() {
                $vm = this;
                axios.get('api/get/department', {
                    params: {
                        dept_id: this.department_select
                    }
                }).then(function(response) {
                    $vm.department = response.data[0].DeptName;
                    $vm.department_code = response.data[0].DeptCode;
                    $vm.department_digit_code = response.data[0].DigitCode;
                }).catch(function(error) {
                    $vm.hasError = true;
                    $vm.error = error.messageText;
                })
            },

            getFacultyDepartment() {

                if (this.show_edit) {
                    $vm = this;
                    // if(this.department_select == ''){
                    //     this.hasError  = true;
                    //     this.error = 'Cannot get departments for empty string';
                    //     this.Department = ''
                    //     return;
                    // }
                    axios.get('api/get/faculty/departments', {
                        params: {
                            faculty_id: this.dept_faculty_select
                        }
                    }).then(function(response) {

                        //reset departments to empty
                        $('#departments').text('');
                        // reset the Department value
                        $vm.Department = ''
                        console.log(response);
                        if (response.data.length > 0) {

                            $('#departments').append("<option value='' >Choose Department...</option>");

                            $.each(response.data, function($key, $obj) {

                                $('#departments').append('<option value=' + $obj.DeptID + '>' + $obj.DeptName + '</option>');
                            });

                        } else {

                            $('#departments').append("<option value='' > No data found </option>");
                        }


                    }).catch(function(error) {

                        console.log(error)
                    });
                }

            },
            deleteUrl() {
                this.url = 'delete/department';

                return true;
            }
        }

    });

    let colourForm = new Vue({
        el: '#hodDepartmentManagementFormContainer',
        data: {
            error: '',
            hasError: false,
            url: 'update/department/hod',
            faculty_select: '',
            department_select:'',
            user_select : ''
        },
       
        methods: {
           
            getFacultyDepartment() {
                
                $vm = this;
                axios.get('api/get/faculty/departments', {
                        params: {
                            faculty_id: this.faculty_select
                        }
                    }).then(function(response) {

                        //reset departments to empty
                        $('#hod-departments').text('');
                        // reset the Department value
                        $vm.Department = ''
                        console.log(response);
                        if (response.data.length > 0) {

                            $('#hod-departments').append("<option value='' >Choose Department...</option>");

                            $.each(response.data, function($key, $obj) {

                                $('#hod-departments').append('<option value=' + $obj.DeptID + '>' + $obj.DeptName + '</option>');
                            });

                        } else {

                            $('#hod-departments').append("<option value='' disabled > No data found </option>");
                        }


                    }).catch(function(error) {

                        console.log(error)
                    });
            },
            getFacultyDepartmentUsers(){
                axios.get('api/get/faculty/department/users', {
                        params: {
                            faculty_id: this.faculty_select,
                            dept_id : this.department_select
                        }
                    }).then(function(response) {

                        //reset departments to empty
                        $('#users').text('');
                        // reset the Department value
                        $vm.Department = ''
                        console.log(response);
                        if (response.data.length > 0) {

                            $('#users').append("<option value='' >Choose HOD...</option>");

                            $.each(response.data, function($key, $obj) {

                                $('#users').append('<option value=' + $obj.UserID + '>' + $obj.Title +' '+$obj.Surname+' '+$obj.Others +'</option>');
                            });

                        } else {

                            $('#users').append("<option value='' > No data found </option>");
                        }


                    }).catch(function(error) {

                        console.log(error)
                    });
            }
        }
    });
</script>
<!-- page script content goes here -->
<script type="text/javacript"></script>
<!-- page script contents ends here -->
<?php
include BASE_URL . '/setup/footer.php';
?>