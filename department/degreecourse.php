<?php

use Lib\Connection;
use Model\DegreeType;
use Model\ProgramType;
use Model\Faculty;
use Model\DegreeCourse;

$page_title = 'Setup Predefined data';
$token = generateCSRFToken();
include BASE_URL . '/setup/header.php';

// Database Operations of getting data
$degreetypes  = DegreeType::all();
$programtypes = ProgramType::all();
$faculties = Faculty::all();
$degreecourses = DegreeCourse::all();

?>


<!-- content section of this page goes here -->
<!-- Main content -->
<div class="main-content">
    <!-- Top navbar -->
    <nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
        <div class="container-fluid">
            <!-- Brand -->
            <!-- <a class="h4 mb-0 text-secondary d-none_ d-lg-inline-block" href="#">Setup</a>
        <a class="h4 mb-0 text-white d-none_ d-lg-inline-block" href="#"> | Pre-Defined Data</a>
         -->
            <ul class="font-weight-300 nav">
                <li class="nav-item">
                    <a href="#" class="nav-link text-white-50 d-lg-inline-block">Setup Management</a>
                </li> <span class="text-white-50 h2">|</span>
                <li class="nav-item">
                    <a href="#" class="nav-link text-white-50 d-lg-inline-block" style="font-size: 1.35rem;line-height: 1;">Degree Course</a>
                </li>
            </ul>

            <!-- User -->
            <ul class="navbar-nav align-items-center d-none d-md-flex">
                <li class="nav-item dropdown">
                    <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <div class="media align-items-center">
                            <span class="avatar avatar-sm rounded-circle">
                                <img alt="Image placeholder" src="<?php echo resource('public/assets/img/theme/team-4-800x800.jpg');?>">
                            </span>
                            <div class="media-body ml-2 d-none d-lg-block">
                                <span class="mb-0 text-sm  font-weight-bold">Jonathan Doe <i class="fa fa-caret-down"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
                        <div class=" dropdown-header noti-title">
                            <h6 class="text-overflow m-0">Welcome!</h6>
                        </div>
                        <a href="../pages/profile.html" class="dropdown-item">
                            <i class="ni ni-single-02"></i>
                            <span>My profile</span>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="#!" class="dropdown-item">
                            <i class="ni ni-user-run"></i>
                            <span>Logout</span>
                        </a>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
    <!-- Header -->
    <div class="header bg-gradient-primary py-5">
        <div class="container-fluid">
            <div class="header-body">
                <!-- Card stats -->

            </div>
        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt-md-4">
    <div class="row">
            <div class="col-md-6  offset-md-1 mb-3">
                <?php
                // check if there is a message in the incoming request
                if (isset($request->session)) {

                    // check if an error exists in the incoming request
                    if (isset($request->session['error'])) {
                        $error = $request->session['error'];
                        echo "<div class='alert alert-danger'>$error</div>";
                    }

                    // check if there is a success msg in the incoming request
                    if (isset($request->session['success'])) {
                        $success = $request->session['success'];
                        echo "<div class='alert alert-success'>$success</div>";
                    }
                }
                ?>
            </div>
        </div>
        <div class="row">

            <div class="col-md-10 offset-md-1 mb-3">
                <div class="">
                    <div class="border-0">
                        <h3 class="mb-0 text-muted">Degree Courses</h3>
                        <hr class="my-3">
                    </div>
                    <div class="form-container" id="degreecourseFormContainer">

                        <form method="POST" :action="url" id="predataMainForm" class="mx-col">


                            <input type="hidden" name="_csrf_token" value="<?php echo $token; ?>">
                            <h3 class="mb-0 text-muted">Search Parameters</h3>
                            <hr class="my-3">
                            <div class="row mb-3">

                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="form-control-label col-md-4" for="input-email"> Degree Type</label>
                                        <div class="input-group col-md-8">
                                            <select class="form-control" id="dataCategorySelect" name="degreetype_id" v-model.number="degreetype_select" @change="getDegreeAwards">
                                                <?php
                                                if (count($degreetypes) > 0) {
                                                    echo "<option disabled value=''>Choose...</option>";
                                                    foreach ($degreetypes as $degreetype) {
                                                        ?>
                                                        <option value="<?php echo $degreetype->DegreeTypeID ?>"><?php echo $degreetype->DegreeType; ?></option>
                                                    <?php
                                                    }
                                                } else {
                                                    echo " <option value=''>---- No data available --</option>";
                                                }
                                                ?>
                                            </select>

                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="form-control-label col-md-4" for="input-email">Program Type</label>
                                        <div class="input-group col-md-8">
                                            <select class="form-control" id="dataCategorySelect" name="programtype_id" v-model.number="programtype_select">
                                                <?php
                                                if (count($programtypes) > 0) {
                                                    echo "<option disabled value=''>Choose...</option>";
                                                    foreach ($programtypes as $programtype) {
                                                        ?>
                                                        <option value="<?php echo $programtype->ProgramTypeID ?>"><?php echo $programtype->ProgramType; ?></option>
                                                    <?php
                                                    }
                                                } else {
                                                    echo " <option value=''>---- No data available --</option>";
                                                }
                                                ?>
                                            </select>

                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="row mb-3">

                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="form-control-label col-md-4" for="input-email">Faculty</label>
                                        <div class="input-group col-md-8">
                                            <select class="form-control" id="dataCategorySelect" name="faculty_id" v-model.number="faculty_select" @change="getDepartments">
                                                <?php
                                                if (count($faculties) > 0) {
                                                    echo "<option disabled value=''>Choose...</option>";
                                                    foreach ($faculties as $faculty) {
                                                        ?>
                                                        <option value="<?php echo $faculty->FacultyID ?>"><?php echo $faculty->FacultyName; ?></option>
                                                    <?php
                                                    }
                                                } else {
                                                    echo " <option value=''>---- No data available --</option>";
                                                }
                                                ?>
                                            </select>

                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="form-control-label col-md-4" for="input-email">Department</label>
                                        <div class="input-group col-md-8">
                                            <select class="form-control" id="departments" name="department_id" v-model.number="department_select" @change="getDegreeCourses">
                                                <option value='' disabled>Choose Department</option>
                                            </select>

                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-md-8 mb-3">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="form-control-label" for="input-email"> Select Degree Course</label>
                                            <div class="input-group">
                                                <select id="degreecourses" v-model.number="degreecourse_select" name="degreecourse_id" class="form-control" @change="getDegreeCourse">
                                                   <option value=''>Choose Degree Courses</option>
                                                </select>
                                                <div class="input-group-append">
                                                    <button id='edittoggler' class="btn btn-primary" :disabled="disabledButton" @click="">
                                                        <input type="checkbox" id="" v-model:checked="checked" v-if="!disabledButton">
                                                        <span>Edit</span>
                                                    </button>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="form-control-label" for="input-email">Duration</label>
                                            <input type="text" class="form-control" name="degreecourse_duration" v-model="degreecourse_duration">
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="form-control-label" for="input-email">Degree Course</label>
                                            <input type="text" class="form-control" name="degreecourse" v-model="degreecourse">
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group ">
                                            <label class="form-control-label" for="input-email">Degree Award</label>
                                            <div class="input-group ">
                                                <select class="form-control" id="degreeawards" name="degreeaward_id" v-model.number="degreeaward_select">
                                                    <option value='' disabled>Choose Degree Award</option>
                                                </select>

                                            </div>
                                        </div>
                                    </div>







                                    <div class="col-md-12 mb-3">
                                        <div class="form-group ">
                                            <button type="submit" name="submit" class="btn btn-primary">Save <i class="fa fa-save"></i></button>
                                            <button class="btn btn-danger" @click="deleteUrl">Delete <i class="fa fa-trash"></i>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="alert alert-danger" id="predata_ajax_error" v-if="hasError">
                                            {{error}}
                                        </div>

                                    </div>
                                </div>




                            </div>


                        </form>

                    </div>
                </div>



            </div>



        </div>
        <!-- Footer -->
        <footer class="footer">
            <div class="row align-items-center justify-content-xl-between">
                <div class="col-xl-6">
                    <div class="copyright text-center text-xl-left text-muted">
                        &copy; 2019 <a href="#" class="font-weight-bold ml-1" target="_blank">Softech Global Associates.</a> All Rights Reserved.
                    </div>
                </div>
                <div class="col-xl-6">
                    <ul class="nav nav-footer justify-content-center justify-content-xl-end">
                        <div class="copyright text-center text-xl-right text-muted">
                            <a href="#">Policy</a>
                        </div>
                    </ul>
                </div>
            </div>
        </footer>
    </div>
</div>
<!-- end of content section of this page -->

<!-- important scrpts goes here -->
<?php
include BASE_URL . '/setup/scripts.php';
?>
<!-- important script ends here -->


<script>
    let predataForm = new Vue({
        el: '#degreecourseFormContainer',
        data: {
            disabledButton: true,
            url: 'save/degreecourse',
            degreetype_select: '',
            programtype_select: '',
            faculty_select: '',
            department_select: '',
            degreeaward_select: '',
            degreecourse_select: '',
            degreecourse_duration: '',
            degreecourse: '',
            show_edit: false,
            checked: false,

            hasError: false,
            error: '',
        },

        watch: {
            degreecourse_select: function(val){
                isNaN(val) ? this.disabledButton = true : this.disabledButton = false;
            },
           checked: function(val){
            if (val) {
                    this.url = 'update/degreecourse';

                    if (!isNaN(this.degreecourse_select)) {
                        this.getDegreeCourse();
                    }
                } else {
                    this.url = 'save/degreecourse'
                    this.degreecourse = ''
                    this.degreecourse_duration = ''
                    this.degreeaward_select = ''
                }
           }
        },
        methods: {

            getDepartments() {

                $vm = this;
                axios.get('api/get/faculty/departments', {
                    params: {
                        faculty_id: this.faculty_select
                    }
                }).then(function(response) {

                    //reset departments to empty
                    $('#departments').text('');
                    // reset the Department value
                    $vm.Department = ''
                    if(! isNaN($vm.department_select)){
                        $vm.getDegreeCourses();
                    }
                   
                    if (response.data.length > 0) {

                        $('#departments').append("<option value='' >Choose Department...</option>");

                        $.each(response.data, function($key, $obj) {

                            $('#departments').append('<option value=' + $obj.DeptID + '>' + $obj.DeptName + '</option>');
                        });

                    } else {

                        $('#departments').append("<option value=''  > No data found </option>");
                    }


                }).catch(function(error) {

                    $vm.error = error.message;
                   $vm.hasError = true
                   setTimeout(function(){
                       $vm.hasError = false;
                       $vm.error = '';
                   },3000);
                });
            },
            getDegreeAwards() {

                $vm = this;
                axios.get('api/degreetype/degreeawards', {
                    params: {
                        degreetype_id: this.degreetype_select
                    }
                }).then(function(response) {

                    //reset departments to empty
                    $('#degreeawards').text('');
                    // reset the Department value
                    $vm.Department = ''
                    if(! isNaN($vm.department_select)){
                        $vm.getDegreeCourses();
                    }
                   
                    
                    if (response.data.length > 0) {

                        $('#degreeawards').append("<option value='' >Choose Degree Award...</option>");

                        $.each(response.data, function($key, $obj) {

                            $('#degreeawards').append('<option value=' + $obj.DegreeAwardID + '>' + $obj.DegreeAward + '</option>');
                        });

                    } else {

                        $('#degreeawards').append("<option value=''  > No data found </option>");
                    }


                }).catch(function(error) {

                    $vm.error = error.message;
                   $vm.hasError = true
                   setTimeout(function(){
                       $vm.hasError = false;
                       $vm.error = '';
                   },3000);
                });
            },
            getDegreeCourses() {
                $vm = this;
                axios.get('api/degreecourses', {
                    params: {
                        degreetype_id: this.degreetype_select,
                        programtype_id: this.programtype_select,
                        department_id: this.department_select
                    }
                }).then(function(response) {

                    //reset departments to empty
                    $('#degreecourses').text('');
                    // reset the Department value
                    $vm.Department = ''
                   
                    if (response.data.length > 0) {

                        $('#degreecourses').append("<option value='' >Choose Degree Course...</option>");

                        $.each(response.data, function($key, $obj) {

                            $('#degreecourses').append('<option value=' + $obj.DegreeCourseID + '>' + $obj.DegreeCourse + '</option>');
                        });

                    } else {

                        $('#degreecourses').append("<option value=''  > No data found </option>");
                    }


                }).catch(function(error) {

                   $vm.error = error.message;
                   $vm.hasError = true
                   setTimeout(function(){
                       $vm.hasError = false;
                       $vm.error = '';
                   },3000);
                });
            },
            getDegreeCourse(){
                if(!this.checked){
                    return;
                }
                $vm = this;
                axios.get('api/degreecourse',{
                    params: {
                        degreecourse_id : this.degreecourse_select
                    }
                }).then(function(response){
                    console.log(response)
                    $vm.degreecourse = response.data.DegreeCourse;
                    $vm.degreecourse_duration = response.data.DegreeCourseDuration
                    $vm.degreeaward_select = response.data.DegreeAwardID
                }).catch(function(error){
                    $vm.error = error.message;
                   $vm.hasError = true
                   setTimeout(function(){
                       $vm.hasError = false;
                       $vm.error = '';
                   },3000);
                });
            },


            deleteUrl() {
                this.url = 'delete/degreecourse';
                console.log('am deleteing this data')
                return true;
            }
        }

    });

  
</script>
<!-- page script content goes here -->
<script type="text/javacript"></script>
<!-- page script contents ends here -->
<?php
include BASE_URL . '/setup/footer.php';
?>