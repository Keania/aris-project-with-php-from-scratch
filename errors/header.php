<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="ARIS - University of Port Harcourt">
  <meta name="author" content="Keme Kenneth">
  <title> <?php echo $page_title;?></title>
  <!-- Favicon -->
  <link href="../assets/img/brand/favicon.png" rel="icon" type="image/png">
  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
  <!-- Icons -->
  <link href="<?php echo resource('public/assets/vendor/nucleo/css/nucleo.css');?>" rel="stylesheet">
  <link href="<?php echo resource('public/assets/vendor/@fortawesome/fontawesome-free/css/all.min.css');?>" rel="stylesheet">
  <!-- Argon CSS -->
  <link type="text/css" href="<?php echo resource('public/assets/css/argon.css?v=1.0.0');?>" rel="stylesheet">
  <!-- Custom / Overwriting CSS -->
  <link type="text/css" href="<?php echo resource('public/assets/css/aris.custom.css?v=1.0.0');?>" rel="stylesheet">
  <!-- Bootstrap DataTables -->
  <link href="<?php echo resource('public/assets/css/dataTables.min.css');?>" rel="stylesheet">
  <!-- <link href="../public/assets/css/dataTable.bootstrap4.min.css" rel="stylesheet"> -->
</head>

<body>
  <!-- Sidenav -->
  