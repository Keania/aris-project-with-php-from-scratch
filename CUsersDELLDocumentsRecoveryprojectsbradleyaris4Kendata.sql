-- MySQL dump 10.17  Distrib 10.3.11-MariaDB, for Win64 (AMD64)
--
-- Host: localhost    Database: aris_2019
-- ------------------------------------------------------
-- Server version	10.3.11-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tblacademiccourse`
--

DROP TABLE IF EXISTS `tblacademiccourse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblacademiccourse` (
  `CourseID` int(11) NOT NULL AUTO_INCREMENT,
  `CourseCode` varchar(10) NOT NULL,
  `CourseTitle` varchar(200) NOT NULL,
  `CourseUnit` int(11) NOT NULL,
  `CourseSemesterID` int(11) NOT NULL,
  `CourseDescription` text DEFAULT NULL,
  `CourseLevelID` int(11) NOT NULL,
  `DegreeTypeID` int(11) NOT NULL,
  `ServiceDeptID` int(11) NOT NULL,
  PRIMARY KEY (`CourseID`),
  UNIQUE KEY `CourseCode` (`CourseCode`,`CourseTitle`),
  KEY `CourseLevelID` (`CourseLevelID`,`DegreeTypeID`,`CourseSemesterID`,`ServiceDeptID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblacademiccourse`
--

LOCK TABLES `tblacademiccourse` WRITE;
/*!40000 ALTER TABLE `tblacademiccourse` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblacademiccourse` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblacademicsession`
--

DROP TABLE IF EXISTS `tblacademicsession`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblacademicsession` (
  `SessionID` int(11) NOT NULL AUTO_INCREMENT,
  `SessionName` varchar(10) NOT NULL,
  `SessionStatusID` int(11) NOT NULL,
  `BillingRate` double DEFAULT NULL,
  `BillingCharge` double DEFAULT NULL,
  `PopulationCount` int(11) DEFAULT NULL,
  PRIMARY KEY (`SessionID`),
  UNIQUE KEY `SessionName` (`SessionName`),
  KEY `SessionStatusID` (`SessionStatusID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblacademicsession`
--

LOCK TABLES `tblacademicsession` WRITE;
/*!40000 ALTER TABLE `tblacademicsession` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblacademicsession` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblacademicsession_mgt`
--

DROP TABLE IF EXISTS `tblacademicsession_mgt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblacademicsession_mgt` (
  `SessionMgtID` int(11) NOT NULL AUTO_INCREMENT,
  `SessionID` int(11) NOT NULL,
  `ProgramTypeID` int(11) NOT NULL,
  `DegreeTypeID` int(11) NOT NULL,
  `SessionStatusID` int(11) NOT NULL,
  `RegistrationStatusID` int(11) NOT NULL,
  `FeePaymentControlID` int(11) NOT NULL,
  `AdmissionStatusID` int(11) NOT NULL,
  `ResultUpdateStatusID` int(11) NOT NULL,
  PRIMARY KEY (`SessionMgtID`),
  KEY `SessionID` (`SessionID`,`ProgramTypeID`,`DegreeTypeID`,`SessionStatusID`,`RegistrationStatusID`,`FeePaymentControlID`,`AdmissionStatusID`,`ResultUpdateStatusID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblacademicsession_mgt`
--

LOCK TABLES `tblacademicsession_mgt` WRITE;
/*!40000 ALTER TABLE `tblacademicsession_mgt` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblacademicsession_mgt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblaccessrequest`
--

DROP TABLE IF EXISTS `tblaccessrequest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblaccessrequest` (
  `RequestID` int(11) NOT NULL AUTO_INCREMENT,
  `GuardianID` int(11) NOT NULL,
  `StudentID` int(11) NOT NULL,
  `RequestTimeStamp` datetime NOT NULL DEFAULT current_timestamp(),
  `RequestStatusID` int(11) NOT NULL,
  `AcceptanceTimeStamp` datetime DEFAULT NULL,
  `AccessStatusID` int(11) DEFAULT NULL,
  PRIMARY KEY (`RequestID`),
  KEY `GuardianID` (`GuardianID`,`StudentID`,`RequestStatusID`,`AccessStatusID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblaccessrequest`
--

LOCK TABLES `tblaccessrequest` WRITE;
/*!40000 ALTER TABLE `tblaccessrequest` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblaccessrequest` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbladmission`
--

DROP TABLE IF EXISTS `tbladmission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbladmission` (
  `AdmissionID` int(11) NOT NULL AUTO_INCREMENT,
  `RegNo` varchar(100) NOT NULL,
  `Surname` varchar(100) NOT NULL,
  `Others` varchar(300) NOT NULL,
  `Sex` varchar(10) NOT NULL,
  `Title` varchar(20) DEFAULT NULL,
  `Religion` varchar(20) DEFAULT NULL,
  `MaritalStatus` varchar(30) DEFAULT NULL,
  `DofB` date DEFAULT NULL,
  `PofB` varchar(100) DEFAULT NULL,
  `Hometown` varchar(100) DEFAULT NULL,
  `HomeAddress` varchar(300) DEFAULT NULL,
  `Phone` varchar(20) DEFAULT NULL,
  `Email` varchar(50) DEFAULT NULL,
  `NationalityID` int(11) DEFAULT NULL,
  `LGAID` int(11) DEFAULT NULL,
  `Photo` varchar(50) DEFAULT NULL,
  `AdmYear` int(11) DEFAULT NULL,
  `AdmissionModeID` int(11) NOT NULL,
  `DegreeCourseID` int(11) NOT NULL,
  `BatchNo` int(11) NOT NULL,
  `Score` int(11) DEFAULT NULL,
  `AdmissionStatusID` int(11) NOT NULL,
  `AcceptanceStatusID` int(11) DEFAULT NULL,
  `SessionID` int(11) NOT NULL,
  `FatherName` varchar(100) DEFAULT NULL,
  `FatherPhone` varchar(20) DEFAULT NULL,
  `FatherOccupation` varchar(100) DEFAULT NULL,
  `FatherAddress` varchar(200) DEFAULT NULL,
  `MotherName` varchar(100) DEFAULT NULL,
  `MotherPhone` varchar(20) DEFAULT NULL,
  `MotherOccupation` varchar(100) DEFAULT NULL,
  `MotherAddress` varchar(200) DEFAULT NULL,
  `KinName` varchar(100) DEFAULT NULL,
  `KinPhone` varchar(20) DEFAULT NULL,
  `KinOccupation` varchar(100) DEFAULT NULL,
  `KinAddress` varchar(200) DEFAULT NULL,
  `CandidatePwd` varchar(500) DEFAULT NULL,
  `MedicalExamDate` date DEFAULT NULL,
  `EntryQualification` varchar(20) DEFAULT NULL,
  `UndertakingID` int(11) DEFAULT NULL,
  `EligibilityTestStatusID` int(11) DEFAULT NULL,
  `VerificationStatusID1` int(11) DEFAULT NULL,
  `VerificationStatusID2` int(11) DEFAULT NULL,
  `VerificationStatusID3` int(11) DEFAULT NULL,
  `VerificationOfficerID1` int(11) DEFAULT NULL,
  `VerificationOfficerID2` int(11) DEFAULT NULL,
  `VerificationOfficerID3` int(11) DEFAULT NULL,
  `VerificationDate1` datetime DEFAULT NULL,
  `VerificationDate2` datetime DEFAULT NULL,
  `VerificationDate3` datetime DEFAULT NULL,
  `NewDegreeCourseID` int(11) DEFAULT NULL,
  `DefermentStatusID` int(11) DEFAULT NULL,
  `DefermentNo` varchar(100) DEFAULT NULL,
  `YearDeferred` int(11) DEFAULT NULL,
  `DefermentReason` varchar(100) DEFAULT NULL,
  `SessionID_Returned` int(11) DEFAULT NULL,
  `Waiver_Acceptance` int(11) DEFAULT NULL,
  `Waiver_Accommodation` int(11) DEFAULT NULL,
  `Waiver_SchoolFees` int(11) DEFAULT NULL,
  PRIMARY KEY (`AdmissionID`),
  UNIQUE KEY `RegNo` (`RegNo`),
  KEY `NationalityID` (`NationalityID`,`LGAID`,`AdmYear`,`AdmissionModeID`,`DegreeCourseID`,`BatchNo`,`AdmissionStatusID`,`AcceptanceStatusID`,`SessionID`,`VerificationOfficerID1`,`VerificationOfficerID2`,`VerificationOfficerID3`,`NewDegreeCourseID`,`SessionID_Returned`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbladmission`
--

LOCK TABLES `tbladmission` WRITE;
/*!40000 ALTER TABLE `tbladmission` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbladmission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblassignedcourse`
--

DROP TABLE IF EXISTS `tblassignedcourse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblassignedcourse` (
  `AssignedCourseID` int(11) NOT NULL AUTO_INCREMENT,
  `SessionID` int(11) NOT NULL,
  `ProgramTypeID` int(11) NOT NULL,
  `CourseID` int(11) NOT NULL,
  `LecturerID` int(11) NOT NULL,
  `HODID` int(11) DEFAULT NULL,
  `DeanID` int(11) DEFAULT NULL,
  `SenateRepID` int(11) DEFAULT NULL,
  `ApprovalDate1` date DEFAULT NULL,
  `ApprovalDate2` date DEFAULT NULL,
  `ApprovalDate3` date DEFAULT NULL,
  `ApprovalDate4` date DEFAULT NULL,
  PRIMARY KEY (`AssignedCourseID`),
  KEY `SessionID` (`SessionID`,`ProgramTypeID`,`CourseID`,`LecturerID`,`HODID`,`DeanID`,`SenateRepID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblassignedcourse`
--

LOCK TABLES `tblassignedcourse` WRITE;
/*!40000 ALTER TABLE `tblassignedcourse` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblassignedcourse` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblcolour`
--

DROP TABLE IF EXISTS `tblcolour`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblcolour` (
  `ColourID` int(11) NOT NULL AUTO_INCREMENT,
  `ColourName` varchar(50) NOT NULL,
  PRIMARY KEY (`ColourID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblcolour`
--

LOCK TABLES `tblcolour` WRITE;
/*!40000 ALTER TABLE `tblcolour` DISABLE KEYS */;
INSERT INTO `tblcolour` VALUES (6,'Green'),(7,'Blue-Green'),(8,'Grey'),(9,'GREENISHa');
/*!40000 ALTER TABLE `tblcolour` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblcourselevel`
--

DROP TABLE IF EXISTS `tblcourselevel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblcourselevel` (
  `LevelID` int(11) NOT NULL AUTO_INCREMENT,
  `DegreeCourseID` int(11) NOT NULL,
  `AcademicAdviserID` int(11) DEFAULT NULL,
  `CourseLevelID` int(11) NOT NULL,
  `Max1` int(11) NOT NULL,
  `Min1` int(11) NOT NULL,
  `Max2` int(11) NOT NULL,
  `Min2` int(11) NOT NULL,
  `Max3` int(11) NOT NULL,
  `Min3` int(11) NOT NULL,
  PRIMARY KEY (`LevelID`),
  KEY `DegreeCourseID` (`DegreeCourseID`,`AcademicAdviserID`,`CourseLevelID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblcourselevel`
--

LOCK TABLES `tblcourselevel` WRITE;
/*!40000 ALTER TABLE `tblcourselevel` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblcourselevel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbldegreeaward`
--

DROP TABLE IF EXISTS `tbldegreeaward`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbldegreeaward` (
  `DegreeAwardID` int(11) NOT NULL AUTO_INCREMENT,
  `DegreeAward` varchar(200) NOT NULL,
  `DegreeAwardCode` varchar(50) NOT NULL,
  `DegreeTypeID` int(11) NOT NULL,
  PRIMARY KEY (`DegreeAwardID`),
  KEY `DegreeTypeID` (`DegreeTypeID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbldegreeaward`
--

LOCK TABLES `tbldegreeaward` WRITE;
/*!40000 ALTER TABLE `tbldegreeaward` DISABLE KEYS */;
INSERT INTO `tbldegreeaward` VALUES (2,'OND4','5678TY',2),(3,'OBDGH','76tTYR',2),(4,'FF658','434278',2);
/*!40000 ALTER TABLE `tbldegreeaward` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbldegreecourse`
--

DROP TABLE IF EXISTS `tbldegreecourse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbldegreecourse` (
  `DegreeCourseID` int(11) NOT NULL AUTO_INCREMENT,
  `DeptID` int(11) NOT NULL,
  `DegreeCourse` varchar(200) NOT NULL,
  `DegreeTypeID` int(11) NOT NULL,
  `ProgramTypeID` int(11) NOT NULL,
  `DegreeCourseDuration` int(11) NOT NULL,
  `DegreeAwardID` int(11) NOT NULL,
  PRIMARY KEY (`DegreeCourseID`),
  KEY `DeptID` (`DeptID`,`DegreeTypeID`,`ProgramTypeID`,`DegreeAwardID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbldegreecourse`
--

LOCK TABLES `tbldegreecourse` WRITE;
/*!40000 ALTER TABLE `tbldegreecourse` DISABLE KEYS */;
INSERT INTO `tbldegreecourse` VALUES (1,3,'OVMIS',2,1,23,2);
/*!40000 ALTER TABLE `tbldegreecourse` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbldegreetype`
--

DROP TABLE IF EXISTS `tbldegreetype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbldegreetype` (
  `DegreeTypeID` int(11) NOT NULL AUTO_INCREMENT,
  `DegreeType` varchar(50) NOT NULL,
  PRIMARY KEY (`DegreeTypeID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbldegreetype`
--

LOCK TABLES `tbldegreetype` WRITE;
/*!40000 ALTER TABLE `tbldegreetype` DISABLE KEYS */;
INSERT INTO `tbldegreetype` VALUES (2,'Bachelors mapping'),(3,'Spinsters Cells'),(4,'Windows XP');
/*!40000 ALTER TABLE `tbldegreetype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbldepartment`
--

DROP TABLE IF EXISTS `tbldepartment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbldepartment` (
  `DeptID` int(11) NOT NULL AUTO_INCREMENT,
  `FacultyID` int(11) NOT NULL,
  `DeptName` varchar(100) NOT NULL,
  `DigitCode` varchar(5) DEFAULT NULL,
  `DeptCode` varchar(5) DEFAULT NULL,
  `HODID` int(11) DEFAULT NULL,
  PRIMARY KEY (`DeptID`),
  UNIQUE KEY `DigitCode` (`DigitCode`,`DeptCode`),
  KEY `FacultyID` (`FacultyID`,`HODID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbldepartment`
--

LOCK TABLES `tbldepartment` WRITE;
/*!40000 ALTER TABLE `tbldepartment` DISABLE KEYS */;
INSERT INTO `tbldepartment` VALUES (1,1,'FISHERY','398','FSH',17),(2,1,'FORESTRY','5646','FORE',0),(3,3,'NUERAL NETWORKS','4556','NNE',0);
/*!40000 ALTER TABLE `tbldepartment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbleligibilityjamb`
--

DROP TABLE IF EXISTS `tbleligibilityjamb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbleligibilityjamb` (
  `JambCheckID` int(11) NOT NULL AUTO_INCREMENT,
  `SessionID` int(11) NOT NULL,
  `AdmissionID` int(11) NOT NULL,
  `DegreeCourseID` int(11) NOT NULL,
  `OLevelID` int(11) NOT NULL,
  `JambSubjectID` int(11) NOT NULL,
  `SubjectScore` int(11) NOT NULL,
  PRIMARY KEY (`JambCheckID`),
  KEY `SessionID` (`SessionID`,`AdmissionID`,`DegreeCourseID`,`OLevelID`,`JambSubjectID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbleligibilityjamb`
--

LOCK TABLES `tbleligibilityjamb` WRITE;
/*!40000 ALTER TABLE `tbleligibilityjamb` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbleligibilityjamb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbleligibilityolevel`
--

DROP TABLE IF EXISTS `tbleligibilityolevel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbleligibilityolevel` (
  `OLevelCheckID` int(11) NOT NULL AUTO_INCREMENT,
  `SessionID` int(11) NOT NULL,
  `AdmissionID` int(11) NOT NULL,
  `DegreeCourseID` int(11) NOT NULL,
  `OLevelID` int(11) NOT NULL,
  `OLevelSubjectID` int(11) NOT NULL,
  `EarnedGradeID` int(11) NOT NULL,
  PRIMARY KEY (`OLevelCheckID`),
  KEY `SessionID` (`SessionID`,`AdmissionID`,`DegreeCourseID`,`OLevelID`,`OLevelSubjectID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbleligibilityolevel`
--

LOCK TABLES `tbleligibilityolevel` WRITE;
/*!40000 ALTER TABLE `tbleligibilityolevel` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbleligibilityolevel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblentryrequirement`
--

DROP TABLE IF EXISTS `tblentryrequirement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblentryrequirement` (
  `RequirementID` int(11) NOT NULL AUTO_INCREMENT,
  `SessionID` int(11) NOT NULL,
  `DegreeCourseID` int(11) NOT NULL,
  `EntryCategoryID` int(11) NOT NULL,
  `SubjectID` int(11) NOT NULL,
  `MinGradeID` int(11) NOT NULL,
  `CompulsoryID` int(11) NOT NULL,
  PRIMARY KEY (`RequirementID`),
  KEY `SessionID` (`SessionID`,`DegreeCourseID`,`EntryCategoryID`,`SubjectID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblentryrequirement`
--

LOCK TABLES `tblentryrequirement` WRITE;
/*!40000 ALTER TABLE `tblentryrequirement` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblentryrequirement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblentryrequirement_alternate`
--

DROP TABLE IF EXISTS `tblentryrequirement_alternate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblentryrequirement_alternate` (
  `AltRequirementID` int(11) NOT NULL AUTO_INCREMENT,
  `DegreeCourseID` int(11) NOT NULL,
  `SessionID` int(11) NOT NULL,
  `EntryCategoryID` int(11) NOT NULL,
  `SubjectID` int(11) NOT NULL,
  `SubjectID_Alt` int(11) NOT NULL,
  `MinGradeID` int(11) NOT NULL,
  `CompulsoryID` int(11) NOT NULL,
  PRIMARY KEY (`AltRequirementID`),
  KEY `DegreeCourseID` (`DegreeCourseID`,`SessionID`,`SubjectID`,`SubjectID_Alt`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblentryrequirement_alternate`
--

LOCK TABLES `tblentryrequirement_alternate` WRITE;
/*!40000 ALTER TABLE `tblentryrequirement_alternate` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblentryrequirement_alternate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblfaculty`
--

DROP TABLE IF EXISTS `tblfaculty`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblfaculty` (
  `FacultyID` int(11) NOT NULL AUTO_INCREMENT,
  `FacultyName` varchar(100) NOT NULL,
  `FacultyCode` varchar(10) NOT NULL,
  `FacultyColourID` int(11) DEFAULT NULL,
  `FacultyDeanID` int(11) DEFAULT NULL,
  PRIMARY KEY (`FacultyID`),
  UNIQUE KEY `FacultyCode` (`FacultyCode`),
  KEY `FacultyColourID` (`FacultyColourID`,`FacultyDeanID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblfaculty`
--

LOCK TABLES `tblfaculty` WRITE;
/*!40000 ALTER TABLE `tblfaculty` DISABLE KEYS */;
INSERT INTO `tblfaculty` VALUES (1,'AGRICULTURAL SCIENCES','AGR',1,19),(3,'COMPUTER SCIENCE','CS',1,0),(4,'MANAGEMENT SCIENCES','MN54',6,0);
/*!40000 ALTER TABLE `tblfaculty` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblfeedetails`
--

DROP TABLE IF EXISTS `tblfeedetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblfeedetails` (
  `FeeDetailID` int(11) NOT NULL AUTO_INCREMENT,
  `FeeID` int(11) NOT NULL,
  `FeeItemID` int(11) NOT NULL,
  `FeeCategoryID` int(11) NOT NULL,
  `FacultyID` int(11) DEFAULT NULL,
  `DeptID` int(11) DEFAULT NULL,
  `DegreeCourseID` int(11) DEFAULT NULL,
  `CourseLevelID` int(11) DEFAULT NULL,
  `FeeAmount` double NOT NULL,
  `2Installments1` double DEFAULT NULL,
  `2Installments2` double DEFAULT NULL,
  `3Installments1` double DEFAULT NULL,
  `3Installments2` double DEFAULT NULL,
  `3Installments3` double DEFAULT NULL,
  PRIMARY KEY (`FeeDetailID`),
  KEY `FeeID` (`FeeID`,`FeeItemID`,`FacultyID`,`DeptID`,`DegreeCourseID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblfeedetails`
--

LOCK TABLES `tblfeedetails` WRITE;
/*!40000 ALTER TABLE `tblfeedetails` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblfeedetails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblfeeitems`
--

DROP TABLE IF EXISTS `tblfeeitems`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblfeeitems` (
  `FeeItemID` int(11) NOT NULL AUTO_INCREMENT,
  `FeeItem` varchar(50) NOT NULL,
  `FeeTypeID` int(11) NOT NULL,
  PRIMARY KEY (`FeeItemID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblfeeitems`
--

LOCK TABLES `tblfeeitems` WRITE;
/*!40000 ALTER TABLE `tblfeeitems` DISABLE KEYS */;
INSERT INTO `tblfeeitems` VALUES (2,'HOSTEL ALLLOWANCES AND TEE',2);
/*!40000 ALTER TABLE `tblfeeitems` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblfees`
--

DROP TABLE IF EXISTS `tblfees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblfees` (
  `FeeID` int(11) NOT NULL AUTO_INCREMENT,
  `SessionID` int(11) NOT NULL,
  `DegreeTypeID` int(11) NOT NULL,
  `ProgramTypeID` int(11) NOT NULL,
  `FeeDeadlineDate` date DEFAULT NULL,
  `FeeClosureDate` date DEFAULT NULL,
  `SurchargeAccountID` int(11) DEFAULT NULL,
  PRIMARY KEY (`FeeID`),
  KEY `SessionID` (`SessionID`,`DegreeTypeID`,`ProgramTypeID`,`SurchargeAccountID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblfees`
--

LOCK TABLES `tblfees` WRITE;
/*!40000 ALTER TABLE `tblfees` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblfees` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblfeesothers`
--

DROP TABLE IF EXISTS `tblfeesothers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblfeesothers` (
  `OtherFeeID` int(11) NOT NULL AUTO_INCREMENT,
  `SessionID` int(11) NOT NULL,
  `DegreeTypeID` int(11) NOT NULL,
  `ProgramTypeID` int(11) NOT NULL,
  `FeeTypeID` int(11) NOT NULL,
  `FeeItemID` int(11) NOT NULL,
  `OtherFeeAmount` double NOT NULL,
  `FeeDeadlineDate` date DEFAULT NULL,
  `FeeClosureDate` date DEFAULT NULL,
  PRIMARY KEY (`OtherFeeID`),
  KEY `SessionID` (`SessionID`,`DegreeTypeID`,`ProgramTypeID`,`FeeTypeID`,`FeeItemID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblfeesothers`
--

LOCK TABLES `tblfeesothers` WRITE;
/*!40000 ALTER TABLE `tblfeesothers` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblfeesothers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblguardian`
--

DROP TABLE IF EXISTS `tblguardian`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblguardian` (
  `GuardianID` int(11) NOT NULL AUTO_INCREMENT,
  `Surname` varchar(100) NOT NULL,
  `Others` varchar(200) NOT NULL,
  `Title` varchar(20) NOT NULL,
  `Email` varchar(100) NOT NULL,
  `Phone` varchar(50) NOT NULL,
  `Address` varchar(200) NOT NULL,
  `UserPwd` varchar(100) DEFAULT NULL,
  `UserPIN` varchar(100) DEFAULT NULL,
  `PwdQuestion1` varchar(100) DEFAULT NULL,
  `PwdAnswer1` varchar(100) DEFAULT NULL,
  `PwdQuestion2` varchar(100) DEFAULT NULL,
  `PwdAnswer2` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`GuardianID`),
  UNIQUE KEY `Email` (`Email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblguardian`
--

LOCK TABLES `tblguardian` WRITE;
/*!40000 ALTER TABLE `tblguardian` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblguardian` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblhostel`
--

DROP TABLE IF EXISTS `tblhostel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblhostel` (
  `HostelID` int(11) NOT NULL AUTO_INCREMENT,
  `HostelName` varchar(100) NOT NULL,
  `HostelTypeID` int(11) NOT NULL,
  `DegreeTypeID` int(11) NOT NULL,
  `HostelCapacity` int(11) NOT NULL,
  PRIMARY KEY (`HostelID`),
  KEY `HostelTypeID` (`HostelTypeID`,`DegreeTypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblhostel`
--

LOCK TABLES `tblhostel` WRITE;
/*!40000 ALTER TABLE `tblhostel` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblhostel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblhostelmanagement`
--

DROP TABLE IF EXISTS `tblhostelmanagement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblhostelmanagement` (
  `BookingID` int(11) NOT NULL AUTO_INCREMENT,
  `StudentID` int(11) DEFAULT NULL,
  `AdmissionID` int(11) DEFAULT NULL,
  `SessionID` int(11) NOT NULL,
  `BookingDate` date NOT NULL,
  `BookingTime` time NOT NULL,
  `BookingstatusID` int(11) NOT NULL,
  `ConfirmedByID` int(11) DEFAULT NULL,
  `ConfrimationDate` date DEFAULT NULL,
  `ConfrimationTime` time DEFAULT NULL,
  `HostelID` int(11) DEFAULT NULL,
  `AllocatedByID` int(11) DEFAULT NULL,
  `AllocationDate` date DEFAULT NULL,
  `AllocationTime` time DEFAULT NULL,
  PRIMARY KEY (`BookingID`),
  KEY `StudentID` (`StudentID`,`AdmissionID`,`SessionID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblhostelmanagement`
--

LOCK TABLES `tblhostelmanagement` WRITE;
/*!40000 ALTER TABLE `tblhostelmanagement` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblhostelmanagement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblimport_admission`
--

DROP TABLE IF EXISTS `tblimport_admission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblimport_admission` (
  `ImportID` int(11) NOT NULL AUTO_INCREMENT,
  `RegNo` varchar(100) NOT NULL,
  `Surname` varchar(100) NOT NULL,
  `Others` varchar(100) NOT NULL,
  `Sex` varchar(6) NOT NULL,
  `DegreeCourseID` int(11) NOT NULL,
  `SessionID` int(11) NOT NULL,
  `AdmissionModeID` int(11) NOT NULL,
  `AdmYear` int(11) NOT NULL,
  `BatchNo` int(11) NOT NULL,
  `Score` int(11) NOT NULL,
  PRIMARY KEY (`ImportID`),
  KEY `DegreeCourseID` (`DegreeCourseID`,`SessionID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblimport_admission`
--

LOCK TABLES `tblimport_admission` WRITE;
/*!40000 ALTER TABLE `tblimport_admission` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblimport_admission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblimport_results`
--

DROP TABLE IF EXISTS `tblimport_results`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblimport_results` (
  `ImportID` int(11) NOT NULL AUTO_INCREMENT,
  `CourseRegID` int(11) NOT NULL,
  `Surname` varchar(100) NOT NULL,
  `Others` varchar(100) NOT NULL,
  `MatNo` varchar(20) NOT NULL,
  `ImportQ1` int(11) NOT NULL,
  `ImportQ2` int(11) NOT NULL,
  `ImportQ3` int(11) NOT NULL,
  `ImportQ4` int(11) NOT NULL,
  `ImportQ5` int(11) NOT NULL,
  `ImportQ6` int(11) NOT NULL,
  `ImportCA` int(11) NOT NULL,
  `ImportModeration` int(11) NOT NULL,
  `ImportPenalty` int(11) NOT NULL,
  `ImportBorderline` int(11) NOT NULL,
  `ImportExam` int(11) NOT NULL,
  `ImportedScore` int(11) NOT NULL,
  `SessionID` int(11) NOT NULL,
  `SemesterID` int(11) NOT NULL,
  `ProgramTypeID` int(11) NOT NULL,
  `AssignedCourseID` int(11) NOT NULL,
  `ActionByID` int(11) NOT NULL,
  PRIMARY KEY (`ImportID`),
  KEY `CourseRegID` (`CourseRegID`,`SessionID`,`SemesterID`,`ProgramTypeID`,`AssignedCourseID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblimport_results`
--

LOCK TABLES `tblimport_results` WRITE;
/*!40000 ALTER TABLE `tblimport_results` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblimport_results` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbllga`
--

DROP TABLE IF EXISTS `tbllga`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbllga` (
  `LGAID` int(11) NOT NULL AUTO_INCREMENT,
  `StateID` int(11) NOT NULL,
  `LGA` varchar(100) NOT NULL,
  PRIMARY KEY (`LGAID`),
  KEY `StateID` (`StateID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbllga`
--

LOCK TABLES `tbllga` WRITE;
/*!40000 ALTER TABLE `tbllga` DISABLE KEYS */;
INSERT INTO `tbllga` VALUES (1,1,'Khana Local Government');
/*!40000 ALTER TABLE `tbllga` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblmedicalexamregime`
--

DROP TABLE IF EXISTS `tblmedicalexamregime`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblmedicalexamregime` (
  `MedicalRegimeID` int(11) NOT NULL AUTO_INCREMENT,
  `SessionID` int(11) NOT NULL,
  `DegreeTypeID` int(11) NOT NULL,
  `ProgramTypeID` int(11) NOT NULL,
  `StartDate` date DEFAULT NULL,
  `DailyCapacity` int(11) DEFAULT NULL,
  `SchedulingPlanID` int(11) DEFAULT NULL,
  `RegimeStatusID` int(11) DEFAULT NULL,
  PRIMARY KEY (`MedicalRegimeID`),
  KEY `SessionID` (`SessionID`,`DegreeTypeID`,`ProgramTypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblmedicalexamregime`
--

LOCK TABLES `tblmedicalexamregime` WRITE;
/*!40000 ALTER TABLE `tblmedicalexamregime` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblmedicalexamregime` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbloleveldetails`
--

DROP TABLE IF EXISTS `tbloleveldetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbloleveldetails` (
  `OLevelID` int(11) NOT NULL AUTO_INCREMENT,
  `AdmissionID` int(11) NOT NULL,
  `ExamCenter` varchar(200) NOT NULL,
  `ExamNumber` varchar(100) NOT NULL,
  `ExamType` varchar(30) NOT NULL,
  `ExamYear` int(11) NOT NULL,
  PRIMARY KEY (`OLevelID`),
  KEY `AdmissionID` (`AdmissionID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbloleveldetails`
--

LOCK TABLES `tbloleveldetails` WRITE;
/*!40000 ALTER TABLE `tbloleveldetails` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbloleveldetails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblonlineclearance`
--

DROP TABLE IF EXISTS `tblonlineclearance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblonlineclearance` (
  `ClearanceID` int(11) NOT NULL AUTO_INCREMENT,
  `StudentID` int(11) NOT NULL,
  `ApplicationDate` date NOT NULL,
  `ClearanceStatusID1` int(11) NOT NULL,
  `ClearanceOfficerID1` int(11) NOT NULL,
  `ClearanceDate1` date NOT NULL,
  `ClearanceStatusID2` int(11) NOT NULL,
  `ClearanceOfficerID2` int(11) NOT NULL,
  `ClearanceDate2` date NOT NULL,
  `ClearanceStatusID3` int(11) NOT NULL,
  `ClearanceOfficerID3` int(11) NOT NULL,
  `ClearanceDate3` date NOT NULL,
  `ClearanceStatusID4` int(11) NOT NULL,
  `ClearanceOfficerID4` int(11) NOT NULL,
  `ClearanceDate4` date NOT NULL,
  `ClearanceStatusID5` int(11) NOT NULL,
  `ClearanceOfficerID5` int(11) NOT NULL,
  `ClearanceDate5` date NOT NULL,
  PRIMARY KEY (`ClearanceID`),
  KEY `ClearanceOfficerID1` (`ClearanceOfficerID1`,`ClearanceOfficerID2`,`ClearanceOfficerID3`,`ClearanceOfficerID4`,`ClearanceOfficerID5`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblonlineclearance`
--

LOCK TABLES `tblonlineclearance` WRITE;
/*!40000 ALTER TABLE `tblonlineclearance` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblonlineclearance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblpendingfailedcourses`
--

DROP TABLE IF EXISTS `tblpendingfailedcourses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblpendingfailedcourses` (
  `CourseChkID` int(11) NOT NULL AUTO_INCREMENT,
  `StudentID` int(11) NOT NULL,
  `SessionID` int(11) NOT NULL,
  `CourseID` int(11) NOT NULL,
  `FailedStatusID` int(11) NOT NULL,
  PRIMARY KEY (`CourseChkID`),
  KEY `StudentID` (`StudentID`,`SessionID`,`CourseID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblpendingfailedcourses`
--

LOCK TABLES `tblpendingfailedcourses` WRITE;
/*!40000 ALTER TABLE `tblpendingfailedcourses` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblpendingfailedcourses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblportalconfig`
--

DROP TABLE IF EXISTS `tblportalconfig`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblportalconfig` (
  `InstitutionID` int(11) NOT NULL AUTO_INCREMENT,
  `InstitutionName` varchar(100) NOT NULL,
  `InstitutionAddress` varchar(300) NOT NULL,
  `InstitutionPrimaryLogo` varchar(50) NOT NULL,
  `InstitutionSecondaryLogo` varchar(50) NOT NULL,
  `PortalID` varchar(50) NOT NULL,
  PRIMARY KEY (`InstitutionID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblportalconfig`
--

LOCK TABLES `tblportalconfig` WRITE;
/*!40000 ALTER TABLE `tblportalconfig` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblportalconfig` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblpredata`
--

DROP TABLE IF EXISTS `tblpredata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblpredata` (
  `DataID` int(11) NOT NULL AUTO_INCREMENT,
  `CategoryID` int(11) NOT NULL,
  `Data` varchar(100) NOT NULL,
  PRIMARY KEY (`DataID`),
  KEY `CategoryID` (`CategoryID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblpredata`
--

LOCK TABLES `tblpredata` WRITE;
/*!40000 ALTER TABLE `tblpredata` DISABLE KEYS */;
INSERT INTO `tblpredata` VALUES (7,1,'mr');
/*!40000 ALTER TABLE `tblpredata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblpredatacategory`
--

DROP TABLE IF EXISTS `tblpredatacategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblpredatacategory` (
  `CategoryID` int(11) NOT NULL AUTO_INCREMENT,
  `DataCategory` varchar(50) NOT NULL,
  PRIMARY KEY (`CategoryID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblpredatacategory`
--

LOCK TABLES `tblpredatacategory` WRITE;
/*!40000 ALTER TABLE `tblpredatacategory` DISABLE KEYS */;
INSERT INTO `tblpredatacategory` VALUES (1,'TITLES');
/*!40000 ALTER TABLE `tblpredatacategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblprogramtype`
--

DROP TABLE IF EXISTS `tblprogramtype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblprogramtype` (
  `ProgramTypeID` int(11) NOT NULL AUTO_INCREMENT,
  `ProgramType` varchar(30) NOT NULL,
  `ProgramTypeCode` varchar(5) NOT NULL,
  PRIMARY KEY (`ProgramTypeID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblprogramtype`
--

LOCK TABLES `tblprogramtype` WRITE;
/*!40000 ALTER TABLE `tblprogramtype` DISABLE KEYS */;
INSERT INTO `tblprogramtype` VALUES (1,'IJMB','00456'),(3,'NAPEDP','CT56');
/*!40000 ALTER TABLE `tblprogramtype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblrecommendedcourses`
--

DROP TABLE IF EXISTS `tblrecommendedcourses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblrecommendedcourses` (
  `RecommendedCourseID` int(11) NOT NULL AUTO_INCREMENT,
  `CourseID` int(11) NOT NULL,
  `LevelID` int(11) NOT NULL,
  `CourseCategoryID` int(11) NOT NULL,
  `AppliedCourseUnit` int(11) NOT NULL,
  `AppliedSemesterID` int(11) NOT NULL,
  `ApplicableID` int(11) NOT NULL,
  PRIMARY KEY (`RecommendedCourseID`),
  KEY `CourseID` (`CourseID`,`LevelID`,`AppliedSemesterID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblrecommendedcourses`
--

LOCK TABLES `tblrecommendedcourses` WRITE;
/*!40000 ALTER TABLE `tblrecommendedcourses` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblrecommendedcourses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblregistration_courses`
--

DROP TABLE IF EXISTS `tblregistration_courses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblregistration_courses` (
  `CourseRegID` int(11) NOT NULL AUTO_INCREMENT,
  `StudentRegID` int(11) NOT NULL,
  `SemesterID` int(11) NOT NULL,
  `AssignedCourseID` int(11) NOT NULL,
  `CourseUnit` int(11) NOT NULL,
  `Score` int(11) DEFAULT NULL,
  `Grade` varchar(5) DEFAULT NULL,
  `GradePoint` double DEFAULT NULL,
  `ModByID` int(11) DEFAULT NULL,
  `ModDateStamp` datetime DEFAULT NULL,
  `ExcessRegID` int(11) DEFAULT NULL,
  `PreviousScore` int(11) DEFAULT NULL,
  `PreviousGrade` varchar(5) DEFAULT NULL,
  `Q1` int(11) DEFAULT NULL,
  `Q2` int(11) DEFAULT NULL,
  `Q3` int(11) DEFAULT NULL,
  `Q4` int(11) DEFAULT NULL,
  `Q5` int(11) DEFAULT NULL,
  `Q6` int(11) DEFAULT NULL,
  `CA` int(11) DEFAULT NULL,
  `Moderation` int(11) DEFAULT NULL,
  `Penalty` int(11) DEFAULT NULL,
  `Borderline` int(11) DEFAULT NULL,
  `Exam` int(11) DEFAULT NULL,
  `RegistrationDateStamp` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`CourseRegID`),
  KEY `StudentRegID` (`StudentRegID`,`SemesterID`,`AssignedCourseID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblregistration_courses`
--

LOCK TABLES `tblregistration_courses` WRITE;
/*!40000 ALTER TABLE `tblregistration_courses` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblregistration_courses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblregistration_excess`
--

DROP TABLE IF EXISTS `tblregistration_excess`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblregistration_excess` (
  `ExcessRegID` int(11) NOT NULL AUTO_INCREMENT,
  `StudentRegID` int(11) NOT NULL,
  `SemesterID` int(11) NOT NULL,
  `AssignedCourseID` int(11) NOT NULL,
  `CourseUnit` int(11) NOT NULL,
  `RegistrationDateStamp` datetime NOT NULL,
  PRIMARY KEY (`ExcessRegID`),
  KEY `StudentRegID` (`StudentRegID`,`SemesterID`,`AssignedCourseID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblregistration_excess`
--

LOCK TABLES `tblregistration_excess` WRITE;
/*!40000 ALTER TABLE `tblregistration_excess` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblregistration_excess` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblregistration_students`
--

DROP TABLE IF EXISTS `tblregistration_students`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblregistration_students` (
  `StudentRegID` int(11) NOT NULL AUTO_INCREMENT,
  `StudentID` int(11) NOT NULL,
  `SessionID` int(11) NOT NULL,
  `LevelID` int(11) NOT NULL,
  `Max1` int(11) NOT NULL,
  `Min1` int(11) NOT NULL,
  `Max2` int(11) NOT NULL,
  `Min2` int(11) NOT NULL,
  `Max3` int(11) NOT NULL,
  `Min3` int(11) NOT NULL,
  `RegDate` date NOT NULL,
  `AdviserApproval` int(11) DEFAULT NULL,
  `HODApproval` int(11) DEFAULT NULL,
  `DeanApproval` int(11) DEFAULT NULL,
  `ApprovalDate1` date DEFAULT NULL,
  `ApprovalDate2` date DEFAULT NULL,
  `ApprovalDate3` date DEFAULT NULL,
  `RegistrationDateStamp` datetime NOT NULL,
  PRIMARY KEY (`StudentRegID`),
  KEY `StudentID` (`StudentID`,`SessionID`,`LevelID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblregistration_students`
--

LOCK TABLES `tblregistration_students` WRITE;
/*!40000 ALTER TABLE `tblregistration_students` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblregistration_students` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblservicetypes`
--

DROP TABLE IF EXISTS `tblservicetypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblservicetypes` (
  `ServiceID` int(11) NOT NULL AUTO_INCREMENT,
  `ServiceTypeID` varchar(50) NOT NULL,
  `ServiceTypeName` varchar(100) NOT NULL,
  `StudentCategoryID` int(11) NOT NULL,
  `InstallmentCount` int(11) NOT NULL,
  `ProgramTypeID` int(11) NOT NULL,
  `DegreeTypeID` int(11) NOT NULL,
  `FeeTypeID` int(11) NOT NULL,
  `FeeItemID` int(11) NOT NULL,
  `FacultyID` int(11) DEFAULT NULL,
  `DeptID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ServiceID`),
  KEY `ProgramTypeID` (`ProgramTypeID`,`DegreeTypeID`,`FeeItemID`,`FacultyID`,`DeptID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblservicetypes`
--

LOCK TABLES `tblservicetypes` WRITE;
/*!40000 ALTER TABLE `tblservicetypes` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblservicetypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblstate`
--

DROP TABLE IF EXISTS `tblstate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblstate` (
  `StateID` int(11) NOT NULL AUTO_INCREMENT,
  `State` varchar(50) NOT NULL,
  PRIMARY KEY (`StateID`),
  UNIQUE KEY `State` (`State`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblstate`
--

LOCK TABLES `tblstate` WRITE;
/*!40000 ALTER TABLE `tblstate` DISABLE KEYS */;
INSERT INTO `tblstate` VALUES (1,'Rivers State');
/*!40000 ALTER TABLE `tblstate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblstudent`
--

DROP TABLE IF EXISTS `tblstudent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblstudent` (
  `StudentID` int(11) NOT NULL AUTO_INCREMENT,
  `AdmissionID` int(11) DEFAULT NULL,
  `RegNo` varchar(100) NOT NULL,
  `MatNo` varchar(100) DEFAULT NULL,
  `Surname` varchar(100) NOT NULL,
  `Others` varchar(300) NOT NULL,
  `Sex` varchar(10) NOT NULL,
  `Title` varchar(20) DEFAULT NULL,
  `Religion` varchar(20) DEFAULT NULL,
  `MaritalStatus` varchar(30) DEFAULT NULL,
  `DofB` date DEFAULT NULL,
  `PofB` varchar(100) DEFAULT NULL,
  `Hometown` varchar(100) DEFAULT NULL,
  `HomeAddress` varchar(300) DEFAULT NULL,
  `Phone` varchar(20) DEFAULT NULL,
  `Email` varchar(50) DEFAULT NULL,
  `NationalityID` int(11) DEFAULT NULL,
  `LGAID` int(11) DEFAULT NULL,
  `Photo` varchar(50) DEFAULT NULL,
  `AdmYear` int(11) DEFAULT NULL,
  `AdmissionModeID` int(11) NOT NULL,
  `DegreeCourseID` int(11) NOT NULL,
  `EntryQualification` varchar(20) DEFAULT NULL,
  `StatusID` int(11) NOT NULL,
  `GradYear` int(11) DEFAULT NULL,
  `GraduationStatusID` int(11) NOT NULL,
  `StudentPIN` varchar(200) DEFAULT NULL,
  `StudentPwd` varchar(200) DEFAULT NULL,
  `FatherName` varchar(100) DEFAULT NULL,
  `FatherPhone` varchar(20) DEFAULT NULL,
  `FatherOccupation` varchar(100) DEFAULT NULL,
  `FatherAddress` varchar(200) DEFAULT NULL,
  `MotherName` varchar(100) DEFAULT NULL,
  `MotherPhone` varchar(20) DEFAULT NULL,
  `MotherOccupation` varchar(100) DEFAULT NULL,
  `MotherAddress` varchar(200) DEFAULT NULL,
  `KinName` varchar(100) DEFAULT NULL,
  `KinPhone` varchar(20) DEFAULT NULL,
  `KinOccupation` varchar(100) DEFAULT NULL,
  `KinAddress` varchar(200) DEFAULT NULL,
  `AdditionalYear` int(11) NOT NULL,
  `FinalClearanceDate` date DEFAULT NULL,
  `ClearanceStatusID` int(11) DEFAULT NULL,
  `PwdQuestion1` varchar(200) DEFAULT NULL,
  `PwdAnswer1` varchar(200) DEFAULT NULL,
  `PwdQuestion2` varchar(200) DEFAULT NULL,
  `PwdAnswer2` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`StudentID`),
  UNIQUE KEY `RegNo` (`RegNo`),
  KEY `NationalityID` (`NationalityID`,`LGAID`,`AdmYear`,`AdmissionModeID`,`DegreeCourseID`),
  KEY `AdmissionID` (`AdmissionID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblstudent`
--

LOCK TABLES `tblstudent` WRITE;
/*!40000 ALTER TABLE `tblstudent` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblstudent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblsubjects`
--

DROP TABLE IF EXISTS `tblsubjects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblsubjects` (
  `SubjectID` int(11) NOT NULL AUTO_INCREMENT,
  `Subject` varchar(50) NOT NULL,
  PRIMARY KEY (`SubjectID`),
  UNIQUE KEY `Subject` (`Subject`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblsubjects`
--

LOCK TABLES `tblsubjects` WRITE;
/*!40000 ALTER TABLE `tblsubjects` DISABLE KEYS */;
INSERT INTO `tblsubjects` VALUES (4,'Chemistry'),(3,'English'),(1,'Mathematics');
/*!40000 ALTER TABLE `tblsubjects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbltemporalwithdrawal`
--

DROP TABLE IF EXISTS `tbltemporalwithdrawal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbltemporalwithdrawal` (
  `WithdrawalID` int(11) NOT NULL AUTO_INCREMENT,
  `StudentID` int(11) NOT NULL,
  `WithdrawalReason` varchar(200) NOT NULL,
  `ActionDate1` date NOT NULL,
  `ActionByID1` int(11) NOT NULL,
  `SessionID_Readmitted` int(11) DEFAULT NULL,
  `ActionDate2` date DEFAULT NULL,
  `ActionByID2` int(11) DEFAULT NULL,
  `WithdrawalStatusID` int(11) NOT NULL,
  PRIMARY KEY (`WithdrawalID`),
  KEY `StudentID` (`StudentID`,`SessionID_Readmitted`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbltemporalwithdrawal`
--

LOCK TABLES `tbltemporalwithdrawal` WRITE;
/*!40000 ALTER TABLE `tbltemporalwithdrawal` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbltemporalwithdrawal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbltrnxs`
--

DROP TABLE IF EXISTS `tbltrnxs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbltrnxs` (
  `TrnxID` int(11) NOT NULL AUTO_INCREMENT,
  `StudentID` int(11) NOT NULL,
  `AdmissionID` int(11) NOT NULL,
  `SessionID` int(11) NOT NULL,
  `LevelID` int(11) NOT NULL,
  `FeeID` int(11) NOT NULL,
  `FeeDetailID` int(11) NOT NULL,
  `FeeTypeID` int(11) NOT NULL,
  `FeeItemID` int(11) NOT NULL,
  `InstallmentCount` int(11) NOT NULL,
  `TrnxType` varchar(3) NOT NULL,
  `TrnxDate` date NOT NULL,
  `TrnxDesc` varchar(100) NOT NULL,
  `DRAmount` double NOT NULL,
  `CRAmount` double NOT NULL,
  `RRR` varchar(100) DEFAULT NULL,
  `OrderID` varchar(100) DEFAULT NULL,
  `ServiceTypeID` varchar(50) DEFAULT NULL,
  `PaymentID` int(11) DEFAULT NULL,
  `VerifiedByID` int(11) DEFAULT NULL,
  `Remark` varchar(100) DEFAULT NULL,
  `TrnxDateStamp` datetime NOT NULL,
  PRIMARY KEY (`TrnxID`),
  KEY `StudentID` (`StudentID`,`AdmissionID`,`SessionID`,`LevelID`,`FeeID`,`FeeDetailID`,`FeeItemID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbltrnxs`
--

LOCK TABLES `tbltrnxs` WRITE;
/*!40000 ALTER TABLE `tbltrnxs` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbltrnxs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbltrnxs_orders`
--

DROP TABLE IF EXISTS `tbltrnxs_orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbltrnxs_orders` (
  `PaymentID` int(11) NOT NULL AUTO_INCREMENT,
  `TrnxID` int(11) NOT NULL,
  `StudentID` int(11) DEFAULT NULL,
  `AdmissionID` int(11) DEFAULT NULL,
  `SessionID` int(11) NOT NULL,
  `LevelID` int(11) NOT NULL,
  `FeeID` int(11) DEFAULT NULL,
  `FeeDetailID` int(11) DEFAULT NULL,
  `FeeTypeID` int(11) NOT NULL,
  `FeeItemID` int(11) NOT NULL,
  `InstallmentCount` int(11) NOT NULL,
  `RRR` varchar(50) NOT NULL,
  `OrderID` varchar(100) NOT NULL,
  `ServiceTypeID` varchar(50) NOT NULL,
  `TrnxType` varchar(3) NOT NULL,
  `TrnxDate` date NOT NULL,
  `TrnxDesc` varchar(200) NOT NULL,
  `DRAmount` double NOT NULL,
  `CRAmount` double NOT NULL,
  `VerifiedByID` int(11) DEFAULT NULL,
  `Remark` varchar(100) DEFAULT NULL,
  `StudyYear` int(11) DEFAULT NULL,
  `TrnxDateStamp` datetime NOT NULL,
  PRIMARY KEY (`PaymentID`),
  UNIQUE KEY `RRR` (`RRR`,`OrderID`),
  KEY `TrnxID` (`TrnxID`,`StudentID`,`AdmissionID`,`SessionID`,`LevelID`,`FeeID`,`FeeDetailID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbltrnxs_orders`
--

LOCK TABLES `tbltrnxs_orders` WRITE;
/*!40000 ALTER TABLE `tbltrnxs_orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbltrnxs_orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbluser`
--

DROP TABLE IF EXISTS `tbluser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbluser` (
  `UserID` int(11) NOT NULL AUTO_INCREMENT,
  `UserPIN` varchar(100) DEFAULT NULL,
  `UserPwd` varchar(100) DEFAULT NULL,
  `Surname` varchar(100) NOT NULL,
  `Others` varchar(200) NOT NULL,
  `Sex` varchar(10) NOT NULL,
  `Title` varchar(50) NOT NULL,
  `UserGroupID` int(11) NOT NULL,
  `UserCategoryID` int(11) NOT NULL,
  `UserDeptID` int(11) DEFAULT NULL,
  `UserStatusID` int(11) NOT NULL,
  `Email` varchar(100) DEFAULT NULL,
  `Phone` varchar(50) DEFAULT NULL,
  `ContactAddress` varchar(200) DEFAULT NULL,
  `StaffNumber` varchar(100) DEFAULT NULL,
  `DyofB` int(11) DEFAULT NULL,
  `MofB` int(11) DEFAULT NULL,
  `EmploymentDate` date DEFAULT NULL,
  `AppointmentDate` date DEFAULT NULL,
  `GradeLevel` int(11) DEFAULT NULL,
  `Photo` varchar(200) DEFAULT NULL,
  `PwdQuestion1` varchar(200) DEFAULT NULL,
  `PwdAnswer1` varchar(200) DEFAULT NULL,
  `PwdQuestion2` varchar(200) DEFAULT NULL,
  `PwdAnswer2` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`UserID`),
  KEY `UserGroupID` (`UserGroupID`,`UserCategoryID`,`UserDeptID`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbluser`
--

LOCK TABLES `tbluser` WRITE;
/*!40000 ALTER TABLE `tbluser` DISABLE KEYS */;
INSERT INTO `tbluser` VALUES (15,'X-FSH-00FBB','1b0ebc798a387fb3ad32b69bb5529a307f77fbe3','BROWNING','BERK RICHARDSON','FEMALE','CLEAN SHOT',2,1,1,0,'mitchell.aaliyah@bogisich.com','08185695490','No. 12 Akambawa Street, Egbelu','45RT',14,9,'2019-07-18','2019-07-23',7,NULL,NULL,NULL,NULL,NULL),(17,'X-FSH-0011HP','8fc63075e6559a758700fad979fec5dff3eb3e7f','HOBBS','STEVEN','FEMALE','SOFT TOUCH',1,1,1,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(18,'X-FSH-0012RM','b34ad05ce61c795d9c9526b260f625aa6170ca74','RUSH','MIKAYLA MAYO','MALE','CLEAN SHOT',2,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(19,'X-FSH-0013DO','0d83eb09e8ae8ef9489a7b6f4a60d67396ef88dc','DAVIS','OTTO FREEMAN','MALE','CLEAN SHOT',1,1,1,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(20,'X-NNE-0014KK','e0cc5943dc80015e963758b0eb8bde502ce3d0e5','KENNETH','KEME','MALE','MR',1,1,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(21,'X-NNE-0015PE','e418b28ca500e2e5d4cff533808f138a8fddc692','PETER','ESTHER','FEMALE','MR',1,1,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `tbluser` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblusercategory`
--

DROP TABLE IF EXISTS `tblusercategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblusercategory` (
  `UserCategoryID` int(11) NOT NULL AUTO_INCREMENT,
  `UserCategory` varchar(100) NOT NULL,
  PRIMARY KEY (`UserCategoryID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblusercategory`
--

LOCK TABLES `tblusercategory` WRITE;
/*!40000 ALTER TABLE `tblusercategory` DISABLE KEYS */;
INSERT INTO `tblusercategory` VALUES (1,'test'),(2,'test');
/*!40000 ALTER TABLE `tblusercategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblverificationofficers`
--

DROP TABLE IF EXISTS `tblverificationofficers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblverificationofficers` (
  `VerificationOfficerID` int(11) NOT NULL AUTO_INCREMENT,
  `AccessLevelID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `FacultyID` int(11) DEFAULT NULL,
  PRIMARY KEY (`VerificationOfficerID`),
  KEY `UserID` (`UserID`,`FacultyID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblverificationofficers`
--

LOCK TABLES `tblverificationofficers` WRITE;
/*!40000 ALTER TABLE `tblverificationofficers` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblverificationofficers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `vw1_predata`
--

DROP TABLE IF EXISTS `vw1_predata`;
/*!50001 DROP VIEW IF EXISTS `vw1_predata`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw1_predata` (
  `DataID` tinyint NOT NULL,
  `Data` tinyint NOT NULL,
  `CategoryID` tinyint NOT NULL,
  `DataCategory` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw2_academicsession`
--

DROP TABLE IF EXISTS `vw2_academicsession`;
/*!50001 DROP VIEW IF EXISTS `vw2_academicsession`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw2_academicsession` (
  `SessionID` tinyint NOT NULL,
  `SessionName` tinyint NOT NULL,
  `SessionFileName` tinyint NOT NULL,
  `SessionYear` tinyint NOT NULL,
  `SessionStatusID` tinyint NOT NULL,
  `SessionStatus` tinyint NOT NULL,
  `BillingRate` tinyint NOT NULL,
  `PopulationCount` tinyint NOT NULL,
  `BillingCharge` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw2_academicsession_mgt`
--

DROP TABLE IF EXISTS `vw2_academicsession_mgt`;
/*!50001 DROP VIEW IF EXISTS `vw2_academicsession_mgt`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw2_academicsession_mgt` (
  `SessionMgtID` tinyint NOT NULL,
  `SessionID` tinyint NOT NULL,
  `SessionName` tinyint NOT NULL,
  `ProgramTypeID` tinyint NOT NULL,
  `DegreeTypeID` tinyint NOT NULL,
  `FeePaymentControlID` tinyint NOT NULL,
  `RegistrationStatusID` tinyint NOT NULL,
  `ResultUpdateStatusID` tinyint NOT NULL,
  `AdmissionStatusID` tinyint NOT NULL,
  `SessionFileName` tinyint NOT NULL,
  `SessionYear` tinyint NOT NULL,
  `SessionStatusID` tinyint NOT NULL,
  `SessionStatus` tinyint NOT NULL,
  `FeePayment` tinyint NOT NULL,
  `RegistrationStatus` tinyint NOT NULL,
  `ResultUpdateStatus` tinyint NOT NULL,
  `AdmissionLockStatus` tinyint NOT NULL,
  `ProgramType` tinyint NOT NULL,
  `ProgramTypeCode` tinyint NOT NULL,
  `DegreeType` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw2_medicalexamregime`
--

DROP TABLE IF EXISTS `vw2_medicalexamregime`;
/*!50001 DROP VIEW IF EXISTS `vw2_medicalexamregime`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw2_medicalexamregime` (
  `MedicalRegimeID` tinyint NOT NULL,
  `SessionID` tinyint NOT NULL,
  `SessionName` tinyint NOT NULL,
  `SessionYear` tinyint NOT NULL,
  `SessionStatusID` tinyint NOT NULL,
  `SessionStatus` tinyint NOT NULL,
  `StartDate` tinyint NOT NULL,
  `DailyCapacity` tinyint NOT NULL,
  `SchedulingPlanID` tinyint NOT NULL,
  `RegimeStatusID` tinyint NOT NULL,
  `RegimeStatus` tinyint NOT NULL,
  `SchedulingPlan` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw3_academiccourse`
--

DROP TABLE IF EXISTS `vw3_academiccourse`;
/*!50001 DROP VIEW IF EXISTS `vw3_academiccourse`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw3_academiccourse` (
  `CourseID` tinyint NOT NULL,
  `CourseCode` tinyint NOT NULL,
  `CourseTitle` tinyint NOT NULL,
  `CourseUnit` tinyint NOT NULL,
  `CourseLevelID` tinyint NOT NULL,
  `CourseLevel` tinyint NOT NULL,
  `DegreeTypeID` tinyint NOT NULL,
  `DegreeType` tinyint NOT NULL,
  `CourseSemesterID` tinyint NOT NULL,
  `CourseSemester` tinyint NOT NULL,
  `CourseDescription` tinyint NOT NULL,
  `FacultyID` tinyint NOT NULL,
  `ServiceDeptID` tinyint NOT NULL,
  `DeptName` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw4_assignedcourse`
--

DROP TABLE IF EXISTS `vw4_assignedcourse`;
/*!50001 DROP VIEW IF EXISTS `vw4_assignedcourse`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw4_assignedcourse` (
  `AssignedCourseID` tinyint NOT NULL,
  `SessionID` tinyint NOT NULL,
  `ProgramTypeID` tinyint NOT NULL,
  `ProgramType` tinyint NOT NULL,
  `ProgramTypeCode` tinyint NOT NULL,
  `CourseID` tinyint NOT NULL,
  `CourseCode` tinyint NOT NULL,
  `CourseTitle` tinyint NOT NULL,
  `CourseUnit` tinyint NOT NULL,
  `CourseLevelID` tinyint NOT NULL,
  `CourseLevel` tinyint NOT NULL,
  `DegreeTypeID` tinyint NOT NULL,
  `DegreeType` tinyint NOT NULL,
  `CourseSemesterID` tinyint NOT NULL,
  `Semester` tinyint NOT NULL,
  `FacultyID` tinyint NOT NULL,
  `ServiceDeptID` tinyint NOT NULL,
  `DeptName` tinyint NOT NULL,
  `LecturerID` tinyint NOT NULL,
  `ResultApproval1ID` tinyint NOT NULL,
  `HODID` tinyint NOT NULL,
  `ResultApproval2ID` tinyint NOT NULL,
  `DeanID` tinyint NOT NULL,
  `ResultApproval3ID` tinyint NOT NULL,
  `SenateRepID` tinyint NOT NULL,
  `ResultApproval4ID` tinyint NOT NULL,
  `ApprovalDate1` tinyint NOT NULL,
  `ApprovalDate2` tinyint NOT NULL,
  `ApprovalDate3` tinyint NOT NULL,
  `ApprovalDate4` tinyint NOT NULL,
  `L1` tinyint NOT NULL,
  `L2` tinyint NOT NULL,
  `L3` tinyint NOT NULL,
  `L4` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw5_deptcount`
--

DROP TABLE IF EXISTS `vw5_deptcount`;
/*!50001 DROP VIEW IF EXISTS `vw5_deptcount`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw5_deptcount` (
  `FacultyID` tinyint NOT NULL,
  `DeptCount` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw6_faculty`
--

DROP TABLE IF EXISTS `vw6_faculty`;
/*!50001 DROP VIEW IF EXISTS `vw6_faculty`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw6_faculty` (
  `FacultyID` tinyint NOT NULL,
  `FacultyName` tinyint NOT NULL,
  `FacultyCode` tinyint NOT NULL,
  `FacultyColourID` tinyint NOT NULL,
  `ColourName` tinyint NOT NULL,
  `FacultyDeanID` tinyint NOT NULL,
  `FacultyDean` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw7_department`
--

DROP TABLE IF EXISTS `vw7_department`;
/*!50001 DROP VIEW IF EXISTS `vw7_department`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw7_department` (
  `DeptID` tinyint NOT NULL,
  `FacultyID` tinyint NOT NULL,
  `FacultyName` tinyint NOT NULL,
  `FacultyCode` tinyint NOT NULL,
  `FacultyColourID` tinyint NOT NULL,
  `ColourName` tinyint NOT NULL,
  `DeptName` tinyint NOT NULL,
  `DeptCode` tinyint NOT NULL,
  `DigitCode` tinyint NOT NULL,
  `HODID` tinyint NOT NULL,
  `HOD` tinyint NOT NULL,
  `FacultyDeanID` tinyint NOT NULL,
  `Dean` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw8_degreecourse`
--

DROP TABLE IF EXISTS `vw8_degreecourse`;
/*!50001 DROP VIEW IF EXISTS `vw8_degreecourse`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw8_degreecourse` (
  `DegreeCourseID` tinyint NOT NULL,
  `DeptID` tinyint NOT NULL,
  `FacultyID` tinyint NOT NULL,
  `FacultyName` tinyint NOT NULL,
  `FacultyCode` tinyint NOT NULL,
  `FacultyColourID` tinyint NOT NULL,
  `ColourName` tinyint NOT NULL,
  `DeptName` tinyint NOT NULL,
  `DeptCode` tinyint NOT NULL,
  `HODID` tinyint NOT NULL,
  `HOD` tinyint NOT NULL,
  `FacultyDeanID` tinyint NOT NULL,
  `Dean` tinyint NOT NULL,
  `DegreeCourse` tinyint NOT NULL,
  `DegreeTypeID` tinyint NOT NULL,
  `DegreeType` tinyint NOT NULL,
  `ProgramTypeID` tinyint NOT NULL,
  `ProgramType` tinyint NOT NULL,
  `ProgramTypeCode` tinyint NOT NULL,
  `DegreeCourseDuration` tinyint NOT NULL,
  `DegreeAwardID` tinyint NOT NULL,
  `DegreeAward` tinyint NOT NULL,
  `DegreeAwardCode` tinyint NOT NULL,
  `DigitCode` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw91_courselevel`
--

DROP TABLE IF EXISTS `vw91_courselevel`;
/*!50001 DROP VIEW IF EXISTS `vw91_courselevel`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw91_courselevel` (
  `LevelID` tinyint NOT NULL,
  `DegreeCourseID` tinyint NOT NULL,
  `DeptID` tinyint NOT NULL,
  `FacultyID` tinyint NOT NULL,
  `FacultyName` tinyint NOT NULL,
  `DeptName` tinyint NOT NULL,
  `HODID` tinyint NOT NULL,
  `HOD` tinyint NOT NULL,
  `FacultyDeanID` tinyint NOT NULL,
  `Dean` tinyint NOT NULL,
  `DegreeCourse` tinyint NOT NULL,
  `DegreeTypeID` tinyint NOT NULL,
  `DegreeType` tinyint NOT NULL,
  `ProgramTypeID` tinyint NOT NULL,
  `ProgramType` tinyint NOT NULL,
  `ProgramTypeCode` tinyint NOT NULL,
  `CourseLevelID` tinyint NOT NULL,
  `CourseLevel` tinyint NOT NULL,
  `AcademicAdviserID` tinyint NOT NULL,
  `AcademicAdviser` tinyint NOT NULL,
  `Max1` tinyint NOT NULL,
  `Min1` tinyint NOT NULL,
  `Max2` tinyint NOT NULL,
  `Min2` tinyint NOT NULL,
  `Max3` tinyint NOT NULL,
  `Min3` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw92_lga`
--

DROP TABLE IF EXISTS `vw92_lga`;
/*!50001 DROP VIEW IF EXISTS `vw92_lga`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw92_lga` (
  `LGAID` tinyint NOT NULL,
  `StateID` tinyint NOT NULL,
  `State` tinyint NOT NULL,
  `LGA` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw93_fees`
--

DROP TABLE IF EXISTS `vw93_fees`;
/*!50001 DROP VIEW IF EXISTS `vw93_fees`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw93_fees` (
  `FeeID` tinyint NOT NULL,
  `SessionID` tinyint NOT NULL,
  `FeeDeadlineDate` tinyint NOT NULL,
  `FeeClosureDate` tinyint NOT NULL,
  `SessionName` tinyint NOT NULL,
  `SessionYear` tinyint NOT NULL,
  `SessionStatusID` tinyint NOT NULL,
  `SessionStatus` tinyint NOT NULL,
  `FeePaymentControlID` tinyint NOT NULL,
  `FeePayment` tinyint NOT NULL,
  `ResultUpdateStatusID` tinyint NOT NULL,
  `ResultUpdateStatus` tinyint NOT NULL,
  `DegreeTypeID` tinyint NOT NULL,
  `DegreeType` tinyint NOT NULL,
  `ProgramTypeID` tinyint NOT NULL,
  `ProgramType` tinyint NOT NULL,
  `ProgramTypeCode` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw94_feedetails`
--

DROP TABLE IF EXISTS `vw94_feedetails`;
/*!50001 DROP VIEW IF EXISTS `vw94_feedetails`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw94_feedetails` (
  `FeeDetailID` tinyint NOT NULL,
  `FeeID` tinyint NOT NULL,
  `SessionID` tinyint NOT NULL,
  `SessionName` tinyint NOT NULL,
  `SessionYear` tinyint NOT NULL,
  `SessionStatusID` tinyint NOT NULL,
  `SessionStatus` tinyint NOT NULL,
  `FeePaymentControlID` tinyint NOT NULL,
  `FeePayment` tinyint NOT NULL,
  `ResultUpdateStatusID` tinyint NOT NULL,
  `ResultUpdateStatus` tinyint NOT NULL,
  `DegreeTypeID` tinyint NOT NULL,
  `DegreeType` tinyint NOT NULL,
  `ProgramTypeID` tinyint NOT NULL,
  `ProgramType` tinyint NOT NULL,
  `ProgramTypeCode` tinyint NOT NULL,
  `For` tinyint NOT NULL,
  `FeeItemID` tinyint NOT NULL,
  `FeeItem` tinyint NOT NULL,
  `FeeCategoryID` tinyint NOT NULL,
  `DegreeCourseID` tinyint NOT NULL,
  `CourseLevelID` tinyint NOT NULL,
  `CourseLevel` tinyint NOT NULL,
  `FacultyID` tinyint NOT NULL,
  `DeptID` tinyint NOT NULL,
  `FeeAmount` tinyint NOT NULL,
  `FeeCat` tinyint NOT NULL,
  `2installments1` tinyint NOT NULL,
  `2installments2` tinyint NOT NULL,
  `3installments1` tinyint NOT NULL,
  `3installments2` tinyint NOT NULL,
  `3installments3` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw94_feedetails1`
--

DROP TABLE IF EXISTS `vw94_feedetails1`;
/*!50001 DROP VIEW IF EXISTS `vw94_feedetails1`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw94_feedetails1` (
  `FeeDetailID` tinyint NOT NULL,
  `FeeID` tinyint NOT NULL,
  `SessionID` tinyint NOT NULL,
  `SessionName` tinyint NOT NULL,
  `SessionYear` tinyint NOT NULL,
  `SessionStatusID` tinyint NOT NULL,
  `SessionStatus` tinyint NOT NULL,
  `FeePaymentControlID` tinyint NOT NULL,
  `FeePayment` tinyint NOT NULL,
  `ResultUpdateStatusID` tinyint NOT NULL,
  `ResultUpdateStatus` tinyint NOT NULL,
  `DegreeTypeID` tinyint NOT NULL,
  `DegreeType` tinyint NOT NULL,
  `ProgramTypeID` tinyint NOT NULL,
  `ProgramType` tinyint NOT NULL,
  `ProgramTypeCode` tinyint NOT NULL,
  `FeeItemID` tinyint NOT NULL,
  `FeeItem` tinyint NOT NULL,
  `FeeCategoryID` tinyint NOT NULL,
  `FacultyName` tinyint NOT NULL,
  `DeptName` tinyint NOT NULL,
  `DegreeCourseID` tinyint NOT NULL,
  `FacultyID` tinyint NOT NULL,
  `DeptID` tinyint NOT NULL,
  `CourseLevelID` tinyint NOT NULL,
  `CourseLevel` tinyint NOT NULL,
  `FeeAmount` tinyint NOT NULL,
  `FeeRef` tinyint NOT NULL,
  `RefID` tinyint NOT NULL,
  `EligibleStudents` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw95_user`
--

DROP TABLE IF EXISTS `vw95_user`;
/*!50001 DROP VIEW IF EXISTS `vw95_user`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw95_user` (
  `UserID` tinyint NOT NULL,
  `UserPIN` tinyint NOT NULL,
  `SearchData` tinyint NOT NULL,
  `UserPwd` tinyint NOT NULL,
  `UserName` tinyint NOT NULL,
  `Surname` tinyint NOT NULL,
  `Others` tinyint NOT NULL,
  `Sex` tinyint NOT NULL,
  `Title` tinyint NOT NULL,
  `UserGroupID` tinyint NOT NULL,
  `UserGroup` tinyint NOT NULL,
  `UserCategoryID` tinyint NOT NULL,
  `UserCategory` tinyint NOT NULL,
  `UserDeptID` tinyint NOT NULL,
  `FacultyID` tinyint NOT NULL,
  `FacultyName` tinyint NOT NULL,
  `FacultyCode` tinyint NOT NULL,
  `FacultyColourID` tinyint NOT NULL,
  `ColourName` tinyint NOT NULL,
  `DeptName` tinyint NOT NULL,
  `DeptCode` tinyint NOT NULL,
  `Phone` tinyint NOT NULL,
  `Email` tinyint NOT NULL,
  `ContactAddress` tinyint NOT NULL,
  `UserStatusID` tinyint NOT NULL,
  `UserStatus` tinyint NOT NULL,
  `PIN` tinyint NOT NULL,
  `Photo` tinyint NOT NULL,
  `PwdQuestion1` tinyint NOT NULL,
  `PwdAnswer1` tinyint NOT NULL,
  `PwdQuestion2` tinyint NOT NULL,
  `PwdAnswer2` tinyint NOT NULL,
  `StaffNumber` tinyint NOT NULL,
  `DyofB` tinyint NOT NULL,
  `MofB` tinyint NOT NULL,
  `EmploymentDate` tinyint NOT NULL,
  `AppointmentDate` tinyint NOT NULL,
  `GradeLevel` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw96_admission`
--

DROP TABLE IF EXISTS `vw96_admission`;
/*!50001 DROP VIEW IF EXISTS `vw96_admission`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw96_admission` (
  `AdmissionID` tinyint NOT NULL,
  `RegNo` tinyint NOT NULL,
  `Surname` tinyint NOT NULL,
  `Others` tinyint NOT NULL,
  `Sex` tinyint NOT NULL,
  `Title` tinyint NOT NULL,
  `Religion` tinyint NOT NULL,
  `MaritalStatus` tinyint NOT NULL,
  `DofB` tinyint NOT NULL,
  `PofB` tinyint NOT NULL,
  `Hometown` tinyint NOT NULL,
  `HomeAddress` tinyint NOT NULL,
  `Phone` tinyint NOT NULL,
  `Email` tinyint NOT NULL,
  `NationalityID` tinyint NOT NULL,
  `LGAID` tinyint NOT NULL,
  `Photo` tinyint NOT NULL,
  `AdmYear` tinyint NOT NULL,
  `AdmissionModeID` tinyint NOT NULL,
  `DegreeCourseID` tinyint NOT NULL,
  `BatchNo` tinyint NOT NULL,
  `Score` tinyint NOT NULL,
  `AdmissionStatusID` tinyint NOT NULL,
  `AcceptanceStatusID` tinyint NOT NULL,
  `SessionID` tinyint NOT NULL,
  `FatherName` tinyint NOT NULL,
  `FatherPhone` tinyint NOT NULL,
  `FatherOccupation` tinyint NOT NULL,
  `FatherAddress` tinyint NOT NULL,
  `MotherName` tinyint NOT NULL,
  `MotherPhone` tinyint NOT NULL,
  `MotherOccupation` tinyint NOT NULL,
  `MotherAddress` tinyint NOT NULL,
  `KinName` tinyint NOT NULL,
  `KinPhone` tinyint NOT NULL,
  `KinOccupation` tinyint NOT NULL,
  `KinAddress` tinyint NOT NULL,
  `CandidatePwd` tinyint NOT NULL,
  `MedicalExamDate` tinyint NOT NULL,
  `EntryQualification` tinyint NOT NULL,
  `UndertakingID` tinyint NOT NULL,
  `EligibilityTestStatusID` tinyint NOT NULL,
  `VerificationStatusID1` tinyint NOT NULL,
  `VerificationStatusID2` tinyint NOT NULL,
  `VerificationStatusID3` tinyint NOT NULL,
  `VerificationOfficerID1` tinyint NOT NULL,
  `VerificationOfficerID2` tinyint NOT NULL,
  `VerificationOfficerID3` tinyint NOT NULL,
  `VerificationDate1` tinyint NOT NULL,
  `VerificationDate2` tinyint NOT NULL,
  `VerificationDate3` tinyint NOT NULL,
  `NewDegreeCourseID` tinyint NOT NULL,
  `DefermentStatusID` tinyint NOT NULL,
  `DefermentNo` tinyint NOT NULL,
  `YearDeferred` tinyint NOT NULL,
  `DefermentReason` tinyint NOT NULL,
  `SessionID_Returned` tinyint NOT NULL,
  `Waiver_Acceptance` tinyint NOT NULL,
  `Waiver_Accommodation` tinyint NOT NULL,
  `Waiver_SchoolFees` tinyint NOT NULL,
  `SearchData` tinyint NOT NULL,
  `AdmissionCandidate` tinyint NOT NULL,
  `Nationality` tinyint NOT NULL,
  `StateID` tinyint NOT NULL,
  `State` tinyint NOT NULL,
  `LGA` tinyint NOT NULL,
  `DeptID` tinyint NOT NULL,
  `DegreeType` tinyint NOT NULL,
  `DegreeTypeID` tinyint NOT NULL,
  `ProgramType` tinyint NOT NULL,
  `ProgramTypeCode` tinyint NOT NULL,
  `ProgramTypeID` tinyint NOT NULL,
  `FacultyID` tinyint NOT NULL,
  `FacultyName` tinyint NOT NULL,
  `DeptName` tinyint NOT NULL,
  `DeptCode` tinyint NOT NULL,
  `DegreeCourse` tinyint NOT NULL,
  `DegreeCourseDuration` tinyint NOT NULL,
  `AdmissionStatus` tinyint NOT NULL,
  `AcceptanceStatus` tinyint NOT NULL,
  `MaleCount` tinyint NOT NULL,
  `FemaleCount` tinyint NOT NULL,
  `Gender` tinyint NOT NULL,
  `AdmissionMode` tinyint NOT NULL,
  `EligibilityTestStatus` tinyint NOT NULL,
  `RegCheck1` tinyint NOT NULL,
  `RegCheck2` tinyint NOT NULL,
  `RegCheck3` tinyint NOT NULL,
  `RegCheck4` tinyint NOT NULL,
  `RegCheck5` tinyint NOT NULL,
  `AcceptedCount` tinyint NOT NULL,
  `NotAcceptedCount` tinyint NOT NULL,
  `ClearedCount` tinyint NOT NULL,
  `NotClearedCount` tinyint NOT NULL,
  `Verification1ClearedCount` tinyint NOT NULL,
  `Verification1NotClearedCount` tinyint NOT NULL,
  `Verification2ClearedCount` tinyint NOT NULL,
  `Verification2NotClearedCount` tinyint NOT NULL,
  `Verification3ClearedCount` tinyint NOT NULL,
  `Verification3NotClearedCount` tinyint NOT NULL,
  `IndigeneStatusID` tinyint NOT NULL,
  `IndigeneStatus` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw96_admission_summary`
--

DROP TABLE IF EXISTS `vw96_admission_summary`;
/*!50001 DROP VIEW IF EXISTS `vw96_admission_summary`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw96_admission_summary` (
  `TotalCount` tinyint NOT NULL,
  `MaleCount` tinyint NOT NULL,
  `FemaleCount` tinyint NOT NULL,
  `AcceptedCount` tinyint NOT NULL,
  `NotAcceptedCount` tinyint NOT NULL,
  `ClearedCount` tinyint NOT NULL,
  `NotClearedCount` tinyint NOT NULL,
  `Verification1ClearedCount` tinyint NOT NULL,
  `Verification1NotClearedCount` tinyint NOT NULL,
  `Verification2ClearedCount` tinyint NOT NULL,
  `Verification2NotClearedCount` tinyint NOT NULL,
  `Verification3ClearedCount` tinyint NOT NULL,
  `Verification3NotClearedCount` tinyint NOT NULL,
  `FacultyID` tinyint NOT NULL,
  `FacultyName` tinyint NOT NULL,
  `DegreeType` tinyint NOT NULL,
  `ProgramType` tinyint NOT NULL,
  `SessionID` tinyint NOT NULL,
  `ProgramTypeID` tinyint NOT NULL,
  `BatchNo` tinyint NOT NULL,
  `DegreeTypeID` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw977_guardians`
--

DROP TABLE IF EXISTS `vw977_guardians`;
/*!50001 DROP VIEW IF EXISTS `vw977_guardians`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw977_guardians` (
  `GuardianID` tinyint NOT NULL,
  `Surname` tinyint NOT NULL,
  `Others` tinyint NOT NULL,
  `Title` tinyint NOT NULL,
  `Email` tinyint NOT NULL,
  `Phone` tinyint NOT NULL,
  `Address` tinyint NOT NULL,
  `UserPwd` tinyint NOT NULL,
  `UserPIN` tinyint NOT NULL,
  `PwdQuestion1` tinyint NOT NULL,
  `PwdAnswer1` tinyint NOT NULL,
  `PwdQuestion2` tinyint NOT NULL,
  `PwdAnswer2` tinyint NOT NULL,
  `PIN` tinyint NOT NULL,
  `UserName` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw977_guardians_accessrequest`
--

DROP TABLE IF EXISTS `vw977_guardians_accessrequest`;
/*!50001 DROP VIEW IF EXISTS `vw977_guardians_accessrequest`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw977_guardians_accessrequest` (
  `RequestID` tinyint NOT NULL,
  `GuardianID` tinyint NOT NULL,
  `GuardianName` tinyint NOT NULL,
  `StudentID` tinyint NOT NULL,
  `StudentName` tinyint NOT NULL,
  `RegNo` tinyint NOT NULL,
  `MatNo` tinyint NOT NULL,
  `RequestTimeStamp` tinyint NOT NULL,
  `RequestStatusID` tinyint NOT NULL,
  `RequestStatus` tinyint NOT NULL,
  `AcceptanceTimeStamp` tinyint NOT NULL,
  `AccessStatusID` tinyint NOT NULL,
  `AccessStatus` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw97_student`
--

DROP TABLE IF EXISTS `vw97_student`;
/*!50001 DROP VIEW IF EXISTS `vw97_student`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw97_student` (
  `StudentID` tinyint NOT NULL,
  `AdmissionID` tinyint NOT NULL,
  `RegNo` tinyint NOT NULL,
  `MatNo` tinyint NOT NULL,
  `Surname` tinyint NOT NULL,
  `Others` tinyint NOT NULL,
  `Sex` tinyint NOT NULL,
  `Title` tinyint NOT NULL,
  `Religion` tinyint NOT NULL,
  `MaritalStatus` tinyint NOT NULL,
  `DofB` tinyint NOT NULL,
  `PofB` tinyint NOT NULL,
  `Hometown` tinyint NOT NULL,
  `HomeAddress` tinyint NOT NULL,
  `Phone` tinyint NOT NULL,
  `Email` tinyint NOT NULL,
  `NationalityID` tinyint NOT NULL,
  `LGAID` tinyint NOT NULL,
  `Photo` tinyint NOT NULL,
  `AdmYear` tinyint NOT NULL,
  `AdmissionModeID` tinyint NOT NULL,
  `DegreeCourseID` tinyint NOT NULL,
  `EntryQualification` tinyint NOT NULL,
  `StatusID` tinyint NOT NULL,
  `GradYear` tinyint NOT NULL,
  `GraduationStatusID` tinyint NOT NULL,
  `StudentPIN` tinyint NOT NULL,
  `StudentPwd` tinyint NOT NULL,
  `FatherName` tinyint NOT NULL,
  `FatherPhone` tinyint NOT NULL,
  `FatherOccupation` tinyint NOT NULL,
  `FatherAddress` tinyint NOT NULL,
  `MotherName` tinyint NOT NULL,
  `MotherPhone` tinyint NOT NULL,
  `MotherOccupation` tinyint NOT NULL,
  `MotherAddress` tinyint NOT NULL,
  `KinName` tinyint NOT NULL,
  `KinPhone` tinyint NOT NULL,
  `KinOccupation` tinyint NOT NULL,
  `KinAddress` tinyint NOT NULL,
  `AdditionalYear` tinyint NOT NULL,
  `FinalClearanceDate` tinyint NOT NULL,
  `ClearanceStatusID` tinyint NOT NULL,
  `PwdQuestion1` tinyint NOT NULL,
  `PwdAnswer1` tinyint NOT NULL,
  `PwdQuestion2` tinyint NOT NULL,
  `PwdAnswer2` tinyint NOT NULL,
  `SearchData` tinyint NOT NULL,
  `ActualMatNo` tinyint NOT NULL,
  `StudentName` tinyint NOT NULL,
  `ClearanceStatus` tinyint NOT NULL,
  `Gender` tinyint NOT NULL,
  `Nationality` tinyint NOT NULL,
  `StateID` tinyint NOT NULL,
  `State` tinyint NOT NULL,
  `LGA` tinyint NOT NULL,
  `DeptID` tinyint NOT NULL,
  `FacultyID` tinyint NOT NULL,
  `FacultyName` tinyint NOT NULL,
  `FacultyCode` tinyint NOT NULL,
  `FacultyColourID` tinyint NOT NULL,
  `ColourName` tinyint NOT NULL,
  `DeptName` tinyint NOT NULL,
  `DeptCode` tinyint NOT NULL,
  `HODID` tinyint NOT NULL,
  `HOD` tinyint NOT NULL,
  `FacultyDeanID` tinyint NOT NULL,
  `Dean` tinyint NOT NULL,
  `DegreeCourse` tinyint NOT NULL,
  `DegreeTypeID` tinyint NOT NULL,
  `DegreeType` tinyint NOT NULL,
  `ProgramTypeID` tinyint NOT NULL,
  `ProgramType` tinyint NOT NULL,
  `ProgramTypeCode` tinyint NOT NULL,
  `DegreeCourseDuration` tinyint NOT NULL,
  `DegreeAwardID` tinyint NOT NULL,
  `DegreeAward` tinyint NOT NULL,
  `DigitCode` tinyint NOT NULL,
  `DegreeAwardCode` tinyint NOT NULL,
  `Status` tinyint NOT NULL,
  `GraduationStatus` tinyint NOT NULL,
  `AdmissionMode` tinyint NOT NULL,
  `PIN` tinyint NOT NULL,
  `Firstname` tinyint NOT NULL,
  `AgeNow` tinyint NOT NULL,
  `dyofb` tinyint NOT NULL,
  `wkofb` tinyint NOT NULL,
  `mnthofb` tinyint NOT NULL,
  `mofb` tinyint NOT NULL,
  `yrofb` tinyint NOT NULL,
  `IndigeneStatusID` tinyint NOT NULL,
  `IndigeneStatus` tinyint NOT NULL,
  `LegalCourseDuration` tinyint NOT NULL,
  `RegCheck1` tinyint NOT NULL,
  `RegCheck2` tinyint NOT NULL,
  `RegCheck3` tinyint NOT NULL,
  `RegCheck4` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw97_students_clearance`
--

DROP TABLE IF EXISTS `vw97_students_clearance`;
/*!50001 DROP VIEW IF EXISTS `vw97_students_clearance`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw97_students_clearance` (
  `ClearanceID` tinyint NOT NULL,
  `StudentID` tinyint NOT NULL,
  `ApplicationDate` tinyint NOT NULL,
  `ClearanceStatusID1` tinyint NOT NULL,
  `ClearanceOfficerID1` tinyint NOT NULL,
  `ClearanceDate1` tinyint NOT NULL,
  `ClearanceStatusID2` tinyint NOT NULL,
  `ClearanceOfficerID2` tinyint NOT NULL,
  `ClearanceDate2` tinyint NOT NULL,
  `ClearanceStatusID3` tinyint NOT NULL,
  `ClearanceOfficerID3` tinyint NOT NULL,
  `ClearanceDate3` tinyint NOT NULL,
  `ClearanceStatusID4` tinyint NOT NULL,
  `ClearanceOfficerID4` tinyint NOT NULL,
  `ClearanceDate4` tinyint NOT NULL,
  `ClearanceStatusID5` tinyint NOT NULL,
  `ClearanceOfficerID5` tinyint NOT NULL,
  `ClearanceDate5` tinyint NOT NULL,
  `StudentPIN` tinyint NOT NULL,
  `RegNo` tinyint NOT NULL,
  `MatNo` tinyint NOT NULL,
  `StudentName` tinyint NOT NULL,
  `Surname` tinyint NOT NULL,
  `AdmissionModeID` tinyint NOT NULL,
  `Others` tinyint NOT NULL,
  `Sex` tinyint NOT NULL,
  `Gender` tinyint NOT NULL,
  `DegreeCourseID` tinyint NOT NULL,
  `DegreeCourse` tinyint NOT NULL,
  `DeptID` tinyint NOT NULL,
  `FacultyID` tinyint NOT NULL,
  `FacultyName` tinyint NOT NULL,
  `FacultyCode` tinyint NOT NULL,
  `DeptName` tinyint NOT NULL,
  `DeptCode` tinyint NOT NULL,
  `DegreeTypeID` tinyint NOT NULL,
  `DegreeType` tinyint NOT NULL,
  `ProgramTypeID` tinyint NOT NULL,
  `ProgramType` tinyint NOT NULL,
  `ProgramTypeCode` tinyint NOT NULL,
  `DegreeCourseDuration` tinyint NOT NULL,
  `AdmYear` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw97_studyyearmgt`
--

DROP TABLE IF EXISTS `vw97_studyyearmgt`;
/*!50001 DROP VIEW IF EXISTS `vw97_studyyearmgt`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw97_studyyearmgt` (
  `StudentID` tinyint NOT NULL,
  `AdmYear` tinyint NOT NULL,
  `StatusID` tinyint NOT NULL,
  `GraduationStatusID` tinyint NOT NULL,
  `AdmissionModeID` tinyint NOT NULL,
  `DegreeCourseDuration` tinyint NOT NULL,
  `AdditionalYear` tinyint NOT NULL,
  `SessionYear` tinyint NOT NULL,
  `SessionStatusID` tinyint NOT NULL,
  `LegalCourseDuration` tinyint NOT NULL,
  `StudyYear` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw98_registration_students`
--

DROP TABLE IF EXISTS `vw98_registration_students`;
/*!50001 DROP VIEW IF EXISTS `vw98_registration_students`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw98_registration_students` (
  `StudentRegID` tinyint NOT NULL,
  `StudentID` tinyint NOT NULL,
  `StudentPIN` tinyint NOT NULL,
  `RegNo` tinyint NOT NULL,
  `MatNo` tinyint NOT NULL,
  `StudentName` tinyint NOT NULL,
  `Surname` tinyint NOT NULL,
  `AdmissionModeID` tinyint NOT NULL,
  `Others` tinyint NOT NULL,
  `Sex` tinyint NOT NULL,
  `Gender` tinyint NOT NULL,
  `MaleCount` tinyint NOT NULL,
  `FemaleCount` tinyint NOT NULL,
  `DegreeCourseID` tinyint NOT NULL,
  `DegreeCourse` tinyint NOT NULL,
  `DeptID` tinyint NOT NULL,
  `FacultyID` tinyint NOT NULL,
  `FacultyName` tinyint NOT NULL,
  `FacultyCode` tinyint NOT NULL,
  `DeptName` tinyint NOT NULL,
  `DeptCode` tinyint NOT NULL,
  `DegreeTypeID` tinyint NOT NULL,
  `DegreeType` tinyint NOT NULL,
  `ProgramTypeID` tinyint NOT NULL,
  `ProgramType` tinyint NOT NULL,
  `ProgramTypeCode` tinyint NOT NULL,
  `DegreeCourseDuration` tinyint NOT NULL,
  `AdmYear` tinyint NOT NULL,
  `StudyYear` tinyint NOT NULL,
  `SessionID` tinyint NOT NULL,
  `SessionName` tinyint NOT NULL,
  `SessionYear` tinyint NOT NULL,
  `LevelID` tinyint NOT NULL,
  `CourseLevelID` tinyint NOT NULL,
  `CourseLevel` tinyint NOT NULL,
  `Max1` tinyint NOT NULL,
  `Min1` tinyint NOT NULL,
  `Max2` tinyint NOT NULL,
  `Min2` tinyint NOT NULL,
  `Max3` tinyint NOT NULL,
  `Min3` tinyint NOT NULL,
  `RegDate` tinyint NOT NULL,
  `AdviserApproval` tinyint NOT NULL,
  `UnitApproval1ID` tinyint NOT NULL,
  `HODApproval` tinyint NOT NULL,
  `UnitApproval2ID` tinyint NOT NULL,
  `DeanApproval` tinyint NOT NULL,
  `UnitApproval3ID` tinyint NOT NULL,
  `ApprovalDate1` tinyint NOT NULL,
  `ApprovalDate2` tinyint NOT NULL,
  `ApprovalDate3` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw992_trnxs`
--

DROP TABLE IF EXISTS `vw992_trnxs`;
/*!50001 DROP VIEW IF EXISTS `vw992_trnxs`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw992_trnxs` (
  `TrnxID` tinyint NOT NULL,
  `StudentID` tinyint NOT NULL,
  `AdmissionID` tinyint NOT NULL,
  `SessionID` tinyint NOT NULL,
  `LevelID` tinyint NOT NULL,
  `FeeID` tinyint NOT NULL,
  `FeeDetailID` tinyint NOT NULL,
  `FeeTypeID` tinyint NOT NULL,
  `FeeItemID` tinyint NOT NULL,
  `InstallmentCount` tinyint NOT NULL,
  `TrnxType` tinyint NOT NULL,
  `TrnxDate` tinyint NOT NULL,
  `TrnxDesc` tinyint NOT NULL,
  `DRAmount` tinyint NOT NULL,
  `CRAmount` tinyint NOT NULL,
  `RRR` tinyint NOT NULL,
  `OrderID` tinyint NOT NULL,
  `ServiceTypeID` tinyint NOT NULL,
  `PaymentID` tinyint NOT NULL,
  `VerifiedByID` tinyint NOT NULL,
  `Remark` tinyint NOT NULL,
  `TrnxDateStamp` tinyint NOT NULL,
  `SessionName` tinyint NOT NULL,
  `SessionYear` tinyint NOT NULL,
  `DegreeCourseID` tinyint NOT NULL,
  `DeptID` tinyint NOT NULL,
  `FacultyID` tinyint NOT NULL,
  `FacultyName` tinyint NOT NULL,
  `DeptName` tinyint NOT NULL,
  `DegreeCourse` tinyint NOT NULL,
  `DegreeTypeID` tinyint NOT NULL,
  `DegreeType` tinyint NOT NULL,
  `ProgramTypeID` tinyint NOT NULL,
  `ProgramType` tinyint NOT NULL,
  `ProgramTypeCode` tinyint NOT NULL,
  `CourseLevelID` tinyint NOT NULL,
  `CourseLevel` tinyint NOT NULL,
  `Amount` tinyint NOT NULL,
  `FeeType` tinyint NOT NULL,
  `TrnxYear` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw992_trnxs_detailed`
--

DROP TABLE IF EXISTS `vw992_trnxs_detailed`;
/*!50001 DROP VIEW IF EXISTS `vw992_trnxs_detailed`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw992_trnxs_detailed` (
  `TrnxID` tinyint NOT NULL,
  `StudentID` tinyint NOT NULL,
  `AdmissionID` tinyint NOT NULL,
  `SessionID` tinyint NOT NULL,
  `LevelID` tinyint NOT NULL,
  `FeeID` tinyint NOT NULL,
  `FeeDetailID` tinyint NOT NULL,
  `FeeTypeID` tinyint NOT NULL,
  `FeeItemID` tinyint NOT NULL,
  `InstallmentCount` tinyint NOT NULL,
  `TrnxType` tinyint NOT NULL,
  `TrnxDate` tinyint NOT NULL,
  `TrnxDesc` tinyint NOT NULL,
  `DRAmount` tinyint NOT NULL,
  `CRAmount` tinyint NOT NULL,
  `RRR` tinyint NOT NULL,
  `OrderID` tinyint NOT NULL,
  `ServiceTypeID` tinyint NOT NULL,
  `PaymentID` tinyint NOT NULL,
  `VerifiedByID` tinyint NOT NULL,
  `Remark` tinyint NOT NULL,
  `TrnxDateStamp` tinyint NOT NULL,
  `SessionName` tinyint NOT NULL,
  `SessionYear` tinyint NOT NULL,
  `DegreeCourseID` tinyint NOT NULL,
  `DeptID` tinyint NOT NULL,
  `FacultyID` tinyint NOT NULL,
  `FacultyName` tinyint NOT NULL,
  `DeptName` tinyint NOT NULL,
  `DegreeCourse` tinyint NOT NULL,
  `DegreeTypeID` tinyint NOT NULL,
  `DegreeType` tinyint NOT NULL,
  `ProgramTypeID` tinyint NOT NULL,
  `ProgramType` tinyint NOT NULL,
  `ProgramTypeCode` tinyint NOT NULL,
  `CourseLevelID` tinyint NOT NULL,
  `CourseLevel` tinyint NOT NULL,
  `TrnxYear` tinyint NOT NULL,
  `Amount` tinyint NOT NULL,
  `FeeType` tinyint NOT NULL,
  `RegNo` tinyint NOT NULL,
  `MatNo` tinyint NOT NULL,
  `Surname` tinyint NOT NULL,
  `Others` tinyint NOT NULL,
  `Sex` tinyint NOT NULL,
  `Gender` tinyint NOT NULL,
  `StudentName` tinyint NOT NULL,
  `State` tinyint NOT NULL,
  `LGA` tinyint NOT NULL,
  `IndigeneStatusID` tinyint NOT NULL,
  `IndigeneStatus` tinyint NOT NULL,
  `Phone` tinyint NOT NULL,
  `Email` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw992_trnxs_orders`
--

DROP TABLE IF EXISTS `vw992_trnxs_orders`;
/*!50001 DROP VIEW IF EXISTS `vw992_trnxs_orders`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw992_trnxs_orders` (
  `PaymentID` tinyint NOT NULL,
  `TrnxID` tinyint NOT NULL,
  `StudentID` tinyint NOT NULL,
  `AdmissionID` tinyint NOT NULL,
  `SessionID` tinyint NOT NULL,
  `LevelID` tinyint NOT NULL,
  `FeeID` tinyint NOT NULL,
  `FeeDetailID` tinyint NOT NULL,
  `FeeTypeID` tinyint NOT NULL,
  `FeeItemID` tinyint NOT NULL,
  `InstallmentCount` tinyint NOT NULL,
  `RRR` tinyint NOT NULL,
  `OrderID` tinyint NOT NULL,
  `ServiceTypeID` tinyint NOT NULL,
  `TrnxType` tinyint NOT NULL,
  `TrnxDate` tinyint NOT NULL,
  `TrnxDesc` tinyint NOT NULL,
  `DRAmount` tinyint NOT NULL,
  `CRAmount` tinyint NOT NULL,
  `VerifiedByID` tinyint NOT NULL,
  `Remark` tinyint NOT NULL,
  `StudyYear` tinyint NOT NULL,
  `TrnxDateStamp` tinyint NOT NULL,
  `SessionName` tinyint NOT NULL,
  `SessionYear` tinyint NOT NULL,
  `DegreeCourseID` tinyint NOT NULL,
  `DeptID` tinyint NOT NULL,
  `FacultyID` tinyint NOT NULL,
  `FacultyName` tinyint NOT NULL,
  `DeptName` tinyint NOT NULL,
  `DegreeCourse` tinyint NOT NULL,
  `DegreeTypeID` tinyint NOT NULL,
  `DegreeType` tinyint NOT NULL,
  `ProgramTypeID` tinyint NOT NULL,
  `ProgramType` tinyint NOT NULL,
  `ProgramTypeCode` tinyint NOT NULL,
  `CourseLevelID` tinyint NOT NULL,
  `CourseLevel` tinyint NOT NULL,
  `Amount` tinyint NOT NULL,
  `FeeType` tinyint NOT NULL,
  `TrnxYear` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw992_trnxs_orders_detailed`
--

DROP TABLE IF EXISTS `vw992_trnxs_orders_detailed`;
/*!50001 DROP VIEW IF EXISTS `vw992_trnxs_orders_detailed`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw992_trnxs_orders_detailed` (
  `PaymentID` tinyint NOT NULL,
  `TrnxID` tinyint NOT NULL,
  `StudentID` tinyint NOT NULL,
  `AdmissionID` tinyint NOT NULL,
  `SessionID` tinyint NOT NULL,
  `LevelID` tinyint NOT NULL,
  `FeeID` tinyint NOT NULL,
  `FeeDetailID` tinyint NOT NULL,
  `FeeTypeID` tinyint NOT NULL,
  `FeeItemID` tinyint NOT NULL,
  `InstallmentCount` tinyint NOT NULL,
  `RRR` tinyint NOT NULL,
  `OrderID` tinyint NOT NULL,
  `ServiceTypeID` tinyint NOT NULL,
  `TrnxType` tinyint NOT NULL,
  `TrnxDate` tinyint NOT NULL,
  `TrnxDesc` tinyint NOT NULL,
  `DRAmount` tinyint NOT NULL,
  `CRAmount` tinyint NOT NULL,
  `VerifiedByID` tinyint NOT NULL,
  `Remark` tinyint NOT NULL,
  `StudyYear` tinyint NOT NULL,
  `TrnxDateStamp` tinyint NOT NULL,
  `SessionName` tinyint NOT NULL,
  `SessionYear` tinyint NOT NULL,
  `DegreeCourseID` tinyint NOT NULL,
  `DeptID` tinyint NOT NULL,
  `FacultyID` tinyint NOT NULL,
  `FacultyName` tinyint NOT NULL,
  `DeptName` tinyint NOT NULL,
  `DegreeCourse` tinyint NOT NULL,
  `DegreeTypeID` tinyint NOT NULL,
  `DegreeType` tinyint NOT NULL,
  `ProgramTypeID` tinyint NOT NULL,
  `ProgramType` tinyint NOT NULL,
  `ProgramTypeCode` tinyint NOT NULL,
  `CourseLevelID` tinyint NOT NULL,
  `CourseLevel` tinyint NOT NULL,
  `Amount` tinyint NOT NULL,
  `FeeType` tinyint NOT NULL,
  `TrnxYear` tinyint NOT NULL,
  `RegNo` tinyint NOT NULL,
  `MatNo` tinyint NOT NULL,
  `Surname` tinyint NOT NULL,
  `Others` tinyint NOT NULL,
  `Sex` tinyint NOT NULL,
  `Gender` tinyint NOT NULL,
  `StudentName` tinyint NOT NULL,
  `State` tinyint NOT NULL,
  `LGA` tinyint NOT NULL,
  `IndigeneStatusID` tinyint NOT NULL,
  `IndigeneStatus` tinyint NOT NULL,
  `Phone` tinyint NOT NULL,
  `Email` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw999_import_results`
--

DROP TABLE IF EXISTS `vw999_import_results`;
/*!50001 DROP VIEW IF EXISTS `vw999_import_results`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw999_import_results` (
  `ImportID` tinyint NOT NULL,
  `CourseRegID` tinyint NOT NULL,
  `Surname` tinyint NOT NULL,
  `Others` tinyint NOT NULL,
  `MatNo` tinyint NOT NULL,
  `ImportedScore` tinyint NOT NULL,
  `Score` tinyint NOT NULL,
  `Grade` tinyint NOT NULL,
  `CourseUnit` tinyint NOT NULL,
  `GradePoint` tinyint NOT NULL,
  `PreviousScore` tinyint NOT NULL,
  `PreviousGrade` tinyint NOT NULL,
  `ModByID` tinyint NOT NULL,
  `ModDateStamp` tinyint NOT NULL,
  `EarnedGrade` tinyint NOT NULL,
  `EarnedGP` tinyint NOT NULL,
  `SessionID` tinyint NOT NULL,
  `SemesterID` tinyint NOT NULL,
  `ProgramTypeID` tinyint NOT NULL,
  `AssignedCourseID` tinyint NOT NULL,
  `ActionByID` tinyint NOT NULL,
  `OutofRangeChk` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw99_registration_courses`
--

DROP TABLE IF EXISTS `vw99_registration_courses`;
/*!50001 DROP VIEW IF EXISTS `vw99_registration_courses`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw99_registration_courses` (
  `CourseRegID` tinyint NOT NULL,
  `StudentRegID` tinyint NOT NULL,
  `SemesterID` tinyint NOT NULL,
  `AssignedCourseID` tinyint NOT NULL,
  `CourseUnit` tinyint NOT NULL,
  `Score` tinyint NOT NULL,
  `Grade` tinyint NOT NULL,
  `GradePoint` tinyint NOT NULL,
  `ModByID` tinyint NOT NULL,
  `ModDateStamp` tinyint NOT NULL,
  `ExcessRegID` tinyint NOT NULL,
  `PreviousScore` tinyint NOT NULL,
  `PreviousGrade` tinyint NOT NULL,
  `Q1` tinyint NOT NULL,
  `Q2` tinyint NOT NULL,
  `Q3` tinyint NOT NULL,
  `Q4` tinyint NOT NULL,
  `Q5` tinyint NOT NULL,
  `Q6` tinyint NOT NULL,
  `CA` tinyint NOT NULL,
  `Moderation` tinyint NOT NULL,
  `Penalty` tinyint NOT NULL,
  `Borderline` tinyint NOT NULL,
  `Exam` tinyint NOT NULL,
  `RegistrationDateStamp` tinyint NOT NULL,
  `ExamScore` tinyint NOT NULL,
  `FinalScore` tinyint NOT NULL,
  `StudentID` tinyint NOT NULL,
  `MatNo` tinyint NOT NULL,
  `StudentName` tinyint NOT NULL,
  `Surname` tinyint NOT NULL,
  `Others` tinyint NOT NULL,
  `Sex` tinyint NOT NULL,
  `Gender` tinyint NOT NULL,
  `MaleCount` tinyint NOT NULL,
  `FemaleCount` tinyint NOT NULL,
  `DegreeCourseID` tinyint NOT NULL,
  `DeptID` tinyint NOT NULL,
  `FacultyID` tinyint NOT NULL,
  `FacultyName` tinyint NOT NULL,
  `DeptName` tinyint NOT NULL,
  `DegreeType` tinyint NOT NULL,
  `ProgramType` tinyint NOT NULL,
  `AdmissionModeID` tinyint NOT NULL,
  `ProgramTypeCode` tinyint NOT NULL,
  `DegreeTypeID` tinyint NOT NULL,
  `ProgramTypeID` tinyint NOT NULL,
  `StudyYear` tinyint NOT NULL,
  `SessionID` tinyint NOT NULL,
  `SessionName` tinyint NOT NULL,
  `SessionYear` tinyint NOT NULL,
  `LevelID` tinyint NOT NULL,
  `CourseLevelID` tinyint NOT NULL,
  `CourseLevel` tinyint NOT NULL,
  `Semester` tinyint NOT NULL,
  `LecturerID` tinyint NOT NULL,
  `CourseSemesterID` tinyint NOT NULL,
  `CourseID` tinyint NOT NULL,
  `ParentDeptID` tinyint NOT NULL,
  `CourseCode` tinyint NOT NULL,
  `CourseTitle` tinyint NOT NULL,
  `ResultApproval1ID` tinyint NOT NULL,
  `ResultApproval2ID` tinyint NOT NULL,
  `ResultApproval3ID` tinyint NOT NULL,
  `ResultApproval4ID` tinyint NOT NULL,
  `ApprovalDate1` tinyint NOT NULL,
  `ApprovalDate2` tinyint NOT NULL,
  `ApprovalDate3` tinyint NOT NULL,
  `ApprovalDate4` tinyint NOT NULL,
  `L1` tinyint NOT NULL,
  `L2` tinyint NOT NULL,
  `L3` tinyint NOT NULL,
  `L4` tinyint NOT NULL,
  `PendingGrade` tinyint NOT NULL,
  `Passed` tinyint NOT NULL,
  `Failed` tinyint NOT NULL,
  `GradeA` tinyint NOT NULL,
  `GradeB` tinyint NOT NULL,
  `GradeC` tinyint NOT NULL,
  `GradeD` tinyint NOT NULL,
  `GradeE` tinyint NOT NULL,
  `GradeF` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw99_registration_courses_gradesummary`
--

DROP TABLE IF EXISTS `vw99_registration_courses_gradesummary`;
/*!50001 DROP VIEW IF EXISTS `vw99_registration_courses_gradesummary`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw99_registration_courses_gradesummary` (
  `ProgramTypeID` tinyint NOT NULL,
  `SessionID` tinyint NOT NULL,
  `SemesterID` tinyint NOT NULL,
  `CourseSemesterID` tinyint NOT NULL,
  `AssignedCourseID` tinyint NOT NULL,
  `CourseID` tinyint NOT NULL,
  `GradeA` tinyint NOT NULL,
  `GradeB` tinyint NOT NULL,
  `GradeC` tinyint NOT NULL,
  `GradeD` tinyint NOT NULL,
  `GradeE` tinyint NOT NULL,
  `GradeF` tinyint NOT NULL,
  `StudentCount` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw99_registration_excess`
--

DROP TABLE IF EXISTS `vw99_registration_excess`;
/*!50001 DROP VIEW IF EXISTS `vw99_registration_excess`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw99_registration_excess` (
  `ExcessRegID` tinyint NOT NULL,
  `StudentRegID` tinyint NOT NULL,
  `StudentID` tinyint NOT NULL,
  `StudentName` tinyint NOT NULL,
  `Surname` tinyint NOT NULL,
  `Others` tinyint NOT NULL,
  `Sex` tinyint NOT NULL,
  `DegreeCourseID` tinyint NOT NULL,
  `DeptID` tinyint NOT NULL,
  `FacultyID` tinyint NOT NULL,
  `FacultyName` tinyint NOT NULL,
  `DeptName` tinyint NOT NULL,
  `DegreeTypeID` tinyint NOT NULL,
  `DegreeType` tinyint NOT NULL,
  `ProgramTypeID` tinyint NOT NULL,
  `ProgramType` tinyint NOT NULL,
  `ProgramTypeCode` tinyint NOT NULL,
  `StudyYear` tinyint NOT NULL,
  `SessionID` tinyint NOT NULL,
  `SessionName` tinyint NOT NULL,
  `SessionYear` tinyint NOT NULL,
  `LevelID` tinyint NOT NULL,
  `CourseLevelID` tinyint NOT NULL,
  `CourseLevel` tinyint NOT NULL,
  `SemesterID` tinyint NOT NULL,
  `AssignedCourseID` tinyint NOT NULL,
  `CourseID` tinyint NOT NULL,
  `CourseCode` tinyint NOT NULL,
  `CourseTitle` tinyint NOT NULL,
  `CourseSemesterID` tinyint NOT NULL,
  `Semester` tinyint NOT NULL,
  `LecturerID` tinyint NOT NULL,
  `CourseUnit` tinyint NOT NULL,
  `AdviserApproval` tinyint NOT NULL,
  `UnitApproval1ID` tinyint NOT NULL,
  `HODApproval` tinyint NOT NULL,
  `UnitApproval2ID` tinyint NOT NULL,
  `DeanApproval` tinyint NOT NULL,
  `UnitApproval3ID` tinyint NOT NULL,
  `ApprovalDate1` tinyint NOT NULL,
  `ApprovalDate2` tinyint NOT NULL,
  `ApprovalDate3` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw9_degreeaward`
--

DROP TABLE IF EXISTS `vw9_degreeaward`;
/*!50001 DROP VIEW IF EXISTS `vw9_degreeaward`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw9_degreeaward` (
  `DegreeAwardID` tinyint NOT NULL,
  `DegreeAward` tinyint NOT NULL,
  `DegreeAwardCode` tinyint NOT NULL,
  `DegreeTypeID` tinyint NOT NULL,
  `DegreeType` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_entryrequirements`
--

DROP TABLE IF EXISTS `vw_entryrequirements`;
/*!50001 DROP VIEW IF EXISTS `vw_entryrequirements`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw_entryrequirements` (
  `RequirementID` tinyint NOT NULL,
  `EntryCategoryID` tinyint NOT NULL,
  `DegreeCourseID` tinyint NOT NULL,
  `DegreeCourse` tinyint NOT NULL,
  `SessionID` tinyint NOT NULL,
  `SubjectID` tinyint NOT NULL,
  `Subject` tinyint NOT NULL,
  `MinGradeID` tinyint NOT NULL,
  `CompulsoryID` tinyint NOT NULL,
  `MinGrade` tinyint NOT NULL,
  `CompulsoryStatus` tinyint NOT NULL,
  `PrimarySubject` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_entryrequirements_alt`
--

DROP TABLE IF EXISTS `vw_entryrequirements_alt`;
/*!50001 DROP VIEW IF EXISTS `vw_entryrequirements_alt`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw_entryrequirements_alt` (
  `RequirementID` tinyint NOT NULL,
  `EntryCategoryID` tinyint NOT NULL,
  `DegreeCourseID` tinyint NOT NULL,
  `DegreeCourse` tinyint NOT NULL,
  `SessionID` tinyint NOT NULL,
  `SubjectID` tinyint NOT NULL,
  `SubjectID_Alt` tinyint NOT NULL,
  `PrimarySubject` tinyint NOT NULL,
  `Subject` tinyint NOT NULL,
  `MinGradeID` tinyint NOT NULL,
  `CompulsoryID` tinyint NOT NULL,
  `MinGrade` tinyint NOT NULL,
  `CompulsoryStatus` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_feeitems`
--

DROP TABLE IF EXISTS `vw_feeitems`;
/*!50001 DROP VIEW IF EXISTS `vw_feeitems`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw_feeitems` (
  `FeeItemID` tinyint NOT NULL,
  `FeeItem` tinyint NOT NULL,
  `FeeTypeID` tinyint NOT NULL,
  `FeeType` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_jambtest`
--

DROP TABLE IF EXISTS `vw_jambtest`;
/*!50001 DROP VIEW IF EXISTS `vw_jambtest`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw_jambtest` (
  `JambCheckID` tinyint NOT NULL,
  `SessionID` tinyint NOT NULL,
  `AdmissionID` tinyint NOT NULL,
  `OLevelID` tinyint NOT NULL,
  `DegreeCourseID` tinyint NOT NULL,
  `JambSubjectID` tinyint NOT NULL,
  `SubjectScore` tinyint NOT NULL,
  `Subject` tinyint NOT NULL,
  `ExamCenter` tinyint NOT NULL,
  `ExamNumber` tinyint NOT NULL,
  `ExamType` tinyint NOT NULL,
  `ExamYear` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_oleveltest`
--

DROP TABLE IF EXISTS `vw_oleveltest`;
/*!50001 DROP VIEW IF EXISTS `vw_oleveltest`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw_oleveltest` (
  `OlevelCheckID` tinyint NOT NULL,
  `SessionID` tinyint NOT NULL,
  `AdmissionID` tinyint NOT NULL,
  `DegreeCourseID` tinyint NOT NULL,
  `OlevelSubjectID` tinyint NOT NULL,
  `OLevelID` tinyint NOT NULL,
  `EarnedGradeID` tinyint NOT NULL,
  `Subject` tinyint NOT NULL,
  `EarnedGrade` tinyint NOT NULL,
  `EarnedGrade1` tinyint NOT NULL,
  `ExamCenter` tinyint NOT NULL,
  `ExamNumber` tinyint NOT NULL,
  `ExamType` tinyint NOT NULL,
  `ExamYear` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_servicetypes`
--

DROP TABLE IF EXISTS `vw_servicetypes`;
/*!50001 DROP VIEW IF EXISTS `vw_servicetypes`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw_servicetypes` (
  `ServiceID` tinyint NOT NULL,
  `ServiceTypeID` tinyint NOT NULL,
  `FeeItemID` tinyint NOT NULL,
  `ServiceTypeName` tinyint NOT NULL,
  `InstallmentCount` tinyint NOT NULL,
  `ProgramTypeID` tinyint NOT NULL,
  `DegreeTypeID` tinyint NOT NULL,
  `FeeTypeID` tinyint NOT NULL,
  `DeptID` tinyint NOT NULL,
  `FacultyID` tinyint NOT NULL,
  `FeeType` tinyint NOT NULL,
  `For` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `vw1_predata`
--

/*!50001 DROP TABLE IF EXISTS `vw1_predata`*/;
/*!50001 DROP VIEW IF EXISTS `vw1_predata`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw1_predata` AS select `tblpredata`.`DataID` AS `DataID`,`tblpredata`.`Data` AS `Data`,`tblpredata`.`CategoryID` AS `CategoryID`,`tblpredatacategory`.`DataCategory` AS `DataCategory` from (`tblpredatacategory` join `tblpredata` on(`tblpredatacategory`.`CategoryID` = `tblpredata`.`CategoryID`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw2_academicsession`
--

/*!50001 DROP TABLE IF EXISTS `vw2_academicsession`*/;
/*!50001 DROP VIEW IF EXISTS `vw2_academicsession`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw2_academicsession` AS select `tblacademicsession`.`SessionID` AS `SessionID`,`tblacademicsession`.`SessionName` AS `SessionName`,concat(left(`tblacademicsession`.`SessionName`,4),'_',right(`tblacademicsession`.`SessionName`,4)) AS `SessionFileName`,left(`tblacademicsession`.`SessionName`,4) AS `SessionYear`,`tblacademicsession`.`SessionStatusID` AS `SessionStatusID`,if(`tblacademicsession`.`SessionStatusID` = 1,'CURRENT','NOT CURRENT') AS `SessionStatus`,`tblacademicsession`.`BillingRate` AS `BillingRate`,`tblacademicsession`.`PopulationCount` AS `PopulationCount`,`tblacademicsession`.`BillingCharge` AS `BillingCharge` from `tblacademicsession` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw2_academicsession_mgt`
--

/*!50001 DROP TABLE IF EXISTS `vw2_academicsession_mgt`*/;
/*!50001 DROP VIEW IF EXISTS `vw2_academicsession_mgt`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw2_academicsession_mgt` AS select `tblacademicsession_mgt`.`SessionMgtID` AS `SessionMgtID`,`tblacademicsession_mgt`.`SessionID` AS `SessionID`,`tblacademicsession`.`SessionName` AS `SessionName`,`tblacademicsession_mgt`.`ProgramTypeID` AS `ProgramTypeID`,`tblacademicsession_mgt`.`DegreeTypeID` AS `DegreeTypeID`,`tblacademicsession_mgt`.`FeePaymentControlID` AS `FeePaymentControlID`,`tblacademicsession_mgt`.`RegistrationStatusID` AS `RegistrationStatusID`,`tblacademicsession_mgt`.`ResultUpdateStatusID` AS `ResultUpdateStatusID`,`tblacademicsession_mgt`.`AdmissionStatusID` AS `AdmissionStatusID`,concat(left(`tblacademicsession`.`SessionName`,4),'_',right(`tblacademicsession`.`SessionName`,4)) AS `SessionFileName`,left(`tblacademicsession`.`SessionName`,4) AS `SessionYear`,`tblacademicsession_mgt`.`SessionStatusID` AS `SessionStatusID`,if(`tblacademicsession_mgt`.`SessionStatusID` = 1,'CURRENT','NOT CURRENT') AS `SessionStatus`,if(`tblacademicsession_mgt`.`FeePaymentControlID` = 1,'ENFORCED','NOT ENFORCED') AS `FeePayment`,if(`tblacademicsession_mgt`.`RegistrationStatusID` = 1,'LOCKED','NOT LOCKED') AS `RegistrationStatus`,if(`tblacademicsession_mgt`.`ResultUpdateStatusID` = 1,'LOCKED','NOT LOCKED') AS `ResultUpdateStatus`,if(`tblacademicsession_mgt`.`AdmissionStatusID` = 1,'LOCKED','NOT LOCKED') AS `AdmissionLockStatus`,`tblprogramtype`.`ProgramType` AS `ProgramType`,`tblprogramtype`.`ProgramTypeCode` AS `ProgramTypeCode`,`tbldegreetype`.`DegreeType` AS `DegreeType` from (((`tblacademicsession_mgt` join `tblacademicsession` on(`tblacademicsession_mgt`.`SessionID` = `tblacademicsession`.`SessionID`)) join `tblprogramtype` on(`tblacademicsession_mgt`.`ProgramTypeID` = `tblprogramtype`.`ProgramTypeID`)) join `tbldegreetype` on(`tblacademicsession_mgt`.`DegreeTypeID` = `tbldegreetype`.`DegreeTypeID`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw2_medicalexamregime`
--

/*!50001 DROP TABLE IF EXISTS `vw2_medicalexamregime`*/;
/*!50001 DROP VIEW IF EXISTS `vw2_medicalexamregime`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw2_medicalexamregime` AS select `tblmedicalexamregime`.`MedicalRegimeID` AS `MedicalRegimeID`,`tblmedicalexamregime`.`SessionID` AS `SessionID`,`vw2_academicsession`.`SessionName` AS `SessionName`,`vw2_academicsession`.`SessionYear` AS `SessionYear`,`vw2_academicsession`.`SessionStatusID` AS `SessionStatusID`,`vw2_academicsession`.`SessionStatus` AS `SessionStatus`,`tblmedicalexamregime`.`StartDate` AS `StartDate`,`tblmedicalexamregime`.`DailyCapacity` AS `DailyCapacity`,`tblmedicalexamregime`.`SchedulingPlanID` AS `SchedulingPlanID`,`tblmedicalexamregime`.`RegimeStatusID` AS `RegimeStatusID`,if(`tblmedicalexamregime`.`RegimeStatusID` = 1,'OPEN','CLOSED') AS `RegimeStatus`,if(`tblmedicalexamregime`.`SchedulingPlanID` = 1,'EVERYDAY',if(`tblmedicalexamregime`.`SchedulingPlanID` = 2,'WEEKDAYS',if(`tblmedicalexamregime`.`SchedulingPlanID` = 3,'WEEKENDS',''))) AS `SchedulingPlan` from (`tblmedicalexamregime` join `vw2_academicsession` on(`tblmedicalexamregime`.`SessionID` = `vw2_academicsession`.`SessionID`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw3_academiccourse`
--

/*!50001 DROP TABLE IF EXISTS `vw3_academiccourse`*/;
/*!50001 DROP VIEW IF EXISTS `vw3_academiccourse`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw3_academiccourse` AS select `tblacademiccourse`.`CourseID` AS `CourseID`,`tblacademiccourse`.`CourseCode` AS `CourseCode`,`tblacademiccourse`.`CourseTitle` AS `CourseTitle`,`tblacademiccourse`.`CourseUnit` AS `CourseUnit`,`tblacademiccourse`.`CourseLevelID` AS `CourseLevelID`,`tblacademiccourse`.`CourseLevelID` * 100 AS `CourseLevel`,`tblacademiccourse`.`DegreeTypeID` AS `DegreeTypeID`,`tbldegreetype`.`DegreeType` AS `DegreeType`,`tblacademiccourse`.`CourseSemesterID` AS `CourseSemesterID`,if(`tblacademiccourse`.`CourseSemesterID` = 1,'FIRST','SECOND') AS `CourseSemester`,`tblacademiccourse`.`CourseDescription` AS `CourseDescription`,`tbldepartment`.`FacultyID` AS `FacultyID`,`tblacademiccourse`.`ServiceDeptID` AS `ServiceDeptID`,`tbldepartment`.`DeptName` AS `DeptName` from ((`tblacademiccourse` join `tbldegreetype` on(`tblacademiccourse`.`DegreeTypeID` = `tbldegreetype`.`DegreeTypeID`)) join `tbldepartment` on(`tblacademiccourse`.`ServiceDeptID` = `tbldepartment`.`DeptID`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw4_assignedcourse`
--

/*!50001 DROP TABLE IF EXISTS `vw4_assignedcourse`*/;
/*!50001 DROP VIEW IF EXISTS `vw4_assignedcourse`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw4_assignedcourse` AS select `tblassignedcourse`.`AssignedCourseID` AS `AssignedCourseID`,`tblassignedcourse`.`SessionID` AS `SessionID`,`tblassignedcourse`.`ProgramTypeID` AS `ProgramTypeID`,`tblprogramtype`.`ProgramType` AS `ProgramType`,`tblprogramtype`.`ProgramTypeCode` AS `ProgramTypeCode`,`tblassignedcourse`.`CourseID` AS `CourseID`,`vw3_academiccourse`.`CourseCode` AS `CourseCode`,`vw3_academiccourse`.`CourseTitle` AS `CourseTitle`,`vw3_academiccourse`.`CourseUnit` AS `CourseUnit`,`vw3_academiccourse`.`CourseLevelID` AS `CourseLevelID`,`vw3_academiccourse`.`CourseLevel` AS `CourseLevel`,`vw3_academiccourse`.`DegreeTypeID` AS `DegreeTypeID`,`vw3_academiccourse`.`DegreeType` AS `DegreeType`,`vw3_academiccourse`.`CourseSemesterID` AS `CourseSemesterID`,`vw3_academiccourse`.`CourseSemester` AS `Semester`,`vw3_academiccourse`.`FacultyID` AS `FacultyID`,`vw3_academiccourse`.`ServiceDeptID` AS `ServiceDeptID`,`vw3_academiccourse`.`DeptName` AS `DeptName`,`tblassignedcourse`.`LecturerID` AS `LecturerID`,if(`tblassignedcourse`.`ApprovalDate1` is null,0,1) AS `ResultApproval1ID`,`tblassignedcourse`.`HODID` AS `HODID`,if(`tblassignedcourse`.`HODID` is null,0,1) AS `ResultApproval2ID`,`tblassignedcourse`.`DeanID` AS `DeanID`,if(`tblassignedcourse`.`DeanID` is null,0,1) AS `ResultApproval3ID`,`tblassignedcourse`.`SenateRepID` AS `SenateRepID`,if(`tblassignedcourse`.`SenateRepID` is null,0,1) AS `ResultApproval4ID`,`tblassignedcourse`.`ApprovalDate1` AS `ApprovalDate1`,`tblassignedcourse`.`ApprovalDate2` AS `ApprovalDate2`,`tblassignedcourse`.`ApprovalDate3` AS `ApprovalDate3`,`tblassignedcourse`.`ApprovalDate4` AS `ApprovalDate4`,if(`tblassignedcourse`.`ApprovalDate1` = 1,'Y','N') AS `L1`,if(`tblassignedcourse`.`HODID` = 1,'Y','N') AS `L2`,if(`tblassignedcourse`.`DeanID` = 1,'Y','N') AS `L3`,if(`tblassignedcourse`.`SenateRepID` = 1,'Y','N') AS `L4` from ((`tblassignedcourse` join `vw3_academiccourse` on(`tblassignedcourse`.`CourseID` = `vw3_academiccourse`.`CourseID`)) join `tblprogramtype` on(`tblassignedcourse`.`ProgramTypeID` = `tblprogramtype`.`ProgramTypeID`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw5_deptcount`
--

/*!50001 DROP TABLE IF EXISTS `vw5_deptcount`*/;
/*!50001 DROP VIEW IF EXISTS `vw5_deptcount`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw5_deptcount` AS select `tbldepartment`.`FacultyID` AS `FacultyID`,count(`tbldepartment`.`DeptID`) AS `DeptCount` from `tbldepartment` group by `tbldepartment`.`FacultyID` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw6_faculty`
--

/*!50001 DROP TABLE IF EXISTS `vw6_faculty`*/;
/*!50001 DROP VIEW IF EXISTS `vw6_faculty`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw6_faculty` AS select `tblfaculty`.`FacultyID` AS `FacultyID`,`tblfaculty`.`FacultyName` AS `FacultyName`,`tblfaculty`.`FacultyCode` AS `FacultyCode`,`tblfaculty`.`FacultyColourID` AS `FacultyColourID`,`tblcolour`.`ColourName` AS `ColourName`,`tblfaculty`.`FacultyDeanID` AS `FacultyDeanID`,if(`tblfaculty`.`FacultyDeanID` is null,'NOT SPECIFIED',ucase(concat(`tbluser`.`Title`,' ',`tbluser`.`Surname`,', ',`tbluser`.`Others`))) AS `FacultyDean` from ((`tblfaculty` left join `tblcolour` on(`tblfaculty`.`FacultyColourID` = `tblcolour`.`ColourID`)) left join `tbluser` on(`tblfaculty`.`FacultyDeanID` = `tbluser`.`UserID`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw7_department`
--

/*!50001 DROP TABLE IF EXISTS `vw7_department`*/;
/*!50001 DROP VIEW IF EXISTS `vw7_department`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw7_department` AS select `tbldepartment`.`DeptID` AS `DeptID`,`tbldepartment`.`FacultyID` AS `FacultyID`,`vw6_faculty`.`FacultyName` AS `FacultyName`,`vw6_faculty`.`FacultyCode` AS `FacultyCode`,`vw6_faculty`.`FacultyColourID` AS `FacultyColourID`,`vw6_faculty`.`ColourName` AS `ColourName`,`tbldepartment`.`DeptName` AS `DeptName`,`tbldepartment`.`DeptCode` AS `DeptCode`,`tbldepartment`.`DigitCode` AS `DigitCode`,`tbldepartment`.`HODID` AS `HODID`,if(`tbldepartment`.`HODID` is null,'NOT SPECIFIED',ucase(concat(`tbluser`.`Title`,' ',`tbluser`.`Surname`,', ',`tbluser`.`Others`))) AS `HOD`,`vw6_faculty`.`FacultyDeanID` AS `FacultyDeanID`,`vw6_faculty`.`FacultyDean` AS `Dean` from ((`tbldepartment` left join `tbluser` on(`tbldepartment`.`HODID` = `tbluser`.`UserID`)) join `vw6_faculty` on(`tbldepartment`.`FacultyID` = `vw6_faculty`.`FacultyID`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw8_degreecourse`
--

/*!50001 DROP TABLE IF EXISTS `vw8_degreecourse`*/;
/*!50001 DROP VIEW IF EXISTS `vw8_degreecourse`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw8_degreecourse` AS select `tbldegreecourse`.`DegreeCourseID` AS `DegreeCourseID`,`tbldegreecourse`.`DeptID` AS `DeptID`,`vw7_department`.`FacultyID` AS `FacultyID`,`vw7_department`.`FacultyName` AS `FacultyName`,`vw7_department`.`FacultyCode` AS `FacultyCode`,`vw7_department`.`FacultyColourID` AS `FacultyColourID`,`vw7_department`.`ColourName` AS `ColourName`,`vw7_department`.`DeptName` AS `DeptName`,`vw7_department`.`DeptCode` AS `DeptCode`,`vw7_department`.`HODID` AS `HODID`,`vw7_department`.`HOD` AS `HOD`,`vw7_department`.`FacultyDeanID` AS `FacultyDeanID`,`vw7_department`.`Dean` AS `Dean`,`tbldegreecourse`.`DegreeCourse` AS `DegreeCourse`,`tbldegreecourse`.`DegreeTypeID` AS `DegreeTypeID`,`tbldegreetype`.`DegreeType` AS `DegreeType`,`tbldegreecourse`.`ProgramTypeID` AS `ProgramTypeID`,`tblprogramtype`.`ProgramType` AS `ProgramType`,`tblprogramtype`.`ProgramTypeCode` AS `ProgramTypeCode`,`tbldegreecourse`.`DegreeCourseDuration` AS `DegreeCourseDuration`,`tbldegreecourse`.`DegreeAwardID` AS `DegreeAwardID`,`tbldegreeaward`.`DegreeAward` AS `DegreeAward`,`tbldegreeaward`.`DegreeAwardCode` AS `DegreeAwardCode`,`vw7_department`.`DigitCode` AS `DigitCode` from ((((`tbldegreecourse` join `vw7_department` on(`tbldegreecourse`.`DeptID` = `vw7_department`.`DeptID`)) join `tbldegreetype` on(`tbldegreecourse`.`DegreeTypeID` = `tbldegreetype`.`DegreeTypeID`)) join `tblprogramtype` on(`tbldegreecourse`.`ProgramTypeID` = `tblprogramtype`.`ProgramTypeID`)) left join `tbldegreeaward` on(`tbldegreecourse`.`DegreeAwardID` = `tbldegreeaward`.`DegreeAwardID`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw91_courselevel`
--

/*!50001 DROP TABLE IF EXISTS `vw91_courselevel`*/;
/*!50001 DROP VIEW IF EXISTS `vw91_courselevel`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw91_courselevel` AS select `tblcourselevel`.`LevelID` AS `LevelID`,`tblcourselevel`.`DegreeCourseID` AS `DegreeCourseID`,`vw8_degreecourse`.`DeptID` AS `DeptID`,`vw8_degreecourse`.`FacultyID` AS `FacultyID`,`vw8_degreecourse`.`FacultyName` AS `FacultyName`,`vw8_degreecourse`.`DeptName` AS `DeptName`,`vw8_degreecourse`.`HODID` AS `HODID`,`vw8_degreecourse`.`HOD` AS `HOD`,`vw8_degreecourse`.`FacultyDeanID` AS `FacultyDeanID`,`vw8_degreecourse`.`Dean` AS `Dean`,`vw8_degreecourse`.`DegreeCourse` AS `DegreeCourse`,`vw8_degreecourse`.`DegreeTypeID` AS `DegreeTypeID`,`vw8_degreecourse`.`DegreeType` AS `DegreeType`,`vw8_degreecourse`.`ProgramTypeID` AS `ProgramTypeID`,`vw8_degreecourse`.`ProgramType` AS `ProgramType`,`vw8_degreecourse`.`ProgramTypeCode` AS `ProgramTypeCode`,`tblcourselevel`.`CourseLevelID` AS `CourseLevelID`,`tblcourselevel`.`CourseLevelID` * 100 AS `CourseLevel`,`tblcourselevel`.`AcademicAdviserID` AS `AcademicAdviserID`,if(`tblcourselevel`.`AcademicAdviserID` is null,'NOT SPECIFIED',ucase(concat(`tbluser`.`Title`,' ',`tbluser`.`Surname`,', ',`tbluser`.`Others`))) AS `AcademicAdviser`,`tblcourselevel`.`Max1` AS `Max1`,`tblcourselevel`.`Min1` AS `Min1`,`tblcourselevel`.`Max2` AS `Max2`,`tblcourselevel`.`Min2` AS `Min2`,`tblcourselevel`.`Max3` AS `Max3`,`tblcourselevel`.`Min3` AS `Min3` from ((`tblcourselevel` join `vw8_degreecourse` on(`tblcourselevel`.`DegreeCourseID` = `vw8_degreecourse`.`DegreeCourseID`)) left join `tbluser` on(`tblcourselevel`.`AcademicAdviserID` = `tbluser`.`UserID`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw92_lga`
--

/*!50001 DROP TABLE IF EXISTS `vw92_lga`*/;
/*!50001 DROP VIEW IF EXISTS `vw92_lga`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw92_lga` AS select `tbllga`.`LGAID` AS `LGAID`,`tbllga`.`StateID` AS `StateID`,`tblstate`.`State` AS `State`,`tbllga`.`LGA` AS `LGA` from (`tbllga` join `tblstate` on(`tbllga`.`StateID` = `tblstate`.`StateID`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw93_fees`
--

/*!50001 DROP TABLE IF EXISTS `vw93_fees`*/;
/*!50001 DROP VIEW IF EXISTS `vw93_fees`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw93_fees` AS select `tblfees`.`FeeID` AS `FeeID`,`tblfees`.`SessionID` AS `SessionID`,`tblfees`.`FeeDeadlineDate` AS `FeeDeadlineDate`,`tblfees`.`FeeClosureDate` AS `FeeClosureDate`,`vw2_academicsession_mgt`.`SessionName` AS `SessionName`,`vw2_academicsession_mgt`.`SessionYear` AS `SessionYear`,`vw2_academicsession_mgt`.`SessionStatusID` AS `SessionStatusID`,`vw2_academicsession_mgt`.`SessionStatus` AS `SessionStatus`,`vw2_academicsession_mgt`.`FeePaymentControlID` AS `FeePaymentControlID`,`vw2_academicsession_mgt`.`FeePayment` AS `FeePayment`,`vw2_academicsession_mgt`.`ResultUpdateStatusID` AS `ResultUpdateStatusID`,`vw2_academicsession_mgt`.`ResultUpdateStatus` AS `ResultUpdateStatus`,`tblfees`.`DegreeTypeID` AS `DegreeTypeID`,`vw2_academicsession_mgt`.`DegreeType` AS `DegreeType`,`tblfees`.`ProgramTypeID` AS `ProgramTypeID`,`vw2_academicsession_mgt`.`ProgramType` AS `ProgramType`,`vw2_academicsession_mgt`.`ProgramTypeCode` AS `ProgramTypeCode` from (`tblfees` join `vw2_academicsession_mgt` on(`vw2_academicsession_mgt`.`ProgramTypeID` = `tblfees`.`ProgramTypeID` and `vw2_academicsession_mgt`.`DegreeTypeID` = `tblfees`.`DegreeTypeID` and `tblfees`.`SessionID` = `vw2_academicsession_mgt`.`SessionID`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw94_feedetails`
--

/*!50001 DROP TABLE IF EXISTS `vw94_feedetails`*/;
/*!50001 DROP VIEW IF EXISTS `vw94_feedetails`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw94_feedetails` AS select `tblfeedetails`.`FeeDetailID` AS `FeeDetailID`,`tblfeedetails`.`FeeID` AS `FeeID`,`vw93_fees`.`SessionID` AS `SessionID`,`vw93_fees`.`SessionName` AS `SessionName`,`vw93_fees`.`SessionYear` AS `SessionYear`,`vw93_fees`.`SessionStatusID` AS `SessionStatusID`,`vw93_fees`.`SessionStatus` AS `SessionStatus`,`vw93_fees`.`FeePaymentControlID` AS `FeePaymentControlID`,`vw93_fees`.`FeePayment` AS `FeePayment`,`vw93_fees`.`ResultUpdateStatusID` AS `ResultUpdateStatusID`,`vw93_fees`.`ResultUpdateStatus` AS `ResultUpdateStatus`,`vw93_fees`.`DegreeTypeID` AS `DegreeTypeID`,`vw93_fees`.`DegreeType` AS `DegreeType`,`vw93_fees`.`ProgramTypeID` AS `ProgramTypeID`,`vw93_fees`.`ProgramType` AS `ProgramType`,`vw93_fees`.`ProgramTypeCode` AS `ProgramTypeCode`,concat(`vw93_fees`.`ProgramType`,' ',`vw93_fees`.`DegreeType`) AS `For`,`tblfeedetails`.`FeeItemID` AS `FeeItemID`,`tblfeeitems`.`FeeItem` AS `FeeItem`,`tblfeedetails`.`FeeCategoryID` AS `FeeCategoryID`,`tblfeedetails`.`DegreeCourseID` AS `DegreeCourseID`,`tblfeedetails`.`CourseLevelID` AS `CourseLevelID`,`tblfeedetails`.`CourseLevelID` * 100 AS `CourseLevel`,`tblfeedetails`.`FacultyID` AS `FacultyID`,`tblfeedetails`.`DeptID` AS `DeptID`,`tblfeedetails`.`FeeAmount` AS `FeeAmount`,if(`tblfeedetails`.`FeeCategoryID` = 0,0,if(`tblfeedetails`.`FeeCategoryID` = 1,`tblfeedetails`.`DegreeCourseID`,`tblfeedetails`.`CourseLevelID`)) AS `FeeCat`,`tblfeedetails`.`2Installments1` AS `2installments1`,`tblfeedetails`.`2Installments2` AS `2installments2`,`tblfeedetails`.`3Installments1` AS `3installments1`,`tblfeedetails`.`3Installments2` AS `3installments2`,`tblfeedetails`.`3Installments3` AS `3installments3` from ((`tblfeedetails` join `tblfeeitems` on(`tblfeedetails`.`FeeItemID` = `tblfeeitems`.`FeeItemID`)) join `vw93_fees` on(`tblfeedetails`.`FeeID` = `vw93_fees`.`FeeID`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw94_feedetails1`
--

/*!50001 DROP TABLE IF EXISTS `vw94_feedetails1`*/;
/*!50001 DROP VIEW IF EXISTS `vw94_feedetails1`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw94_feedetails1` AS select `vw94_feedetails`.`FeeDetailID` AS `FeeDetailID`,`vw94_feedetails`.`FeeID` AS `FeeID`,`vw94_feedetails`.`SessionID` AS `SessionID`,`vw94_feedetails`.`SessionName` AS `SessionName`,`vw94_feedetails`.`SessionYear` AS `SessionYear`,`vw94_feedetails`.`SessionStatusID` AS `SessionStatusID`,`vw94_feedetails`.`SessionStatus` AS `SessionStatus`,`vw94_feedetails`.`FeePaymentControlID` AS `FeePaymentControlID`,`vw94_feedetails`.`FeePayment` AS `FeePayment`,`vw94_feedetails`.`ResultUpdateStatusID` AS `ResultUpdateStatusID`,`vw94_feedetails`.`ResultUpdateStatus` AS `ResultUpdateStatus`,`vw94_feedetails`.`DegreeTypeID` AS `DegreeTypeID`,`vw94_feedetails`.`DegreeType` AS `DegreeType`,`vw94_feedetails`.`ProgramTypeID` AS `ProgramTypeID`,`vw94_feedetails`.`ProgramType` AS `ProgramType`,`vw94_feedetails`.`ProgramTypeCode` AS `ProgramTypeCode`,`vw94_feedetails`.`FeeItemID` AS `FeeItemID`,`vw94_feedetails`.`FeeItem` AS `FeeItem`,`vw94_feedetails`.`FeeCategoryID` AS `FeeCategoryID`,`tblfaculty`.`FacultyName` AS `FacultyName`,`tbldepartment`.`DeptName` AS `DeptName`,`vw94_feedetails`.`DegreeCourseID` AS `DegreeCourseID`,`vw94_feedetails`.`FacultyID` AS `FacultyID`,`vw94_feedetails`.`DeptID` AS `DeptID`,`vw94_feedetails`.`CourseLevelID` AS `CourseLevelID`,`vw94_feedetails`.`CourseLevel` AS `CourseLevel`,`vw94_feedetails`.`FeeAmount` AS `FeeAmount`,hex(`vw94_feedetails`.`FeeDetailID` * `vw94_feedetails`.`FeeID` * `vw94_feedetails`.`SessionID` * `vw94_feedetails`.`FeeItemID`) AS `FeeRef`,hex(`vw94_feedetails`.`FeeDetailID` * `vw94_feedetails`.`FeeID` * `vw94_feedetails`.`SessionID`) AS `RefID`,if(`vw94_feedetails`.`FeeCategoryID` = 1 and `vw94_feedetails`.`DeptID` is null,concat('FACULTY of ',`tblfaculty`.`FacultyName`,' ',`vw94_feedetails`.`For`,' RETURNING STUDENTS'),if(`vw94_feedetails`.`FeeCategoryID` = 1 and `vw94_feedetails`.`FacultyID` is null,concat('DEPARTMENT of ',`tbldepartment`.`DeptName`,' ',`vw94_feedetails`.`For`,' RETURNING STUDENTS'),if(`vw94_feedetails`.`FeeCategoryID` = 2 and `vw94_feedetails`.`DeptID` is null,concat('FACULTY of ',`tblfaculty`.`FacultyName`,' ',`vw94_feedetails`.`For`,' FRESH STUDENTS'),if(`vw94_feedetails`.`FeeCategoryID` = 2 and `vw94_feedetails`.`FacultyID` is null,concat('DEPARTMENT of ',`tbldepartment`.`DeptName`,' ',`vw94_feedetails`.`For`,' FRESH STUDENTS'),'')))) AS `EligibleStudents` from ((`vw94_feedetails` left join `tblfaculty` on(`vw94_feedetails`.`FacultyID` = `tblfaculty`.`FacultyID`)) left join `tbldepartment` on(`vw94_feedetails`.`DeptID` = `tbldepartment`.`DeptID`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw95_user`
--

/*!50001 DROP TABLE IF EXISTS `vw95_user`*/;
/*!50001 DROP VIEW IF EXISTS `vw95_user`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw95_user` AS select `tbluser`.`UserID` AS `UserID`,`tbluser`.`UserPIN` AS `UserPIN`,concat(`tbluser`.`UserPIN`,' ',`tbluser`.`Surname`,' ',`tbluser`.`Others`,' ',`tbluser`.`Sex`,' ',`tbluser`.`Title`,' ',`tblusercategory`.`UserCategory`) AS `SearchData`,`tbluser`.`UserPwd` AS `UserPwd`,concat(`tbluser`.`Surname`,', ',`tbluser`.`Others`) AS `UserName`,`tbluser`.`Surname` AS `Surname`,`tbluser`.`Others` AS `Others`,`tbluser`.`Sex` AS `Sex`,`tbluser`.`Title` AS `Title`,`tbluser`.`UserGroupID` AS `UserGroupID`,if(`tbluser`.`UserGroupID` = 1,'ACADEMIC STAFF','NON-ACADEMIC STAFF') AS `UserGroup`,`tbluser`.`UserCategoryID` AS `UserCategoryID`,`tblusercategory`.`UserCategory` AS `UserCategory`,`tbluser`.`UserDeptID` AS `UserDeptID`,`vw7_department`.`FacultyID` AS `FacultyID`,`vw7_department`.`FacultyName` AS `FacultyName`,`vw7_department`.`FacultyCode` AS `FacultyCode`,`vw7_department`.`FacultyColourID` AS `FacultyColourID`,`vw7_department`.`ColourName` AS `ColourName`,`vw7_department`.`DeptName` AS `DeptName`,`vw7_department`.`DeptCode` AS `DeptCode`,`tbluser`.`Phone` AS `Phone`,`tbluser`.`Email` AS `Email`,`tbluser`.`ContactAddress` AS `ContactAddress`,`tbluser`.`UserStatusID` AS `UserStatusID`,if(`tbluser`.`UserStatusID` = 1,'ACTIVATED','DEACTIVATED') AS `UserStatus`,concat('X-',if(`vw7_department`.`DeptCode` is null,'AUX',`vw7_department`.`DeptCode`),'-',convert(if(octet_length(`tbluser`.`UserID`) = 1,concat('000',hex(`tbluser`.`UserID`)),if(octet_length(`tbluser`.`UserID`) = 2,concat('00',hex(`tbluser`.`UserID`)),if(octet_length(`tbluser`.`UserID`) = 3,concat('0',hex(`tbluser`.`UserID`)),hex(`tbluser`.`UserID`)))) using latin1),left(`tbluser`.`Surname`,1),left(`tbluser`.`Others`,1)) AS `PIN`,`tbluser`.`Photo` AS `Photo`,`tbluser`.`PwdQuestion1` AS `PwdQuestion1`,`tbluser`.`PwdAnswer1` AS `PwdAnswer1`,`tbluser`.`PwdQuestion2` AS `PwdQuestion2`,`tbluser`.`PwdAnswer2` AS `PwdAnswer2`,`tbluser`.`StaffNumber` AS `StaffNumber`,`tbluser`.`DyofB` AS `DyofB`,`tbluser`.`MofB` AS `MofB`,`tbluser`.`EmploymentDate` AS `EmploymentDate`,`tbluser`.`AppointmentDate` AS `AppointmentDate`,`tbluser`.`GradeLevel` AS `GradeLevel` from ((`tbluser` left join `vw7_department` on(`tbluser`.`UserDeptID` = `vw7_department`.`DeptID`)) join `tblusercategory` on(`tbluser`.`UserCategoryID` = `tblusercategory`.`UserCategoryID`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw96_admission`
--

/*!50001 DROP TABLE IF EXISTS `vw96_admission`*/;
/*!50001 DROP VIEW IF EXISTS `vw96_admission`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw96_admission` AS select `tbladmission`.`AdmissionID` AS `AdmissionID`,`tbladmission`.`RegNo` AS `RegNo`,`tbladmission`.`Surname` AS `Surname`,`tbladmission`.`Others` AS `Others`,`tbladmission`.`Sex` AS `Sex`,`tbladmission`.`Title` AS `Title`,`tbladmission`.`Religion` AS `Religion`,`tbladmission`.`MaritalStatus` AS `MaritalStatus`,`tbladmission`.`DofB` AS `DofB`,`tbladmission`.`PofB` AS `PofB`,`tbladmission`.`Hometown` AS `Hometown`,`tbladmission`.`HomeAddress` AS `HomeAddress`,`tbladmission`.`Phone` AS `Phone`,`tbladmission`.`Email` AS `Email`,`tbladmission`.`NationalityID` AS `NationalityID`,`tbladmission`.`LGAID` AS `LGAID`,`tbladmission`.`Photo` AS `Photo`,`tbladmission`.`AdmYear` AS `AdmYear`,`tbladmission`.`AdmissionModeID` AS `AdmissionModeID`,`tbladmission`.`DegreeCourseID` AS `DegreeCourseID`,`tbladmission`.`BatchNo` AS `BatchNo`,`tbladmission`.`Score` AS `Score`,`tbladmission`.`AdmissionStatusID` AS `AdmissionStatusID`,`tbladmission`.`AcceptanceStatusID` AS `AcceptanceStatusID`,`tbladmission`.`SessionID` AS `SessionID`,`tbladmission`.`FatherName` AS `FatherName`,`tbladmission`.`FatherPhone` AS `FatherPhone`,`tbladmission`.`FatherOccupation` AS `FatherOccupation`,`tbladmission`.`FatherAddress` AS `FatherAddress`,`tbladmission`.`MotherName` AS `MotherName`,`tbladmission`.`MotherPhone` AS `MotherPhone`,`tbladmission`.`MotherOccupation` AS `MotherOccupation`,`tbladmission`.`MotherAddress` AS `MotherAddress`,`tbladmission`.`KinName` AS `KinName`,`tbladmission`.`KinPhone` AS `KinPhone`,`tbladmission`.`KinOccupation` AS `KinOccupation`,`tbladmission`.`KinAddress` AS `KinAddress`,`tbladmission`.`CandidatePwd` AS `CandidatePwd`,`tbladmission`.`MedicalExamDate` AS `MedicalExamDate`,`tbladmission`.`EntryQualification` AS `EntryQualification`,`tbladmission`.`UndertakingID` AS `UndertakingID`,`tbladmission`.`EligibilityTestStatusID` AS `EligibilityTestStatusID`,`tbladmission`.`VerificationStatusID1` AS `VerificationStatusID1`,`tbladmission`.`VerificationStatusID2` AS `VerificationStatusID2`,`tbladmission`.`VerificationStatusID3` AS `VerificationStatusID3`,`tbladmission`.`VerificationOfficerID1` AS `VerificationOfficerID1`,`tbladmission`.`VerificationOfficerID2` AS `VerificationOfficerID2`,`tbladmission`.`VerificationOfficerID3` AS `VerificationOfficerID3`,`tbladmission`.`VerificationDate1` AS `VerificationDate1`,`tbladmission`.`VerificationDate2` AS `VerificationDate2`,`tbladmission`.`VerificationDate3` AS `VerificationDate3`,`tbladmission`.`NewDegreeCourseID` AS `NewDegreeCourseID`,`tbladmission`.`DefermentStatusID` AS `DefermentStatusID`,`tbladmission`.`DefermentNo` AS `DefermentNo`,`tbladmission`.`YearDeferred` AS `YearDeferred`,`tbladmission`.`DefermentReason` AS `DefermentReason`,`tbladmission`.`SessionID_Returned` AS `SessionID_Returned`,`tbladmission`.`Waiver_Acceptance` AS `Waiver_Acceptance`,`tbladmission`.`Waiver_Accommodation` AS `Waiver_Accommodation`,`tbladmission`.`Waiver_SchoolFees` AS `Waiver_SchoolFees`,concat(`tbladmission`.`RegNo`,' ',`tbladmission`.`Surname`,' ',`tbladmission`.`Others`,' ',`tbladmission`.`Sex`,' ',`vw8_degreecourse`.`DegreeCourse`,' ',`vw8_degreecourse`.`FacultyName`,' ',`vw8_degreecourse`.`DeptName`) AS `SearchData`,ucase(concat(`tbladmission`.`Surname`,', ',`tbladmission`.`Others`,' (',`tbladmission`.`RegNo`,')')) AS `AdmissionCandidate`,if(`tbladmission`.`NationalityID` = 0,'FOREIGNER',if(`tbladmission`.`NationalityID` = 1,'NIGERIAN','')) AS `Nationality`,`vw92_lga`.`StateID` AS `StateID`,`vw92_lga`.`State` AS `State`,`vw92_lga`.`LGA` AS `LGA`,`vw8_degreecourse`.`DeptID` AS `DeptID`,`vw8_degreecourse`.`DegreeType` AS `DegreeType`,`vw8_degreecourse`.`DegreeTypeID` AS `DegreeTypeID`,`vw8_degreecourse`.`ProgramType` AS `ProgramType`,`vw8_degreecourse`.`ProgramTypeCode` AS `ProgramTypeCode`,`vw8_degreecourse`.`ProgramTypeID` AS `ProgramTypeID`,`vw8_degreecourse`.`FacultyID` AS `FacultyID`,`vw8_degreecourse`.`FacultyName` AS `FacultyName`,`vw8_degreecourse`.`DeptName` AS `DeptName`,`vw8_degreecourse`.`DeptCode` AS `DeptCode`,`vw8_degreecourse`.`DegreeCourse` AS `DegreeCourse`,`vw8_degreecourse`.`DegreeCourseDuration` AS `DegreeCourseDuration`,if(`tbladmission`.`AdmissionStatusID` = 0,'PENDING',if(`tbladmission`.`AdmissionStatusID` = 1,'NOT ADMITTED','ADMITTED')) AS `AdmissionStatus`,if(`tbladmission`.`AcceptanceStatusID` = 2,'ACCEPTED','NOT ACCEPTED') AS `AcceptanceStatus`,if(`tbladmission`.`Sex` = 'MALE',1,0) AS `MaleCount`,if(`tbladmission`.`Sex` = 'FEMALE',1,0) AS `FemaleCount`,left(`tbladmission`.`Sex`,1) AS `Gender`,if(`vw8_degreecourse`.`DegreeType` <> 'UNDER-GRADUATE','NOT APPLICABLE',if(left(`vw8_degreecourse`.`ProgramTypeCode`,1) = 'P',`vw8_degreecourse`.`ProgramTypeCode`,convert(if(`tbladmission`.`AdmissionModeID` = 1,'UTME',if(`tbladmission`.`AdmissionModeID` = 2,'DIRECT-ENTRY','')) using latin1))) AS `AdmissionMode`,if(`tbladmission`.`EligibilityTestStatusID` = 1,'CLEARED','NOT CLEARED') AS `EligibilityTestStatus`,if(`tbladmission`.`Sex` is null or `tbladmission`.`Title` is null or `tbladmission`.`Religion` is null or `tbladmission`.`MaritalStatus` is null or `tbladmission`.`DofB` is null or `tbladmission`.`PofB` is null or `tbladmission`.`Hometown` is null or `tbladmission`.`NationalityID` is null or `tbladmission`.`LGAID` is null,1,0) AS `RegCheck1`,if(`tbladmission`.`Phone` is null or `tbladmission`.`Email` is null or `tbladmission`.`HomeAddress` is null,1,0) AS `RegCheck2`,if(`tbladmission`.`Sex` is null or `tbladmission`.`FatherName` is null or `tbladmission`.`FatherPhone` is null or `tbladmission`.`FatherOccupation` is null or `tbladmission`.`FatherAddress` is null or `tbladmission`.`MotherName` is null or `tbladmission`.`MotherPhone` is null or `tbladmission`.`MotherOccupation` is null or `tbladmission`.`MotherAddress` is null,1,0) AS `RegCheck3`,if(`tbladmission`.`KinName` is null or `tbladmission`.`KinPhone` is null or `tbladmission`.`KinOccupation` is null or `tbladmission`.`KinAddress` is null,1,0) AS `RegCheck4`,if(`tbladmission`.`CandidatePwd` is null,1,0) AS `RegCheck5`,if(`tbladmission`.`AcceptanceStatusID` = 2,1,0) AS `AcceptedCount`,if(`tbladmission`.`AcceptanceStatusID` <> 2 or `tbladmission`.`AcceptanceStatusID` is null,1,0) AS `NotAcceptedCount`,if(`tbladmission`.`EligibilityTestStatusID` = 1,1,0) AS `ClearedCount`,if(`tbladmission`.`EligibilityTestStatusID` <> 1 or `tbladmission`.`EligibilityTestStatusID` is null,1,0) AS `NotClearedCount`,if(`tbladmission`.`VerificationStatusID1` = 1,1,0) AS `Verification1ClearedCount`,if(`tbladmission`.`VerificationStatusID1` <> 1 or `tbladmission`.`VerificationStatusID1` is null,1,0) AS `Verification1NotClearedCount`,if(`tbladmission`.`VerificationStatusID2` = 1,1,0) AS `Verification2ClearedCount`,if(`tbladmission`.`VerificationStatusID2` <> 1 or `tbladmission`.`VerificationStatusID2` is null,1,0) AS `Verification2NotClearedCount`,if(`tbladmission`.`VerificationStatusID3` = 1,1,0) AS `Verification3ClearedCount`,if(`tbladmission`.`VerificationStatusID3` <> 1 or `tbladmission`.`VerificationStatusID3` is null,1,0) AS `Verification3NotClearedCount`,if(`vw92_lga`.`State` = 'RIVERS',1,2) AS `IndigeneStatusID`,if(`vw92_lga`.`State` = 'RIVERS','INDIGENE','NON-INDIGENE') AS `IndigeneStatus` from ((`tbladmission` left join `vw92_lga` on(`tbladmission`.`LGAID` = `vw92_lga`.`LGAID`)) left join `vw8_degreecourse` on(`tbladmission`.`DegreeCourseID` = `vw8_degreecourse`.`DegreeCourseID`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw96_admission_summary`
--

/*!50001 DROP TABLE IF EXISTS `vw96_admission_summary`*/;
/*!50001 DROP VIEW IF EXISTS `vw96_admission_summary`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw96_admission_summary` AS select count(`vw96_admission`.`AdmissionID`) AS `TotalCount`,sum(`vw96_admission`.`MaleCount`) AS `MaleCount`,sum(`vw96_admission`.`FemaleCount`) AS `FemaleCount`,sum(`vw96_admission`.`AcceptedCount`) AS `AcceptedCount`,sum(`vw96_admission`.`NotAcceptedCount`) AS `NotAcceptedCount`,sum(`vw96_admission`.`ClearedCount`) AS `ClearedCount`,sum(`vw96_admission`.`NotClearedCount`) AS `NotClearedCount`,sum(`vw96_admission`.`Verification1ClearedCount`) AS `Verification1ClearedCount`,sum(`vw96_admission`.`Verification1NotClearedCount`) AS `Verification1NotClearedCount`,sum(`vw96_admission`.`Verification2ClearedCount`) AS `Verification2ClearedCount`,sum(`vw96_admission`.`Verification2NotClearedCount`) AS `Verification2NotClearedCount`,sum(`vw96_admission`.`Verification3ClearedCount`) AS `Verification3ClearedCount`,sum(`vw96_admission`.`Verification3NotClearedCount`) AS `Verification3NotClearedCount`,`vw96_admission`.`FacultyID` AS `FacultyID`,`vw96_admission`.`FacultyName` AS `FacultyName`,`vw96_admission`.`DegreeType` AS `DegreeType`,`vw96_admission`.`ProgramType` AS `ProgramType`,`vw96_admission`.`SessionID` AS `SessionID`,`vw96_admission`.`ProgramTypeID` AS `ProgramTypeID`,`vw96_admission`.`BatchNo` AS `BatchNo`,`vw96_admission`.`DegreeTypeID` AS `DegreeTypeID` from `vw96_admission` group by `vw96_admission`.`FacultyID`,`vw96_admission`.`FacultyName`,`vw96_admission`.`DegreeType`,`vw96_admission`.`ProgramType`,`vw96_admission`.`SessionID`,`vw96_admission`.`ProgramTypeID`,`vw96_admission`.`DegreeTypeID`,`vw96_admission`.`BatchNo` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw977_guardians`
--

/*!50001 DROP TABLE IF EXISTS `vw977_guardians`*/;
/*!50001 DROP VIEW IF EXISTS `vw977_guardians`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw977_guardians` AS select `tblguardian`.`GuardianID` AS `GuardianID`,`tblguardian`.`Surname` AS `Surname`,`tblguardian`.`Others` AS `Others`,`tblguardian`.`Title` AS `Title`,`tblguardian`.`Email` AS `Email`,`tblguardian`.`Phone` AS `Phone`,`tblguardian`.`Address` AS `Address`,`tblguardian`.`UserPwd` AS `UserPwd`,`tblguardian`.`UserPIN` AS `UserPIN`,`tblguardian`.`PwdQuestion1` AS `PwdQuestion1`,`tblguardian`.`PwdAnswer1` AS `PwdAnswer1`,`tblguardian`.`PwdQuestion2` AS `PwdQuestion2`,`tblguardian`.`PwdAnswer2` AS `PwdAnswer2`,concat('A-UPH-',convert(if(octet_length(`tblguardian`.`GuardianID`) = 1,concat('000',hex(`tblguardian`.`GuardianID`)),if(octet_length(`tblguardian`.`GuardianID`) = 2,concat('00',hex(`tblguardian`.`GuardianID`)),if(octet_length(`tblguardian`.`GuardianID`) = 3,concat('0',hex(`tblguardian`.`GuardianID`)),hex(`tblguardian`.`GuardianID`)))) using latin1),left(`tblguardian`.`Surname`,1),left(`tblguardian`.`Others`,1)) AS `PIN`,concat(`tblguardian`.`Surname`,', ',`tblguardian`.`Others`) AS `UserName` from `tblguardian` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw977_guardians_accessrequest`
--

/*!50001 DROP TABLE IF EXISTS `vw977_guardians_accessrequest`*/;
/*!50001 DROP VIEW IF EXISTS `vw977_guardians_accessrequest`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw977_guardians_accessrequest` AS select `tblaccessrequest`.`RequestID` AS `RequestID`,`tblaccessrequest`.`GuardianID` AS `GuardianID`,`vw977_guardians`.`UserName` AS `GuardianName`,`tblaccessrequest`.`StudentID` AS `StudentID`,`vw97_student`.`StudentName` AS `StudentName`,`vw97_student`.`RegNo` AS `RegNo`,`vw97_student`.`MatNo` AS `MatNo`,`tblaccessrequest`.`RequestTimeStamp` AS `RequestTimeStamp`,`tblaccessrequest`.`RequestStatusID` AS `RequestStatusID`,if(`tblaccessrequest`.`RequestStatusID` = 0,'PENDING',if(`tblaccessrequest`.`RequestStatusID` = 1,'ACCEPTED','DECLINED')) AS `RequestStatus`,`tblaccessrequest`.`AcceptanceTimeStamp` AS `AcceptanceTimeStamp`,`tblaccessrequest`.`AccessStatusID` AS `AccessStatusID`,if(`tblaccessrequest`.`AccessStatusID` = 0,'PENDING',if(`tblaccessrequest`.`AccessStatusID` = 1,'TEMPORAL ACCESS','PERMANENT ACCESS')) AS `AccessStatus` from ((`tblaccessrequest` join `vw977_guardians` on(`tblaccessrequest`.`GuardianID` = `vw977_guardians`.`GuardianID`)) left join `vw97_student` on(`tblaccessrequest`.`StudentID` = `vw97_student`.`StudentID`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw97_student`
--

/*!50001 DROP TABLE IF EXISTS `vw97_student`*/;
/*!50001 DROP VIEW IF EXISTS `vw97_student`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw97_student` AS select `tblstudent`.`StudentID` AS `StudentID`,`tblstudent`.`AdmissionID` AS `AdmissionID`,`tblstudent`.`RegNo` AS `RegNo`,`tblstudent`.`MatNo` AS `MatNo`,`tblstudent`.`Surname` AS `Surname`,`tblstudent`.`Others` AS `Others`,`tblstudent`.`Sex` AS `Sex`,`tblstudent`.`Title` AS `Title`,`tblstudent`.`Religion` AS `Religion`,`tblstudent`.`MaritalStatus` AS `MaritalStatus`,`tblstudent`.`DofB` AS `DofB`,`tblstudent`.`PofB` AS `PofB`,`tblstudent`.`Hometown` AS `Hometown`,`tblstudent`.`HomeAddress` AS `HomeAddress`,`tblstudent`.`Phone` AS `Phone`,`tblstudent`.`Email` AS `Email`,`tblstudent`.`NationalityID` AS `NationalityID`,`tblstudent`.`LGAID` AS `LGAID`,`tblstudent`.`Photo` AS `Photo`,`tblstudent`.`AdmYear` AS `AdmYear`,`tblstudent`.`AdmissionModeID` AS `AdmissionModeID`,`tblstudent`.`DegreeCourseID` AS `DegreeCourseID`,`tblstudent`.`EntryQualification` AS `EntryQualification`,`tblstudent`.`StatusID` AS `StatusID`,`tblstudent`.`GradYear` AS `GradYear`,`tblstudent`.`GraduationStatusID` AS `GraduationStatusID`,`tblstudent`.`StudentPIN` AS `StudentPIN`,`tblstudent`.`StudentPwd` AS `StudentPwd`,`tblstudent`.`FatherName` AS `FatherName`,`tblstudent`.`FatherPhone` AS `FatherPhone`,`tblstudent`.`FatherOccupation` AS `FatherOccupation`,`tblstudent`.`FatherAddress` AS `FatherAddress`,`tblstudent`.`MotherName` AS `MotherName`,`tblstudent`.`MotherPhone` AS `MotherPhone`,`tblstudent`.`MotherOccupation` AS `MotherOccupation`,`tblstudent`.`MotherAddress` AS `MotherAddress`,`tblstudent`.`KinName` AS `KinName`,`tblstudent`.`KinPhone` AS `KinPhone`,`tblstudent`.`KinOccupation` AS `KinOccupation`,`tblstudent`.`KinAddress` AS `KinAddress`,`tblstudent`.`AdditionalYear` AS `AdditionalYear`,`tblstudent`.`FinalClearanceDate` AS `FinalClearanceDate`,`tblstudent`.`ClearanceStatusID` AS `ClearanceStatusID`,`tblstudent`.`PwdQuestion1` AS `PwdQuestion1`,`tblstudent`.`PwdAnswer1` AS `PwdAnswer1`,`tblstudent`.`PwdQuestion2` AS `PwdQuestion2`,`tblstudent`.`PwdAnswer2` AS `PwdAnswer2`,concat(`tblstudent`.`RegNo`,' ',`tblstudent`.`Surname`,' ',`tblstudent`.`Others`,' ',`tblstudent`.`Sex`,' ',`tblstudent`.`Title`,' ',`vw8_degreecourse`.`DegreeCourse`,' ',`vw8_degreecourse`.`FacultyName`,' ',`vw8_degreecourse`.`DeptName`,' ',`vw8_degreecourse`.`ProgramType`,' ',`vw8_degreecourse`.`DegreeType`,' ',`tblstudent`.`AdmYear`,' ',`tblstudent`.`StudentPIN`) AS `SearchData`,right(`tblstudent`.`MatNo`,octet_length(`tblstudent`.`MatNo`) - 6) AS `ActualMatNo`,concat(`tblstudent`.`Surname`,', ',`tblstudent`.`Others`) AS `StudentName`,if(`tblstudent`.`ClearanceStatusID` = 1,'CLEARED','NOT CLEARED') AS `ClearanceStatus`,left(`tblstudent`.`Sex`,1) AS `Gender`,if(`tblstudent`.`NationalityID` = 0,'FOREIGNER','NIGERIAN') AS `Nationality`,`vw92_lga`.`StateID` AS `StateID`,`vw92_lga`.`State` AS `State`,`vw92_lga`.`LGA` AS `LGA`,`vw8_degreecourse`.`DeptID` AS `DeptID`,`vw8_degreecourse`.`FacultyID` AS `FacultyID`,`vw8_degreecourse`.`FacultyName` AS `FacultyName`,`vw8_degreecourse`.`FacultyCode` AS `FacultyCode`,`vw8_degreecourse`.`FacultyColourID` AS `FacultyColourID`,`vw8_degreecourse`.`ColourName` AS `ColourName`,`vw8_degreecourse`.`DeptName` AS `DeptName`,`vw8_degreecourse`.`DeptCode` AS `DeptCode`,`vw8_degreecourse`.`HODID` AS `HODID`,`vw8_degreecourse`.`HOD` AS `HOD`,`vw8_degreecourse`.`FacultyDeanID` AS `FacultyDeanID`,`vw8_degreecourse`.`Dean` AS `Dean`,`vw8_degreecourse`.`DegreeCourse` AS `DegreeCourse`,`vw8_degreecourse`.`DegreeTypeID` AS `DegreeTypeID`,`vw8_degreecourse`.`DegreeType` AS `DegreeType`,`vw8_degreecourse`.`ProgramTypeID` AS `ProgramTypeID`,`vw8_degreecourse`.`ProgramType` AS `ProgramType`,`vw8_degreecourse`.`ProgramTypeCode` AS `ProgramTypeCode`,`vw8_degreecourse`.`DegreeCourseDuration` AS `DegreeCourseDuration`,`vw8_degreecourse`.`DegreeAwardID` AS `DegreeAwardID`,`vw8_degreecourse`.`DegreeAward` AS `DegreeAward`,`vw8_degreecourse`.`DigitCode` AS `DigitCode`,`vw8_degreecourse`.`DegreeAwardCode` AS `DegreeAwardCode`,if(`tblstudent`.`StatusID` = 0,'DEACTIVATED','ACTIVATED') AS `Status`,if(`tblstudent`.`GraduationStatusID` = 0,'NOT GRADUATED','GRADUATED') AS `GraduationStatus`,if(`vw8_degreecourse`.`DegreeType` <> 'UNDER-GRADUATE','NOT APPLICABLE',if(left(`vw8_degreecourse`.`ProgramTypeCode`,1) = 'P',`vw8_degreecourse`.`ProgramTypeCode`,convert(if(`tblstudent`.`AdmissionModeID` = 1,'UTME',if(`tblstudent`.`AdmissionModeID` = 2,'DIRECT-ENTRY','')) using latin1))) AS `AdmissionMode`,concat(convert(if(`vw8_degreecourse`.`DegreeType` like '%UNDER%' or `vw8_degreecourse`.`DegreeType` = 'ACIB','U',if(`vw8_degreecourse`.`DegreeType` like '%POST%','G',if(`vw8_degreecourse`.`DegreeType` like '%MASTER%','M',if(`vw8_degreecourse`.`DegreeType` like '%DOCTOR%','D','')))) using latin1),'-',`vw8_degreecourse`.`DeptCode`,'-',convert(if(octet_length(`tblstudent`.`StudentID`) = 1,concat('000',hex(`tblstudent`.`StudentID`)),if(octet_length(`tblstudent`.`StudentID`) = 2,concat('00',hex(`tblstudent`.`StudentID`)),if(octet_length(`tblstudent`.`StudentID`) = 3,concat('0',hex(`tblstudent`.`StudentID`)),hex(`tblstudent`.`StudentID`)))) using latin1),left(`tblstudent`.`Surname`,1),left(`tblstudent`.`Others`,1)) AS `PIN`,if(locate('',`tblstudent`.`Others`) = 0,`tblstudent`.`Others`,left(`tblstudent`.`Others`,locate(' ',`tblstudent`.`Others`))) AS `Firstname`,if(month(curdate()) - month(`tblstudent`.`DofB`) = 0,year(curdate()) - year(`tblstudent`.`DofB`),year(curdate()) - year(`tblstudent`.`DofB`) - 1) AS `AgeNow`,dayofmonth(`tblstudent`.`DofB`) AS `dyofb`,week(`tblstudent`.`DofB`,0) AS `wkofb`,date_format(`tblstudent`.`DofB`,'%b') AS `mnthofb`,month(`tblstudent`.`DofB`) AS `mofb`,year(`tblstudent`.`DofB`) AS `yrofb`,if(`vw92_lga`.`State` = 'RIVERS',1,2) AS `IndigeneStatusID`,if(`vw92_lga`.`State` = 'RIVERS','INDIGENE','NON-INDIGENE') AS `IndigeneStatus`,`vw8_degreecourse`.`DegreeCourseDuration` + `tblstudent`.`AdditionalYear` + 2 AS `LegalCourseDuration`,if(`tblstudent`.`Sex` is null or `tblstudent`.`Title` is null or `tblstudent`.`Religion` is null or `tblstudent`.`MaritalStatus` is null or `tblstudent`.`DofB` is null or `tblstudent`.`PofB` is null or `tblstudent`.`Hometown` is null or `tblstudent`.`NationalityID` is null or `tblstudent`.`LGAID` is null or `tblstudent`.`RegNo` is null,1,0) AS `RegCheck1`,if(`tblstudent`.`Phone` is null or `tblstudent`.`Email` is null or `tblstudent`.`HomeAddress` is null,1,0) AS `RegCheck2`,if(`tblstudent`.`FatherName` is null or `tblstudent`.`FatherPhone` is null or `tblstudent`.`FatherOccupation` is null or `tblstudent`.`FatherAddress` is null or `tblstudent`.`MotherName` is null or `tblstudent`.`MotherPhone` is null or `tblstudent`.`MotherOccupation` is null or `tblstudent`.`MotherAddress` is null,1,0) AS `RegCheck3`,if(`tblstudent`.`KinName` is null or `tblstudent`.`KinPhone` is null or `tblstudent`.`KinOccupation` is null or `tblstudent`.`KinAddress` is null,1,0) AS `RegCheck4` from ((`tblstudent` left join `vw92_lga` on(`tblstudent`.`LGAID` = `vw92_lga`.`LGAID`)) left join `vw8_degreecourse` on(`tblstudent`.`DegreeCourseID` = `vw8_degreecourse`.`DegreeCourseID`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw97_students_clearance`
--

/*!50001 DROP TABLE IF EXISTS `vw97_students_clearance`*/;
/*!50001 DROP VIEW IF EXISTS `vw97_students_clearance`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw97_students_clearance` AS select `tblonlineclearance`.`ClearanceID` AS `ClearanceID`,`tblonlineclearance`.`StudentID` AS `StudentID`,`tblonlineclearance`.`ApplicationDate` AS `ApplicationDate`,`tblonlineclearance`.`ClearanceStatusID1` AS `ClearanceStatusID1`,`tblonlineclearance`.`ClearanceOfficerID1` AS `ClearanceOfficerID1`,`tblonlineclearance`.`ClearanceDate1` AS `ClearanceDate1`,`tblonlineclearance`.`ClearanceStatusID2` AS `ClearanceStatusID2`,`tblonlineclearance`.`ClearanceOfficerID2` AS `ClearanceOfficerID2`,`tblonlineclearance`.`ClearanceDate2` AS `ClearanceDate2`,`tblonlineclearance`.`ClearanceStatusID3` AS `ClearanceStatusID3`,`tblonlineclearance`.`ClearanceOfficerID3` AS `ClearanceOfficerID3`,`tblonlineclearance`.`ClearanceDate3` AS `ClearanceDate3`,`tblonlineclearance`.`ClearanceStatusID4` AS `ClearanceStatusID4`,`tblonlineclearance`.`ClearanceOfficerID4` AS `ClearanceOfficerID4`,`tblonlineclearance`.`ClearanceDate4` AS `ClearanceDate4`,`tblonlineclearance`.`ClearanceStatusID5` AS `ClearanceStatusID5`,`tblonlineclearance`.`ClearanceOfficerID5` AS `ClearanceOfficerID5`,`tblonlineclearance`.`ClearanceDate5` AS `ClearanceDate5`,`vw97_student`.`StudentPIN` AS `StudentPIN`,`vw97_student`.`RegNo` AS `RegNo`,`vw97_student`.`MatNo` AS `MatNo`,`vw97_student`.`StudentName` AS `StudentName`,`vw97_student`.`Surname` AS `Surname`,`vw97_student`.`AdmissionModeID` AS `AdmissionModeID`,`vw97_student`.`Others` AS `Others`,`vw97_student`.`Sex` AS `Sex`,`vw97_student`.`Gender` AS `Gender`,`vw97_student`.`DegreeCourseID` AS `DegreeCourseID`,`vw97_student`.`DegreeCourse` AS `DegreeCourse`,`vw97_student`.`DeptID` AS `DeptID`,`vw97_student`.`FacultyID` AS `FacultyID`,`vw97_student`.`FacultyName` AS `FacultyName`,`vw97_student`.`FacultyCode` AS `FacultyCode`,`vw97_student`.`DeptName` AS `DeptName`,`vw97_student`.`DeptCode` AS `DeptCode`,`vw97_student`.`DegreeTypeID` AS `DegreeTypeID`,`vw97_student`.`DegreeType` AS `DegreeType`,`vw97_student`.`ProgramTypeID` AS `ProgramTypeID`,`vw97_student`.`ProgramType` AS `ProgramType`,`vw97_student`.`ProgramTypeCode` AS `ProgramTypeCode`,`vw97_student`.`DegreeCourseDuration` AS `DegreeCourseDuration`,`vw97_student`.`AdmYear` AS `AdmYear` from (`tblonlineclearance` join `vw97_student` on(`tblonlineclearance`.`StudentID` = `vw97_student`.`StudentID`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw97_studyyearmgt`
--

/*!50001 DROP TABLE IF EXISTS `vw97_studyyearmgt`*/;
/*!50001 DROP VIEW IF EXISTS `vw97_studyyearmgt`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw97_studyyearmgt` AS select `vw97_student`.`StudentID` AS `StudentID`,`vw97_student`.`AdmYear` AS `AdmYear`,`vw97_student`.`StatusID` AS `StatusID`,`vw97_student`.`GraduationStatusID` AS `GraduationStatusID`,`vw97_student`.`AdmissionModeID` AS `AdmissionModeID`,`vw97_student`.`DegreeCourseDuration` AS `DegreeCourseDuration`,`vw97_student`.`AdditionalYear` AS `AdditionalYear`,`vw2_academicsession`.`SessionYear` AS `SessionYear`,`vw2_academicsession`.`SessionStatusID` AS `SessionStatusID`,`vw97_student`.`LegalCourseDuration` AS `LegalCourseDuration`,`vw2_academicsession`.`SessionYear` - `vw97_student`.`AdmYear` + `vw97_student`.`AdmissionModeID` AS `StudyYear` from (`vw2_academicsession` join `vw97_student`) where `vw97_student`.`StatusID` = 1 and `vw97_student`.`GraduationStatusID` = 0 and `vw2_academicsession`.`SessionStatusID` = 1 */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw98_registration_students`
--

/*!50001 DROP TABLE IF EXISTS `vw98_registration_students`*/;
/*!50001 DROP VIEW IF EXISTS `vw98_registration_students`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw98_registration_students` AS select `tblregistration_students`.`StudentRegID` AS `StudentRegID`,`tblregistration_students`.`StudentID` AS `StudentID`,`vw97_student`.`StudentPIN` AS `StudentPIN`,`vw97_student`.`RegNo` AS `RegNo`,`vw97_student`.`MatNo` AS `MatNo`,`vw97_student`.`StudentName` AS `StudentName`,`vw97_student`.`Surname` AS `Surname`,`vw97_student`.`AdmissionModeID` AS `AdmissionModeID`,`vw97_student`.`Others` AS `Others`,`vw97_student`.`Sex` AS `Sex`,`vw97_student`.`Gender` AS `Gender`,if(`vw97_student`.`Gender` = 'M',1,0) AS `MaleCount`,if(`vw97_student`.`Gender` = 'F',1,0) AS `FemaleCount`,`vw97_student`.`DegreeCourseID` AS `DegreeCourseID`,`vw97_student`.`DegreeCourse` AS `DegreeCourse`,`vw97_student`.`DeptID` AS `DeptID`,`vw97_student`.`FacultyID` AS `FacultyID`,`vw97_student`.`FacultyName` AS `FacultyName`,`vw97_student`.`FacultyCode` AS `FacultyCode`,`vw97_student`.`DeptName` AS `DeptName`,`vw97_student`.`DeptCode` AS `DeptCode`,`vw97_student`.`DegreeTypeID` AS `DegreeTypeID`,`vw97_student`.`DegreeType` AS `DegreeType`,`vw97_student`.`ProgramTypeID` AS `ProgramTypeID`,`vw97_student`.`ProgramType` AS `ProgramType`,`vw97_student`.`ProgramTypeCode` AS `ProgramTypeCode`,`vw97_student`.`DegreeCourseDuration` AS `DegreeCourseDuration`,`vw97_student`.`AdmYear` AS `AdmYear`,`vw2_academicsession`.`SessionYear` - `vw97_student`.`AdmYear` + `vw97_student`.`AdmissionModeID` AS `StudyYear`,`tblregistration_students`.`SessionID` AS `SessionID`,`vw2_academicsession`.`SessionName` AS `SessionName`,`vw2_academicsession`.`SessionYear` AS `SessionYear`,`tblregistration_students`.`LevelID` AS `LevelID`,`vw91_courselevel`.`CourseLevelID` AS `CourseLevelID`,`vw91_courselevel`.`CourseLevel` AS `CourseLevel`,`tblregistration_students`.`Max1` AS `Max1`,`tblregistration_students`.`Min1` AS `Min1`,`tblregistration_students`.`Max2` AS `Max2`,`tblregistration_students`.`Min2` AS `Min2`,`tblregistration_students`.`Max3` AS `Max3`,`tblregistration_students`.`Min3` AS `Min3`,`tblregistration_students`.`RegDate` AS `RegDate`,`tblregistration_students`.`AdviserApproval` AS `AdviserApproval`,if(`tblregistration_students`.`AdviserApproval` is null,0,`tblregistration_students`.`AdviserApproval`) AS `UnitApproval1ID`,`tblregistration_students`.`HODApproval` AS `HODApproval`,if(`tblregistration_students`.`HODApproval` is null,0,1) AS `UnitApproval2ID`,`tblregistration_students`.`DeanApproval` AS `DeanApproval`,if(`tblregistration_students`.`DeanApproval` is null,0,1) AS `UnitApproval3ID`,`tblregistration_students`.`ApprovalDate1` AS `ApprovalDate1`,`tblregistration_students`.`ApprovalDate2` AS `ApprovalDate2`,`tblregistration_students`.`ApprovalDate3` AS `ApprovalDate3` from (((`tblregistration_students` join `vw97_student` on(`tblregistration_students`.`StudentID` = `vw97_student`.`StudentID`)) join `vw2_academicsession` on(`tblregistration_students`.`SessionID` = `vw2_academicsession`.`SessionID`)) join `vw91_courselevel` on(`tblregistration_students`.`LevelID` = `vw91_courselevel`.`LevelID`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw992_trnxs`
--

/*!50001 DROP TABLE IF EXISTS `vw992_trnxs`*/;
/*!50001 DROP VIEW IF EXISTS `vw992_trnxs`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw992_trnxs` AS select `tbltrnxs`.`TrnxID` AS `TrnxID`,`tbltrnxs`.`StudentID` AS `StudentID`,`tbltrnxs`.`AdmissionID` AS `AdmissionID`,`tbltrnxs`.`SessionID` AS `SessionID`,`tbltrnxs`.`LevelID` AS `LevelID`,`tbltrnxs`.`FeeID` AS `FeeID`,`tbltrnxs`.`FeeDetailID` AS `FeeDetailID`,`tbltrnxs`.`FeeTypeID` AS `FeeTypeID`,`tbltrnxs`.`FeeItemID` AS `FeeItemID`,`tbltrnxs`.`InstallmentCount` AS `InstallmentCount`,`tbltrnxs`.`TrnxType` AS `TrnxType`,`tbltrnxs`.`TrnxDate` AS `TrnxDate`,`tbltrnxs`.`TrnxDesc` AS `TrnxDesc`,`tbltrnxs`.`DRAmount` AS `DRAmount`,`tbltrnxs`.`CRAmount` AS `CRAmount`,`tbltrnxs`.`RRR` AS `RRR`,`tbltrnxs`.`OrderID` AS `OrderID`,`tbltrnxs`.`ServiceTypeID` AS `ServiceTypeID`,`tbltrnxs`.`PaymentID` AS `PaymentID`,`tbltrnxs`.`VerifiedByID` AS `VerifiedByID`,`tbltrnxs`.`Remark` AS `Remark`,`tbltrnxs`.`TrnxDateStamp` AS `TrnxDateStamp`,`vw2_academicsession`.`SessionName` AS `SessionName`,`vw2_academicsession`.`SessionYear` AS `SessionYear`,`vw91_courselevel`.`DegreeCourseID` AS `DegreeCourseID`,`vw91_courselevel`.`DeptID` AS `DeptID`,`vw91_courselevel`.`FacultyID` AS `FacultyID`,`vw91_courselevel`.`FacultyName` AS `FacultyName`,`vw91_courselevel`.`DeptName` AS `DeptName`,`vw91_courselevel`.`DegreeCourse` AS `DegreeCourse`,`vw91_courselevel`.`DegreeTypeID` AS `DegreeTypeID`,`vw91_courselevel`.`DegreeType` AS `DegreeType`,`vw91_courselevel`.`ProgramTypeID` AS `ProgramTypeID`,`vw91_courselevel`.`ProgramType` AS `ProgramType`,`vw91_courselevel`.`ProgramTypeCode` AS `ProgramTypeCode`,`vw91_courselevel`.`CourseLevelID` AS `CourseLevelID`,`vw91_courselevel`.`CourseLevel` AS `CourseLevel`,if(`tbltrnxs`.`DRAmount` = 0,`tbltrnxs`.`CRAmount` * -1,`tbltrnxs`.`DRAmount`) AS `Amount`,if(`tbltrnxs`.`FeeTypeID` = 1,'ACCEPTANCE FEE',if(`tbltrnxs`.`FeeTypeID` = 2,'ACCOMMODATION FEE',if(`tbltrnxs`.`FeeTypeID` = 3,'SCHOOL FEE CHARGE',if(`tbltrnxs`.`FeeTypeID` = 4,'OTHER CHARGES','')))) AS `FeeType`,if(`tbltrnxs`.`SessionID` is null,year(`tbltrnxs`.`TrnxDate`),`vw2_academicsession`.`SessionYear`) AS `TrnxYear` from ((`tbltrnxs` left join `vw2_academicsession` on(`tbltrnxs`.`SessionID` = `vw2_academicsession`.`SessionID`)) left join `vw91_courselevel` on(`tbltrnxs`.`LevelID` = `vw91_courselevel`.`LevelID`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw992_trnxs_detailed`
--

/*!50001 DROP TABLE IF EXISTS `vw992_trnxs_detailed`*/;
/*!50001 DROP VIEW IF EXISTS `vw992_trnxs_detailed`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw992_trnxs_detailed` AS select `vw992_trnxs`.`TrnxID` AS `TrnxID`,`vw992_trnxs`.`StudentID` AS `StudentID`,`vw992_trnxs`.`AdmissionID` AS `AdmissionID`,`vw992_trnxs`.`SessionID` AS `SessionID`,`vw992_trnxs`.`LevelID` AS `LevelID`,`vw992_trnxs`.`FeeID` AS `FeeID`,`vw992_trnxs`.`FeeDetailID` AS `FeeDetailID`,`vw992_trnxs`.`FeeTypeID` AS `FeeTypeID`,`vw992_trnxs`.`FeeItemID` AS `FeeItemID`,`vw992_trnxs`.`InstallmentCount` AS `InstallmentCount`,`vw992_trnxs`.`TrnxType` AS `TrnxType`,`vw992_trnxs`.`TrnxDate` AS `TrnxDate`,`vw992_trnxs`.`TrnxDesc` AS `TrnxDesc`,`vw992_trnxs`.`DRAmount` AS `DRAmount`,`vw992_trnxs`.`CRAmount` AS `CRAmount`,`vw992_trnxs`.`RRR` AS `RRR`,`vw992_trnxs`.`OrderID` AS `OrderID`,`vw992_trnxs`.`ServiceTypeID` AS `ServiceTypeID`,`vw992_trnxs`.`PaymentID` AS `PaymentID`,`vw992_trnxs`.`VerifiedByID` AS `VerifiedByID`,`vw992_trnxs`.`Remark` AS `Remark`,`vw992_trnxs`.`TrnxDateStamp` AS `TrnxDateStamp`,`vw992_trnxs`.`SessionName` AS `SessionName`,`vw992_trnxs`.`SessionYear` AS `SessionYear`,`vw992_trnxs`.`DegreeCourseID` AS `DegreeCourseID`,`vw992_trnxs`.`DeptID` AS `DeptID`,`vw992_trnxs`.`FacultyID` AS `FacultyID`,`vw992_trnxs`.`FacultyName` AS `FacultyName`,`vw992_trnxs`.`DeptName` AS `DeptName`,`vw992_trnxs`.`DegreeCourse` AS `DegreeCourse`,`vw992_trnxs`.`DegreeTypeID` AS `DegreeTypeID`,`vw992_trnxs`.`DegreeType` AS `DegreeType`,`vw992_trnxs`.`ProgramTypeID` AS `ProgramTypeID`,`vw992_trnxs`.`ProgramType` AS `ProgramType`,`vw992_trnxs`.`ProgramTypeCode` AS `ProgramTypeCode`,`vw992_trnxs`.`CourseLevelID` AS `CourseLevelID`,`vw992_trnxs`.`CourseLevel` AS `CourseLevel`,`vw992_trnxs`.`TrnxYear` AS `TrnxYear`,`vw992_trnxs`.`Amount` AS `Amount`,`vw992_trnxs`.`FeeType` AS `FeeType`,if(`vw992_trnxs`.`StudentID` is null,`vw96_admission`.`RegNo`,`vw97_student`.`RegNo`) AS `RegNo`,if(`vw992_trnxs`.`StudentID` is null,'',`vw97_student`.`MatNo`) AS `MatNo`,if(`vw992_trnxs`.`StudentID` is null,`vw96_admission`.`Surname`,`vw97_student`.`Surname`) AS `Surname`,if(`vw992_trnxs`.`StudentID` is null,`vw96_admission`.`Others`,`vw97_student`.`Others`) AS `Others`,if(`vw992_trnxs`.`StudentID` is null,`vw96_admission`.`Sex`,`vw97_student`.`Sex`) AS `Sex`,if(`vw992_trnxs`.`StudentID` is null,`vw96_admission`.`Gender`,`vw97_student`.`Gender`) AS `Gender`,if(`vw992_trnxs`.`StudentID` is null,`vw96_admission`.`AdmissionCandidate`,`vw97_student`.`StudentName`) AS `StudentName`,if(`vw992_trnxs`.`StudentID` is null,`vw96_admission`.`State`,`vw97_student`.`State`) AS `State`,if(`vw992_trnxs`.`StudentID` is null,`vw96_admission`.`LGA`,`vw97_student`.`LGA`) AS `LGA`,if(`vw992_trnxs`.`StudentID` is null,`vw96_admission`.`IndigeneStatusID`,`vw97_student`.`IndigeneStatusID`) AS `IndigeneStatusID`,if(`vw992_trnxs`.`StudentID` is null,`vw96_admission`.`IndigeneStatus`,`vw97_student`.`IndigeneStatus`) AS `IndigeneStatus`,if(`vw992_trnxs`.`StudentID` is null,`vw96_admission`.`Phone`,`vw97_student`.`Phone`) AS `Phone`,if(`vw992_trnxs`.`StudentID` is null,`vw96_admission`.`Email`,`vw97_student`.`Email`) AS `Email` from ((`vw992_trnxs` left join `vw97_student` on(`vw992_trnxs`.`StudentID` = `vw97_student`.`StudentID`)) left join `vw96_admission` on(`vw992_trnxs`.`AdmissionID` = `vw96_admission`.`AdmissionID`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw992_trnxs_orders`
--

/*!50001 DROP TABLE IF EXISTS `vw992_trnxs_orders`*/;
/*!50001 DROP VIEW IF EXISTS `vw992_trnxs_orders`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw992_trnxs_orders` AS select `tbltrnxs_orders`.`PaymentID` AS `PaymentID`,`tbltrnxs_orders`.`TrnxID` AS `TrnxID`,`tbltrnxs_orders`.`StudentID` AS `StudentID`,`tbltrnxs_orders`.`AdmissionID` AS `AdmissionID`,`tbltrnxs_orders`.`SessionID` AS `SessionID`,`tbltrnxs_orders`.`LevelID` AS `LevelID`,`tbltrnxs_orders`.`FeeID` AS `FeeID`,`tbltrnxs_orders`.`FeeDetailID` AS `FeeDetailID`,`tbltrnxs_orders`.`FeeTypeID` AS `FeeTypeID`,`tbltrnxs_orders`.`FeeItemID` AS `FeeItemID`,`tbltrnxs_orders`.`InstallmentCount` AS `InstallmentCount`,`tbltrnxs_orders`.`RRR` AS `RRR`,`tbltrnxs_orders`.`OrderID` AS `OrderID`,`tbltrnxs_orders`.`ServiceTypeID` AS `ServiceTypeID`,`tbltrnxs_orders`.`TrnxType` AS `TrnxType`,`tbltrnxs_orders`.`TrnxDate` AS `TrnxDate`,`tbltrnxs_orders`.`TrnxDesc` AS `TrnxDesc`,`tbltrnxs_orders`.`DRAmount` AS `DRAmount`,`tbltrnxs_orders`.`CRAmount` AS `CRAmount`,`tbltrnxs_orders`.`VerifiedByID` AS `VerifiedByID`,`tbltrnxs_orders`.`Remark` AS `Remark`,`tbltrnxs_orders`.`StudyYear` AS `StudyYear`,`tbltrnxs_orders`.`TrnxDateStamp` AS `TrnxDateStamp`,`vw2_academicsession`.`SessionName` AS `SessionName`,`vw2_academicsession`.`SessionYear` AS `SessionYear`,`vw91_courselevel`.`DegreeCourseID` AS `DegreeCourseID`,`vw91_courselevel`.`DeptID` AS `DeptID`,`vw91_courselevel`.`FacultyID` AS `FacultyID`,`vw91_courselevel`.`FacultyName` AS `FacultyName`,`vw91_courselevel`.`DeptName` AS `DeptName`,`vw91_courselevel`.`DegreeCourse` AS `DegreeCourse`,`vw91_courselevel`.`DegreeTypeID` AS `DegreeTypeID`,`vw91_courselevel`.`DegreeType` AS `DegreeType`,`vw91_courselevel`.`ProgramTypeID` AS `ProgramTypeID`,`vw91_courselevel`.`ProgramType` AS `ProgramType`,`vw91_courselevel`.`ProgramTypeCode` AS `ProgramTypeCode`,`vw91_courselevel`.`CourseLevelID` AS `CourseLevelID`,`vw91_courselevel`.`CourseLevel` AS `CourseLevel`,if(`tbltrnxs_orders`.`DRAmount` = 0,`tbltrnxs_orders`.`CRAmount` * -1,`tbltrnxs_orders`.`DRAmount`) AS `Amount`,if(`tbltrnxs_orders`.`FeeTypeID` = 1,'ACCEPTANCE FEE',if(`tbltrnxs_orders`.`FeeTypeID` = 2,'ACCOMMODATION FEE',if(`tbltrnxs_orders`.`FeeTypeID` = 3,'SCHOOL FEE CHARGE',if(`tbltrnxs_orders`.`FeeTypeID` = 4,'OTHER CHARGES','')))) AS `FeeType`,if(`tbltrnxs_orders`.`SessionID` is null,year(`tbltrnxs_orders`.`TrnxDate`),`vw2_academicsession`.`SessionYear`) AS `TrnxYear` from ((`tbltrnxs_orders` left join `vw2_academicsession` on(`tbltrnxs_orders`.`SessionID` = `vw2_academicsession`.`SessionID`)) left join `vw91_courselevel` on(`tbltrnxs_orders`.`LevelID` = `vw91_courselevel`.`LevelID`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw992_trnxs_orders_detailed`
--

/*!50001 DROP TABLE IF EXISTS `vw992_trnxs_orders_detailed`*/;
/*!50001 DROP VIEW IF EXISTS `vw992_trnxs_orders_detailed`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw992_trnxs_orders_detailed` AS select `vw992_trnxs_orders`.`PaymentID` AS `PaymentID`,`vw992_trnxs_orders`.`TrnxID` AS `TrnxID`,`vw992_trnxs_orders`.`StudentID` AS `StudentID`,`vw992_trnxs_orders`.`AdmissionID` AS `AdmissionID`,`vw992_trnxs_orders`.`SessionID` AS `SessionID`,`vw992_trnxs_orders`.`LevelID` AS `LevelID`,`vw992_trnxs_orders`.`FeeID` AS `FeeID`,`vw992_trnxs_orders`.`FeeDetailID` AS `FeeDetailID`,`vw992_trnxs_orders`.`FeeTypeID` AS `FeeTypeID`,`vw992_trnxs_orders`.`FeeItemID` AS `FeeItemID`,`vw992_trnxs_orders`.`InstallmentCount` AS `InstallmentCount`,`vw992_trnxs_orders`.`RRR` AS `RRR`,`vw992_trnxs_orders`.`OrderID` AS `OrderID`,`vw992_trnxs_orders`.`ServiceTypeID` AS `ServiceTypeID`,`vw992_trnxs_orders`.`TrnxType` AS `TrnxType`,`vw992_trnxs_orders`.`TrnxDate` AS `TrnxDate`,`vw992_trnxs_orders`.`TrnxDesc` AS `TrnxDesc`,`vw992_trnxs_orders`.`DRAmount` AS `DRAmount`,`vw992_trnxs_orders`.`CRAmount` AS `CRAmount`,`vw992_trnxs_orders`.`VerifiedByID` AS `VerifiedByID`,`vw992_trnxs_orders`.`Remark` AS `Remark`,`vw992_trnxs_orders`.`StudyYear` AS `StudyYear`,`vw992_trnxs_orders`.`TrnxDateStamp` AS `TrnxDateStamp`,`vw992_trnxs_orders`.`SessionName` AS `SessionName`,`vw992_trnxs_orders`.`SessionYear` AS `SessionYear`,`vw992_trnxs_orders`.`DegreeCourseID` AS `DegreeCourseID`,`vw992_trnxs_orders`.`DeptID` AS `DeptID`,`vw992_trnxs_orders`.`FacultyID` AS `FacultyID`,`vw992_trnxs_orders`.`FacultyName` AS `FacultyName`,`vw992_trnxs_orders`.`DeptName` AS `DeptName`,`vw992_trnxs_orders`.`DegreeCourse` AS `DegreeCourse`,`vw992_trnxs_orders`.`DegreeTypeID` AS `DegreeTypeID`,`vw992_trnxs_orders`.`DegreeType` AS `DegreeType`,`vw992_trnxs_orders`.`ProgramTypeID` AS `ProgramTypeID`,`vw992_trnxs_orders`.`ProgramType` AS `ProgramType`,`vw992_trnxs_orders`.`ProgramTypeCode` AS `ProgramTypeCode`,`vw992_trnxs_orders`.`CourseLevelID` AS `CourseLevelID`,`vw992_trnxs_orders`.`CourseLevel` AS `CourseLevel`,`vw992_trnxs_orders`.`Amount` AS `Amount`,`vw992_trnxs_orders`.`FeeType` AS `FeeType`,`vw992_trnxs_orders`.`TrnxYear` AS `TrnxYear`,if(`vw992_trnxs_orders`.`StudentID` is null,`vw96_admission`.`RegNo`,`vw97_student`.`RegNo`) AS `RegNo`,if(`vw992_trnxs_orders`.`StudentID` is null,'',`vw97_student`.`MatNo`) AS `MatNo`,if(`vw992_trnxs_orders`.`StudentID` is null,`vw96_admission`.`Surname`,`vw97_student`.`Surname`) AS `Surname`,if(`vw992_trnxs_orders`.`StudentID` is null,`vw96_admission`.`Others`,`vw97_student`.`Others`) AS `Others`,if(`vw992_trnxs_orders`.`StudentID` is null,`vw96_admission`.`Sex`,`vw97_student`.`Sex`) AS `Sex`,if(`vw992_trnxs_orders`.`StudentID` is null,`vw96_admission`.`Gender`,`vw97_student`.`Gender`) AS `Gender`,if(`vw992_trnxs_orders`.`StudentID` is null,`vw96_admission`.`AdmissionCandidate`,`vw97_student`.`StudentName`) AS `StudentName`,if(`vw992_trnxs_orders`.`StudentID` is null,`vw96_admission`.`State`,`vw97_student`.`State`) AS `State`,if(`vw992_trnxs_orders`.`StudentID` is null,`vw96_admission`.`LGA`,`vw97_student`.`LGA`) AS `LGA`,if(`vw992_trnxs_orders`.`StudentID` is null,`vw96_admission`.`IndigeneStatusID`,`vw97_student`.`IndigeneStatusID`) AS `IndigeneStatusID`,if(`vw992_trnxs_orders`.`StudentID` is null,`vw96_admission`.`IndigeneStatus`,`vw97_student`.`IndigeneStatus`) AS `IndigeneStatus`,if(`vw992_trnxs_orders`.`StudentID` is null,`vw96_admission`.`Phone`,`vw97_student`.`Phone`) AS `Phone`,if(`vw992_trnxs_orders`.`StudentID` is null,`vw96_admission`.`Email`,`vw97_student`.`Email`) AS `Email` from ((`vw992_trnxs_orders` left join `vw97_student` on(`vw992_trnxs_orders`.`StudentID` = `vw97_student`.`StudentID`)) left join `vw96_admission` on(`vw992_trnxs_orders`.`AdmissionID` = `vw96_admission`.`AdmissionID`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw999_import_results`
--

/*!50001 DROP TABLE IF EXISTS `vw999_import_results`*/;
/*!50001 DROP VIEW IF EXISTS `vw999_import_results`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw999_import_results` AS select `tblimport_results`.`ImportID` AS `ImportID`,`tblimport_results`.`CourseRegID` AS `CourseRegID`,`tblimport_results`.`Surname` AS `Surname`,`tblimport_results`.`Others` AS `Others`,`tblimport_results`.`MatNo` AS `MatNo`,`tblimport_results`.`ImportedScore` AS `ImportedScore`,`tblregistration_courses`.`Score` AS `Score`,`tblregistration_courses`.`Grade` AS `Grade`,`tblregistration_courses`.`CourseUnit` AS `CourseUnit`,`tblregistration_courses`.`GradePoint` AS `GradePoint`,`tblregistration_courses`.`PreviousScore` AS `PreviousScore`,`tblregistration_courses`.`PreviousGrade` AS `PreviousGrade`,`tblregistration_courses`.`ModByID` AS `ModByID`,`tblregistration_courses`.`ModDateStamp` AS `ModDateStamp`,if(`tblimport_results`.`ImportedScore` >= 70,'A',if(`tblimport_results`.`ImportedScore` >= 60,'B',if(`tblimport_results`.`ImportedScore` >= 50,'C',if(`tblimport_results`.`ImportedScore` >= 45,'D',if(`tblimport_results`.`ImportedScore` >= 40,'E','F'))))) AS `EarnedGrade`,if(`tblimport_results`.`ImportedScore` >= 70,5,if(`tblimport_results`.`ImportedScore` >= 60,4,if(`tblimport_results`.`ImportedScore` >= 50,3,if(`tblimport_results`.`ImportedScore` >= 45,2,if(`tblimport_results`.`ImportedScore` >= 40,1,0))))) * `tblregistration_courses`.`CourseUnit` AS `EarnedGP`,`tblimport_results`.`SessionID` AS `SessionID`,`tblimport_results`.`SemesterID` AS `SemesterID`,`tblimport_results`.`ProgramTypeID` AS `ProgramTypeID`,`tblimport_results`.`AssignedCourseID` AS `AssignedCourseID`,`tblimport_results`.`ActionByID` AS `ActionByID`,if(`tblimport_results`.`ImportedScore` < 0 or `tblimport_results`.`ImportedScore` > 100,1,0) AS `OutofRangeChk` from (`tblimport_results` join `tblregistration_courses` on(`tblimport_results`.`CourseRegID` = `tblregistration_courses`.`CourseRegID`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw99_registration_courses`
--

/*!50001 DROP TABLE IF EXISTS `vw99_registration_courses`*/;
/*!50001 DROP VIEW IF EXISTS `vw99_registration_courses`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw99_registration_courses` AS select `tblregistration_courses`.`CourseRegID` AS `CourseRegID`,`tblregistration_courses`.`StudentRegID` AS `StudentRegID`,`tblregistration_courses`.`SemesterID` AS `SemesterID`,`tblregistration_courses`.`AssignedCourseID` AS `AssignedCourseID`,`tblregistration_courses`.`CourseUnit` AS `CourseUnit`,`tblregistration_courses`.`Score` AS `Score`,`tblregistration_courses`.`Grade` AS `Grade`,`tblregistration_courses`.`GradePoint` AS `GradePoint`,`tblregistration_courses`.`ModByID` AS `ModByID`,`tblregistration_courses`.`ModDateStamp` AS `ModDateStamp`,`tblregistration_courses`.`ExcessRegID` AS `ExcessRegID`,`tblregistration_courses`.`PreviousScore` AS `PreviousScore`,`tblregistration_courses`.`PreviousGrade` AS `PreviousGrade`,`tblregistration_courses`.`Q1` AS `Q1`,`tblregistration_courses`.`Q2` AS `Q2`,`tblregistration_courses`.`Q3` AS `Q3`,`tblregistration_courses`.`Q4` AS `Q4`,`tblregistration_courses`.`Q5` AS `Q5`,`tblregistration_courses`.`Q6` AS `Q6`,`tblregistration_courses`.`CA` AS `CA`,`tblregistration_courses`.`Moderation` AS `Moderation`,`tblregistration_courses`.`Penalty` AS `Penalty`,`tblregistration_courses`.`Borderline` AS `Borderline`,`tblregistration_courses`.`Exam` AS `Exam`,`tblregistration_courses`.`RegistrationDateStamp` AS `RegistrationDateStamp`,`tblregistration_courses`.`Q1` + `tblregistration_courses`.`Q2` + `tblregistration_courses`.`Q3` + `tblregistration_courses`.`Q4` + `tblregistration_courses`.`Q5` + `tblregistration_courses`.`Q6` + `tblregistration_courses`.`Moderation` + `tblregistration_courses`.`Borderline` - `tblregistration_courses`.`Penalty` AS `ExamScore`,`tblregistration_courses`.`Exam` + `tblregistration_courses`.`CA` AS `FinalScore`,`vw98_registration_students`.`StudentID` AS `StudentID`,`vw98_registration_students`.`MatNo` AS `MatNo`,`vw98_registration_students`.`StudentName` AS `StudentName`,`vw98_registration_students`.`Surname` AS `Surname`,`vw98_registration_students`.`Others` AS `Others`,`vw98_registration_students`.`Sex` AS `Sex`,`vw98_registration_students`.`Gender` AS `Gender`,`vw98_registration_students`.`MaleCount` AS `MaleCount`,`vw98_registration_students`.`FemaleCount` AS `FemaleCount`,`vw98_registration_students`.`DegreeCourseID` AS `DegreeCourseID`,`vw98_registration_students`.`DeptID` AS `DeptID`,`vw98_registration_students`.`FacultyID` AS `FacultyID`,`vw98_registration_students`.`FacultyName` AS `FacultyName`,`vw98_registration_students`.`DeptName` AS `DeptName`,`vw98_registration_students`.`DegreeType` AS `DegreeType`,`vw98_registration_students`.`ProgramType` AS `ProgramType`,`vw98_registration_students`.`AdmissionModeID` AS `AdmissionModeID`,`vw98_registration_students`.`ProgramTypeCode` AS `ProgramTypeCode`,`vw98_registration_students`.`DegreeTypeID` AS `DegreeTypeID`,`vw98_registration_students`.`ProgramTypeID` AS `ProgramTypeID`,`vw98_registration_students`.`StudyYear` AS `StudyYear`,`vw98_registration_students`.`SessionID` AS `SessionID`,`vw98_registration_students`.`SessionName` AS `SessionName`,`vw98_registration_students`.`SessionYear` AS `SessionYear`,`vw98_registration_students`.`LevelID` AS `LevelID`,`vw98_registration_students`.`CourseLevelID` AS `CourseLevelID`,`vw98_registration_students`.`CourseLevel` AS `CourseLevel`,if(`tblregistration_courses`.`SemesterID` = 1,'FIRST',if(`tblregistration_courses`.`SemesterID` = 2,'SECOND','THIRD')) AS `Semester`,`vw4_assignedcourse`.`LecturerID` AS `LecturerID`,`vw4_assignedcourse`.`CourseSemesterID` AS `CourseSemesterID`,`vw4_assignedcourse`.`CourseID` AS `CourseID`,`vw4_assignedcourse`.`ServiceDeptID` AS `ParentDeptID`,`vw4_assignedcourse`.`CourseCode` AS `CourseCode`,`vw4_assignedcourse`.`CourseTitle` AS `CourseTitle`,`vw4_assignedcourse`.`ResultApproval1ID` AS `ResultApproval1ID`,`vw4_assignedcourse`.`ResultApproval2ID` AS `ResultApproval2ID`,`vw4_assignedcourse`.`ResultApproval3ID` AS `ResultApproval3ID`,`vw4_assignedcourse`.`ResultApproval4ID` AS `ResultApproval4ID`,`vw4_assignedcourse`.`ApprovalDate1` AS `ApprovalDate1`,`vw4_assignedcourse`.`ApprovalDate2` AS `ApprovalDate2`,`vw4_assignedcourse`.`ApprovalDate3` AS `ApprovalDate3`,`vw4_assignedcourse`.`ApprovalDate4` AS `ApprovalDate4`,`vw4_assignedcourse`.`L1` AS `L1`,`vw4_assignedcourse`.`L2` AS `L2`,`vw4_assignedcourse`.`L3` AS `L3`,`vw4_assignedcourse`.`L4` AS `L4`,if(`tblregistration_courses`.`Grade` is null,1,0) AS `PendingGrade`,if(`tblregistration_courses`.`Grade` <> 'F',1,0) AS `Passed`,if(`tblregistration_courses`.`Grade` = 'F',1,0) AS `Failed`,if(`tblregistration_courses`.`Grade` = 'A',1,0) AS `GradeA`,if(`tblregistration_courses`.`Grade` = 'B',1,0) AS `GradeB`,if(`tblregistration_courses`.`Grade` = 'C',1,0) AS `GradeC`,if(`tblregistration_courses`.`Grade` = 'D',1,0) AS `GradeD`,if(`tblregistration_courses`.`Grade` = 'E',1,0) AS `GradeE`,if(`tblregistration_courses`.`Grade` = 'F',1,0) AS `GradeF` from ((`tblregistration_courses` join `vw98_registration_students` on(`tblregistration_courses`.`StudentRegID` = `vw98_registration_students`.`StudentRegID`)) join `vw4_assignedcourse` on(`tblregistration_courses`.`AssignedCourseID` = `vw4_assignedcourse`.`AssignedCourseID`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw99_registration_courses_gradesummary`
--

/*!50001 DROP TABLE IF EXISTS `vw99_registration_courses_gradesummary`*/;
/*!50001 DROP VIEW IF EXISTS `vw99_registration_courses_gradesummary`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw99_registration_courses_gradesummary` AS select `vw99_registration_courses`.`ProgramTypeID` AS `ProgramTypeID`,`vw99_registration_courses`.`SessionID` AS `SessionID`,`vw99_registration_courses`.`SemesterID` AS `SemesterID`,`vw99_registration_courses`.`CourseSemesterID` AS `CourseSemesterID`,`vw99_registration_courses`.`AssignedCourseID` AS `AssignedCourseID`,`vw99_registration_courses`.`CourseID` AS `CourseID`,sum(`vw99_registration_courses`.`GradeA`) AS `GradeA`,sum(`vw99_registration_courses`.`GradeB`) AS `GradeB`,sum(`vw99_registration_courses`.`GradeC`) AS `GradeC`,sum(`vw99_registration_courses`.`GradeD`) AS `GradeD`,sum(`vw99_registration_courses`.`GradeE`) AS `GradeE`,sum(`vw99_registration_courses`.`GradeF`) AS `GradeF`,count(`vw99_registration_courses`.`CourseRegID`) AS `StudentCount` from `vw99_registration_courses` group by `vw99_registration_courses`.`ProgramTypeID`,`vw99_registration_courses`.`SessionID`,`vw99_registration_courses`.`CourseID`,`vw99_registration_courses`.`SemesterID`,`vw99_registration_courses`.`AssignedCourseID` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw99_registration_excess`
--

/*!50001 DROP TABLE IF EXISTS `vw99_registration_excess`*/;
/*!50001 DROP VIEW IF EXISTS `vw99_registration_excess`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw99_registration_excess` AS select `tblregistration_excess`.`ExcessRegID` AS `ExcessRegID`,`tblregistration_excess`.`StudentRegID` AS `StudentRegID`,`vw98_registration_students`.`StudentID` AS `StudentID`,`vw98_registration_students`.`StudentName` AS `StudentName`,`vw98_registration_students`.`Surname` AS `Surname`,`vw98_registration_students`.`Others` AS `Others`,`vw98_registration_students`.`Sex` AS `Sex`,`vw98_registration_students`.`DegreeCourseID` AS `DegreeCourseID`,`vw98_registration_students`.`DeptID` AS `DeptID`,`vw98_registration_students`.`FacultyID` AS `FacultyID`,`vw98_registration_students`.`FacultyName` AS `FacultyName`,`vw98_registration_students`.`DeptName` AS `DeptName`,`vw98_registration_students`.`DegreeTypeID` AS `DegreeTypeID`,`vw98_registration_students`.`DegreeType` AS `DegreeType`,`vw98_registration_students`.`ProgramTypeID` AS `ProgramTypeID`,`vw98_registration_students`.`ProgramType` AS `ProgramType`,`vw98_registration_students`.`ProgramTypeCode` AS `ProgramTypeCode`,`vw98_registration_students`.`StudyYear` AS `StudyYear`,`vw98_registration_students`.`SessionID` AS `SessionID`,`vw98_registration_students`.`SessionName` AS `SessionName`,`vw98_registration_students`.`SessionYear` AS `SessionYear`,`vw98_registration_students`.`LevelID` AS `LevelID`,`vw98_registration_students`.`CourseLevelID` AS `CourseLevelID`,`vw98_registration_students`.`CourseLevel` AS `CourseLevel`,`tblregistration_excess`.`SemesterID` AS `SemesterID`,`tblregistration_excess`.`AssignedCourseID` AS `AssignedCourseID`,`vw4_assignedcourse`.`CourseID` AS `CourseID`,`vw4_assignedcourse`.`CourseCode` AS `CourseCode`,`vw4_assignedcourse`.`CourseTitle` AS `CourseTitle`,`vw4_assignedcourse`.`CourseSemesterID` AS `CourseSemesterID`,`vw4_assignedcourse`.`Semester` AS `Semester`,`vw4_assignedcourse`.`LecturerID` AS `LecturerID`,`tblregistration_excess`.`CourseUnit` AS `CourseUnit`,`vw98_registration_students`.`AdviserApproval` AS `AdviserApproval`,`vw98_registration_students`.`UnitApproval1ID` AS `UnitApproval1ID`,`vw98_registration_students`.`HODApproval` AS `HODApproval`,`vw98_registration_students`.`UnitApproval2ID` AS `UnitApproval2ID`,`vw98_registration_students`.`DeanApproval` AS `DeanApproval`,`vw98_registration_students`.`UnitApproval3ID` AS `UnitApproval3ID`,`vw98_registration_students`.`ApprovalDate1` AS `ApprovalDate1`,`vw98_registration_students`.`ApprovalDate2` AS `ApprovalDate2`,`vw98_registration_students`.`ApprovalDate3` AS `ApprovalDate3` from ((`tblregistration_excess` join `vw98_registration_students` on(`tblregistration_excess`.`StudentRegID` = `vw98_registration_students`.`StudentRegID`)) join `vw4_assignedcourse` on(`tblregistration_excess`.`AssignedCourseID` = `vw4_assignedcourse`.`AssignedCourseID`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw9_degreeaward`
--

/*!50001 DROP TABLE IF EXISTS `vw9_degreeaward`*/;
/*!50001 DROP VIEW IF EXISTS `vw9_degreeaward`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw9_degreeaward` AS select `tbldegreeaward`.`DegreeAwardID` AS `DegreeAwardID`,`tbldegreeaward`.`DegreeAward` AS `DegreeAward`,`tbldegreeaward`.`DegreeAwardCode` AS `DegreeAwardCode`,`tbldegreeaward`.`DegreeTypeID` AS `DegreeTypeID`,`tbldegreetype`.`DegreeType` AS `DegreeType` from (`tbldegreeaward` join `tbldegreetype` on(`tbldegreeaward`.`DegreeTypeID` = `tbldegreetype`.`DegreeTypeID`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_entryrequirements`
--

/*!50001 DROP TABLE IF EXISTS `vw_entryrequirements`*/;
/*!50001 DROP VIEW IF EXISTS `vw_entryrequirements`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_entryrequirements` AS select `tblentryrequirement`.`RequirementID` AS `RequirementID`,`tblentryrequirement`.`EntryCategoryID` AS `EntryCategoryID`,`tblentryrequirement`.`DegreeCourseID` AS `DegreeCourseID`,`vw8_degreecourse`.`DegreeCourse` AS `DegreeCourse`,`tblentryrequirement`.`SessionID` AS `SessionID`,`tblentryrequirement`.`SubjectID` AS `SubjectID`,`tblsubjects`.`Subject` AS `Subject`,`tblentryrequirement`.`MinGradeID` AS `MinGradeID`,`tblentryrequirement`.`CompulsoryID` AS `CompulsoryID`,if(`tblentryrequirement`.`MinGradeID` <= 3,concat('A',`tblentryrequirement`.`MinGradeID`),if(`tblentryrequirement`.`MinGradeID` <= 6,concat('C',`tblentryrequirement`.`MinGradeID`),if(`tblentryrequirement`.`MinGradeID` <= 8,concat('P',`tblentryrequirement`.`MinGradeID`),if(`tblentryrequirement`.`MinGradeID` = 9,concat('F',`tblentryrequirement`.`MinGradeID`),'')))) AS `MinGrade`,if(`tblentryrequirement`.`CompulsoryID` = 1,'YES','NO') AS `CompulsoryStatus`,'NOT APPLICABLE' AS `PrimarySubject` from ((`tblentryrequirement` join `tblsubjects` on(`tblentryrequirement`.`SubjectID` = `tblsubjects`.`SubjectID`)) left join `vw8_degreecourse` on(`tblentryrequirement`.`DegreeCourseID` = `vw8_degreecourse`.`DegreeCourseID`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_entryrequirements_alt`
--

/*!50001 DROP TABLE IF EXISTS `vw_entryrequirements_alt`*/;
/*!50001 DROP VIEW IF EXISTS `vw_entryrequirements_alt`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_entryrequirements_alt` AS select `tblentryrequirement_alternate`.`AltRequirementID` AS `RequirementID`,`tblentryrequirement_alternate`.`EntryCategoryID` AS `EntryCategoryID`,`tblentryrequirement_alternate`.`DegreeCourseID` AS `DegreeCourseID`,`vw8_degreecourse`.`DegreeCourse` AS `DegreeCourse`,`tblentryrequirement_alternate`.`SessionID` AS `SessionID`,`tblentryrequirement_alternate`.`SubjectID` AS `SubjectID`,`tblentryrequirement_alternate`.`SubjectID_Alt` AS `SubjectID_Alt`,`tblsubjects_1`.`Subject` AS `PrimarySubject`,`tblsubjects`.`Subject` AS `Subject`,`tblentryrequirement_alternate`.`MinGradeID` AS `MinGradeID`,`tblentryrequirement_alternate`.`CompulsoryID` AS `CompulsoryID`,if(`tblentryrequirement_alternate`.`MinGradeID` <= 3,concat('A',`tblentryrequirement_alternate`.`MinGradeID`),if(`tblentryrequirement_alternate`.`MinGradeID` <= 6,concat('C',`tblentryrequirement_alternate`.`MinGradeID`),if(`tblentryrequirement_alternate`.`MinGradeID` <= 8,concat('P',`tblentryrequirement_alternate`.`MinGradeID`),if(`tblentryrequirement_alternate`.`MinGradeID` = 9,concat('F',`tblentryrequirement_alternate`.`MinGradeID`),'')))) AS `MinGrade`,if(`tblentryrequirement_alternate`.`CompulsoryID` = 1,'YES','NO') AS `CompulsoryStatus` from (((`tblentryrequirement_alternate` join `tblsubjects` on(`tblentryrequirement_alternate`.`SubjectID` = `tblsubjects`.`SubjectID`)) join `tblsubjects` `tblsubjects_1` on(`tblentryrequirement_alternate`.`SubjectID_Alt` = `tblsubjects_1`.`SubjectID`)) left join `vw8_degreecourse` on(`tblentryrequirement_alternate`.`DegreeCourseID` = `vw8_degreecourse`.`DegreeCourseID`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_feeitems`
--

/*!50001 DROP TABLE IF EXISTS `vw_feeitems`*/;
/*!50001 DROP VIEW IF EXISTS `vw_feeitems`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_feeitems` AS select `tblfeeitems`.`FeeItemID` AS `FeeItemID`,`tblfeeitems`.`FeeItem` AS `FeeItem`,`tblfeeitems`.`FeeTypeID` AS `FeeTypeID`,if(`tblfeeitems`.`FeeTypeID` = 1,'ACCEPATNCE FEE',if(`tblfeeitems`.`FeeTypeID` = 2,'ACCOMODATION FEE',if(`tblfeeitems`.`FeeTypeID` = 3,'SCHOOL CHARGES','OTHER FEES'))) AS `FeeType` from `tblfeeitems` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_jambtest`
--

/*!50001 DROP TABLE IF EXISTS `vw_jambtest`*/;
/*!50001 DROP VIEW IF EXISTS `vw_jambtest`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_jambtest` AS select `tbleligibilityjamb`.`JambCheckID` AS `JambCheckID`,`tbleligibilityjamb`.`SessionID` AS `SessionID`,`tbleligibilityjamb`.`AdmissionID` AS `AdmissionID`,`tbleligibilityjamb`.`OLevelID` AS `OLevelID`,`tbleligibilityjamb`.`DegreeCourseID` AS `DegreeCourseID`,`tbleligibilityjamb`.`JambSubjectID` AS `JambSubjectID`,`tbleligibilityjamb`.`SubjectScore` AS `SubjectScore`,`tblsubjects`.`Subject` AS `Subject`,`tbloleveldetails`.`ExamCenter` AS `ExamCenter`,`tbloleveldetails`.`ExamNumber` AS `ExamNumber`,`tbloleveldetails`.`ExamType` AS `ExamType`,`tbloleveldetails`.`ExamYear` AS `ExamYear` from ((`tbleligibilityjamb` join `tblsubjects` on(`tbleligibilityjamb`.`JambSubjectID` = `tblsubjects`.`SubjectID`)) left join `tbloleveldetails` on(`tbleligibilityjamb`.`AdmissionID` = `tbloleveldetails`.`AdmissionID` and `tbleligibilityjamb`.`OLevelID` = `tbloleveldetails`.`OLevelID`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_oleveltest`
--

/*!50001 DROP TABLE IF EXISTS `vw_oleveltest`*/;
/*!50001 DROP VIEW IF EXISTS `vw_oleveltest`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_oleveltest` AS select `tbleligibilityolevel`.`OLevelCheckID` AS `OlevelCheckID`,`tbleligibilityolevel`.`SessionID` AS `SessionID`,`tbleligibilityolevel`.`AdmissionID` AS `AdmissionID`,`tbleligibilityolevel`.`DegreeCourseID` AS `DegreeCourseID`,`tbleligibilityolevel`.`OLevelSubjectID` AS `OlevelSubjectID`,`tbleligibilityolevel`.`OLevelID` AS `OLevelID`,`tbleligibilityolevel`.`EarnedGradeID` AS `EarnedGradeID`,`tblsubjects`.`Subject` AS `Subject`,if(`tbleligibilityolevel`.`EarnedGradeID` = 1,'A1',if(`tbleligibilityolevel`.`EarnedGradeID` = 2,'B2',if(`tbleligibilityolevel`.`EarnedGradeID` = 3,'B3',if(`tbleligibilityolevel`.`EarnedGradeID` = 4,'C4',if(`tbleligibilityolevel`.`EarnedGradeID` = 5,'C5',if(`tbleligibilityolevel`.`EarnedGradeID` = 6,'C6',if(`tbleligibilityolevel`.`EarnedGradeID` = 7,'D7',if(`tbleligibilityolevel`.`EarnedGradeID` = 8,'E8',if(`tbleligibilityolevel`.`EarnedGradeID` = 9,'F9',''))))))))) AS `EarnedGrade`,if(`tbleligibilityolevel`.`EarnedGradeID` = 1,'A1/A',if(`tbleligibilityolevel`.`EarnedGradeID` = 2,'A2/B2/B',if(`tbleligibilityolevel`.`EarnedGradeID` = 3,'A3/B3',if(`tbleligibilityolevel`.`EarnedGradeID` = 4,'C4/C',if(`tbleligibilityolevel`.`EarnedGradeID` = 5,'C5',if(`tbleligibilityolevel`.`EarnedGradeID` = 6,'C6',if(`tbleligibilityolevel`.`EarnedGradeID` = 7,'P7/D7',if(`tbleligibilityolevel`.`EarnedGradeID` = 8,'P8/E8',if(`tbleligibilityolevel`.`EarnedGradeID` = 9,'F9',''))))))))) AS `EarnedGrade1`,`tbloleveldetails`.`ExamCenter` AS `ExamCenter`,`tbloleveldetails`.`ExamNumber` AS `ExamNumber`,`tbloleveldetails`.`ExamType` AS `ExamType`,`tbloleveldetails`.`ExamYear` AS `ExamYear` from ((`tbleligibilityolevel` join `tblsubjects` on(`tbleligibilityolevel`.`OLevelSubjectID` = `tblsubjects`.`SubjectID`)) left join `tbloleveldetails` on(`tbleligibilityolevel`.`AdmissionID` = `tbloleveldetails`.`AdmissionID` and `tbleligibilityolevel`.`OLevelID` = `tbloleveldetails`.`OLevelID`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_servicetypes`
--

/*!50001 DROP TABLE IF EXISTS `vw_servicetypes`*/;
/*!50001 DROP VIEW IF EXISTS `vw_servicetypes`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_servicetypes` AS select `tblservicetypes`.`ServiceID` AS `ServiceID`,`tblservicetypes`.`ServiceTypeID` AS `ServiceTypeID`,`tblservicetypes`.`FeeItemID` AS `FeeItemID`,`tblservicetypes`.`ServiceTypeName` AS `ServiceTypeName`,`tblservicetypes`.`InstallmentCount` AS `InstallmentCount`,`tblservicetypes`.`ProgramTypeID` AS `ProgramTypeID`,`tblservicetypes`.`DegreeTypeID` AS `DegreeTypeID`,`tblservicetypes`.`FeeTypeID` AS `FeeTypeID`,`tblservicetypes`.`DeptID` AS `DeptID`,`tblservicetypes`.`FacultyID` AS `FacultyID`,if(`tblservicetypes`.`FeeTypeID` = 1,'FOR ACCEPATNCE FEE',if(`tblservicetypes`.`FeeTypeID` = 2,'FOR ACCOMODATION FEE',if(`tblservicetypes`.`FeeTypeID` = 3,'FOR SCHOOL CHARGES','FOR OTHER FEES'))) AS `FeeType`,if(`tblservicetypes`.`FacultyID` is null,concat('FOR ',`tbldepartment`.`DeptName`),concat('FOR ',`tblfaculty`.`FacultyName`)) AS `For` from ((`tblservicetypes` left join `tblfaculty` on(`tblservicetypes`.`FacultyID` = `tblfaculty`.`FacultyID`)) left join `tbldepartment` on(`tblservicetypes`.`DeptID` = `tbldepartment`.`DeptID`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-07-21 18:47:15
