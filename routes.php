<?php
use Lib\Router;
use Lib\Request;


if(! session_id()){
    session_start();
}
$router = new Router(new Request());

// home page route
$router->get('/',function(){
    require BASE_URL.'/default.php';
 });


 // Error 404 page
 $router->get('/error/page',function(Request $request){
    
    require BASE_URL.'/errors/404.php';
 });
 
//routes for faculty
require BASE_URL.'/routes/faculty.routes.php';

//routes for user management
require BASE_URL.'/routes/admin.routes.php';

//routes for department
require BASE_URL.'/routes/department.routes.php';

//routes for department degree course
require BASE_URL.'/routes/degree_course.routes.php';

// the routes and actions of predata
require BASE_URL.'/routes/predata.routes.php';

// the routes and actions of state and lga setup
require BASE_URL.'/routes/state_lga.routes.php';

// the routes and actions of degree award setup
require BASE_URL.'/routes/degree_award.routes.php';

// the routes and actions of program and degree type setup
require BASE_URL.'/routes/programtype_degreetype.routes.php';

// the routes and actions of feeitem setup
require BASE_URL.'/routes/feeitem.routes.php';

// routes for olevel setup
require BASE_URL.'/routes/olevel.routes.php';







?>