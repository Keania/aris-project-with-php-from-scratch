<?php

use Lib\Connection;
use Model\Faculty;
use Model\FacultyColour;
use Model\Faculty_VW6;


$page_title = 'Set Up Faculty';
$token = generateCSRFToken();
include BASE_URL . '/setup/header.php';

// Database Operations of getting data
$faculties_vw6 = Faculty_VW6::all();
$colours = FacultyColour::all();

?>


<!-- content section of this page goes here -->
<div class="main-content">
    <!-- Top navbar -->
    <nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
        <div class="container-fluid">
            <!-- Brand -->
            <!-- <a class="h4 mb-0 text-secondary d-none_ d-lg-inline-block" href="#">Setup</a>
        <a class="h4 mb-0 text-white d-none_ d-lg-inline-block" href="#"> | Pre-Defined Data</a>
         -->
            <ul class="font-weight-300 nav">
                <li class="nav-item">
                    <a href="#" class="nav-link text-white-50 d-lg-inline-block">Setup Management</a>
                </li> <span class="text-white-50 h2">|</span>
                <li class="nav-item">
                    <a href="#" class="nav-link text-white-50 d-lg-inline-block" style="font-size: 1.35rem;line-height: 1;">Faculty Management</a>
                </li>
            </ul>

            <!-- Form -->
            <!--  <form class="navbar-search navbar-search-dark form-inline mr-3 d-none d-md-flex ml-lg-auto">
          <div class="form-group mb-0">
            <div class="input-group input-group-alternative">
              <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-search"></i></span>
              </div>
              <input class="form-control" placeholder="Search" type="text">
            </div>
          </div>
        </form> -->
            <!-- User -->
            <ul class="navbar-nav align-items-center d-none d-md-flex">
                <li class="nav-item dropdown">
                    <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <div class="media align-items-center">
                            <span class="avatar avatar-sm rounded-circle">
                                <img alt="Image placeholder" src="<?php echo resource('public/assets/img/theme/team-4-800x800.jpg');?>">
                            </span>
                            <div class="media-body ml-2 d-none d-lg-block">
                                <span class="mb-0 text-sm  font-weight-bold">Jonathan Doe <i class="fa fa-caret-down"></i></span>
                            </div>
                        </div>
                    </a>
                    <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
                        <div class=" dropdown-header noti-title">
                            <h6 class="text-overflow m-0">Welcome!</h6>
                        </div>
                        <a href="../pages/profile.html" class="dropdown-item">
                            <i class="ni ni-single-02"></i>
                            <span>My profile</span>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="#!" class="dropdown-item">
                            <i class="ni ni-user-run"></i>
                            <span>Logout</span>
                        </a>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
    <!-- Header -->
    <div class="header bg-gradient-primary py-5">
        <div class="container-fluid">
            <div class="header-body">
                <!-- Card stats -->

            </div>
        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt-md-4">
    <div class="row">
            <div class="col-md-6  offset-md-1 mb-3">
                <?php
                // check if there is a message in the incoming request
                if (isset($request->session)) {

                    // check if an error exists in the incoming request
                    if (isset($request->session['error'])) {
                        $error = $request->session['error'];
                        echo "<div class='alert alert-danger'>$error</div>";
                    }

                    // check if there is a success msg in the incoming request
                    if (isset($request->session['success'])) {
                        $success = $request->session['success'];
                        echo "<div class='alert alert-success'>$success</div>";
                    }
                }
                ?>
            </div>
        </div>
        <div class="row">

            <div class="col-md-6 offset-md-1 mb-3">
                <div class="">
                    <div class="border-0">
                        <h3 class="mb-0 text-muted">Faculty Management</h3>
                        <hr class="my-3">
                    </div>
                    <div class="form-container" id="facultyManagementFormContainer">

                        <form method="POST" :action="url" class="mx-col">


                            <input type="hidden" name="_csrf_token" value="<?php echo $token; ?>">

                            <div class="row">

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-email">Select Faculty</label>
                                        <div class="input-group">
                                            <select v-model.number="faculty_id" name="faculty_id" class="form-control" @change="getFaculty">
                                                <?php
                                                if (count($faculties_vw6) > 0) {
                                                    echo "<option value=''>Choose...</option>";
                                                    foreach ($faculties_vw6 as $faculty) {
                                                        ?>
                                                        <option value="<?php echo $faculty->FacultyID ?>"><?php echo $faculty->FacultyName; ?></option>
                                                    <?php
                                                    }
                                                } else {
                                                    echo " <option value=''>---- No data available --</option>";
                                                }
                                                ?>
                                            </select>
                                            <div class="input-group-append">
                                                <button id='edittoggler' class="btn btn-primary"  @click="">
                                                    <input type="checkbox" id="" v-model:checked="checked">
                                                    <span>Edit</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>



                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-control-label">Faculty Name</label>
                                        <input type="text" class="form-control" name="faculty_name" v-model="faculty">
                                    </div>

                                </div>

                                <div class="col-md-12">
                                    <div class="form-group ">
                                        <label class="form-control-label">Faculty Code</label>
                                        <input type="text" class="form-control" name="faculty_code" v-model="facultycode">
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-control-label">Select Colour</label>
                                        <select class="form-control" name="faculty_colour_id" v-model="faculty_colour">
                                            <?php
                                            if(count($colours) > 0){
                                                echo "<option value=''>...Choose Faculty Colour</option>";
                                                foreach($colours as $colour){
                                                    ?>
                                                    <option value="<?php echo $colour->ColourID ?>"><?php echo $colour->ColourName; ?></option>
                                                    <?php
                                                }
                                            }else{
                                                echo "<option value=''> No data available</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>


                                <div class="col-md-12 mb-3">
                                    <div class="form-group ">
                                        <button type="submit" name="submit" class="btn btn-primary">Save <i class="fa fa-save"></i></button>
                                        <button class="btn btn-danger" @click="deleteUrl">Delete <i class="fa fa-trash"></i>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="alert alert-danger" id="predata_ajax_error" v-show="hasError">
                                        {{error}}
                                    </div>

                                </div>




                            </div>


                        </form>

                    </div>
                </div>



            </div>

            <div class="col-md-6 offset-md-1 mb-3">
                <div class="">
                    <div class="border-0">
                        <h3 class="mb-0 text-muted">Faculty Dean Management</h3>
                        <hr class="my-3">
                    </div>
                    <div class="form-container" id="facultyDeanManagementFormContainer">

                        <form method="POST" action="save/faculty/dean" class="mx-col">


                            <input type="hidden" name="_csrf_token" value="<?php echo $token; ?>">

                            <div class="row">

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-email">Select Faculty</label>
                                        <div class="input-group">
                                            <select v-model.number="faculty_id" name="faculty_id" class="form-control" @change="getFacultyUsers">
                                                <?php
                                                if (count($faculties_vw6) > 0) {
                                                    echo "<option value=''>Choose...</option>";
                                                    foreach ($faculties_vw6 as $faculty) {
                                                        ?>
                                                        <option value="<?php echo $faculty->FacultyID ?>"><?php echo $faculty->FacultyName; ?></option>
                                                    <?php
                                                    }
                                                } else {
                                                    echo " <option value=''>---- No data available --</option>";
                                                }
                                                ?>
                                            </select>
                                           
                                        </div>
                                    </div>
                                </div>



                              
                               
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-control-label">Select Faculty Dean</label>
                                        <select class="form-control" name="faculty_dean_id" v-model="dean_id" id="users">
                                            <option value=''>Choose</option>
                                        </select>
                                    </div>
                                </div>


                                <div class="col-md-12 mb-3">
                                    <div class="form-group ">
                                        <button type="submit" name="submit" class="btn btn-primary">Save <i class="fa fa-save"></i></button>
                                       
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="alert alert-danger" id="predata_ajax_error" v-show="hasError">
                                        {{error}}
                                    </div>

                                </div>




                            </div>


                        </form>

                    </div>
                </div>



            </div>

           

        </div>
        <!-- Footer -->
        <footer class="footer">
            <div class="row align-items-center justify-content-xl-between">
                <div class="col-xl-6">
                    <div class="copyright text-center text-xl-left text-muted">
                        &copy; 2019 <a href="#" class="font-weight-bold ml-1" target="_blank">Softech Global Associates.</a> All Rights Reserved.
                    </div>
                </div>
                <div class="col-xl-6">
                    <ul class="nav nav-footer justify-content-center justify-content-xl-end">
                        <div class="copyright text-center text-xl-right text-muted">
                            <a href="#">Policy</a>
                        </div>
                    </ul>
                </div>
            </div>
        </footer>
    </div>
</div>
<!-- end of content section of this page -->

<!-- important scrpts goes here -->
<?php
include BASE_URL . '/setup/scripts.php';
?>
<!-- important script ends here -->


<script>
    let facultyForm = new Vue({
        el: '#facultyManagementFormContainer',
        data: {
          
            url: 'save/faculty',
            show_edit: false,
            checked: false,
            faculty_id: '',
            faculty: '',
            facultycode: '',
            faculty_colour:'',
            hasError: false,
            error: '',
        },

        watch: {
           
            checked: function(val) {
                if (val) {
                    this.url = 'update/faculty';

                    if (!isNaN(this.faculty_id)) {
                        this.getFaculty();
                    }
                } else {
                    this.url = 'save/faculty'
                    this.faculty = '';
                    this.faculty_colour = '';
                    this.facultycode = '';
                }
            },
        },
        methods: {

            getFaculty() {
                if (!this.checked) {
                    return;
                }
                $vm = this;
                axios.get('api/get/faculty', {
                    params: {
                        faculty_id: this.faculty_id
                    }
                }).then(function(response) {
                    $vm.faculty = response.data[0].FacultyName;
                    $vm.facultycode = response.data[0].FacultyCode;
                    $vm.faculty_colour = response.data[0].FacultyColourID;
                }).catch(function(error) {
                    $vm.hasError = true;
                    $vm.error = error.messageText;
                })
            },

           
            deleteUrl() {
                this.url = 'delete/faculty';
              
                return true;
            }
        }

    });

    let facultyDeanForm = new Vue({
        el:'#facultyDeanManagementFormContainer',
        data: {
            faculty_id : '',
            dean_id : '',
            hasError:false,
            error : ''
        },
        methods : {
            getFacultyUsers(){
                $vm = this;
                axios.get('api/get/faculty/users', {
                        params: {
                            faculty_id: this.faculty_id
                        }
                    }).then(function(response) {

                        //reset predatas to empty
                        $('#users').text('');
                        // reset the predata value
                        $vm.dean_id = ''

                        if (response.data.length > 0) {

                            $('#users').append("<option value='' >Choose Dean...</option>");

                            $.each(response.data, function($key, $obj) {

                                $('#users').append('<option value=' + $obj.UserID + '>' + $obj.Title +' '+$obj.Surname+' '+$obj.Others +'</option>');
                            });

                        } else {

                            console.log('no data found')
                            $('#users').append("<option  value='' > No data found </option>");
                        }


                    }).catch(function(error) {

                        console.log(error)
                    });
            }
        }

    })
</script>
<!-- page script content goes here -->
<script type="text/javacript"></script>
<!-- page script contents ends here -->
<?php
include BASE_URL . '/setup/footer.php';
?>