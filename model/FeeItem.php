<?php
namespace Model;
use Model\BaseModel;

class FeeItem extends BaseModel {

    protected $table = 'tblfeeitems';
    public $FeeItemID;
    public $FeeItem;
    public $FeeTypeID;


    public function fee_details()
    {
        return $this->hasMany('Model\FeeDetail');
    }

    public function other_fees()
    {
        return $this->hasMany('Model\OtherFee');
    }

    public function trnxs(){

        return $this->hasMany('Model\Transaction');
    }

    public function getKeyName()
    {
        return 'FeeItemID';
    }
}

?>
