<?php
namespace Model;
use Model\BaseModel;

class Transaction extends BaseModel {

    protected $table = 'tbltrnxs';

    public $TrnxID;
    public $StudentID;
    public $AdmissionID;
    public $SessionID;
    public $LevelID;
    public $FeeID;
    public $FeeDetailID;
    public $FeeTypeID;
    public $FeeItemID;
    public $InstallmentCount;
    public $TrnxType;
    public $TrnxDate;
    public $TrnxDesc;
    public $DRAmount;
    public $CRAmount;
    public $RRR;
    public $OrderID;
    public $ServiceTypeID;
    public $PaymentID;
    public $VerifiedByID;
    public $Remark;
    public $TrnxDateStamp;


    public function getKeyName()
    {
        return 'TrnxID';
    }
}

?>