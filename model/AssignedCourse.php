<?php
namespace Model;
use Model\BaseModel;


class AssignedCourse extends BaseModel {
    
    protected $table = 'tblassignedcourse';

    public $AssignedCourseID;
    public $SessionID;
    public $ProgramTypeID;
    public $CourseID;
    public $LecturerID;
    public $HODID;
    public $DeanID;
    public $SenateRepID;
    public $ApprovalDate1;
    public $ApprovalDate2;
    public $ApprovalDate3;
    public $ApprovalDate4;


    public function getKeyName()
    {
        return 'AssignedCourseID';
    }
}
?>