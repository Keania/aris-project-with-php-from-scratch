<?php
namespace Model;

use Model\BaseModel;

class AcademicSessionMgt extends BaseModel{

    protected $table = 'tblacademicsession_mgt';
    public $SessionMgtID;
    public $SessionID;
    public $ProgramTypeID;
    public $DegreeTypeID;
    public $SessionStatusID;
    public $RegistrationStatusID;
    public $FeePaymentControlID;
    public $AdmissionStatusID;
    public $RequestUpdateStatusID;


    public function getKeyName()
    {
        return 'SessionMgtID';
    }
}

?>