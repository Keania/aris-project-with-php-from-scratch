<?php
namespace Model;
use Lib\Connection;
use Exception;
use stdClass;
use BadFunctionCallException;

abstract class BaseModel {

    private static $db;
    private static $connection;
    protected $updatable = array();

    /**
     * db
     *
     * @return void
     */
    public static function db()
    {
        self::$connection = new Connection();
        self::$db = self::$connection->db;
        return true;
    }

    public function __construct()
    {
       
    }

    // persist a model into the database
    public function save()
    {

     
        $class = new \ReflectionClass($this);
        $table_name = $this->table;
        $propsToImplode = [];

        foreach($class->getProperties(\ReflectionProperty::IS_PUBLIC) as $prop){
            $prop_name = $prop->getName();
            if(isset($this->{$prop_name})){

                if(count($this->updatable) > 0 && in_array($prop_name,$this->updatable)){

                    $propsToImplode[] = '`'.$prop_name.'` = "'. $this->{$prop_name}.'"';
                    continue;
                }else{
                    if(count($this->updatable) > 0){
                        continue;
                    }
                    $propsToImplode[] = '`'.$prop_name.'` = "'. $this->{$prop_name}.'"';
                }
               
            }
               
        }
        $setClause  = implode(',',$propsToImplode);
       if(isset($this->{$this->getKeyName()}) && is_numeric($this->{$this->getKeyName()})){
            $query = 'UPDATE  `'.$table_name.'` SET '.$setClause.' WHERE '.$this->getKeyName().' = '.$this->{$this->getKeyName()};
       }else{
            // clean off the id column
            $insertClause = explode(',',$setClause);
            foreach($insertClause as $key=>$item)
            {  
                if(trim(str_replace(['=','`','""'],'',$item)," ") == $this->getKeyName()) {
                    unset($insertClause[$key]);
                }
            }
            $setClause = implode(',',$insertClause);
            $query = 'INSERT INTO `'.$table_name.'` SET '.$setClause;
            
       }
     
      
       // start a database connection
       static::db();

       $result = self::$db->exec($query);
       if(self::$db->errorCode() != '00000'){
           throw new \Exception(self::$db->errorInfo()[2]);
       }
      
       return $result;
    }



    /**
     * morph
     *
     * @param  mixed $object
     *
     * @return void
     */
    public static function morph(array $object)
    {
        $class = new \ReflectionClass(get_called_class());
        $entity = $class->newInstance();
        foreach($class->getProperties(\ReflectionProperty::IS_PUBLIC) as $prop){
            if(isset($object[$prop->getName()])){
                $prop->setValue($entity,$object[$prop->getName()]);
            }
        }
        
        return $entity;
    }





    /**
     * find
     *
     *  Find from model given the $key=>$value pair
     * 
     * @param  array $options
     *
     * @return \Lib\Collection
     */
    public static function find($options = [])
    {
        $result = collect([]);
        $whereConditions = [];
        if(empty($options)){
            throw new Exception('Conditions for where cannot be null');
        }

        foreach($options as $key=>$value){
            if(is_null($value)){
                throw new Exception("invalid value null given to {$key}");
            }
            $whereConditions[] = '`'.$key.'`="'.$value.'" ';
        }
        $whereClause = implode(' AND ',$whereConditions);
        $called_class = get_called_class();
        $obj = new $called_class;
        $table_name = $obj->table;
        $query = 'SELECT * FROM `'.$table_name.'` WHERE '.$whereClause;
       
        
        // start a database connection
        static::db();
        $raw = self::$db->query($query);
       
        if(self::$db->errorCode() != '00000'){
            throw new \Exception(self::$db->errorInfo()[2]);
        }

        if($raw->rowCount() > 0){
            
            foreach($raw->fetchAll() as $data){
                $result[] = self::morph($data);
            }
        }
     
       
        return $result;

    }


    
    
    
    /**
     * hasMany
     *
     * @param  mixed $object
     * @param  mixed $foreignKey
     *
     * @return \Lib\Collection
     */
    public function hasMany($object,$foreignKey=null){

        $results = collect([]);
        $class = new \ReflectionClass(get_called_class());
        $ref_class = new $object;
        $referenced_class_table = $ref_class->table;
        if(is_null($foreignKey)){
            $foreignKey = $class->newInstance()->getKeyName();
        }
        $results = $ref_class::find([$foreignKey=>$this->{$this->getKeyName()}]);
        return $results;

    }

  
    /**
     * where
     *
     * @param  mixed $conditions
     *  array ['column_name','operator',value]
     *  ['ProgramTypeID','!=',5]
     * @return void
     */
    public static function where(array ...$conditions)
    {

        $result = collect([]);
        $whereConditions = array();
        foreach($conditions as $condition){
            if(count($condition) != 3){
                throw new BadFunctionCallException('Incorrect array condition length pass to where');
            }
            list($column,$operator,$columnValue) = $condition;
            if(is_null($columnValue)){

                $whereConditions[] = '`'.$column.'`'.$operator.' null ';
            }else{
                $whereConditions[] = '`'.$column.'`'.$operator.'"'.$columnValue.'" ';
            }
           

        }
        $whereClause = implode(' AND ',$whereConditions);
        $called_class = get_called_class();
        $obj = new $called_class;
        $table_name = $obj->table;
        $query = 'SELECT * FROM `'.$table_name.'` WHERE '.$whereClause;
      

      
       
        // start a database connection
        static::db();
        $raw = self::$db->query($query);
       
        if(self::$db->errorCode() != '00000'){
            throw new \Exception(self::$db->errorInfo()[2]);
        }

        if($raw->rowCount() > 0){
            
            foreach($raw->fetchAll() as $data){
                $result[] = self::morph($data);
            }
        }
     
       
        return $result;


    }

    
    /**
     * where
     *
     * @param  mixed $conditions
     *  array ['column_name','operator',value]
     *  ['ProgramTypeID','!=',5]
     * @return void
     */
    public static function orWhere(array ...$conditions)
    {

        $result = collect([]);
        $whereConditions = array();
        foreach($conditions as $condition){
            if(count($condition) != 3){
                throw new BadFunctionCallException('Incorrect array condition length pass to where');
            }
            list($column,$operator,$columnValue) = $condition;
            if(is_null($columnValue)){

                $whereConditions[] = '`'.$column.'`'.$operator.' null ';
            }else{
                $whereConditions[] = '`'.$column.'`'.$operator.'"'.$columnValue.'" ';
            }
           

        }
        $whereClause = implode(' OR ',$whereConditions);
        $called_class = get_called_class();
        $obj = new $called_class;
        $table_name = $obj->table;
        $query = 'SELECT * FROM `'.$table_name.'` WHERE '.$whereClause;
      
       
        // start a database connection
        static::db();
        $raw = self::$db->query($query);
       
        if(self::$db->errorCode() != '00000'){
            throw new \Exception(self::$db->errorInfo()[2]);
        }

        if($raw->rowCount() > 0){
            
            foreach($raw->fetchAll() as $data){
                $result[] = self::morph($data);
            }
        }
     
       
        return $result;


    }


    
    
    //deleting models
    public function delete($options = [])
    {
        $class = new \ReflectionClass($this);
        $table = $this->table;
        $keyColumnValue = $this->{$this->getKeyName()};
        $keyColumn = $this->getKeyName();
        $whereConditions = [];
        $query = ' DELETE FROM `'.$table.'` WHERE `'.$keyColumn.'` = '.$keyColumnValue.'';
        if(isset($options) && !empty($options)){
            foreach($options as $key=>$value){
                $whereConditions[] = '`'.$key.'` = '.$value.'';
            }
            $whereClause = implode(' AND ',$whereConditions);
         
           $query .= ' AND '.$whereClause;
        }
         // start a database connection
       static::db();

       $result = self::$db->exec($query);
       if(self::$db->errorCode() != '00000'){
           throw new \Exception(self::$db->errorInfo()[2]);
       }
      
       return $result;
    }


    
    
    public static function all()
    {
        $result = collect([]);
        $called_class = get_called_class();
        $obj = new $called_class;
        $table_name = $obj->table;
        $query = 'SELECT * FROM `'.$table_name.'`';
        
        // start a database connection
        static::db();
        $raw = self::$db->query($query);
       
        if(self::$db->errorCode() != '00000'){
            throw new \Exception(self::$db->errorInfo()[2]);
        }
        if($raw->rowCount() > 0){
            
            foreach($raw->fetchAll() as $data){
                $result[] = self::morph($data);
            }
        }
     
       
        return $result;
    }

    


    public function getKeyName()
    {
        return 'id';
    }
}
