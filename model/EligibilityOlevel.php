<?php
namespace Model;
use Model\BaseModel;

class EligibilityOlevel extends BaseModel{

    protected $table = 'tbleligibilityolevel';

    public $OlevelCheckID;
    public $SessionID;
    public $AdmissionID;
    public $DegreeCourseID;
    public $OlevelID;
    public $OlevelSubjectID;
    public $EarnedGradeID;

    public function getKeyName()
    {
        return 'OlevelCheckID';
    }
}

?>