<?php
namespace Model;
use Model\BaseModel;

class FeeDetail extends BaseModel {

    protected $table = 'tblfeedetails';
    public $FeeDetailID;
    public $FeeID;
    public $FeeItemID;
    public $FeeCategoryID;
    public $FacultyID;
    public $DeptID;
    public $DegreeCourseID;
    public $CourseLevelID;
    public $FeeAmount;
    public $_2Installments1;
    public $_2Installments2;
    public $_3Installments1;
    public $_3Installments2;
    public $_3Installments3;


    /**
     * getKeyName
     *   
     * The primary key column of the table
     *  
     * @return string
     */
    public function getKeyName()
    {
        return 'FeeDetailID';
    }
}

?>
