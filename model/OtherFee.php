<?php
namespace Model;
use Model\BaseModel;

class OtherFee extends BaseModel {

    protected $table = 'tblfeesothers';
    public $OtherFeeID;
    public $SessionID;
    public $DegreeTypeID;
    public $ProgramTypeID;
    public $FeeTypeID;
    public $FeeItemID;
    public $OtherFeeAmount;
    public $FeeDeadlineDate;
    public $FeeClosureDate;

    public function getKeyName()
    {
        return 'OtherFeeID';
    }
}

?>