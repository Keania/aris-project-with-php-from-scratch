<?php
namespace Model;
use Model\BaseModel;

class ServiceType extends BaseModel {

    protected $table = 'tblservicetypes';
    public $ServiceTypeID;
    public $ServiceID;
    public $ServiceTypeName;
    public $StudentCategoryID;
    public $InstallmentCount;
    public $ProgramTypeID;
    public $DegreeTypeID;
    public $FeeTypeID;
    public $FeeItemID;
    public $FacultyID;
    public $DeptID;

    public function getKeyName()
    {
        return 'ServiceID';
    }
}

?>