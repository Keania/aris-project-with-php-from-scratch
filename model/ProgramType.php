<?php
namespace Model;
use Model\BaseModel;

class ProgramType extends BaseModel {

    protected $table = 'tblprogramtype';
    public $ProgramTypeID;
    public $ProgramType;
    public $ProgramTypeCode;


   
    public function degreecourses()
    {
        return $this->hasMany('Model\DegreeCourse');
    }

    public function fees()
    {
        return $this->hasMany('Model\Fee');
    }

    public function academic_session_mgts()
    {
        return $this->hasMany('Model\AcademicSessionMgt');
    }

   
    public function medical_regimes()
    {
        return $this->hasMany('Model\MedicalRegime');
    }

    public function service_types()
    {
        return $this->hasMany('Model\ServiceType');
    }

    public function other_fees(){
        return $this->hasMany('Model\OtherFee');
    }

    public function assigned_courses()
    {
        return $this->hasMany('Model\AssignedCourse');
    }

    public function getKeyName()
    {
        return 'ProgramTypeID';
    }
}

?>
