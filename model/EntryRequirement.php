<?php
namespace Model;
use Model\BaseModel;


class EntryRequirement extends BaseModel {

    protected $table = 'tblentryrequirement';
    public $RequirementID;
    public $SessionID;
    public $DegreeCourseID;
    public $SubjectID;
    public $EntryCategoryID;
    public $MinGradeID;
    public $CompulsoryID;

    public function getKeyName()
    {
        return 'RequirementID';
    }
}

?>