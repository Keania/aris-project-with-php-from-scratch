<?php
namespace Model;
use Model\BaseModel;

class Faculty_VW6 extends BaseModel {

    protected $table = 'vw6_faculty';
    public $FacultyID;
    public $FacultyName;
    public $FacultyCode;
    public $FacultyColourID;
    public $FacultyDeanID;


    public function departments()
    {
        return $this->hasMany('Model\Department');
    }

    public function feedetails()
    {
        return $this->hasMany('Model\FeeDetail');
    }

    public function service_types()
    {
        return $this->hasMany('Model\ServiceType');
    }

    /**
     * getKeyName
     *   
     * The primary key column of the table
     *  
     * @return string
     */
    public function getKeyName()
    {
        return 'FacultyID';
    }
}

?>
