<?php
namespace Model;
use Model\BaseModel;

class User_VW95 extends BaseModel {

    protected $table = 'vw95_user';
    protected $updatable = ['UserPIN','UserPwd','UserStatusID'];

    public $UserID;
    public $UserPIN;
    public $SearchData;
    public $UserPwd;
    public $UserName;
    public $Surname;
    public $Others;
    public $Sex;
    public $Title;
    public $UserGroupID;
    public $UserGroup;
    public $UserCategoryID;
    public $UserCategory;
    public $UserDeptID;
    public $FacultyID;
    public $FacultyName;
    public $FacultyCode;
    public $FacultyColourID;
    public $ColourName;
    public $DeptName;
    public $DeptCode;
    public $Phone;
    public $Email;
    public $ContactAddress;
    public $UserStatusID;
    public $UserStatus;
    public $PIN;
    public $Photo;
    public $PwdQuestion1;
    public $PwdQuestion2;
    public $PwdAnswer1;
    public $PwdAnswer2;
    public $StaffNumber;
    public $DyofB;
    public $MofB;
    public $EmploymentDate;
    public $AppointmentDate;
    public $GradeLevel;


    public function getKeyName()
    {
        return 'UserID';
    }
}
?>