<?php
namespace Model;
use Model\BaseModel;

class DegreeCourse extends BaseModel {

    protected $table = 'tbldegreecourse';
    public $DegreeCourseID;
    public $DegreeAwardID;
    public $DeptID;
    public $DegreeTypeID;
    public $DegreeCourse;
    public $ProgramTypeID;
    public $DegreeCourseDuration;

    public function students()
    {
        return $this->hasMany('Model\Student');
    }

    public function admissions()
    {
        return $this->hasMany('Model\Admission');
    }

    public function fee_details()
    {
        return $this->hasMany('Model\FeeDetail');
    }

    public function entry_requirement_alt()
    {
        return $this->hasMany('Model\EntryRequirementAlt');
    }

    public function entry_requirement()
    {
        return $this->hasMany('Model\EntryRequirement');
    }

    public function eligibility_olevel()
    {
        return $this->hasMany('Model\EligibilityOlevel');
    }

    public function eligibility_jamb()
    {
        return $this->hasMany('Model\EligibilityJamb');
    }

    public function course_level()
    {
        return $this->hasMany('Model\CourseLevel');
    }

    /**
     * getKeyName
     *
     * @return void
     */
    public function getKeyName()
    {
        return 'DegreeCourseID';
    }
}

?>
