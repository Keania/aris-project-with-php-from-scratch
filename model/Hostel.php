<?php
namespace Model;
use Model\BaseModel;


class Hostel extends BaseModel {

    protected $table = 'tblhostel';
    public $HostelID;
    public $HostelName;
    public $HostelTypeID;
    public $DegreeTypeID;
    public $HostelCapacity;


    /**
     * getKeyName
     *
     * @return void
     */
    public function getKeyName()
    {
        return 'HostelID';
    }
}
?>