<?php
namespace Model;
use Model\BaseModel;

class FacultyColour extends BaseModel {

    protected $table = 'tblcolour';
    public $ColourID;
    public $ColourName;


    public function faculties(){
        return $this->hasMany('Model\Faculty','FacultyColourID');
    }

    /**
     * getKeyName
     *   
     * The primary key column of the table
     *  
     * @return string
     */
    public function getKeyName()
    {
        return 'ColourID';
    }
}

?>
