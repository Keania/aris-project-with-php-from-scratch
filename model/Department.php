<?php
namespace Model;
use Model\BaseModel;

class Department extends BaseModel {

    protected $table = 'tbldepartment';

    public $DeptID;
    public $FacultyID;
    public $DeptName;
    public $DeptCode;
    public $DigitCode;
    public $HODID;


    public function degree_courses()
    {
        return $this->hasMany('Model\DegreeCourse','DeptID');
    }

    public function users()
    {
        return $this->hasMany('Model\User_VW95','UserDeptID');
    }

    public function academic_courses()
    {
        return $this->hasMany('Model\AcademicCourse','ServiceDeptID');
    }

    public function fee_details()
    {
        return $this->hasMany('Model\FeeDetail','DeptID');
    }

    public function service_types()
    {
        return $this->hasMany('Model\ServiceType','DeptID');
    }
    /**
     * getKeyName
     *   
     * The primary key column of the table
     *  
     * @return string
     */
    public function getKeyName()
    {
        return 'DeptID';
    }
}

?>
