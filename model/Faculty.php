<?php
namespace Model;
use Model\BaseModel;

class Faculty extends BaseModel {

    protected $table = 'tblfaculty';
    public $FacultyID;
    public $FacultyName;
    public $FacultyCode;
    public $FacultyColourID;
    public $FacultyDeanID;


    public function departments()
    {
        return $this->hasMany('Model\Department');
    }

    public function feedetails()
    {
        return $this->hasMany('Model\FeeDetail');
    }

    public function service_types()
    {
        return $this->hasMany('Model\ServiceType');
    }

    public function vw95_users()
    {
        return $this->hasMany('Model\User_VW95');
    }

    public function vw7_departments()
    {
        return $this->hasMany('Model\VW7_Department');
    }

    /**
     * getKeyName
     *   
     * The primary key column of the table
     *  
     * @return string
     */
    public function getKeyName()
    {
        return 'FacultyID';
    }
}

?>
