<?php
namespace Model;
use Model\BaseModel;

class UserCategory extends BaseModel {

    protected $table = 'tblusercategory';

    public $UserCategoryID;
    public $UserCategory;
    public $UserGroupID;

    public function getKeyName()
    {
        return 'UserCategoryID';
    }

}

?>