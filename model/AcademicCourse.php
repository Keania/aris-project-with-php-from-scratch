<?php
namespace Model;
use Model\BaseModel;


class AcademicCourse extends BaseModel {

    protected $table = 'tblacademiccourse';
    public $CourseID;
    public $CourseCode;
    public $CourseTitle;
    public $CourseUnit;
    public $CourseSemesterID;
    public $CourseDescription;
    public $CourseLevelID;
    public $HostelTypeID;
    public $DegreeTypeID;
    public $ServiceDeptID;


    /**
     * getKeyName
     *
     * @return void
     */
    public function getKeyName()
    {
        return 'CourseID';
    }
}
?>