<?php
namespace Model;
use Model\BaseModel;

class User extends BaseModel {

    protected $table = 'tbluser';

    public $UserID;
    public $UserPIN;
    public $UserPwd;
    public $Surname;
    public $Others;
    public $Sex;
    public $Title;
    public $UserGroupID;
    public $UserCategoryID;
    public $UserDeptID;
    public $Phone;
    public $Email;
    public $ContactAddress;
    public $UserStatusID;
    public $Photo;
    public $PwdQuestion1;
    public $PwdQuestion2;
    public $PwdAnswer1;
    public $PwdAnswer2;
    public $StaffNumber;
    public $DyofB;
    public $MofB;
    public $EmploymentDate;
    public $AppointmentDate;
    public $GradeLevel;


    public function getKeyName()
    {
        return 'UserID';
    }
}
?>