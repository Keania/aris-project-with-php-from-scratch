<?php
namespace Model;

use Model\BaseModel;


class Fee extends BaseModel {

    protected $table = 'tblfees';
    public $FeeID;
    public $SessionID;
    public $DegreeTypeID;
    public $ProgramTypeID;
    public $FeeDeadlineDate;
    public $FeeClosureDate;
    public $SurchargeAccountID;

    /**
     * getKeyName
     *
     * @return void
     */
    public function getKeyName()
    {
        return 'FeeID';
    }

}

?>