<?php
namespace Model;

use Model\BaseModel;

class CourseLevel extends BaseModel {

    protected $table = 'tblcourselevel';
    public $LevelID;
    public $DegreeCourseID;
    public $AcademicAdviserID;
    public $CourseLevelID;
    public $Max1;
    public $Max2;
    public $Max3;
    public $Min1;
    public $Min2;
    public $Min3;

    public function getKeyName()
    {
        return 'LevelID';
    }
}

?>