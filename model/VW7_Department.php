<?php
namespace Model;
use Model\BaseModel;

class VW7_Department extends BaseModel {

    protected $table = 'vw7_department';

    public $DeptID;
    public $FacultyID;
    public $FacultyName;
    public $FacultyCode;
    public $FacultyColourID;
    public $ColourName;
    public $DeptName;
    public $DeptCode;
    public $DigitCode;
    public $HODID;
    public $HOD;
    public $FacultyDeanID;
    public $Dean;


    public function degree_courses()
    {
        return $this->hasMany('App\DegreeCourse','DeptID');
    }

    public function users()
    {
        return $this->hasMany('App\User_VW95','UserDeptID');
    }

    public function academic_courses()
    {
        return $this->hasMany('App\AacdemicCourse','ServiceDeptID');
    }

    public function fee_details()
    {
        return $this->hasMany('App\FeeDetail','DeptID');
    }

    public function service_types()
    {
        return $this->hasMany('App\ServiceType','DeptID');
    }
    /**
     * getKeyName
     *   
     * The primary key column of the table
     *  
     * @return string
     */
    public function getKeyName()
    {
        return 'DeptID';
    }
}

?>
