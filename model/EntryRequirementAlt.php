<?php
namespace Model;
use Model\BaseModel;


class EntryRequirementAlt extends BaseModel {

    protected $table = 'tblentryrequirement_alternate';
    public $AltRequirementID;
    public $SessionID;
    public $DegreeCourseID;
    public $SubjectID;
    public $SubjectID_Alt;
    public $EntryCategoryID;
    public $MinGradeID;
    public $CompulsoryID;

    public function getKeyName()
    {
        return 'AltRequirementID';
    }
}

?>