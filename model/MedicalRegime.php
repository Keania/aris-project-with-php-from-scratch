<?php
namespace Model;
use Model\BaseModel;

class MedicalRegime extends BaseModel {

    protected $table = 'tblmedicalexamregime';
    public $MedicalRegimeID;
    public $SessionID;
    public $DegreeTypeID;
    public $ProgramTypeID;
    public $StartDate;
    public $DailyCapacity;
    public $SchedulingPlanID;
    public $RegimeStatusID;

    public function getKeyName()
    {
        return 'MedicalRegimeID';
    }
}

?>