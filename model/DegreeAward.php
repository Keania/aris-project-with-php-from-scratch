<?php
namespace Model;
use Model\BaseModel;

class DegreeAward extends BaseModel {

    protected $table = 'tbldegreeaward';
    public $DegreeAwardID;
    public $DegreeAward;
    public $DegreeTypeID;
    public $DegreeAwardCode;

    /**
     * degreecourses
     *  
     * Degree Courses that belongs to this model instance
     * @return void
     */
    public function degreecourses(){
        return $this->hasMany('Model\DegreeCourse');
    }

    
    /**
     * getKeyName
     *
     * @return void
     */
    public function getKeyName()
    {
        return 'DegreeAwardID';
    }
}

?>
