<?php
namespace Model;
use Model\BaseModel;

class EligibilityJamb extends BaseModel{

    protected $table = 'tbleligibilityjamb';

    public $JambCheckID;
    public $SessionID;
    public $AdmissionID;
    public $DegreeCourseID;
    public $OlevelID;
    public $JambSubjectID;
    public $SubjectScore;

    public function getKeyName()
    {
        return 'JambCheckID';
    }
}