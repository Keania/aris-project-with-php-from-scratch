<?php
namespace Model;
use Model\BaseModel;


class Student extends BaseModel {

    protected $table = 'tblstudent';
    public $StudentID;
    public $AdmissionID;
    public $RegNo;
    public $Matno;
    public $Surname;
    public $Others;
    public $Title;
    public $Sex;
    public $Religion;
    public $MaritalStatus;
    public $DofB;
    public $PofB;
    public $HomeTown;
    public $HomeAddress;
    public $Email;
    public $Phone;
    public $NationalityID;
    public $LGAID;
    public $Photo;
    public $AdmYear;
    public $AdmissionModelID;
    public $DegreeCourseID;
    public $EntryQualification;
    public $StatusID;
    public $GradYear;
    public $GraduationStatusID;
    public $StudentPIN;
    public $StudentPwd;
    public $FatherName;
    public $FatherPhone;
    public $FatherOccupation;
    public $FatherAddress;
    public $MotherName;
    public $MotherPhone;
    public $MotherOccupation;
    public $MotherAddress;
    public $KinName;
    public $KinPhone;
    public $KinAddress;
    public $AdditionalYear;
    public $FinalClearanceDate;
    public $ClearanceStatusID;
    public $PwdQuestion1;
    public $PwdQuestion2;
    public $PwdAnswer1;
    public $PwdAnswer2;



    public function getKeyName()
    {
        return 'StudentID';
    }
}
?>