<?php
namespace Model;
use Model\BaseModel;

class State extends BaseModel {

    protected $table = 'tblstate';
    public $StateID;
    public $State;


    public function lgas()
    {
        return $this->hasMany('Model\LGA');
    }

    public function getKeyName()
    {
        return 'StateID';
    }
}

?>
