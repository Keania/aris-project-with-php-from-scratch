<?php
namespace Model;
use Model\BaseModel;

class OlevelSubject extends BaseModel {

    protected $table = 'tblsubjects';
    public $SubjectID;
    public $Subject;

    public function getKeyName()
    {
        return 'SubjectID';
    }

    public function olevel_eligibilities()
    {
        return $this->hasMany('Model\EligibilityOlevel','OlevelSubjectID');
    }

    public function jamb_eligibilities()
    {
        return $this->hasMany('Model\EligibilityJamb','JambSubjectID');
    }
    
    public function entry_requirements()
    {
        return $this->hasMany('Model\EntryRequirement','SubjectID');
    }

    public function entry_requirement_alts()
    {
        return $this->hasMany('Model\EntryRequirementAlt','SubjectID');
    }
}

?>
