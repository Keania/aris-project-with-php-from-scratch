<?php
namespace Model;
use Model\BaseModel;

class VW8_DegreeCourse extends BaseModel {

    protected $table = 'vw8_degreecourse';
    public $DegreeCourseID;
    public $DegreeAwardID;
    public $DeptID;
    public $FacultyID;
    public $FacultyName;
    public $FacultyCode;
    public $FacultyColourID;
    public $ColourName;
    public $DeptName;
    public $DeptCode;
    public $HODID;
    public $HOD;
    public $FacultyDeanID;
    public $Dean;
    public $DegreeTypeID;
    public $DegreeType;
    public $DegreeCourse;
    public $ProgramTypeID;
    public $ProgramType;
    public $ProgramTypeCode;
    public $DegreeCourseDuration;
    public $DegreeAward;
    public $DegreeAwardCode;
    public $DigitCode;

    /**
     * getKeyName
     *
     * @return void
     */
    public function getKeyName()
    {
        return 'DegreeCourseID';
    }
}

?>
