<?php
namespace Model;
use Model\BaseModel;

class DegreeType extends BaseModel {

    protected $table = 'tbldegreetype';
   
    public $DegreeTypeID;
    public $DegreeType;


    /**
     * awards
     *  
     *
     * @return \Lib\Collection
     */
    public function awards(){
        return $this->hasMany('Model\DegreeAward');
    }

    public function degreecourses()
    {
        return $this->hasMany('Model\DegreeCourse');
    }

    public function fees()
    {
        return $this->hasMany('Model\Fee');
    }

    public function academic_session_mgts()
    {
        return $this->hasMany('Model\AcademicSessionMgt');
    }

    public function hostels()
    {
        return $this->hasMany('Model\Hostel');
    }

    public function medical_regimes()
    {
        return $this->hasMany('Model\MedicalRegime');
    }

    public function service_types()
    {
        return $this->hasMany('Model\ServiceType');
    }

    public function other_fees(){
        return $this->hasMany('Model\OtherFee');
    }

    public function academic_courses(){
        return $this->hasMany('Model\AcademicCourse');
    }

    /**
     * getKeyName
     *
     *  Primary Key Column of model
     * @return void
     */
    public function getKeyName()
    {
        return 'DegreeTypeID';
    }
}

?>
