<?php
namespace Model;
use Model\BaseModel;




class PredataCategory extends BaseModel {

    protected $table = 'tblpredatacategory';
    public $DataCategory;
    public $CategoryID;


    public function getKeyName()
    {
        return 'CategoryID';
    }

    public function predatas()
    {
        return $this->hasMany('Model\Predata');
    }

}

?>