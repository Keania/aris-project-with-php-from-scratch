<?php
namespace Model;
use Model\BaseModel;

class LGA extends BaseModel {

    protected $table = 'tbllga';
    public $StateID;
    public $LGAID;
    public $LGA;

    public function students()
    {
        return $this->hasMany('Model\Student');
    }

    public function getKeyName()
    {
        return 'LGAID';
    }
}

?>
