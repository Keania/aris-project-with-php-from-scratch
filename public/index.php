<?php
use Lib\Connection;

require_once BASE_URL.'/autoload.php';
require_once BASE_URL.'/helpers.php';
require_once BASE_URL.'/constants.php';


// take the user to the routes page
require BASE_URL.'/routes.php';

?>