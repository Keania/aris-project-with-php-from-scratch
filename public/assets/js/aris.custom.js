$(document).ready(function(){

/************************************************************************************* /
  FUNCTIONS CALL
 ************************************************************************************/

	initialPopulation('dataCategorySelect', 'tblpredatacategory');

	saveDelete(); /* function call*/
	
	injectTextToNextSibling('#preDataSelect', 'preDataInput'); /* Inject input with editable text from selection */


	$('#delete').on('click', function(e)
	{
		e.preventDefault();
		deleteQuery();
	})

	/***************************************************
	 * Activate/ Deactivate .controlPreDataSelect (Edit) by #dataCategorySelect
	 */
	$('#dataCategorySelect').change(function(e)
	{
		$('.controlPreDataSelect').removeAttr('disabled').prop('checked', false);
		$('.collapse#PreDataWpr').collapse('hide');
		$('.preDataInput').val('');
	})


	/***************************************************
	 * Call populateDesendant() by .controlPreDataSelect to populate #preDataSelect
	 */

	 $('.controlPreDataSelect').on('change', function(e)
	 {
	 	if(this.checked)
	 	{
	 		let selectedOption = e.target.parentElement.parentElement.parentElement.querySelectorAll('#dataCategorySelect')[0].value;

			populateDesendant('preDataSelect', 'CategoryID', selectedOption, 'tblpredata');
			$('.collapse#PreDataWpr').collapse('show');
	 	}else 
	 	{	 		
			$('.collapse#PreDataWpr').collapse('hide');
			$('.preDataInput').val('');
	 	}
	 });


	

/*********************************************************************************** /
  FUNCTIONS IMPLEMENTATION
 ************************************************************************************ /



	/*****************************************
	 * Populate initial values - SELECT Query
	 */
	function initialPopulation(elmId, table)
	{
		let elm = document.getElementById(elmId);
		$.ajax({
			url: '../src/request.php',
			type: 'post',
			data: {'table': table, 'queryType': 'select'}
		}).done(function(data){
			for (var i = JSON.parse(data).length-1; i >= 0; i--){
				let soption = document.createElement('option');
				soptionText = document.createTextNode(JSON.parse(data)[i]['DataCategory']);
				soption.value = JSON.parse(data)[i]['CategoryID'];
				soption.appendChild(soptionText);
				elm.appendChild(soption);
			}
		})
	}

	/*****************************************
	 * Inject input with editable text from selection
	 */
	 injectTextToNextSibling('#preDataSelect', 'preDataInput');

	function injectTextToNextSibling(selectElm, inputElm)
	{
		$(selectElm).change( function(e)
		{
		    // let editableValue = e.target.value;
		    let editableValue = e.target[e.target.selectedIndex].textContent;
		    e.target.parentElement.parentElement.nextElementSibling.querySelectorAll('input.' +inputElm)[0].value = editableValue;
		});
	}

	/******************************************
	 * Save or Delete Operation
	 */
	function saveDelete()
	{
		$("#predata").on('submit', function(e)
		{
			e.preventDefault();
			let preData  = e.target.querySelectorAll('.preDataInput')[0].value;
			let categoryId  = document.getElementById('dataCategorySelect').value;
			let preDataSelect  = document.getElementById('preDataSelect').value;

			if (preData.length == "" || isNaN(categoryId))
			{	
				displayError("#predata", "<strong>Error!</strong> Please enter required fields" );
			} 
			else
			{
				if (isNaN(preDataSelect)) /* Do INSERT */
				{
					insertQuery('tblpredata', categoryId, preData, 'insert');
				}
				else  /* Edit selected, Do UPDATE */
				{
					updateQuery('tblpredata', categoryId, preDataSelect, preData, 'update');
				}
			}
			
		// clear editable on save/delete 
		});
	}

	/******************************************
	 * Save/Delete AJAX
	 */
	function insertQuery(table, categoryId, preData, queryType)
	{ //UPDATE `tblpredata` SET `DataID`=value WHERE DataID = this AND CategoryID = that
		$.ajax({
			url: '../src/request.php',
			type: 'post',
			data: {'table': table, 'categoryId': categoryId, 'preData': preData, 'queryType': queryType}
		}).done(function(data){
			if (data.trim() === 'success')
			{	
				$("#predata .preDataInput").val("");
				$('#predata .alert').html("<strong>Success!</strong> entry saved").addClass('alert-success').removeClass('d-none alert-danger');
				setTimeout(function(){$('.alert').addClass('d-none')}, 4000);
			} else
			{
				$('#predata .alert').html("<strong>Failed!</strong> entry was not saved").addClass('alert-danger').removeClass('d-none alert-success');
				setTimeout(function(){$('.alert').addClass('d-none')}, 4000);
			}
		});
	}

	/******************************************
	 * Save/Delete AJAX
	 */
	function updateQuery(table, categoryId, dataId, preData, queryType)
	{
		$.ajax({
			url: '../src/request.php',
			type: 'post',
			data: {'table': table, 'categoryId': categoryId, 'dataId': dataId, 'preData': preData, 'queryType': queryType}
		}).done(function(data){
			if (data.trim() === 'success')
			{	
				$("#predata .preDataInput").val("");
				$('#predata .alert').html("<strong>Success!</strong> entry updated").addClass('alert-success').removeClass('d-none alert-danger');
				setTimeout(function(){$('.alert').addClass('d-none')}, 4000);
			} else
			{	console.log(data);
				$('#predata .alert').html("<strong>Failed!</strong> entry was not updated").addClass('alert-danger').removeClass('d-none alert-success');
				setTimeout(function(){$('.alert').addClass('d-none')}, 4000);
			}
		});
	}
	/******************************************
	 * Populate descendant
	 */
	function populateDesendant(elmId, colName, selectedOption, table)
	{	
		let elm = document.getElementById(elmId);
		elm.options.length = 0; /* Remove all options.  jQuery - .children.remove() */
		// elm.appendChild(document.createElement('option').appendChild(document.createTextNode('foo')));
		$('#' +elmId).html("<option>Select Pre-Data to edit</option>");

		$.ajax({
			url: '../src/request.php',
			type: 'post',
			data: {'colName': colName, 'selectedOption': selectedOption, 'table': table, 'queryType': 'select'}
		}).done(function(data){
			for (var i = JSON.parse(data).length-1; i >= 0; i--){
				let soption = document.createElement('option');
				soptionText = document.createTextNode(JSON.parse(data)[i]['Data']);
				soption.value = JSON.parse(data)[i]['DataID'];
				soption.appendChild(soptionText);
				elm.appendChild(soption);
			}
		});
	}

	/******************************************
	 * Display error
	 */

	 function displayError(elm, msg)
	 {
	 	$(elm+ ' .alert').html(msg).addClass('alert-danger').removeClass('d-none alert-success');
		setTimeout(function(){$('.alert').addClass('d-none')}, 4000);
	 }

	/******************************************
	 * Delete Operation
	 */
	function deleteQuery()
	{
		let preData  = $('#predata .preDataInput').val();
		let categoryId  = document.getElementById('dataCategorySelect').value;
		let preDataSelect  = document.getElementById('preDataSelect').value;

		if (preData.length == "" || isNaN(categoryId) || isNaN(preDataSelect))
		{	
			displayError("#predata", "<strong>Error!</strong> Select field to delete" );
		} 
		else
		{		
			$.ajax({
				url: '../src/request.php',
				type: 'post',
				data: {'table': 'tblpredata', 'categoryId': categoryId, 'dataId': 'preDataSelect', 'queryType': 'delete'}
			}).done(function(data){
				// if (data.trim() === 'deleted')
				// {	
				// 	$("#predata .preDataInput").val("");
				// 	$('#predata .alert').html("<strong>Success!</strong> entry updated").addClass('alert-success').removeClass('d-none alert-danger');
				// 	setTimeout(function(){$('.alert').addClass('d-none')}, 4000);
				// } else
				// {	console.log(data);
				// 	$('#predata .alert').html("<strong>Failed!</strong> entry was not updated").addClass('alert-danger').removeClass('d-none alert-success');
				// 	setTimeout(function(){$('.alert').addClass('d-none')}, 4000);
				// }
				console.log(data);
			});
		}
		
	}


});