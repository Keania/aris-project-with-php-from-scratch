<?php

use Lib\Connection;
use Lib\Collection;


if (!function_exists('asset')) {

    function asset($asset)
    {
        return BASE_URL . '/public/assets/' . $asset;
    }
}

if(! function_exists('url')){
    function url($url){
        return $url;
    }
}

if(! function_exists('resource')){
    function resource($name){
        return $name;
    }
}

if (!function_exists('collect')){
    
    function collect(array $array){
        return new Collection($array);
    }
}

if(! function_exists('redirect')){
    function redirect($url){
        return header("Location:$url");
    }
}


// if(! function_exists('request_session')){
//     function request_session($key){
//         return $_SESSION['request_session'][$key];
//     }
// }


if(! function_exists('generateCSRFToken')){
    function generateCSRFToken()
    {
        $_csrf_expire =	time() + 7200;
        $_csrf_hash = bin2hex(random_bytes(16));
        setcookie('_csrf_token',$_csrf_hash,$_csrf_expire,'/');
        return $_csrf_hash;
        
    }
}

if(!function_exists('dd')){
    function dd($data){
        echo '<pre>';
        echo var_dump($data);
        echo '</pre>';
        die();
    }
}


// This function retrieves all predatas from the database
// if (!function_exists('fetchPredatasFromDB')) {
//     function fecthPredatasFromDB()
//     {
//         try{
//             $connection = new Connection();
//             $query = "SELECT * FROM tblpredata";
//             $statement = $connection->db->prepare($query);
//             if (!$statement->execute()) {
//                 echo 'Fail fetching data';
//             } else {
//                 return $statement->fetchAll();
//             }

//         }catch(Exception | PDOException $e){
//             echo 'Error: '.isset($e->errInfo[2]) ? $e->errInfo[2] : $e->getMessage();
//             return;
//         }
       
//     }
// }


// // This function retrives all predatacategories from the database
// if(! function_exists('fetchPredataCategoriesFromDB')){
//     function fetchPredataCategoriesFromDB()
//     {
//        try{
//             $connection = new Connection();
//             $query = "SELECT * FROM tblpredatacategory";
//             $statement = $connection->db->prepare($query);
//             if (!$statement->execute()) {
//                 echo 'Fail fetching data';
//             } else {
//                 return $statement->fetchAll();
//             }
//         }catch(Exception | PDOException $e){
//             echo 'Error: '.isset($e->errInfo[2]) ? $e->errInfo[2] : $e->getMessage();
//             return;
//         }
       
//     }
// }
