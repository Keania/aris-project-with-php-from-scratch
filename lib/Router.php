<?php
namespace Lib;

use Lib\IRequest;


class Router
{


    private $request;
    private $supportedHttpMethods  = array("get", "post");

    public function __construct(IRequest $request)
    {
        $this->request = $request;
    }

    /**
     * __call
     * 
     *  This method gets called on a function 
     *  when an undefined function is called passing in the name and arguments
     * @param  mixed $name
     * @param  mixed $arguments
     *
     * @return void
     */
    public function __call($name, $arguments)
    {
       
        list($route, $method) = $arguments;

      
        if (!in_array($name, $this->supportedHttpMethods)) {
           
            return $this->invalidMethodHandler();
        }
        $this->{strtolower($name)}[$this->formatRoute($route)] = $method;
      
       
    }

    /**
     * formatRoute
     *   get the route trims trailing slashes and get params
     * @param  mixed $route
     *
     * @return string
     */
    public function formatRoute($route)
    {
        $route = $this->getOffBaseName($route);
        $result = rtrim($route, '/');
       
        if ($result === '') {
           
            return '/';
        }

        if($this->request->requestMethod == 'GET'){
            // trim query params
            $current_url = explode('?',$route);
            return $current_url[0];
        }
        return BASE_NAME.$result;
    }

    private function invalidMethodHandler()
    {
        header("{$this->request->serverProtocol} 405 Method Not Allowed");
    }
    private function defaultRequestHandler()
    {
       
       // header("HTTP/1.0 404 Not Found");
       http_response_code(404);
       return include BASE_URL.'/errors/404.php';
    }
    /**
     * Resolves a route
     */
    function resolve()
    {
      
        $methodDictionary = $this->{strtolower($this->request->requestMethod)};
       
        $formatedRoute = $this->formatRoute($this->request->requestUri);
        if(! array_key_exists($formatedRoute,$methodDictionary)){
            return $this->defaultRequestHandler();
        }
       
        $method = $methodDictionary[$formatedRoute];
        if (is_null($method)) {
        
            return $this->defaultRequestHandler();
            
        }
        echo call_user_func_array($method, array($this->request));
    }


    /**
     * verifyCsrfToken
     *  Ensure that every post request made is coming from our website
     * @param  mixed $token
     *
     * @return void
     */
    public static function verifyCsrfToken($token)
    {
       $_cookie_token = $_COOKIE['_csrf_token'];
       if($token == $_cookie_token){
           unset($_COOKIE['_csrf_token']);
           
           return true;
       }
       return false;
    }
    function __destruct()
    {
        $this->resolve();
    }


    /**
     * getOffBaseName
     *  Get off attached project base name
     * @param  string $route
     *
     * @return string
     */
    public function getOffBaseName(string $route)
    {
       
       $pos = stripos($route,BASE_NAME);
        if($pos){
           return str_replace(BASE_NAME.'/','',$route);
            
        }
        return $route;
    }
}
