<?php
namespace Lib;


class Request implements IRequest{

    public function __construct()
    {
        $this->bootstrapSelf();
        $this->loadRequest();
        if(isset($_SESSION['tmp_status'])){
            $this->session = $_SESSION['tmp_status'];
            unset($_SESSION['tmp_status']);
        }
    }

    /**
     * bootstrapSelf 
     *
     * load server variables into new created request
     * @return void
     */
    private function bootstrapSelf()
    {
        foreach($_SERVER as $key=>$value){
        
            $this->{$this->toCamelCase($key)} = $value;
        }
    }

    /**
     * loadRequest
     *
     * @return void
     */
    private function loadRequest(){

        if($this->requestMethod == 'POST'){
          
            foreach($_POST as $key=>$value){
                $this->{$key} = filter_var($value,FILTER_SANITIZE_SPECIAL_CHARS);
            }
        }

        if($this->requestMethod == 'GET'){
            foreach($_GET as $key=>$value){
                $this->{$key} = filter_var($value,FILTER_SANITIZE_SPECIAL_CHARS);
            }
        }

    }

    /**
     * toCamelCase
     *
     * @param  mixed $string
     *
     * @return void
     */
    private function toCamelCase($string)
    {
        $result = strtolower($string);
        preg_match_all('/_[a-z]/',$result,$matches);
        foreach($matches[0] as $match)
        {
            $c = str_replace('_','',strtoupper($match));
            $result = str_replace($match,$c,$result);
        }

        return $result;
    }


    /**
     * all
     *
     * @return void
     */
    public function all()
    {
        if($this->requestMethod === 'GET'){
            $body = array();
            foreach($_POST as $key=>$value){
                $body[$key] = filter_var($value,FILTER_SANITIZE_SPECIAL_CHARS);
            }
            return $body;
        }

        if($this->requestMethod == 'POST'){
            $body = array();
            foreach($_POST as $key=>$value){
               
                $body[$key] = filter_var($value,FILTER_SANITIZE_SPECIAL_CHARS);
            }
            return $body;
        }
    }


  

    

    /**
     * validateEmpty
     *
     * @param  mixed $array
     *
     * @return void
     */
    public function validateEmpty(array $array)
    {
       foreach($array as $key=>$value){
           
           if(empty($this->all()[$key]) ){
              
               if(!is_numeric($key)){
                  
                   throw new \Exception($value);
               }
               throw new \Exception("$value cannot be empty");
           }
       }
    }

    /**
     * input
     *
     * @param  mixed $key
     *
     * @return void
     */
    public function input($key){
        return $this->all()[$key];
    }
}
?>