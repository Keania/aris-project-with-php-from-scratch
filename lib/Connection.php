<?php
namespace Lib;


use PDO;
use PDOException;

class Connection{

    public $db;
 

    public function __construct()
    {
       $config = $this->getConfig();
       $username = $config['MYSQL_DB_USER'];
       $password = $config['MYSQL_DB_PASSWORD'];
       $host = $config['MYSQL_DB_HOST'];
       $database = $config['MYSQL_DB_DATABASE'];
       $dsn  = "mysql:host=$host;dbname=$database";
        $this->connect($dsn,$username,$password);
       
    }

    protected  function getConfig()
    {
        return $config = parse_ini_file(BASE_URL.'/config.ini');
    }

   

    public function connect($connection_params,$username,$password)
    {
        try{

            return $this->db =new PDO($connection_params,$username,$password);


        }catch(PDOException $e){

            $err = $e->getMessage();

            throw new PDOException("Connection failed : $err");
        }
    }

    public function close(){
        $this->db = null;
    }
}

?>