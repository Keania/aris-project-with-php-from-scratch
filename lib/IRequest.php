<?php
namespace Lib;

interface IRequest {

    public function all();
    public function input($key);

}
?>