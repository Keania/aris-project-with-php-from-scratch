<?php
namespace Lib;

class Collection implements \ArrayAccess,\Countable, \Iterator, \JsonSerializable {

    private $array = [];

    public function __construct(array $array)
    {
        $this->position = 0;
        foreach($array as $key=>$value){
            $this->array[$key] = $value;
        }
    }

    public function offsetSet($offset, $value):void
    {
        if(is_null($offset)){
            $this->array[] = $value;
        }else{
            $this->array[$offset] = $value;
        }
    }


    public function offsetGet($offset)
    {
        return is_null($offset) || !isset($offset) ? null : $this->array[$offset];
    }

    public function offsetExists($offset)
    {
        return isset($this->array[$offset]);
    }

    public function offsetUnset($offset)
    {
        unset($this->array[$offset]);
    }

    public function count()
    {
        return count($this->array);
    }

    public function rewind()
    {
        $this->position = 0;
    }

    public function key()
    {
        return $this->position;
    }

    public function current()
    {
        return $this->array[$this->position];
    }

    public function next()
    {
        ++$this->position;
    }

    public function valid()
    {
        return isset($this->array[$this->position]);
    }

    public function first()
    {
        return !empty($this->array) ? $this->array[array_key_first($this->array)] : null; 
    }

    public function isEmpty(){
        return empty($this->array);
    }

    public function jsonSerialize()
    {
        return $this->array;
    }
}